/*
script para enviar form campanha para metódo no PHP e salvar em sessão
*/
$(document).ready(function() {
    //Submit forms
    $("#flash-messages").click(function(){$(this).hide()});

    $("#btn_campaign").click(function(e) {
        $("#formCampaign").submit();
    });

     $("#btn_product").click(function(e) {    
        $("#formProduct").submit();
    });
    
    $("#btn_customer").click(function(e) {
        $("#formLocation").submit();
    });
    
    $("#btn_media").click(function(e) {
        $("#formMedia").submit();
    });
    
    $("#btn_noticerisk").click(function(e) {
        $("#formRisk").submit();
    });
    
    $("#btn_process").click(function(e) {
        $("#formProcess").submit();
    });
});