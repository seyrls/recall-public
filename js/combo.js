// JavaScript Document
var XMLHttpRequestObject=false;
function display(state_id) {
	var urlpath = $('body').data('baseurl');

	if(window.XMLHttpRequest) {
		XMLHttpRequestObject=new XMLHttpRequest();
	}else if(window.ActiveXObject) {
		XMLHttpRequestObject=new ActiveXObject("Microsoft.XMLHTTP");
	} 
	XMLHttpRequestObject.onreadystatechange=function() {
		if (XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status==200) {
			document.getElementById("show_city").innerHTML=XMLHttpRequestObject.responseText;
		}
	}

	XMLHttpRequestObject.open("GET",urlpath + "cities/combocities/"+state_id,true);

	XMLHttpRequestObject.send();
}



/*
Função carrega combo veículo de mídia
*/
$(document).ready(function(){
	$("select[name='origin']").change(function(){
            var country_id = $("select[name='origin'] option:selected").attr('value');
            var urlpath = $('body').data('baseurl');
            $("#media").html( "" );
            $("#mediaReq").removeClass("hide");
            if (country_id.length > 0 ) {
                $.ajax({
                    type: "POST",
                    url: urlpath + "recalls/comboMedia/" + country_id,
                    cache: false,
                    beforeSend: function () {
                            $('#media').html('<img src="loader.gif" alt="" width="24" height="24">');
                    },

                    success: function(html) {
                            $("#media").html( html );
                    }
                });
            }
	});
        
        $("#media").change(function(){
            var media_id = $("select#media option:selected").attr('value');
            var urlpath = $('body').data('baseurl');
            $("#source").html( "" );
            $("#addSource").removeClass("hide");
            $("#fonteReq").removeClass("hide");
            if (media_id.length > 0 ) {
                $.ajax({
                    type: "POST",
                    url: urlpath + "recalls/comboSource/" + media_id,
                    cache: false,
                    beforeSend: function () {
                            $('#source').html('<img src="loader.gif" alt="" width="24" height="24">');
                    },

                    success: function(html) {
                            $("#source").html( html );
                    }
                });
            }
	});
});