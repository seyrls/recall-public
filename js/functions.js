$(document).ready(function(){
    setTimeout(function() {
      $(".flash-messages").fadeOut("slow");
    }, 5000);

    $(".calendario").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy"
    });
    
    $(".calendario_prev").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        maxDate: new Date()
    });
    
    $(".calendario_next").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        minDate: new Date()
    });
    
    $('.calendario_time').datetimepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        timeFormat: 'hh:mm',
        stepMinute: 1,
        stepHour: 1,
        minDate: new Date()
    });
    
    $('.calendario_time_prev').datetimepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        timeFormat: 'hh:mm',
        stepMinute: 1,
        stepHour: 1,
        maxDate: new Date()
    });
    $('.calendario_time_next').datetimepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        timeFormat: 'hh:mm',
        stepMinute: 1,
        stepHour: 1,
        minDate: new Date()
    });
    
    $('.numeric').keyup(function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });
});

/*
 * Funcao para comparar duas datas e retornar TRUE para a data inicial 
 * menor que a data final e FALSE se a data final for menor que a inicial
 */
function compareDate(dt_ini, dt_fim) {
    // transforma a data inicial para o formato americano
    var data_ini_Br = dt_ini;
    var d_1  = data_ini_Br.substring(0, 2);
    var m_1  = data_ini_Br.substring(3, 5);
    var y_1  = data_ini_Br.substring(6, 10);
    var h_1  = data_ini_Br.substring(11, 13);
    var mi_1 = data_ini_Br.substring(14, 16);
    var data_ini_ingles = y_1+"/"+m_1+"/"+d_1+" "+h_1+":"+mi_1;

    // transforma a data inicial em objeto
    var data1    = new Date(data_ini_ingles);
    var minutos1 = data1.getMinutes();
    var hora1    = data1.getHours();
    var dia1     = data1.getDate();
    var mes1     = data1.getMonth();
    var ano1     = data1.getFullYear();

    // concatena os valores em uma unica variavel, sem retirar os (zeros a esquerda) com a funcao "pad"
    var data_inicial = ano1.pad(4)+''+mes1.pad()+''+dia1.pad()+''+hora1.pad()+''+minutos1.pad();

    // transforma a data final para o formato americano
    var data_fim_Br = dt_fim;
    var d_2  = data_fim_Br.substring(0, 2);
    var m_2  = data_fim_Br.substring(3, 5);
    var y_2  = data_fim_Br.substring(6, 10);
    var h_2  = data_fim_Br.substring(11, 13);
    var mi_2 = data_fim_Br.substring(14, 16);
    var data_fim_ingles = y_2+"/"+m_2+"/"+d_2+" "+h_2+":"+mi_2;

    // transforma a data final em objeto
    var data2    = new Date(data_fim_ingles);
    var minutos2 = data2.getMinutes();
    var hora2    = data2.getHours();
    var dia2     = data2.getDate();
    var mes2     = data2.getMonth();
    var ano2     = data2.getFullYear();

    // concatena os valores em uma unica variavel, sem retirar os (zeros a esquerda) com a funcao "pad"
    var data_final = ano2.pad(4)+''+mes2.pad()+''+dia2.pad()+''+hora2.pad()+''+minutos2.pad();

    // faz a magica para verificar se a data final eh maior que a data inicial
    if (parseInt(data_final) > parseInt(data_inicial)){
        return true;
    } else {
        return false;
    }
}

/*
 * Retirar zero a esquerda
 */
Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}
/*
 * Somente sumeros
 */
function validate_only_number(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
          theEvent.returnValue = false;
          if(theEvent.preventDefault) theEvent.preventDefault();
        }
  }