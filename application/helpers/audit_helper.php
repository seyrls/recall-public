<?php
/*
 * Log de auditoria
 * insere a acao, funcao e o usuario na tabela logs
 */
function log_audit($action,$function){
    
    $ci = & get_instance();
    $ci->load->database();
    $user = $ci->session->userdata('login');
    
    if (($action) && ($function) && ($user['id'])) {
        $data = array(
            'action'    => $action,
            'function'  => $function,
            'date'      => date("Y-m-d : H:i:s"),
            'user_id'   => $user['id']
         );
         $ci->db->insert('logs', $data);
    } else {
        return false;
    }
}