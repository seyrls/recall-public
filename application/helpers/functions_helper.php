<?php

/* 
    funcoes diversas
 */

function valor_por_extenso($valor=0,$moeda = TRUE) {
	$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
	$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
 
	$c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
	$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
	$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
	$u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
 
	$z=0;
 
	$valor = number_format($valor, 2, ".", ".");
	$inteiro = explode(".", $valor);
	for($i=0;$i<count($inteiro);$i++)
		for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
			$inteiro[$i] = "0".$inteiro[$i];
 
	// $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;) 
	$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
	for ($i=0;$i<count($inteiro);$i++) {
		$valor = $inteiro[$i];
		$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
		$rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
		$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";
	
		$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
		$t = count($inteiro)-1-$i;
                if ($moeda) {
                    $r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
                }
		
                if ($valor == "000") {
                    $z++;  
                }  elseif ($z > 0) {
                    $z--;
                }
                if ($moeda) {
                    if (($t==1) && ($z>0) && ($inteiro[0] > 0)) {
                        $r .= (($z>1) ? " de " : "").$plural[$t]; 
                    }
                }
                if ($r) {
                    $rt = ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
                } 
	}
 
	return($rt ? $rt : "zero");
}

// transforma a data po extenso
function getDataExtenso($date){
    $dado = '';
    if ($date) {
        header( 'Content-Type: text/html; charset=utf-8');
        setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' );
        date_default_timezone_set( 'America/Sao_Paulo' );
        $dado = strftime( '%A, %d de %B de %Y', strtotime($date));
    }
    return $dado;
}

    /*
     * Adiciona Mascara para campos
     */
    function mask($mask,$str){

        $str = str_replace(" ","",$str);

        for($i=0;$i<strlen($str);$i++){
            $mask[strpos($mask,"#")] = $str[$i];
        }

        return $mask;

    }
    
/*
 * Verifica se data final eh maior ou igual a data inicial
 */
function dif_datas($dt_inicial, $dt_final){ 
    
    list($dia_i, $mes_i, $ano_i) = explode("/", $dt_inicial); //Data inicial 
    list($dia_f, $mes_f, $ano_f) = explode("/", $dt_final); //Data final 
    $mk_i = mktime(0, 0, 0, $mes_i, $dia_i, $ano_i); // obtem tempo unix no formato timestamp 
    $mk_f = mktime(0, 0, 0, $mes_f, $dia_f, $ano_f); // obtem tempo unix no formato timestamp 
    $diferenca = $mk_f - $mk_i; //Acha a diferença entre as datas 
    
    if($diferenca >= 0 ){
        return TRUE; 
    }elseif($diferenca < 0 ){ 
        return FALSE; 
    } 
} 