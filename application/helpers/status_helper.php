<?php
/*
 * Status da Campanha
 * Retorna o status da campanha com a class referente ao estado atual da mesma
 */
function get_status_campaign($status_campaign, $label = TRUE){
    $status = '';
    switch ($status_campaign) {
        case STATUS_ID_EM_CADASTRAMENTO:
            if ($label) {
                $status = '<span class="label label-default">'.STATUS_EM_CADASTRAMENTO.'</span>';
            } else {
                $status = STATUS_EM_CADASTRAMENTO;
            }
            break;
        case STATUS_ID_AGUARDANDO_PUBLICACAO:
            if ($label) {
                $status = '<span class="label label-info">'.STATUS_AGUARDANDO_PUBLICACAO.'</span>';
            } else {
                $status = STATUS_AGUARDANDO_PUBLICACAO;
            }
            break;
        case STATUS_ID_PUBLICADA:
            if ($label) {
                $status = '<span class="label label-primary">'.STATUS_PUBLICADO.'</span>';
            } else {
                $status = STATUS_PUBLICADO;
            }
            break;
        case STATUS_ID_PUBLICADA_COM_RESSALVA:
            if ($label) {
                $status = '<span class="label label-warning">'.STATUS_PUBLICADO_COM_RESSALVA.'</span>';
            } else {
                $status = STATUS_PUBLICADO_COM_RESSALVA;
            }
            break;
        case STATUS_ID_PENDENCIA_CADASTRO:
            if ($label) {
                $status = '<span class="label label-danger">'.STATUS_PENDENCIA_CADASTRO.'</span>';
            } else {
                $status = STATUS_PENDENCIA_CADASTRO;
            }
            break;
        case STATUS_ID_ARQUIVADO:
            if ($label) {
                $status = '<span class="label label-danger">'.STATUS_ARQUIVADO.'</span>';
            } else {
                $status = STATUS_ARQUIVADO;
            }
            break;
        case STATUS_ID_FINALIZADA:
            if ($label) {
                $status = '<span class="label label-success">'.STATUS_FINALIZADA.'</span>';
            } else {
                $status = STATUS_FINALIZADA;
            }
            break;
        default:
            $status = '---';
            break;
    }
    return $status;
}