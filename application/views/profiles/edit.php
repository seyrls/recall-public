<form class="form-horizontal" method="post" action="<?php echo base_url();?>profiles/edit">
    <fieldset>

        <!-- Form Name -->
        <legend>Perfil</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="username">Usuário</label>  
            <div class="col-md-5">
                <input id="username" name="username" value="<?php echo $user['username'];?>" placeholder="Nome de usuário" class="form-control input-md" type="text" readonly>
            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Senha</label>
            <div class="col-md-5">
                <input id="password" name="password" placeholder="Senha do usuário" class="form-control input-md" type="password">
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="cnpj">CNPJ</label>  
            <div class="col-md-4">
                <?php //tirar a opção readonly para administrador
                if ($user['level'] == 1){
                    echo '<input id="cnpj" name="cnpj" value="'.$user['cnpj'].'" placeholder="CNPJ do Fornecedor" class="form-control input-md" type="text">';
                }else{
                    echo '<input id="cnpj" name="cnpj" value="'.$user['cnpj'].'" placeholder="CNPJ do Fornecedor" class="form-control input-md" type="text" readonly>';
                }
                ?>
                <span class="help-block">Cnpj do Fornecedor</span>  
            </div>
        </div>

        <!-- Button (Double) -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="btn_save">Opção</label>
            <div class="col-md-8">
                <button type="submit" id="btn_save" name="btn_save" class="btn btn-primary">Atualizar</button>
                <button type="button" id="btn_cancel" name="btn_cancel" onClick="window.location='<?php echo base_url()?>administrations'" class="btn btn-danger">Cancelar</button>
            </div>
        </div>

    </fieldset>
</form>