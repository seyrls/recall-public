<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Tipo de produtos</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                    <li><a href="<?php echo base_url(); ?>branches/lists">Listar setores de atividades</a></li>
                    <li><a href="<?php echo base_url(); ?>branches/insert">Novo setor de atividade</a></li>
                    <li><a href="<?php echo base_url(); ?>type_products/lists">Listar tipo de produtos</a></li>
                    <li><a href="<?php echo base_url(); ?>type_products/insert">Novo tipo de produto</a></li>
            </ul>
    </div>
</nav>