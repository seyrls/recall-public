<script>
$(document).ready(function() {
    $('#type_product').dataTable();
});

function excluir(id,type) {
    if (confirm('Deseja excluir o tipo de produto '+type+'?')) {
        window.location='<?php echo base_url().'type_products/remove/';?>'+id;
    }
}

</script>
<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Tipos de Produto</h5>
    </div>
    <table class="table table-striped table-bordered" id="type_product" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 60px">Ação</th>
                <th>Tipo de produto</th>
                <th>Setor</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            if ($dados) : 
                foreach ($dados as $d) :
                    $status = ($d['status'] == TRUE) ? 
                    '<span class="label label-success">Ativo</span>' :
                    '<span class="label label-danger">Inativo</span>';
                    $id = $d['type_product_id'];
                    $name = $d['type_product'];
        ?>
                    
                    <tr>
                        <td>
                            <a class="btn btn-sm btn-default" href='<?php echo base_url()."type_products/edit/".$id; ?>'>
                                <img src="<?php echo base_url()."img/fi-page-edit.svg";?>" 
                                     width='14px' title='Editar' style='text-align: center'/>
                            </a>
                            <a class="btn btn-sm btn-default" href='javascript:void(o)' 
                               onClick="excluir(<?php echo $id.",'".$name."'"; ?>)">
                                <img src="<?php echo base_url()."img/fi-x.svg";?>" 
                                     width='14px' title='Excluir' style='text-align: center'/>
                            </a>
                        </td>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $d['branch']; ?></td>
                        <td><?php echo $status; ?></td>
                    </tr>
        <?php
                endforeach;
            endif;
        ?>
        </tbody>
    </table>
    <div style="margin: 30px auto; text-align: center">
        <a class='btn btn-primary' href="<?php echo base_url(); ?>type_products/insert">
            Inserir tipo de produto
        </a>
    </div>
</div>