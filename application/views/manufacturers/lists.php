<script>
$(document).ready(function() {
    $('#manufacturer').dataTable();
});

function excluir(id,nome) {
    if (confirm('Deseja excluir o fabricante '+nome+'?')) {
        window.location='<?php echo base_url().'manufacturers/remove/';?>'+id;
    }
}
</script>
<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Fabricantes</h5>
    </div>
    <table class="table table-striped table-bordered" id="manufacturer" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 60px">Ação</th>
                <th>Nome Fantasia</th>
                <th>Razão Social</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            if ($dados) : 
                foreach ($dados as $d) :
                $status = ($d['status'] == TRUE) ? 
                    '<span class="label label-success">Ativo</span>' : 
                    '<span class="label label-danger">Inativo</span>';
                $id = $d['manufacturer_id'];
                $corporate = $d['corporate_name'];
        ?>
                <tr>
                    <td>
                        <a class="btn btn-sm btn-default" href='<?php echo base_url()."manufacturers/edit/".$id; ?>'>
                            <img src="<?php echo base_url()."img/fi-page-edit.svg";?>" 
                                 width='14px' title='Editar' style='text-align: center'/>
                        </a>
                        <a class="btn btn-sm btn-default" href='javascript:void(o)' 
                           onClick="excluir(<?php echo $id.",'".$corporate."'"; ?>)">
                            <img src="<?php echo base_url()."img/fi-x.svg";?>" 
                                 width='14px' title='Excluir' style='text-align: center'/>
                        </a>
                    </td>
                    <td><?php echo $corporate; ?></td>
                    <td><?php echo $d['trade_name']; ?></td>
                    <td><?php echo $status; ?></td>
                </tr>
        <?php
                endforeach;
            endif;
        ?>
        </tbody>
    </table>
    <div style="margin: 30px auto; text-align: center">
        <a class='btn btn-primary' href="<?php echo base_url(); ?>manufacturers/insert">
            Inserir novo fabricante
        </a>
    </div>
</div>