<div class="container">
	<form class="form-horizontal" method="post" action="">
            <input type="hidden" name="manufacturer_id"
                   value="<?php echo isset($dados['manufacturer_id']) ? $dados['manufacturer_id'] : NULL;?>"/>
		<fieldset>
                        <div class="row col-md-9">
                            <legend>
                                Dados do Fabricante
                                <div style="float: right; font-size: 11px; padding-top: 10px;">
                                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/> 
                                Indica campo obrigatório
                            </div>
                            </legend>
                        </div>
                        <div class="clearfix"></div>
                        
                        
                        <div class="form-group">
                            <?php 
                                echo form_label('Razão social', 'corporate_name', array('class' => 'col-md-4 control-label'));
                                echo "<div class='col-md-5'>";
                                $attCorporateName = array(
                                    'name'      => 'corporate_name',
                                    'id'        => 'corporate_name',
                                    'value'     => isset($dados['corporate_name']) ? $dados['corporate_name'] : set_value('corporate_name'),
                                    'class'     => 'form-control input-md requiredField',
                                );
                                echo form_input($attCorporateName);
                                echo form_error('corporate_name', '<div class="error">', '</div>');
                                echo "</div>"
                             ?>
                            <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        </div>
                        
                        <div class="form-group">
                            <?php 
                                echo form_label('Nome fantasia', 'corporate_name', array('class' => 'col-md-4 control-label'));
                                echo "<div class='col-md-5'>";
                                $attTradeName = array(
                                    'name'      => 'trade_name',
                                    'id'        => 'trade_name',
                                    'value'     => isset($dados['trade_name']) ? $dados['trade_name'] : set_value('trade_name'),
                                    'class'     => 'form-control input-md requiredField',
                                );
                                echo form_input($attTradeName);
                                echo form_error('trade_name', '<div class="error">', '</div>');
                                echo "</div>"
                             ?>
                            <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        </div>
                        
                        <div class="form-group">
                            <?php echo form_label('Status', 'status', array('class' => 'col-md-4 control-label')); ?>
                            <div class="col-md-3">

                                <?php
                                    $arrStatus = array(0 => 'Inativo', 1 => 'Ativo');
                                    $defaultStatus = isset($dados['status']) ? $dados['status'] : 1;
                                    echo form_dropdown('status', $arrStatus, $defaultStatus, 'class="form-control input-small requiredField" id="status"'); 
                                ?>
                            </div>
                            <img src="<?php echo base_url();?>img/required.gif"/>
                        </div>
                        
                        <div style="margin: 30px auto; text-align: center">
                            <a class='btn btn-default' href="<?php echo base_url()."manufacturers/lists";?>">
                                <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                           </a>
                            <button id="save" name="save" class="btn btn-success">Salvar</button>
                        </div>
		</fieldset>
	</form>
</div>