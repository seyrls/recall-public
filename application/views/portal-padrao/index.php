<script>
    $(document).ready(function(){
        $('#showForm').click(function(event) {
            if ($('#txtShow').text() == 'Pesquisa avançada') {
                $('#avancado').val(1);
                $('#txtShow').text('Ocultar pesquisa avançada');
            } else {
                $('#avancado').val(0);
                $('#txtShow').text('Pesquisa avançada');
            }
            event.preventDefault();
            $('img:visible', this).hide().siblings().show();
            $('#searchForm').toggle($('img:visible').is('.up'));
        });
        $("#reset").click(function(){
            $("input[name='search']").val("");
            $("input[name='start_date_ini']").val("");
            $("input[name='start_date_fim']").val("");
            $('select').val('');
    });
        
        $(".calendario").datepicker({
            changeMonth: true,//this option for allowing user to select month
            changeYear: true, //this option for allowing user to select from year range
            dateFormat: "dd/mm/yy"
        });
        
        // Combo Tipo de Produto
        $("select[name='branch_id']").change(function(){
            var branch_id = $("select[name='branch_id'] option:selected").attr('value');
            if (branch_id.length > 0) {
                var urlpath = '<?php echo base_url();?>';
                $("#type_product_id").html("<option value=''>Selecione</option>");
                $.ajax({
                    url: urlpath+'type_products/loadCombo/',
                    type:'POST',
                    data: 'branch_id='+branch_id,
                    dataType: 'json',
                    success: function( data ) {
                        $.each(data, function(i, value) {
                            $('#type_product_id').append($('<option/>', {
                                value: i,
                                text : value 
                            }));
                        });
                    }
                });
            }
	});
    });
</script>

<?php

if ($this->input->post()) {
    extract($this->input->post());
}
?>
<div id="content" class="span9">
    <section id="content-section">							
        <span class="hide">Início do conteúdo da página</span>
        <div class="row-fluid">
            <div class="span12 module">
                <div class="outstanding-header">
                    <h2 class="outstanding-title">Pesquise os Alertas de Recall</h2>
                </div>
                <!-- Destaques alertas de recall -->
                <div class="no-margin">
                    <form name="pesquisa-recall" method="post" action="">
                        <fieldset>
                            <legend class="hide">Busca</legend>
                            <h2 class="hidden">Buscar no portal</h2>
                            <div class="input-append">
                                <label for="portal-searchbox-field" class="hide">Informe o nome da campanha, 
                                    produto ou fornecedor </label>
                                <input type="text" id="portal-searchbox-field" class="searchField input-xxlarge" 
                                        placeholder="Informe o nome da campanha, produto ou fornecedor" 
                                        value="<?php echo isset($search) ? $search : '';?>"
                                        title="Informe o nome do produto/fornecedor"
                                        name="search">
                                <input type="submit" class="btn btn-default searchButton" 
                                       value='Buscar'/>
                            </div>
                            <div class="clear"></div>
                            <a href="javascript:void(0)" id="showForm">
                                <input type="hidden" name="avancado" 
                                       value="<?php echo isset($avancado) ? $avancado : ''; ?>" id="avancado"/>
                                <img src="<?php echo base_url();?>img/fi-plus.svg" 
                                     style="width: 16px;"title="Pesquisa avançada"/>
                                <img src="<?php echo base_url();?>img/fi-minus.svg" class="up" 
                                     style="display:none; width: 16px;" title="Ocultar Pesquisa avançada"/>
                                <span id="txtShow">Pesquisa avançada</span>
                            </a>
                            <table id="searchForm" class='col-md-12 tbSearch' 
                                   style="<?php echo (isset($avancado) && $avancado == '1') ? 
                                                    'display:block' : 'display:none'; ?>">
                                <tr>
                                    <td>
                                        <?php echo form_label('Período da campanha', 'start_date', 
                                                array('class' => 'col-md-4')); ?>
                                    </td>
                                    <td>
                                        De:
                                        <?php
                                            $arrStart_date_ini = array(
                                                'name'      => 'start_date_ini',
                                                'id'        => 'start_date_ini',
                                                'value'     => isset($start_date_ini) ? $start_date_ini : '',
                                                'class'     => 'form-control input-small calendario',
                                            );
                                            echo form_input($arrStart_date_ini);
                                        ?>
                                        Até: 
                                        <?php
                                            $arrStart_date_fim = array(
                                                'name'      => 'start_date_fim',
                                                'id'        => 'start_date_fim',
                                                'value'     => isset($start_date_fim) ? $start_date_fim : '',
                                                'class'     => 'form-control input-small calendario',
                                            );
                                            echo form_input($arrStart_date_fim);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo form_label('Setor de Atividade', 'branch', array('class' => 'col-md-4')); ?>
                                    </td>
                                    <td>
                                        <?php echo form_dropdown('branch_id', $comboBranch, 
                                                isset($branch_id) ? $branch_id : '', 
                                                'class="form-control input-xlarge"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo form_label('Tipo de Produto', 'type_product', 
                                                array('class' => 'col-md-4')); ?>
                                    </td>
                                    <td>
                                        <?php echo form_dropdown('type_product_id', $type_product, 
                                                isset($type_product_id) ? $type_product_id : '', 
                                                'class="form-control input-xlarge" id="type_product_id"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo form_label('Fornecedor', 'trade_name', 
                                                array('class' => 'col-md-4')); ?>
                                    </td>
                                    <td>
                                        <?php echo form_dropdown('supplier_id', $comboSupplier, 
                                                isset($supplier_id) ? $supplier_id : '', 
                                                'class="form-control input-xlarge"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <button class="btn btn-default btn-small searchButton" type="submit">
                                            <i class="icon-search"></i> Pesquisar
                                        </button>
                                        <button class="btn btn-default btn-small" type="button" id="reset">
                                            <i class="icon-refresh"></i> Limpar
                                        </button>
                                    </td>
                                </tr>
                            </table>                                        
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <span class="hide">Fim do conteúdo da página</span>							
    </section>
</div>

<div id="content" class="span9">
    <section id="content-section">							
        <span class="hide">Início do conteúdo da página</span>
        <div class="row-fluid">
            <div class="span12 module">
                <div class="outstanding-header">
                    <h2 class="outstanding-title">Últimos Alertas de Recall</h2>
                </div>
                <?php if ($recall) : ?>
                    <?php foreach($recall as $r) : ?>
                        <div class="span6 lastRecall">
                            <div class='titleLastRecall'>
                                <a href="<?php echo base_url().'principal/detailRecall/'.$r['id'];?>">
                                    <strong>
                                        <?php 
                                            echo (strlen($r['title']) > 70) ? 
                                                    substr($r['title'], 0, 70).'...' : $r['title'];
                                        ?>
                                    </strong>
                                </a>
                            </div>
                            <a href="<?php echo base_url().'principal/detailRecall/'.$r['id']; ?>" class="img-rounded">
                                <img src="<?php echo ($r['path']) ? $r['path'] : base_url().'img/default_product.png' ;?>" 
                                     alt="imagem decorativa">
                            </a>
                            <p> 
                                <?php 
                                    echo (strlen($r['description']) > 220) ? 
                                            substr($r['description'], 0, 220).'...' : $r['description'];
                                ?>
                            </p>
                        </div>
                    <?php endforeach; ?>
                    <div class="clear"></div>
                    <div class="paginacao">
                        <p style="text-align: right; padding: 10px;"><b>Total: <?php echo $num_results; ?> </b></p>
                        <?php 
                            if (strlen($pagination)) :
                              echo $pagination;
                            endif;
                        ?>
                    </div>
                <?php else : ?>
                        <p>Nenhum resultado encontrado.</p>
                <?php endif; ?>        
            </div>
        </div>
        <span class="hide">Fim do conteúdo da página</span>							
    </section>
</div>