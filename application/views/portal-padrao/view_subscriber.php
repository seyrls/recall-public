<script> 
    $(document).ready(function(){
        $("#removeItens").hide();
    });
    
    
    function removeItem(id,branch,type) {
        var rowCount = $('.newslleter').length;
        $('#'+id).remove();
        $('#itens').append("<tr style='border: 1px solid #ccc;'><td style='border: 1px solid #ccc; padding: 5px'>"+branch+"</td><td style='padding: 5px'>"+type+"</td></tr>");
        $("#removeItens").append("<input type='hidden' name='removeNewsletter[]' value='"+id+"'/>");
        $("#removeItens").show();
        if (rowCount === 1) {
            $("#headerNewsletter").hide();
        }
    }
    
</script>

<div id="content" class="span9">
                <section id="content-section">							
                    <span class="hide">Início do conteúdo da página</span>
                    <div class="row-fluid">
                        <div class="span12 module">
                            <div class="outstanding-header">
                                <h2 class="outstanding-title">Solicitações de recebimento de alertas de Recall</h2>
                            </div>
                            
                            <?php if ($this->session->flashdata('success')) : ?>
                                <div class="flash-messages">
                                    <div class="alert alert-success">
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                </div>
                            <?php elseif ($this->session->flashdata('error')) : ?>
                                <div class="flash-messages">
                                    <div class="alert alert-danger">
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            
                            <p>Informe o e-mail no campo abaixo e clique no botão "Visualizar" para 
                                encontrar as solicitações de envio de alertas de Recall.</p>
                            
                            <div class="no-margin">
                                <form name="newsletter-recall" method="post" action="" class="form-horizontal form-group">
                                    <fieldset>
                                        <legend class="hide">Dados para cadastro</legend>
                                        <table class='col-md-12 tbSearch'>
                                            <tr>
                                                <td>
                                                    <?php echo form_label('E-mail', 'email', array('class' => 'col-md-4')); ?>
                                                </td>
                                                <td>
                                                   <?php
                                                       $email = array(
                                                           'name'      => 'email',
                                                           'id'        => 'email',
                                                           'value'     => set_value('email'),
                                                           'class'     => 'form-control input-xlarge',
                                                       );
                                                       echo form_input($email);
                                                       echo form_error('email', '<div class="display-error">', '</div>');
                                                   ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <button class="btn btn-primary btn-small searchButton" type="submit">
                                                        <i class="icon-search"></i> Visualizar
                                                    </button>
                                                    <a class='btn btn-default btn-small'
                                                        href="<?php echo base_url();?>principal/addSubscriber">
                                                         <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>                                      
                                    </fieldset>
                                </form>
                            </div>
                            <div id="addSubscriber">
                                <?php if (isset($options)) : ?>
                                <p id="headerNewsletter" class="label label-info" style="text-align: center; display: block; margin: auto">
                                    Solicitações cadastradas pelo usuário: <?php echo (strlen($options[0]['name']) > 60) ? substr($options[0]['name'],0,60).'...' : $options[0]['name']; ?> 
                                </p>
                                    <?php foreach($options as $option) : 
                                            $id = $option['option_id'];
                                            $branch = ($option['branch']) ? $option['branch'] : 'Todos';
                                            $typeProduct = ($option['type_product']) ? $option['type_product'] : 'Todos';
                                    ?>
                                    <div class="newslleter" id="<?php echo $id; ?>" style="border: 1px solid #ccc; padding: 10px; margin-bottom:10px">
                                        <button onClick="removeItem(<?php echo $id; ?>,'<?php echo $branch; ?>','<?php echo $typeProduct; ?>')"
                                         title='Excluir Solicitação' style='cursor:pointer; float: right;' class="btn btn-default btn-mini searchButton">
                                            Excluir Solicitação
                                        </button>
                                        <p><b>Setor de Atividade: </b> <?php echo $branch; ?></p>
                                        <p><b>Tipo de Produto: </b> <?php echo $typeProduct ?></p>
                                        <?php if ($option['flag_all']): ?>
                                        <p style="font-style: italic; font-size: 11px; color: #0088cc"> 
                                            Essa solicitação recebrá o envio de todos os alertas de Recall
                                        </p>
                                        <?php endif; ?>
                                    </div>
                                <?php 
                                        endforeach;
                                    endif;
                                ?>
                                
                                <form name="remove-newsletter" method="post" action="removeItensNewsletter" class="form-horizontal form-group">
                                    <div id="removeItens">
                                        <p class="label label-default" style="text-align: center; display: block; margin: auto">
                                            Solicitação de exclusão de alertas de recall
                                        </p>
                                        <table class='col-md-12' style="border: 1px solid #ccc; font-size: 13px" width='100%'>
                                            <thead>
                                                <tr style='border: 1px solid #ccc; background-color: #eee;'>
                                                    <th>Setor de Atividade</th>
                                                    <th>Tipo de Produto</th>
                                                </tr>
                                            </thead>
                                            <tbody id="itens">
                                            </tbody>
                                        </table>
                                        <div style="clear: both; margin: auto; text-align: center; margin-top: 20px;">
                                            <button class="btn btn-success btn-small searchButton" type="submit">
                                                Confirmar Exclusão
                                            </button>
                                            <button class="btn btn-default btn-small" type="button" onclick="location.reload();">
                                                Cancelar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <span class="hide">Fim do conteúdo da página</span>							
                </section>
            </div>