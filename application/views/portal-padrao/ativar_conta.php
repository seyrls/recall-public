<script>
    function salvar(){
        if (confirm('Deseja salvar a nova senha?')) {
            $("#password" ).submit();
        }
    }
</script>
            <div id="content" class="span9">
                <section id="content-section">							
                    <span class="hide">Início do conteúdo da página</span>
                    <div class="row-fluid">
                        <div class="span12 module">
                            <div class="outstanding-header">
                                <h2 class="outstanding-title">Ativar Conta</h2>
                            </div>
                            
                            <?php if ($this->session->flashdata('success')) : ?>
                                <div class="flash-messages">
                                    <div class="alert alert-success">
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                </div>
                            <?php elseif ($this->session->flashdata('error')) : ?>
                                <div class="flash-messages">
                                    <div class="alert alert-danger">
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            
                            <p>
                                Para acessar sua conta no sistema de Recall 3.0, favor informe nos campos abaixo
                                a senha enviada para o seu e-mail e cadastre uma nova senha.
                            </p>
                            <div class="no-margin">
                                <form name="password" id="password" method="post" action="" class="form-horizontal form-group">
                                    <fieldset>
                                        <legend class="hide">Dados para cadastro de senha</legend>
                                        <table id="newsletterForm" class='col-md-12 tbSearch'>
                                            <tr>
                                                <td>
                                                    <?php echo form_label('Senha Atual', 'password', array('class' => 'col-md-4')); ?>
                                                </td>
                                                <td>
                                                   <?php
                                                       $password = array(
                                                           'name'      => 'password',
                                                           'id'        => 'password',
                                                           'value'     => set_value('password'),
                                                           'class'     => 'form-control input-xlarge',
                                                       );
                                                       echo form_password($password);
                                                   ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo form_label('Nova Senha', 'password_new', array('class' => 'col-md-4')); ?>
                                                </td>
                                                <td>
                                                   <?php
                                                       $passwordNew = array(
                                                           'name'      => 'password_new',
                                                           'id'        => 'password_new',
                                                           'value'     => set_value('password_new'),
                                                           'class'     => 'form-control input-xlarge',
                                                       );
                                                       echo form_password($passwordNew);
                                                   ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo form_label('Confirmar Nova Senha', 'confirm_password_new', array('class' => 'col-md-4')); ?>
                                                </td>
                                                <td>
                                                   <?php
                                                       $passwordConfirm = array(
                                                           'name'      => 'confirm_password_new',
                                                           'id'        => 'confirm_password_new',
                                                           'value'     => set_value('confirm_password_new'),
                                                           'class'     => 'form-control input-xlarge',
                                                       );
                                                       echo form_password($passwordConfirm);
                                                   ?>
                                                </td>
                                            </tr>
                                            <tr></tr>
                                        </table>
                                        <div class="clear" style="margin-top: 30px">
                                            <button class="btn btn-success btn-small" type="button"
                                                    style="margin: auto; display: block;" onclick="salvar()">
                                                Salvar Alteração
                                            </button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <span class="hide">Fim do conteúdo da página</span>							
                </section>
            </div>