<main> 
    <div class="container">
        <div class="row-fluid">
            <div id="navigation" class="span3">
                <a href="#" class="visible-phone visible-tablet mainmenu-toggle btn"><i class="icon-list"></i>&nbsp;Menu</a>
                <section id="navigation-section" id="menu">							
                    <span class="hide">Início do menu principal</span>
                    <nav class="menu-de-apoio span9">
                        <h2 class="hide">Temas relevantes</h2>
                        <ul>
                            <li>
                                <a href="interna-de-noticias.html" title="Conheça a identidade digital do governo">Conheça a identidade digital do governo</a>
                            </li>
                        </ul>							
                    </nav>
                    <nav class="span9 assuntos">
                        <h2>Consumidor <i class="icon-chevron-down visible-phone visible-tablet pull-right"></i></h2>
                        <ul>
                            <li><a href="<?php echo base_url().'principal/index'?>" title="Alertas de Recall">Alertas de Recall</a></li>
                            <li><a href="<?php echo base_url().'principal/addSubscriber'?>" title="Receber Alertas de Recall">Receber Alertas</a></li>
                            <li><a href="<?php echo base_url().'principal/report'?>" title="Indicadores">Indicadores</a></li>
                        </ul>
                    </nav>
                    <nav class="span9 sobre">
                        <h2>Sobre <i class="icon-chevron-down visible-phone visible-tablet pull-right"></i></h2>
                        <ul>
                            <li><a href="http://www.justica.gov.br/seus-direitos/consumidor/saude-e-seguranca" title="O que é Recall">O que é Recall</a></li>
                            <li><a href="http://www.justica.gov.br/seus-direitos/consumidor/saude-e-seguranca" title="Saúde e Segurança">Saúde e Segurança</a></li>
                            <li><a href="http://www.justica.gov.br/seus-direitos/consumidor/defesa-do-consumidor-no-brasil" title="Código de Defesa do Consumidor">Código de Defesa do Consumidor</a></li>								
                            <li><a href="http://siac.justica.gov.br" title="Sistema de Informações sobre Acidentes de Consumo">Sistema de Informações sobre Acidentes de Consumo</a></li>
                            <li><a href="http://www.justica.gov.br/Acesso/servico-de-informacao-ao-cidadao-sic" title="Serviço de Informação ao Cidadão - SIC">Serviço de Informação ao Cidadão - SIC</a></li>
                        </ul>
                    </nav>	
                    <span class="hide">Fim do menu principal</span>
                </section>
            </div><!-- fim #navigation.span3 -->