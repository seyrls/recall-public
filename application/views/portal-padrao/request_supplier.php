<script>
    $(document).ready(function(){
        $("#reset").click(function(){
            $("#username").val("");
            $("#email").val("");
            $("#corporate_name").val("");
            $("#trade_name").val("");
            $("#cnpj").val("");
            $("#state_registration").val("");
            $("#address").val("");
            $("#complement").val("");
            $("#district").val("");
            $("#zip").val("");
            $("#commercial_phone").val("");
            $("#fax_phone").val("");
            $("select").text("");
        });
        
        $("#cnpj").mask("99.999.999/9999-99");
        $("#zip").mask("99999-999"); 
        $("#commercial_phone").mask("(99) 9999-9999?9");
        $("#fax_phone").mask("(99) 9999-9999?9");
        
        $("select[name='state_id']").change(function(){
            var state_id = $("select[name='state_id'] option:selected").attr('value');
            $("#city_id").html("<option value=''>selecione</option>");
            if (state_id.length > 0) {
                var urlpath = '<?php echo base_url();?>';
                $.ajax({
                    url: urlpath+'cities/loadCombo/',
                    type:'POST',
                    data: 'state_id='+state_id,
                    dataType: 'json',
                    success: function( data ) {
                        $.each(data, function(i, value) {
                            $('#city_id').append($('<option/>', {
                                value: i,
                                text : value 
                            }));
                        });
                    }
                });
            }
	}); 
    });
</script>

<?php
    $city_id = ($this->input->post('city_id')) ? $this->input->post('city_id') : NULL;
    $defaultCity = isset($dados['supplier']['city_id']) ? $dados['supplier']['city_id'] : $city_id;
?>
<div id="content" class="span9">
    <section id="content-section">
        <span class="hide">Início do conteúdo da página</span>
        <div class="row-fluid">
            <div class="span12 module">
                <div class="outstanding-header">
                    <h2 class="outstanding-title">Solicitação de cadastro de fornecedor</h2>
                </div>

            <?php if ($this->session->flashdata('success')) : ?>
                <div class="flash-messages">
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                </div>
            <?php elseif ($this->session->flashdata('error')) : ?>
                <div class="flash-messages">
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                </div>
            <?php endif; ?>

                <p>Seja bem-vindo!</p>
                <p>Para fazer parte do grupo de fornecedores da SENACON,
                    é necessário preencher o formulário de solicitação de cadastro abaixo
                    e aguardar a análise dos dados enviados.
                </p>
                <p>
                    Já possui uma conta?
                    <a title="Area de Login" href="<?php echo base_url();?>logins/accountSupplier">Clique aqui</a>
                    para acessar o sistema
                </p>
                <div style="float: right; font-size: 11px; padding: 10px;">
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/> 
                    Indica campo obrigatório
                </div>
                
        <div class="no-margin">
            <form name="request-supplier" id="request-supplier" method="post" action="" class="form-horizontal form-group">
                <fieldset>
                    <legend class="hide">Dados para cadastro</legend>
                        <table id="requestSupplier" class='col-md-12 tbSearch'>
                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Nome', 'username', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attUsername = array(
                                            'name'      => 'username',
                                            'id'        => 'username',
                                            'value'     => isset($dados['username']) ? ($dados['username']) : set_value('username'),
                                            'class'     => 'form-control input-xlarge requiredField',
                                        );
                                        echo form_input($attUsername);
                                    ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                    <?php echo form_error('username', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <?php
                                        echo form_label('E-mail', 'email', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attEmail = array(
                                            'name'      => 'email',
                                            'id'        => 'email',
                                            'value'     => (isset($dados['email']) && !empty($dados['email'])) ? ($dados['email']) : set_value('email'),
                                            'class'     => 'form-control input-xlarge requiredField',
                                        );
                                        echo form_input($attEmail);
                                    ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                    <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Razão Social', 'corporate_name', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                            $attCorporate = array(
                                                'name'      => 'corporate_name',
                                                'id'        => 'corporate_name',
                                                'value'     => isset($dados['supplier']['corporate_name']) ? ($dados['supplier']['corporate_name']) : set_value('corporate_name'),
                                                'class'     => 'form-control input-xlarge requiredField',
                                            );
                                            echo form_input($attCorporate);
                                    ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                    <?php echo form_error('corporate_name', '<div class="error">', '</div>');?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Nome Fantasia', 'trade_name', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attTrade = array(
                                            'name'      => 'trade_name',
                                            'id'        => 'trade_name',
                                            'value'     => isset($dados['supplier']['trade_name']) ? ($dados['supplier']['trade_name']) : set_value('trade_name'),
                                            'class'     => 'form-control input-xlarge requiredField',
                                        );
                                        echo form_input($attTrade);
                                    ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                    <?php echo form_error('trade_name', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <?php
                                        echo form_label('CNPJ', 'cnpj', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attCnpj = array(
                                            'name'      => 'cnpj',
                                            'id'        => 'cnpj',
                                            'value'     => isset($dados['supplier']['cnpj']) ? ($dados['supplier']['cnpj']) : set_value('cnpj'),
                                            'class'     => 'form-control input-xlarge requiredField',
                                        );
                                        echo form_input($attCnpj);
                                    ?>
                                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                <?php echo form_error('cnpj', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Inscrição Estadual', 'state_registration', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attState = array(
                                            'name'      => 'state_registration',
                                            'id'        => 'state_registration',
                                            'value'     => isset($dados['supplier']['state_registration']) ? ($dados['supplier']['state_registration']) : set_value('state_registration'),
                                            'class'     => 'form-control input-xlarge',
                                            'maxlength' => 20,
                                        );
                                        echo form_input($attState);
                                        echo form_error('state_registration', '<div class="error">', '</div>');
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Ramo de atividade', 'branch', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $defaultBranch = isset($dados['supplier']['branch_id']) ? $dados['supplier']['branch_id'] : set_value('branch_id');
                                        echo form_dropdown('branch_id', $comboBranch, $defaultBranch, 'class="form-control requiredField"');
                                    ?>
                                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                <?php echo form_error('branch_id', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Endereço', 'address', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attAddress = array(
                                            'name'      => 'address',
                                            'id'        => 'address',
                                            'value'     => isset($dados['supplier']['address']) ? ($dados['supplier']['address']) : set_value('address'),
                                            'class'     => 'form-control input-xlarge requiredField',
                                        );
                                        echo form_input($attAddress);
                                    ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                    <?php echo form_error('address', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Complemento', 'complement', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attComplement = array(
                                            'name'      => 'complement',
                                            'id'        => 'complement',
                                            'value'     => isset($dados['supplier']['complement']) ? ($dados['supplier']['complement']) : set_value('complement'),
                                            'class'     => 'form-control input-xlarge',
                                        );
                                        echo form_input($attComplement);
                                        echo form_error('complement', '<div class="error">', '</div>');
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Bairro', 'district', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                         $attDistrict = array(
                                            'name'      => 'district',
                                            'id'        => 'district',
                                            'value'     => isset($dados['supplier']['district']) ? ($dados['supplier']['district']) : set_value('district'),
                                            'class'     => 'form-control input-xlarge requiredField',
                                        );
                                        echo form_input($attDistrict);
                                    ?>
                                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                <?php echo form_error('district', '<div class="error">', '</div>');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                        echo form_label('UF', 'state', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $defaultUf = isset($dados['supplier']['state_id']) ? $dados['supplier']['state_id'] : set_value('state_id');
                                        echo form_dropdown('state_id', $comboState, $defaultUf, 'class="form-control requiredField"');
                                    ?>
                                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                <?php echo form_error('state_id', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Município', 'city_id', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $city = $defaultCity ? $defaultCity : set_value('city_id');
                                        echo form_dropdown('city_id', $comboCity, $city, 'class="form-control requiredField" id="city_id"');
                                    ?>
                                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                <?php echo form_error('city_id', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                        echo form_label('CEP', 'zip', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attZip = array(
                                            'name'      => 'zip',
                                            'id'        => 'zip',
                                            'value'     => isset($dados['supplier']['zip']) ? ($dados['supplier']['zip']) : set_value('zip'),
                                            'class'     => 'form-control input-xlarge requiredField',
                                        );
                                        echo form_input($attZip);
                                    ?>
                                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                <?php echo form_error('zip', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Telefone comercial', 'commercial_phone', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attComercialPhone = array(
                                            'name'      => 'commercial_phone',
                                            'id'        => 'commercial_phone',
                                            'value'     => isset($dados['supplier']['commercial_phone']) ? ($dados['supplier']['commercial_phone']) : set_value('commercial_phone'),
                                            'class'     => 'form-control input-xlarge requiredField',
                                        );
                                        echo form_input($attComercialPhone);
                                    ?>
                                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                <?php echo form_error('commercial_phone', '<div class="error">', '</div>'); ?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                        echo form_label('Fax', 'fax_phone', array('class' => 'col-md-4'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $attFaxPhone = array(
                                            'name'      => 'fax_phone',
                                            'id'        => 'fax_phone',
                                            'value'     => isset($dados['supplier']['fax_phone']) ? ($dados['supplier']['fax_phone']) : set_value('fax_phone'),
                                            'class'     => 'form-control input-xlarge',
                                        );
                                        echo form_input($attFaxPhone);
                                        echo form_error('fax_phone', '<div class="error">', '</div>');  
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" value="Salvar" class="btn btn-primary btn-small"/>
                                    <button id="reset" type="button" class="btn btn-default btn-small">
                                        <span class="glyphicon glyphicon-plus-sign"></span> Limpar
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        <span class="hide">Fim do conteúdo da página</span>							
    </section>
</div>