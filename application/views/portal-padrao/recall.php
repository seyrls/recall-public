<script>
    $(document).ready(function() {
        //www.jqueryscript.net/slider/Powerful-Customizable-jQuery-Carousel-Slider-OWL-Carousel.html
        $(".owl-demo").owlCarousel({
            navigation : false,
            slideSpeed : 300,
            paginationNumbers: true, 
            paginationSpeed : 400,
            items : 1, 
            itemsDesktop : false,
            itemsDesktopSmall : false,
            itemsTablet: false,
            itemsMobile : false,
            responsive: true
        });
  });
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your share button code -->
<div class="fb-share-button" 
        data-href="http://www.your-domain.com/your-page.html" 
        data-layout="button_count">
</div>

<div id="content" class="internas span9">
    <section id="content-section">							
        <span class="hide">Início do conteúdo da página</span>
        <span class="documentCategory">Recalls</span>
        <h1 class="secondaryHeading">
            Alerta de recall para <?php echo $dados['campaign']['title']; ?>
        </h1>
        <div class="subtitle">
            <?php echo $dados['campaign']['description']; ?>
        </div>
        <div class="content-header-options-1 row-fluid">
            <div class="documentByLine span7">
                <ul>
                    <li class="documentAuthor">por <strong>Senacon</strong></li>
                    <li class="documentPublished"><strong>publicado em </strong> <?php echo date("d/m/Y", strtotime($dados['campaign']['start_date']['date'])); ?></li>
                </ul>
            </div>
            <!-- 
            LINK DE AJUDA PARA CONIGURAR TODAS AS MIDIAS SOCIAIS 
            http://www.devmedia.com.br/social-buttons-incluindo-botoes-sociais-em-seu-site/22071
            -->
            <div style="float: right; padding: 10px;">
                <div style="float: left; ">
                    <script src="https://apis.google.com/js/platform.js" async defer>
                      {lang: 'pt-BR'}
                    </script>
                    <div class="g-plusone" data-size="medium" data-href="http://portal.mj.gov.br/recall/"></div>
                </div>
                <div style="float: left; margin-right: 5px; ">
                    <a href="https://twitter.com/share" class="twitter-share-button" data-text="Recall">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                </div>
                <div style="float: right">
                    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
<!--                    <iframe src="http://www.facebook.com/plugins/like.php?href=http://portal.mj.gov.br/recall/&layout=button_count& show_faces=true&width=380&action=like&colorscheme=light&height=30&locale=pt_BR" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:25px;" allowTransparency="true"> </iframe>-->
                </div>
            </div>
        </div>
        <p>
            <?php 
                echo get_status_campaign($dados['campaign']['status_campaign']);
                if (($dados['campaign']['status_campaign'] == STATUS_ID_PUBLICADA_COM_RESSALVA) && ($dados['reasonReservation'])) :
            ?>
                <div class="alert alert-warning">
                    <?php echo ' - ' .$dados['reasonReservation']; ?>
                </div>    
            <?php 
                endif;
            ?>
        </p>
        <p>AVISO DE RISCO: <?php echo $dados['notice_risk'];?></p>
        <p style="text-align: right"><i>Data de início da campanha: <?php echo ($dados['campaign']['start_date']) ? date("d/m/Y", strtotime($dados['campaign']['start_date']['date'])) : "";?></i></p>
        <hr/>
        <div class="productDetails">
            <?php if ($dados['product']) : ?>
                <h5>Produto(s):</h5>
                <?php foreach ($dados['product'] as $key => $product) : ?>
                    <p><b><?php echo $product['product'];?></b></p>
                    <p><span>Tipo de produto:</span>      <?php echo $product['type_product'];?></p>
                    <p><span>Modelo:</span>               <?php echo $product['model'];?></p>
                    <p><span>Fabricante:</span>       <?php echo $product['corporate_name'];?></p>
                    <p><span>País de origem:</span>       <?php echo $product['country'];?></p>
                    <p><span>Ano de fabricação:</span><?php echo $product['year_manufacture'];?></p>
                    <?php if ($product['image']) : ?> 
                    <div class="owl-demo owl-carousel owl-theme">
                        <?php foreach ($product['image'] as $image) : ?>
                                <div class="item"><img src="<?php echo $image; ?>" alt="The Last of us"></div>
                        <?php endforeach; ?>
                    </div>  
                    <?php endif; ?>
                    <hr/>
                <?php endforeach; ?>
            
            <?php endif; ?>

            <h5>Informações sobre a empresa:</h5>
            <p><span>Local para atendimento: </span>
                <?php echo ($dados['campaign']['customer_service']) ? $dados['campaign']['customer_service'] : "-";?></p>
            <p><span>Site: </span><?php echo ($dados['location']['site']) ?
                    "<a href='http:// ".$dados['location']['site']."'>".$dados['location']['site']."</a>" : "-";?>
            </p>
            <p><span>Email: </span><?php echo $dados['location']['email'];?></p>
            <p><span>Telefone p/ contato: </span><?php echo $dados['location']['phone_service'];?></p>
        </div>
        <hr/>
        <span class="hide">Fim do conteúdo da página</span>							
    </section>
</div>

</div><!-- fim .row-fluid -->
                        
</div><!-- fim .container -->
                    
</main>


