
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
<script type="text/javascript">
        google.load('visualization', '1.0', {'packages':['corechart','bar']});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            
           // chart affected
            var data_affected = google.visualization.arrayToDataTable(
                <?php echo $series_data_affected; ?>
            );
            var options_affected = {
              chart: {
                  title: 'Performance no atendimento das campanhas',
//                  subtitle: 'Produtos Atendidos e Produtos Afetados: 2010-2015',
                },
                bars: 'vertical'
            };
            var chart_affected = new google.charts.Bar(document.getElementById('chart_affected'));
            chart_affected.draw(data_affected, options_affected);
           
            // chart risk
            var data_risk = google.visualization.arrayToDataTable(
                <?php  echo $series_data_risk; ?>
            );
            var options_risk = {
              is3D: true
            };

            var chart_risk = new google.visualization.PieChart(document.getElementById('chart_risk'));
            chart_risk.draw(data_risk, options_risk);
            
            
            // chart branch
            var data_branch = google.visualization.arrayToDataTable(
                <?php echo $series_data_branch; ?>
            );
            var options_branch = {
              is3D: true
            };

            var chart_branch = new google.visualization.PieChart(document.getElementById('chart_branch'));
            chart_branch.draw(data_branch, options_branch);
        }
        
    $(document).ready(function () {
        $("select[name='supplier_id']").change(function(){
            var supplier_id = $(this).val();
            window.location='<?php echo base_url().'administrations/index/';?>'+supplier_id;
        });
    });
</script>
<div id="content" class="span9">
    <div class="row" style="margin-bottom: 5px">
        <div class="col-md-12">
            <h1 class="page-header">
                <small> 
                    Estatísticas de Campanhas de Recall
                </small>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-bullhorn fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div style="font-size: 20px; font-weight: bold;">
                                <?php echo $countRecallPublish; ?>
                            </div>
                            <div>
                                Campanhas de Recall Publicadas
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-map-marker fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div style="font-size: 20px; font-weight: bold;">
                                <?php echo $totalAffected; ?>
                            </div>
                            <div>
                                Produtos Afetados em todo o Brasil
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-check-square-o fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div style="font-size: 20px; font-weight: bold;">
                                <?php echo $totalServiced; ?>
                            </div>
                            <div>
                                Produtos Atendidos. Percentual: <b><?php echo $indiceServiced; ?></b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading col-md-12">
                    <h3 class="panel-title col-md-9"><i class="fa fa-bar-chart-o fa-fw"></i> 
                        Quantidade de produtos afetados/atendidos por ano
                    </h3>
                </div>
                <div class="clear"></div>
                <div class="panel-body">
                     <div id="chart_affected" style="height: 300px"></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading col-md-12">
                    <h3 class="panel-title col-md-9"><i class="fa fa-bar-chart-o fa-fw"></i> 
                        Tipos de Riscos
                    </h3>
                </div>
                <div class="clear"></div>
                <div class="panel-body">
                     <div id="chart_risk" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading col-md-12">
                    <h3 class="panel-title col-md-9"><i class="fa fa-bar-chart-o fa-fw"></i> 
                        Setores de Atividades
                    </h3>
                </div>
                <div class="clear"></div>
                <div class="panel-body">
                     <div id="chart_branch" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>