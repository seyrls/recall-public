<script>
    $(document).ready(function(){
        var k = 0;
        $("#all_alert").click(function(){
            if ($(this).is(':checked')) {
                $("#branch_id").val("");
                $("#branch_id").attr('disabled','disabled');
                $("#branch_id").find('option:selected').text('Todos');

                $("#type_product_id").val("");
                $("#type_product_id").attr('disabled','disabled');
                $("#type_product_id").find('option:selected').text('Todos');
            } else {
                $("#branch_id").removeAttr('disabled');
                $("#type_product_id").removeAttr('disabled');
            }
        });
        
        $("#reset").click(function(){
            $("#branch_id").removeAttr('disabled');
            $("#type_product_id").removeAttr('disabled');
            $("#name").removeAttr('readonly');
            $("#email").removeAttr('readonly');
        });
        
        $("#add_row_subscriber").click(function(){
            var name = $("#name").val();
            var email = $("#email").val();
            var branch = $("#branch_id").find('option:selected').text();
            var branch_id = $("#branch_id").find('option:selected').val();
            var type_product = $("#type_product_id").find('option:selected').text();
            var type_product_id = $("#type_product_id").find('option:selected').val();
            var all_alert = $("#all_alert").is(':checked');
            
            if (name === "") {
                alert('É necessário informar um nome');
                return false;
            }
            if (email === "") {
                alert('É necessário informar um e-mail');
                return false;
            }
            if (!validateEmail(email)) {
                alert('É necessário informar um e-mail válido');
                return false;
            }
            if ((branch_id === "") && (!all_alert)){
                alert('É necessário informar um Setor de Atividade');
                return false;
            }
            if ((type_product_id === "") || (type_product_id === "Selecione")){
                 type_product = 'Todos';
                 type_product_id = 0;
            }
            if (all_alert) {
                branch = 'Todos';
                branch_id = 0;
                type_product = 'Todos';
            }
            
            $('#sendSubscriber').show();
            $('#name_').text(name);
            $('#email_').text(email);
            
            $('#subscriber_'+k).html(
                "<td><input name='branch[]' value='"+branch+"' type='text' class='form-control input-xlarge' readonly>"+
                    "<input name='branch_id[]' value='"+branch_id+"' type='hidden'></td>"+
                "<td><input name='type_product[]' value='"+type_product+"' type='text' class='form-control input-xlarge' readonly/>"+
                    "<input name='type_product_id[]' value='"+type_product_id+"' type='hidden'></td>"+
                "<td style='text-align:center'><a href='javascript:void(0)' onclick='removeItem("+(k+1)+")' title='Remover Item'  class='removeItemColumn'>"+
                    "<img src='<?php echo base_url();?>img/fi-x.svg' width='20px'></a></td>"
            );
            $("#name").attr('readonly','readonly');
            $("#email").attr('readonly','readonly');
            $("#branch_id").val("");
            $("#type_product_id").val("");
            $("#type_product_id").text("");
            $('#tab_subscriber').append('<tr id="subscriber_'+(k+1)+'"></tr>');
            k++;
        });
        
        
       // Combo Tipo de Produto
        $("select[name='branch_id']").change(function(){
            var branch_id = $("select[name='branch_id'] option:selected").attr('value');
            if (branch_id.length > 0) {
                var urlpath = '<?php echo base_url();?>';
                $("#type_product_id").html("<option value=''>Todos</option>");
                $.ajax({
                    url: urlpath+'type_products/loadCombo/',
                    type:'POST',
                    data: 'branch_id='+branch_id,
                    dataType: 'json',
                    success: function( data ) {
                        $.each(data, function(i, value) {
                            $('#type_product_id').append($('<option/>', {
                                value: i,
                                text : value 
                            }));
                        });
                    }
                });
            }
	}); 
    });
    
    function enviar(){
        var rowCount = $('#tab_subscriber tr').length;   
        if (rowCount < 4) {
            alert('É necessário adicionar pelo menos uma solicitação.');
        } else {
            if (confirm('Deseja salvar as solicitações para recebimento de alertas?')) {
                $("#newsletter-recall" ).submit();
            }
        }
    }
    
    function removeItem(k) {
        $('#subscriber_'+(k-1)).remove();
    }
    
    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
    
</script>
<div id="content" class="span9">
    <section id="content-section">							
        <span class="hide">Início do conteúdo da página</span>
        <div class="row-fluid">
            <div class="span12 module">
                <div class="outstanding-header">
                    <h2 class="outstanding-title">Cadastro para recebimento de alertas de Recall</h2>
                </div>

                <?php if ($this->session->flashdata('success')) : ?>
                    <div class="flash-messages">
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    </div>
                <?php elseif ($this->session->flashdata('error')) : ?>
                    <div class="flash-messages">
                        <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    </div>
                <?php endif; ?>

                <p>Seja bem-vindo!</p>
                <p>Tendo em vista que o objetivo do recall é proteger o consumidor de acidentes ocasionados por defeitos, 
                    um dos aspectos mais relevantes é a ampla e correta divulgação dos avisos de risco de acidente na mídia (jornal, rádio e televisão), 
                    com informações claras e precisas quanto ao objeto do recall, descrição do defeito e riscos, 
                    além das medidas preventivas e corretivas que o consumidor deve tomar. 
                    Daí a importância do recall para evitar ou minorar os acidentes de consumo.</p>
                <p>Para receber os avisos de risco de acidente em seu e-mail, por favor preencha as informações abaixo.</p>
                <p>Caso já esteja cadastrado em nosso sistema, 
                    <a href="<?php echo base_url().'principal/viewSubscriber';?>">Clique aqui</a> para visualizar suas solicitações de recall.
                </p>
                <div class="no-margin">
                    <form name="newsletter-recall" id="newsletter-recall" method="post" action="" class="form-horizontal form-group">
                        <fieldset>
                            <legend class="hide">Dados para cadastro</legend>
                            <table id="newsletterForm" class='col-md-12 tbSearch'>
                                <tr>
                                    <td>
                                        <?php echo form_label('Nome', 'name', array('class' => 'col-md-4')); ?>
                                    </td>
                                    <td>
                                       <?php
                                           $name = array(
                                               'name'      => 'name',
                                               'id'        => 'name',
                                               'value'     => set_value('name'),
                                               'class'     => 'form-control input-xlarge',
                                               'maxlength' => 80,
                                           );
                                           echo form_input($name);
                                       ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo form_label('E-mail', 'email', array('class' => 'col-md-4')); ?>
                                    </td>
                                    <td>
                                       <?php
                                           $email = array(
                                               'name'      => 'email',
                                               'id'        => 'email',
                                               'value'     => set_value('email'),
                                               'class'     => 'form-control input-xlarge',
                                               'maxlength' => 80,
                                           );
                                           echo form_input($email);
                                       ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo form_label('Setor de Atividade', 'branch', array('class' => 'col-md-4 ')); ?>
                                    </td>
                                    <td>
                                        <?php echo form_dropdown('branch_id', $comboBranch, set_value('branch_id'), 'class="form-control input-xlarge" id="branch_id"'); ?>
                                        <span style="font-size: 11px; color: #0088cc">
                                            Selecionar todos os Setores? 
                                        </span>
                                        <input type="checkbox" name="all" id="all_alert"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo form_label('Tipo de Produto', 'type_product', array('class' => 'col-md-4')); ?>
                                    </td>
                                    <td>
                                        <?php echo form_dropdown('type_product_id', $type_product, set_value('type_product_id'), 'class="form-control input-xlarge" id="type_product_id"'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <a id="add_row_subscriber" class="btn btn-default btn-small" style="margin-top: 10px;">
                                            <span class="glyphicon glyphicon-plus-sign"></span> Adicionar
                                        </a>
                                        <button id="reset" type="reset" class="btn btn-default btn-small" style="margin-top: 10px;">
                                            <span class="glyphicon glyphicon-plus-sign"></span> Limpar
                                        </button>
                                    </td>
                                </tr>
                            </table>

                            <div class="clear"></div>

                            <div id="sendSubscriber" style="margin-top: 30px; display: none;">
                                <p class="label label-default" style="text-align: center; display: block; margin: auto">
                                    Solicitações cadastradas por: <span id="name_"></span>
                                </p>
                                <table class="table table-bordered table-hover col-md-12" id="tab_subscriber">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Setor de Atividade</th>
                                            <th class="text-center">Tipo de Produto</th>
                                            <th class="text-center">Excluir</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" id="somaCusto" style="text-align:center">
                                                <span style="font-size: 11px; padding-right: 10px;">
                                                    E-mail para envio de alertas de recall: 
                                                </span>
                                                <strong id="email_"></strong>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr id="subscriber_0">
                                            <td colspan="3">Nenhum plano de mídia cadastrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button class="btn btn-success btn-small" type="button"
                                        style="margin: auto; display: block;" onclick="enviar()">
                                    Salvar Solicitação
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <span class="hide">Fim do conteúdo da página</span>							
    </section>
</div>