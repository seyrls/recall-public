<style>
    table.tbHeader{
        width: 100%;
        margin-bottom: 20px;
    }
    table.tbHeader thead, table.tbHeader2 thead{
        background-color: #eee;
        border: 1px solid #ccc;
    }
    table.tbHeader thead tr th{
        text-align: center;
        border: 1px solid #ccc;
        padding: 5px;
    }
    table.tbHeader thead tr th:last-child{
        width: 200px;
    }
    table.tbHeader tr td{
        padding: 5px;
        text-align: center;
        border: 1px solid #ccc;
    }
</style>
<div id="content" class="span9">
    <section id="content-section">							
        <span class="hide">Início do conteúdo da página</span>
        <div class="row-fluid">
            <div class="span12 module">
                <div class="outstanding-header">
                    <h2 class="outstanding-title">Dados Abertos - Campanhas de Recall</h2>
                </div>
                
                    Os dados abertos correspondentes às campanhas de recall poderão 
                    ser visualizados nos formatos: XLS; HTML; XML e JSON. 
                
                <h5>Quais dados estão disponíveis?</h5> 
                
                    A Senacon mantém registro de todos as publicações de campanhas de recall.
                    Abaixo uma lista dos dados disponíveis por essa API: <br/><br/>
                    <ul>
                        <li style="display: list-item; list-style: disc;">
                            Data de início da campanha
                        </li>
                        <li style="display: list-item; list-style: disc;">
                            Título da campanha
                        </li>
                        <li style="display: list-item; list-style: disc;">
                            Nome da empresa do fornecedor
                        </li>
                        <li style="display: list-item; list-style: disc;">
                            Tipo de risco
                        </li>
                        <li style="display: list-item; list-style: disc;">
                            País de origem
                        </li>
                        <li style="display: list-item; list-style: disc;">
                            Nome do(s) produto(s)
                        </li>
                        <li style="display: list-item; list-style: disc;">
                            Total de produtos afetados
                        </li>
                        <li style="display: list-item; list-style: disc;">
                            Total de produtos atendidos
                        </li>
                    </ul>
                    Para visualizá-los, clique sobre o formato desejado referente
                    ao ano de publicação da campanha interessada.
                    <br/><br/>
                    
                <table class="tbHeader">
                    <thead>
                        <tr>
                            <th>
                                Ano da publicação
                            </th>
                            <th>
                                Formatos
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if ($year) : 
                                foreach ($year as $value) : 
                                    $year = $value['year'];
                        ?>
                                    <tr>
                                        <td>
                                            Campanhas de Recall publicadas em <?php echo $year; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('api/rest/recalls/format/csv/'.$year); ?>">
                                                <img src="<?php echo base_url();?>img/i_csv.png" 
                                                     title="Formato CSV" style="width: 24px"/>
                                            </a>
                                            <a href="<?php echo site_url('api/rest/recalls/format/html/'.$year); ?>">
                                                <img src="<?php echo base_url();?>img/i_html.png"
                                                     title="Formato HTML" style="width: 24px"/>
                                            </a>
                                            <a href="<?php echo site_url('api/rest/recalls/format/xml/'.$year); ?>">
                                                <img src="<?php echo base_url();?>img/i_xml.png"
                                                     title="Formato XML" style="width: 24px"/>
                                            </a>
                                            <a href="<?php echo site_url('api/rest/recalls/format/json/'.$year); ?>">
                                                <img src="<?php echo base_url();?>img/i_json.png"
                                                     title="Formato JSON" style="width: 24px"/>
                                            </a>
                                        </td>
                                    </tr>
                        <?php
                                endforeach;
                            else :
                        ?>
                                    <tr>
                                        <td colspan="2">
                                            Nenhum dado encontrado.
                                        </td>
                                    </tr>
                        <?php  
                            endif;
                        ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <span class="hide">Fim do conteúdo da página</span>							
    </section>
</div>