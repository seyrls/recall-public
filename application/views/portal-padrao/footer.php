        </div><!-- fim .row-fluid -->
    </div><!-- fim .container -->
</main>
<footer>
        <div class="footer-atalhos">
            <div class="container">
                <div class="pull-right voltar-ao-topo"><a href="#portal-siteactions"><i class="icon-chevron-up"></i>&nbsp;Voltar para o topo</a></div>
            </div>
        </div>
        <div class="container container-menus">
            <div id="footer" class="row footer-menus">
                <span class="hide">Início da navegação de rodapé</span>
                			
                <span class="hide">Fim da navegação de rodapé</span>					
            </div><!-- fim .row -->
        </div><!-- fim .container -->
        
        <div class="footer-logos">
            <div class="container">
                <div id="footer-brasil"></div>  
            </div>				
        </div>
<!--        <div class="footer-ferramenta">
            <div class="container">
                <p>Interface preparada para desenvolvimento com o framework <a href="http://www.codeigniter.com">Codeigniter</a></p>		
            </div>				
        </div>-->
        <div class="footer-atalhos visible-phone">
            <div class="container">
                <span class="hide">Fim do conteúdo da página</span>
                <div class="pull-right voltar-ao-topo">
                    <a href="#portal-siteactions"><i class="icon-chevron-up"></i>&nbsp;Voltar para o topo</a>
                </div>
            </div>
        </div>
    </footer>		
</div>
    <!-- fim div#wrapper -->	
    <!-- scripts principais -->	
    <script src="<?php echo base_url(); ?>js/portal-padrao/jquery.min.js" type="text/javascript"></script><noscript>&nbsp;<!-- item para fins de acessibilidade --></noscript>
    <script src="<?php echo base_url(); ?>js/portal-padrao/jquery-noconflict.js" type="text/javascript"></script><noscript>&nbsp;<!-- item para fins de acessibilidade --></noscript>
    <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script><noscript>&nbsp;<!-- item para fins de acessibilidade --></noscript>
    <script src="<?php echo base_url(); ?>js/portal-padrao/jquery.cookie.js" type="text/javascript"></script><noscript>&nbsp;<!-- item para fins de acessibilidade --></noscript>
    <script src="<?php echo base_url(); ?>js/portal-padrao/template.js" type="text/javascript"></script><noscript>&nbsp;<!-- item para fins de acessibilidade --></noscript>
    <!-- Script do portal Brasil deve ficar preferencialmente no rodape para nao atrasar o carregamento da pagina principal -->
    <script src="http://barra.brasil.gov.br/barra.js?cor=azul" type="text/javascript"></script><noscript>&nbsp;<!-- item para fins de acessibilidade --></noscript>
</body>
</html>