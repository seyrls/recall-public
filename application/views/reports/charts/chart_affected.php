<script type="text/javascript">
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable(
            <?php echo $series_data; ?>
        );
        var options = {
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
      
    $('document').ready(function(){
        $('#close').click(function(){
            window.close();
        })
    });
</script>
    <div class="container-fluid" style="padding: 20px;">
                 
        <div class="col-md-12">
            <div class="row">
                <h4 style="text-align: center">
                    Gráfico de Produtos Afetados
                </h4>
                <p class="alert alert-info">
                    <b> Setor de atividade:  <?php echo $branch; ?></b> <br/>
                    <b> Tipo de produto: <?php echo $type_product; ?></b>
                </p>
            </div>
            
            <div class="col-md-12 row">
                <div id="piechart_3d" style="width: 600px; height: 350px;"></div>
            </div>
            <button id="close" style="float: right" class="btn-sm btn btn-primary">Fechar</button>
        </div>
    </div>
    
    
    