    <script type='text/javascript'>
     google.load('visualization', '1', {'packages': ['geochart']});
     google.setOnLoadCallback(drawMarkersMap);

    function drawMarkersMap() {
        var data = google.visualization.arrayToDataTable(
          <?php echo $series_data; ?>
        );

        var options = {
          region: 'BR',
          resolution: 'provinces',
          displayMode: 'regions',
          colorAxis: {colors: ['#ffdfdf', 'red']},
          backgroundColor: '#fff',
          datalessRegionColor: '#fff',
          defaultColor: '#fff',
          title: 'Gráfico de campanhas',
        };

        var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    };
    
    $('document').ready(function(){
       $('#close').click(function(){
           window.close();
       })
    });
    </script>
    
    <div class="container-fluid" style="padding: 20px;">
                 
        <div class="col-md-12">
            <div class="row">
                <h4 style="text-align: center">
                    Mapa de calor dos produtos afetados/atendidos
                </h4>
                <p class="alert alert-info">
                    <b>Campanha: <?php echo $title; ?></b> <br/>
                    <b>Produtos Afetados: &nbsp;</b> 
                    <span class="label label-danger"><?php echo $totalAffected; ?></span> <br/>
                    <b>Produtos Atendidos: </b> 
                    <span class="label label-danger"><?php echo $totalServiced; ?></span>
                </p>
            </div>
            <div class="col-md-12 row">
                <div id="chart_div" class="col-md-8" style='width: 650px;  height: 400px;'></div>
            </div>
            <button id="close" style="float: right" class="btn-sm btn btn-primary">Fechar</button>
        </div>
    </div>