<script type="text/javascript">
    google.load("visualization", "1.1", {packages:["bar"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable(
                <?php echo $series_data; ?>);

        var options = {
          chart: {
            //title: 'Campanhas de Recall',
            //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          },
          bars: 'vertical'
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_material'));

        chart.draw(data, options);
    }
    $('document').ready(function(){
        $('#close').click(function(){
            window.close();
        })
    });
</script>
    
    <div class="container-fluid" style="padding: 20px;">
                 
        <div class="col-md-12">
            <div class="row">
                <h4 style="text-align: center">
                    Gráfico de Índice de atendimento
                </h4>
                <p class="alert alert-info">
                    <b>Fornecedor: <?php echo $corporate_name; ?></b>
                </p>
            </div>
            <div class="col-md-12 row">
                <div id="barchart_material" style="width: 800px; height: 450px;"></div>
            </div>
            <button id="close" style="float: right" class="btn-sm btn btn-primary">Fechar</button>
        </div>
    </div>
    
    
 