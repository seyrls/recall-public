<script type="text/javascript">

$(document).ready(function () {
    $('#report-risk').dataTable();
    // combo fornecedor
    $("select[name='branch_id']").change(function(){
        var branch_id = $("select[name='branch_id'] option:selected").attr('value');
        if (branch_id.length > 0) {
            var urlpath = '<?php echo base_url();?>';
            $("#type_product_id").html("<option value=''>Todos</option>");
            $.ajax({
                url: urlpath+'type_products/loadCombo/',
                type:'POST',
                data: 'branch_id='+branch_id,
                dataType: 'json',
                success: function( data ) {
                    $.each(data, function(i, value) {
                        $('#type_product_id').append($('<option/>', {
                            value: i,
                            text : value 
                        }));
                    });
                }
            });
        }
    });
});

function popup(supplier_id) {

    // backup as 16:00
    var urlpath = $('body').data('baseurl');
    var url = urlpath+'reports/chartRisk/'+supplier_id;
    
    var w = 600;
    var h = 500;
    var left = Number((screen.width/2)-(w/2));
    var tops = Number((screen.height/2)-(h/2));
    window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
}

</script>
<?php
    $attributes = array('class' => 'formRecall', 'id' => 'formRecall');
    echo form_open('reports/risk', $attributes);
?>


<div class="col-md-12">
    <div class="dvformulario row">
        <div class="title-form">
            <h4>Relatório de tipo de risco</h4>
        </div>
        <div class="col-md-9 row">
            <div class="form-group">
                <?php 
                    echo form_label('Fornecedor', 'trade_name', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    echo form_dropdown('supplier_id', $comboSupplier, set_value('supplier_id'), 'class="form-control"');
                    echo form_error('supplier_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
            </div>
        </div>
        <div class="col-md-3 alert alert-info" role="alert">
            Este relatório apresenta os tipos de risco do fornecedor. <br/>
        </div>
        <div class="clear"></div>

        <div style="text-align: center">
            <input type="submit" class="btn btn-primary" value="Gerar Relatório"/>
        </div>
    </div>

<?php
    echo form_close(); 
    if ($this->input->post()) :
?>
    
    <div class="row topo">
        <div class="topo-title">
            <h5> Resultado da pesquisa</h5>
        </div>
        <table class="table table-striped table-bordered tbCenter"
               id="report-risk" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Fornecedor</th>
                    <th>Tipo de Risco</th>
                    <th>Quantidade</th>
                    <th style="width: 60px">Detalhar</th>
                </tr>
            </thead>
            <tbody>
            <?php  
                $total = 0;
                if ($dados) :
                    foreach ($dados as $d) :
                        if (isset($d['total_risk'])) {
                            $total += ($d['total_risk']);
                        }
                        $supplier_id = $d['supplier_id'];
                ?>
                        <tr>
                            <td><?php echo $d['trade_name'];?></td>
                            <td><?php echo $d['type_risk'];?></td>
                            <td><?php echo $d['total_risk'];?></td>
                            <td>
                                <a class="btn btn-sm btn-default" href='javascript:void(0)' onclick="popup(<?php echo $supplier_id; ?>)">
                                    <img src="<?php echo base_url()."img/i_pie_chart.png";?>" 
                                         width='20px' title='Detalhar' style='text-align: center'/>
                                </a>
                            </td>
                        </tr>

                <?php 
                    endforeach; 
                else:
            ?>
                    <tr>
                        <td>Nenhum registro encontrado</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
            <?php endif; ?>
            </tbody>
            <tfoot>
                <tr style="background-color: #ccc;">
                    <td><b>Total:</b> </td>
                    <td></td>
                    <td><?php echo $total; ?></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>
<?php 
    endif;
?>
</div>