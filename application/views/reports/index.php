<nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                </button>
<!--                <a class="navbar-brand" href="javascript:void(0);">Relatórios</a>-->
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo base_url(); ?>reports/recall">Campanhas de Recall</a></li>
                    <li><a href="<?php echo base_url(); ?>reports/serviced">Índice de Atendimento</a></li>
                    <li><a href="<?php echo base_url(); ?>reports/affected">Produtos Afetados</a></li>
                    <li><a href="<?php echo base_url(); ?>reports/risk">Tipos de Risco</a></li>
                </ul>
        </div>
</nav>