<script type="text/javascript">
$(document).ready(function(){
    $('#report-serviced').dataTable();
});

function popup(supplier_id) {
    var urlpath = $('body').data('baseurl');
    var url = urlpath+'reports/chartServiced/'+supplier_id;

    var w = 900;
    var h = 650;
    var left = Number((screen.width/2)-(w/2));
    var tops = Number((screen.height/2)-(h/2));
    window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
}

function serviceds(supplier_id,recall_id) {
    var supplier_id = 6;
    var recall_id = 10;
    window.location='<?php echo base_url().'serviceds/lists/';?>'+supplier_id+'/'+recall_id;
}

</script>

<?php
    $attributes = array('class' => 'formServiced', 'id' => 'formServiced');
    echo form_open('reports/serviced', $attributes);
?>

<div class="col-md-12">
    <div class="dvformulario row">
        <div class="title-form">
            <h4>Relatório de índice de atendimento</h4>
        </div>
        <div class="col-md-9 row">
            <div class="form-group">
                <?php 
                    echo form_label('Fornecedor', 'trade_name', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    echo form_dropdown('supplier_id', $comboSupplier, set_value('supplier_id'), 'class="form-control"');
                    echo form_error('supplier_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
            </div>
        </div>
        <div class="col-md-3 alert alert-info" role="alert">
            <i class="fa fa-info-circle"></i>  
            Este relatório apresenta o índice de atendimento de cada fornecedor,
            cuja o status da campanha é: <b>"Publicada"</b>, <b>"Publicada com Ressalva"</b> ou  <b>"Finalizada"</b>.
        </div>
        <div class="clear"></div>

        <div style="text-align: center">
            <input type="submit" class="btn btn-primary" value="Gerar Relatório"/>
        </div>
    </div>

<?php 
    echo form_close();
    
    if ($this->input->post()) :
?>
    <div class="row topo">
        <div class="topo-title">
            <h5> Resultado da pesquisa</h5>
        </div>
        <table class="table table-striped table-bordered" id="report-serviced" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Fornecedor</th>
                    <th>Campanha(s)</th>
                    <th>Quantidade Afetada</th>
                    <th>Quantidade Atendida</th>
                    <th>Eficiência</th>
                    <th style="width: 60px">Detalhar</th>
                </tr>
            </thead>
            <tbody>
                
                <?php 
                if ($dados) : 
                    foreach ($dados as $dado) :
                    
                        $supplier_id = $dado['supplier_id'];
                        
                        $qtd_affected = (int)$dado['total_affected'];
                        $qtd_serviced = (int)$dado['total_serviced'];
                    
                        $percent = ($qtd_affected > 0) ? (($qtd_serviced*100)/$qtd_affected) : 0;
                        $progress = round($percent, 2).'%';
                        if (substr($progress,0,-1) == 100) {
                            $labelAtendimento = 'label-success';
                        } else{
                            $labelAtendimento = 'label-danger';
                        }
                ?>
                    <tr>
                        <td><?php echo $dado['trade_name']; ?></td>
                        <td><?php echo $dado['recall']; ?></td>
                        <td><?php echo $qtd_affected; ?></td>
                        <td><?php echo $qtd_serviced; ?></td>
                        <td>
                            <a href='javascript:void(0)' onclick='serviceds(1,2)'>
                                <span class="label <?php echo $labelAtendimento; ?>">
                                    <?php echo $progress; ?>
                                </span>
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-sm btn-default" href='javascript:void(0)' onclick="popup(<?php echo $supplier_id; ?>)">
                                <img src="<?php echo base_url()."img/i_pie_chart.png";?>" 
                                     width='20px' title='Detalhar' style='text-align: center'/>
                            </a>
                        </td>
                    </tr>
            <?php 
                    endforeach;
                else :
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
            <?php 
                endif; 
            ?>
            </tbody>
        </table>
    </div>
<?php 
    endif;
?>
</div>

