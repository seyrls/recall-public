<script type="text/javascript">
$(document).ready(function(){
    
    $('#report-recall').dataTable();
    
    $("input[name='protocol']").mask("99999.999999/9999-99");
    $("input[name='start_date']").mask("99/99/9999");
    $("input[name='end_date']").mask("99/99/9999");
    
    $("select[name='branch_id']").change(function(){
        var branch_id = $("select[name='branch_id'] option:selected").attr('value');
        if (branch_id.length > 0) {
            var urlpath = '<?php echo base_url();?>';
            $("#type_product_id").html("<option value=''>Todos</option>");
            $.ajax({
                url: urlpath+'type_products/loadCombo/',
                type:'POST',
                data: 'branch_id='+branch_id,
                dataType: 'json',
                success: function( data ) {
                    $.each(data, function(i, value) {
                        $('#type_product_id').append($('<option/>', {
                            value: i,
                            text : value 
                        }));
                    });
                }
            });
        }
    });
});

function popup(recall_id) {
    var urlpath = $('body').data('baseurl');
    var url = urlpath+'reports/chartRecall/'+recall_id;

    var w = 700;
    var h = 670;
    var left = Number((screen.width/2)-(w/2));
    var tops = Number((screen.height/2)-(h/2));
    window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
}

function serviceds(supplier_id,recall_id) {
    window.location='<?php echo base_url().'serviceds/lists/';?>'+supplier_id+'/'+recall_id;
}

function viewRecall(id) {
    $("#formRecall_"+id).submit();
}

</script>

<?php
    $attributes = array('class' => 'formRecall', 'id' => 'formRecall');
    echo form_open('reports/recall', $attributes);
?>

<div class="col-md-12">
    <div class="dvformulario row">
        <div class="title-form">
            <h4>Relatório de campanhas de recall</h4>
        </div>
        <div class="col-md-9 row">
            <div class="form-group">
                <?php 
                    echo form_label('Período', 'start_date', array('class' => 'col-md-4 control-label'));
                    echo "<div style='float:left; padding: 10px 0px 0px 10px;'> De: </div>";
                    echo "<div class='col-md-3'>";
                    $start_date = array(
                        'name'      => 'start_date',
                        'id'        => 'start_date',
                        'value'     => set_value('start_date'),
                        'class'     => 'form-control input-md calendario',
                    );
                    echo form_input($start_date);
                    echo "</div>";
                    
                    echo "<div style='float:left; padding: 10px 0px 0px 10px;'> Até: </div>";
                    echo "<div class='col-md-3'>";
                    $end_date = array(
                        'name'      => 'end_date',
                        'id'        => 'end_date',
                        'value'     => set_value('end_date'),
                        'class'     => 'form-control input-md calendario',
                    );
                    echo form_input($end_date);
                    echo "</div>"
                 ?>
            </div>
            <div class="form-group">
                <?php 
                    echo form_label('Protocolo', 'protocol', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-4'>";
                    $attProtocol = array(
                        'name'      => 'protocol',
                        'id'        => 'protocol',
                        'value'     => set_value('protocol'),
                        'class'     => 'form-control input-md',
                    );
                    echo form_input($attProtocol);
                    echo "</div>"
                 ?>
            </div>
            <div class="form-group">
                <?php 
                    echo form_label('Titulo', 'title', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    $attTitle = array(
                        'name'      => 'title',
                        'id'        => 'title',
                        'value'     => set_value('title'),
                        'class'     => 'form-control input-md',
                    );
                    echo form_input($attTitle);
                    echo "</div>"
                 ?>
            </div>
            <div class="form-group">
                <?php 
                    echo form_label('Situação', 'status_campaign', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    echo form_dropdown('status_campaign', $comboStatusCampaign, set_value('status_campaign'), 'class="form-control"');
                    echo form_error('status_campaign', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
            </div>
            <div class="form-group">
                <?php 
                    echo form_label('Fornecedor', 'trade_name', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    echo form_dropdown('supplier_id', $comboSupplier, set_value('supplier_id'), 'class="form-control"');
                    echo form_error('supplier_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
            </div>
            <div class="form-group">
                <?php 
                    echo form_label('País do Produto', 'country', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    echo form_dropdown('country_id', $comboCountry, set_value('country_id'), 'class="form-control"');
                    echo form_error('country_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
            </div>
            <div class="form-group">
                <?php 
                    echo form_label('Tipo de Risco', 'type_risk', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    echo form_dropdown('type_risk_id', $comboRisk, set_value('type_risk_id'), 'class="form-control"');
                    echo form_error('type_risk_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
            </div>
        </div>
        <div class="col-md-3 alert alert-info" role="alert">
            <i class="fa fa-info-circle"></i>  
            Este relatório apresenta todas as campanhas de recall cuja o status da campanha é: 
            <b>"Publicada"</b>, <b>"Publicada com Ressalva"</b> ou <b>"Finalizada"</b>.
        </div>
        <div class="clear"></div>

        <div style="margin-top: 30px; text-align: center">
            <input type="submit" class="btn btn-primary" value="Pesquisar"/>
        </div>
    </div>

<?php
    echo form_close(); 
    if ($this->input->post()) :
?>
    
    <div class="row topo">
        <div class="topo-title">
            <h5> Resultado da pesquisa</h5>
        </div>
        <table class="table table-striped table-bordered tbCenter"
               id="report-recall" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Data de Início</th>
                    <th>Protocolo</th>
                    <th>Situação</th>
                    <th>Fornecedor</th>
                    <th>País do produto</th>
                    <th>Tipo de Risco</th>
                    <th>Índice de Atendimento</th>
                    <th style="width: 60px">Detalhar</th>
                </tr>
            </thead>
            <tbody>
            <?php  
                if ($dados) :
                    foreach ($dados as $d) :
                        $start_date = ($d['start_date']) ?  date('d/m/Y', strtotime($d['start_date'])) : '---';
                        $status = (get_status_campaign($d['status_campaign'])) ? 
                                get_status_campaign($d['status_campaign']) : '';

                        $qtd_affected = ($d['total_affected']) ? $d['total_affected'] : 0;
                        $qtd_serviced = ($d['total_serviced']) ? $d['total_serviced'] : 0;

                        $percent = ($qtd_affected > 0) ? (($qtd_serviced*100)/$qtd_affected) : 0;
                        $progress = round($percent, 2).'%';
                        if (substr($progress,0,-1) == 100) {
                            $labelAtendimento = 'label-success';
                        } else{
                            $labelAtendimento = 'label-danger';
                        }
                ?>
                        <tr>
                            <form method="post" action="<?php echo base_url()."recalls/view";?>" 
                                  id="formRecall_<?php echo $d['recall_id']; ?>" 
                                  name="formRecall_<?php echo $d['recall_id']; ?>">
                                <input type="hidden" name="recall" value="<?php echo $d['recall_id']; ?>"/>
                            <td>
                                <a href="javascript:void(0)" onclick="viewRecall(<?php echo $d['recall_id']; ?>)">
                                    <?php echo $d['title'];?>
                                </a>
                            </td>
                            <td><?php echo $start_date;?></td>
                            <td><?php echo ($d['protocol']) ? $d['protocol'] : '---';?></td>
                            <td>
                                <?php echo $status;?>
                            </td>
                            <td><?php echo $d['trade_name'];?></td>
                            <td><?php echo $d['country'];?></td>
                            <td><?php echo $d['type_risk'];?></td>
                            <td>
                                <?php 
                                    echo $qtd_serviced.' / '. $qtd_affected.'<br/>';
                                
                                        if (($d['status_campaign'] == STATUS_ID_PUBLICADA) || 
                                          ($d['status_campaign'] == STATUS_ID_PUBLICADA_COM_RESSALVA) || 
                                          ($d['status_campaign'] == STATUS_ID_FINALIZADA)
                                        ) : 
                                ?>
                                    <a href='javascript:void(0)' onclick='serviceds(<?php echo $d['supplier_id']; ?>,<?php echo $d['recall_id']; ?>);'>
                                        <span class="label <?php echo $labelAtendimento; ?>">
                                            <?php echo $progress; ?>
                                        </span>
                                    </a>
                                <?php else : ?>
                                        -
                                <?php endif; ?>
                            </td>
                            <td>
                                <a class="btn btn-sm btn-default" href='javascript:void(0)' onclick="popup(<?php echo $d['recall_id']; ?>);">
                                    <img src="<?php echo base_url()."img/i_pie_chart.png";?>" 
                                         width='20px' title='Detalhar' style='text-align: center'/>
                                </a>
                            </td>
                            </form>
                        </tr>

                <?php 
                    endforeach; 
                else:
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
<?php 
    endif;
?>
</div>

