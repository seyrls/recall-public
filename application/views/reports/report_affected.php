<script type="text/javascript">

$(document).ready(function () {
    $('#report-affected').dataTable();
    // combo fornecedor
    $("select[name='branch_id']").change(function(){
        var branch_id = $("select[name='branch_id'] option:selected").attr('value');
        if (branch_id.length > 0) {
            var urlpath = '<?php echo base_url();?>';
            $("#type_product_id").html("<option value=''>Todos</option>");
            $.ajax({
                url: urlpath+'type_products/loadCombo/',
                type:'POST',
                data: 'branch_id='+branch_id,
                dataType: 'json',
                success: function( data ) {
                    $.each(data, function(i, value) {
                        $('#type_product_id').append($('<option/>', {
                            value: i,
                            text : value 
                        }));
                    });
                }
            });
        }
    });
});

function popup(type_product_id) {

    var urlpath = $('body').data('baseurl');
    var url = urlpath+'reports/chartAffected/'+type_product_id;
    
    var w = 650;
    var h = 570;
    var left = Number((screen.width/2)-(w/2));
    var tops = Number((screen.height/2)-(h/2));
    window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
}

</script>
<?php
    $attributes = array('class' => 'formRecall', 'id' => 'formRecall');
    echo form_open('reports/affected', $attributes);
?>


<div class="col-md-12">
    <div class="dvformulario row">
        <div class="title-form">
            <h4>Relatório de produtos afetados por setor de atividade</h4>
        </div>
        <div class="col-md-9 row">
            <div class="form-group">
                <?php 
                    echo form_label('Setor de atividade', 'branch_id', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    echo form_dropdown('branch_id', $comboBranch, set_value('branch_id'), 'class="form-control"');
                    echo form_error('branch_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
            </div>
            <div class="form-group">
                <?php 
                    echo form_label('Tipo de produto', 'type_product_id', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    echo form_dropdown('type_product_id', $comboTypeProduct, set_value('type_product_id'), 'class="form-control" id="type_product_id"');
                    echo form_error('type_product_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
            </div>
        </div>
        <div class="col-md-3 alert alert-info" role="alert">
            <i class="fa fa-info-circle"></i>  
            Este relatório apresenta os setores de atividades, os tipos de produtos e 
            os produtos afetados cuja o status da campanha é: 
            <b>"Publicada"</b>, <b>"Publicada com Ressalva"</b> ou <b>"Finalizada"</b>.
        </div>
        <div class="clear"></div>

        <div style="text-align: center">
            <input type="submit" class="btn btn-primary" value="Gerar Relatório"/>
        </div>
    </div>

<?php
    echo form_close(); 
    if ($this->input->post()) :
?>
    
    <div class="row topo">
        <div class="topo-title">
            <h5> Resultado da pesquisa</h5>
        </div>
        <table class="table table-striped table-bordered tbCenter"
               id="report-affected" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Setor de atividade</th>
                    <th>Tipo de produto</th>
                    <th>Produto(s)</th>
                    <th>Quantidade Afetada</th>
                    <th>Quantidade Atendida</th>
                    <th>Eficiência</th>
                    <th style="width: 60px">Detalhar</th>
                </tr>
            </thead>
            <tbody>
            <?php  
                $total = 0;
                if ($dados) :
                    foreach ($dados as $dado) :
                    
                        $type_product_id = $dado['type_product_id'];
                        $qtd_affected = (int)$dado['total_affected'];
                        $qtd_serviced = (int)$dado['total_serviced'];

                        $percent = ($qtd_affected > 0) ? (($qtd_serviced*100)/$qtd_affected) : 0;
                        $progress = round($percent, 2).'%';
                        if (substr($progress,0,-1) == 100) {
                            $labelAtendimento = 'label-success';
                        } else{
                            $labelAtendimento = 'label-danger';
                        }
                ?>
                        <tr>
                            <td><?php echo $dado['branch'];?></td>
                            <td><?php echo $dado['type_product'];?></td>
                            <td><?php echo $dado['product'];?></td>
                            <td><?php echo $qtd_affected; ?></td>
                            <td><?php echo $qtd_serviced; ?></td>
                            <td>
                                <span class="label <?php echo $labelAtendimento; ?>">
                                    <?php echo $progress; ?>
                                </span>
                            </td>
                            <td>
                                <a class="btn btn-sm btn-default" href='javascript:void(0)' onclick="popup(<?php echo $type_product_id; ?>)">
                                    <img src="<?php echo base_url()."img/i_pie_chart.png";?>" 
                                         width='20px' title='Detalhar' style='text-align: center'/>
                                </a>
                            </td>
                        </tr>

                <?php 
                    endforeach; 
                else:
            ?>
                    <tr>
                        <td>Nenhum registro encontrado</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
<?php 
    endif;
?>
</div>