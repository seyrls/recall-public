<div class="container">
    <form class="form-horizontal" method="post" action="">
      <fieldset>
        <div class="row col-md-9">
                <legend>
                    Cadastro de grupos parceiros
                    <div style="float: right; font-size: 11px; padding-top: 10px;">
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/> 
                    Indica campo obrigatório
                </div>
                </legend>
            </div>
            <div class="clearfix"></div>

         <div class="form-group">
            <?php 
                echo form_label('Grupo', 'name', array('class' => 'col-md-4 control-label'));
                echo "<div class='col-md-5'>";
                $attName = array(
                    'name'      => 'name',
                    'id'        => 'name',
                    'value'     => isset($dados['name']) ? $dados['name'] : set_value('name'),
                    'class'     => 'form-control input-md requiredField',
                );
                echo form_input($attName);
                echo form_error('name', '<div class="error">', '</div>');
                echo "</div>"
             ?>
            <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
        </div>
        <div class="form-group">
            <?php echo form_label('Status', 'status', array('class' => 'col-md-4 control-label')); ?>
            <div class="col-md-3">

                <?php
                    $arrStatus = array(0 => 'Inativo', 1 => 'Ativo');
                    $defaultStatus = isset($dados['status']) ? $dados['status'] : 1;
                    echo form_dropdown('status', $arrStatus, $defaultStatus, 'class="form-control input-small requiredField" id="status"'); 
                ?>
            </div>
            <img src="<?php echo base_url();?>img/required.gif"/>
        </div>
        <div style="margin: 30px auto; text-align: center">
            <a class='btn btn-default' href="<?php echo base_url()."groups/index/";?>">
                 <span class="glyphicon glyphicon-chevron-left"></span> Voltar
            </a>
            <button id="save" name="save" class="btn btn-success">Salvar</button>
        </div>

      </fieldset>
    </form>
</div>
