<script>
$(document).ready(function() {
    $('#example').dataTable();
});
function excluir(id,nome) {
    if (confirm('Deseja excluir o grupo '+nome+'?')) {
        window.location='<?php echo base_url().'groups/remove/';?>'+id;
    }
}
</script>
<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Grupos Parceiros</h5>
    </div>
    <table class="table table-striped table-bordered" id="example" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 60px">Ação</th>
                <th>Grupo</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if ($dados) :
                foreach ($dados as $dado) :
                    $id = $dado['group_id'];
                    $name = $dado['name'];
                    if ($dado['status'] == STATUS_ATIVO) {
                        $status = '<span class="label label-success">Ativo</span>';
                    } else {
                        $status = '<span class="label label-danger">Inativo</span>';
                    }
                ?>  
                    <tr>
                        <td>
                            <?php /*
                             
                                <a class="btn btn-sm btn-default" href='<?php echo base_url()."groups/view/".$id; ?>'>
                                    <img src="<?php echo base_url()."img/fi-magnifying-glass.svg";?>" width='14px' title='Visualuzar' style='text-align: center'/>
                                </a>
                             */ ?>
                            <a class="btn btn-sm btn-default" href='<?php echo base_url()."groups/edit/".$id; ?>'>
                                <img src="<?php echo base_url()."img/fi-page-edit.svg";?>" width='14px' title='Editar' style='text-align: center'/>
                            </a>
                            <a class="btn btn-sm btn-default" href='javascript:void(o)' onClick="excluir(<?php echo $id.",'".$name."'"; ?>)">
                                <img src="<?php echo base_url()."img/fi-x.svg";?>" width='14px' title='Excluir' style='text-align: center'/>
                            </a>
                        </td>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $status; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>
<div style="margin: 30px auto; text-align: center">
    <a class='btn btn-primary' href="<?php echo base_url(); ?>groups/insert">
        Inserir Grupo
    </a>
</div>