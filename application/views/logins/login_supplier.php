<script>
    function salvar(){
        $("#login" ).submit();
    }
    $(document).ready(function(){
        document.onkeydown=function(){
            if(window.event.keyCode=='13'){
                $("#login" ).submit();
            }
        }
    })
</script>

<div id="content" class="span9">
    <section id="content-section">							
        <span class="hide">Início do conteúdo da página</span>
        <div class="row-fluid">
            <div class="span12 module">
                <div class="outstanding-header">
                    <h2 class="outstanding-title">Acessar Conta - Fornecedor</h2>
                </div>

                <?php if ($this->session->flashdata('success')) : ?>
                    <div class="flash-messages">
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    </div>
                <?php elseif ($this->session->flashdata('error')) : ?>
                    <div class="flash-messages">
                        <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <p>
                    Para acessar o sistema de Recall como fornecedor, 
                    informe o seu e-mail e senha nos campos abaixo.
                </p>
                <div class="no-margin">
                    <form name="login" id="login" method="post" action="" class="form-horizontal form-group">
                        <fieldset>
                            <legend class="hide">Dados para cadastro de senha</legend>
                            <table id="newsletterForm" class='col-md-12 tbSearch'>
                                <tr>
                                    <td>
                                        <?php echo form_label('E-mail', 'email', array('class' => 'col-md-4')); ?>
                                    </td>
                                    <td>
                                       <?php
                                           $email = array(
                                               'name'      => 'email',
                                               'id'        => 'email',
                                               'value'     => set_value('email'),
                                               'class'     => 'form-control input-xlarge',
                                           );
                                           echo form_input($email);
                                       ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo form_label('Senha', 'password', array('class' => 'col-md-4')); ?>
                                    </td>
                                    <td>
                                       <?php
                                           $password = array(
                                               'name'      => 'password',
                                               'id'        => 'password',
                                               'value'     => set_value('password'),
                                               'class'     => 'form-control input-xlarge',
                                           );
                                           echo form_password($password);
                                       ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="font-size: 11px;">
                                        <a href="<?php echo base_url('logins/forgotPassword'); ?>">
                                            Esqueci minha senha
                                        </a>
                                    </td>
                                </tr>
                            </table>
                            <div class="clear" style="margin-top: 20px">
                                <button class="btn btn-primary btn-small" type="button"
                                        style="margin: auto; display: block;" onclick="salvar()">
                                        Acessar Minha Conta
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <span class="hide">Fim do conteúdo da página</span>							
    </section>
</div>