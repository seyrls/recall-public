<div id="content" class="span9">
                <section id="content-section">							
                    <span class="hide">Início do conteúdo da página</span>
                    <div class="row-fluid">
                        <div class="span12 module">
                            <div class="outstanding-header">
                                <h2 class="outstanding-title">Recuperar senha</h2>
                            </div>
                            
                            <?php if ($this->session->flashdata('success')) : ?>
                                <div class="flash-messages">
                                    <div class="alert alert-success">
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                </div>
                            <?php elseif ($this->session->flashdata('error')) : ?>
                                <div class="flash-messages">
                                    <div class="alert alert-danger">
                                        <?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            
                            <p>
                                Para alterar a senha, é necessário informar seu e-mail no campo abaixo.
                                Nós enviaremos uma nova senha temporária no seu e-mail para acessar o sistema novamente.
                            </p>
                            
                            <div class="no-margin">
                                <form name="forgot-password" method="post" action="" class="form-horizontal form-group">
                                    <fieldset>
                                        <legend class="hide">Dados para cadastro</legend>
                                        <table class='col-md-12 tbSearch'>
                                            <tr>
                                                <td>
                                                    <?php echo form_label('E-mail', 'email', array('class' => 'col-md-4')); ?>
                                                </td>
                                                <td>
                                                   <?php
                                                       $email = array(
                                                           'name'      => 'email',
                                                           'id'        => 'email',
                                                           'value'     => set_value('email'),
                                                           'class'     => 'form-control input-xlarge',
                                                       );
                                                       echo form_input($email);
                                                       echo form_error('email', '<div class="display-error">', '</div>');
                                                   ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <button class="btn btn-success btn-small searchButton" type="submit">
                                                        <i class="icon-search"></i> Recuperar Senha
                                                    </button>
                                                    <a class='btn btn-default btn-small'
                                                        href="<?php echo base_url();?>logins/accountSupplier">
                                                         <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>                                      
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <span class="hide">Fim do conteúdo da página</span>							
                </section>
            </div>