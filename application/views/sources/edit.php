<div class="container">
    <form class="form-horizontal" method="post" action="">
        <fieldset>
            <div class="row col-md-9">
                <legend>
                    Dados da Mídia
                    <div style="float: right; font-size: 11px; padding-top: 10px;">
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/> 
                    Indica campo obrigatório
                </div>
                </legend>
            </div>
            <div class="clearfix"></div>

            <div class="form-group">
                <?php 
                    echo form_label('Mídia', 'media', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attMedia = array(
                        'name'      => 'media',
                        'id'        => 'media',
                        'value'     => ($dados) ? $dados['media'] : set_value('media'),
                        'class'     => 'form-control input-md',
                        'readOnly'  => TRUE
                    );
                    echo form_input($attMedia);
                    echo form_error('media', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <input type="hidden" name="media_id" value="<?php echo $dados['media_id']; ?>"/>
                <img src="<?php echo base_url();?>img/required.gif"/>
            </div>
            
            <div class="form-group">
                <?php 
                    echo form_label('Fonte', 'source', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attSource = array(
                        'name'      => 'source',
                        'id'        => 'source',
                        'value'     => ($dados) ? $dados['source'] : set_value('source'),
                        'class'     => 'form-control input-md requiredField',
                    );
                    echo form_input($attSource);
                    echo form_error('source', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>
            
            <!-- Text input-->
            <div class="form-group">
                <?php echo form_label('Status', 'status', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-3">
                    <?php
                        $arrStatus = array(1 => 'Ativo',0 => 'Inativo');
                        echo form_dropdown('status', $arrStatus, $dados['status'], 'class="form-control input-small" id="status"'); 
                    ?>
                </div>
            </div>
            
            <input type='hidden' name='source_id' value="<?php echo isset($dados['source_id']) ? $dados['source_id'] : NULL?>">

            <div style="margin: 30px auto; text-align: center">
                <a class='btn btn-default' href="<?php echo base_url()."medias/view/".$dados['media_id'];?>">
                     <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                </a>
                <button id="save" name="save" class="btn btn-success">Salvar</button>
            </div>
        </fieldset>
    </form>
</div>
