<script>
$(document).ready(function() {
    $('#example').dataTable();
});

function excluir(id,nome) {
    if (confirm('Deseja excluir o meio de veiculação '+nome+'?')) {
        window.location='<?php echo base_url().'sources/remove/';?>'+id;
    }
}

</script>

<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Fornecedores</h5>
    </div>

    <h3> <a href="<?php echo base_url().'medias/view/'.$source['media_id'];?>">
            <?php echo $source['media']; ?>
        </a> > <?php echo $source['source']; ?></h3>
    <table class="table table-striped table-bordered" id="example" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>ID</th>
                <th>Fonte</th>
                <th>Status</th>
                <th>Opção</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if ($source) :
                foreach ($source as $s) : 
                    $id = $s['source_id'];
                    $nome = $s['source'];
                    $status = ($s['status'] == TRUE) ? '<span class="label label-success">Ativo</span>' : '<span class="label label-danger">Inativo</span>';
            ?>  
                    <tr>
                        <td><a href='<?php echo base_url()."sources/edit/".$id; ?>'><?php echo $id; ?></a></td>
                        <td><?php echo $nome; ?></td>
                        <td><?php echo $status; ?></td>
                        <td>
                            <a href='<?php echo base_url()."sources/edit/".$id; ?>'>Editar</a> |
                            <a href='javascript:void(o)' onClick="excluir(<?php echo $id.",'".$nome."'"; ?>)">Remover</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>