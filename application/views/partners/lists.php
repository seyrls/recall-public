<script>
$(document).ready(function() {
    $('#example').dataTable();
});
function excluir(id,nome) {
    if (confirm('Deseja excluir a entidade '+nome+'?')) {
        window.location='<?php echo base_url().'partners/remove/';?>'+id;
    }
}
</script>
<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Entidades Parceiras</h5>
    </div>
    <table class="table table-striped table-bordered" id="example" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 60px">Ação</th>
                <th>Entidade Parceira</th>
                <th>E-mail(s)</th>
                <th>Grupo</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if (isset($dados)) :
                foreach ($dados as $dado) :
                    $id = $dado['partner_id'];
                    $partner = $dado['partner'];
                    if ($dado['status'] == STATUS_ATIVO) {
                        $status = '<span class="label label-success">Ativo</span>';
                    } else {
                        $status = '<span class="label label-danger">Inativo</span>';
                    }
                ?>  
                    <tr>
                        <td>
                            <a class="btn btn-sm btn-default" href='<?php echo base_url()."partners/edit/".$id; ?>'>
                                <img src="<?php echo base_url()."img/fi-page-edit.svg";?>" width='14px' title='Editar' style='text-align: center'/>
                            </a>
                            <a class="btn btn-sm btn-default" href='javascript:void(o)' onClick="excluir(<?php echo $id.",'".$partner."'"; ?>)">
                                <img src="<?php echo base_url()."img/fi-x.svg";?>" width='14px' title='Excluir' style='text-align: center'/>
                            </a>
                        </td>
                        <td><?php echo $dado['partner']; ?></td>
                        <td><?php echo $dado['email']; ?></td>
                        <td><?php echo $dado['name']; ?></td>
                        <td><?php echo $status; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>
<div style="margin: 30px auto; text-align: center">
    <a class='btn btn-primary' href="<?php echo base_url(); ?>partners/insert">
        Inserir Entidade
    </a>
</div>