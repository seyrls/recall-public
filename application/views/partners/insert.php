<script>
    $(document).ready(function(){
        $("select[name='level']").change(function(){
            var level = $("select[name='level'] option:selected").attr('value');
            var level_supplier = <?php echo LEVEL_FORNECEDOR; ?>;
            if (level == level_supplier) {
                $('#divSupplier').show();
            } else {
                $('#divSupplier').hide();
            }
	});
        
//        $(".checkGroup").click(function() {
//            if ($(this).prop("checked")) {
//                $("#addGroup").append("<input type='hidden' name='idGroup[]' value='"+$(this).val()+"'/>");
//            }
//        });
        
        // Combo Tipo de Produto
        $("select[name='branch_id']").change(function(){
            var branch_id = $("select[name='branch_id'] option:selected").attr('value');
            if (branch_id.length > 0) {
                var urlpath = '<?php echo base_url();?>';
                $("#type_product_id").html("<option value=''>Selecione</option>");
                $.ajax({
                    url: urlpath+'type_products/loadCombo/',
                    type:'POST',
                    data: 'branch_id='+branch_id,
                    dataType: 'json',
                    success: function( data ) {
                        $.each(data, function(i, value) {
                            $('#type_product_id').append($('<option/>', {
                                value: i,
                                text : value 
                            }));
                        });
                    }
                });
            }
	});
        
    });
</script>
<?php
// verifica se tem grupos marcados
$arrIdGroupSelect = array();
if (($this->form_validation->run('partners') == FALSE) && (array_key_exists('idGroup',$this->input->post()))) {
    $arrIdGroupSelect = $this->input->post('idGroup');
}
?>

<div class="col-md-12">
    <div class="col-md-8">
        <form class="form-horizontal form-group" method="post" action="">
          <fieldset>

              <div class="row col-md-12">
                <legend>
                    Dados da entidade parceira
                    <div style="float: right; font-size: 11px; padding-top: 10px;">
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/> 
                    Indica campo obrigatório
                </div>
                </legend>
            </div>
            <div class="clearfix"></div>

             <div class="form-group">
                <?php 
                    echo form_label('Entidade', 'partner', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-7'>";
                    $attName = array(
                        'name'      => 'partner',
                        'id'        => 'partner',
                        'value'     => isset($dados['partner']) ? $dados['partner'] : set_value('partner'),
                        'class'     => 'form-control input-md requiredField',
                    );
                    echo form_input($attName);
                    echo form_error('partner', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('E-mail', 'email', array('class' => 'col-md-4 control-label labelTextareaSmall'));
                    echo "<div class='col-md-7'>";
                    $attEmail = array(
                        'name'      => 'email',
                        'id'        => 'email',
                        'value'     => isset($dados['email']) ? $dados['email'] : set_value('email'),
                        'class'     => 'form-control input-md requiredField',
                        'rows'      => '4',
                    );
                    echo form_textarea($attEmail);
                    echo form_error('email', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

             <div class="form-group">
                <?php echo form_label('Status', 'status', array('class' => 'col-md-4 control-label')); ?>
                 <div class="col-md-3">
                     <?php
                         $arrStatus = array(0 => 'Inativo', 1 => 'Ativo');
                         $default = isset($dados['status']) ? $dados['status'] : 1;
                         echo form_dropdown('status', $arrStatus, $default, 'class="form-control input-small requiredField" id="status"'); 
                     ?>
                 </div>
                 <img src="<?php echo base_url();?>img/required.gif"/>
            </div>

            <div class="col-md-12">
                <table class="table table-bordered table-hover"
                        style=" margin-top: 30px; margin-left: -20px">
                     <thead>
                         <tr>
                             <th>Grupos Parceiros</th>
                             <th style="text-align: center">Selecionar Grupo</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php 
                             if ($groups): 
                                 if (isset($dados['group_partner_id'])) {
                                     $arrGroupPartner = explode(',',$dados['group_partner_id']);
                                 } else {
                                     $arrGroupPartner = array();
                                 }
                                 foreach ($groups as $group) :
                                     if (!empty($arrIdGroupSelect)) {
                                         $checked = in_array($group['group_id'], $arrIdGroupSelect) ? 'checked=""' : '';
                                     } else {
                                         $checked = in_array($group['group_id'], $arrGroupPartner) ? 'checked=""' : '';
                                     }
                         ?>
                                     <tr>
                                         <td>
                                             <?php echo $group['name']; ?>
                                         </td>
                                         <td style="text-align: center">
                                             <input type="checkbox" class="checkGroup" <?php echo $checked; ?>
                                                    name='idGroup[]' value="<?php echo $group['group_id']; ?>"/>
                                         </td>
                                     </tr>
                         <?php
                                 endforeach;
                             endif;
                         ?>
                     </tbody>
                 </table>
                <div id="addGroup"></div>
            </div>
            <div class="clear">
            </div>

            <div style="margin: 30px auto; text-align: center">
                <a class='btn btn-default' href="<?php echo base_url()."partners/index/";?>">
                     <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                </a>
                <button id="save" name="save" class="btn btn-success">Salvar</button>
            </div>

          </fieldset>
        </form>
    </div>
    <div class="col-md-4 alert alert-info" style="margin-top: 50px;" role="alert">
        <b style="text-align: center; display: block">Nota: </b> 
        Para receber os alertas de recall, é necessário informar o(s) e-mail(s)
        separado(s) por vírgulas. <br/>
        <b>Ex: </b>email1@teste.com<b>,</b> email2@teste.com<b>,</b> email3@teste.com
    </div>
</div>
