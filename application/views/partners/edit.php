<script>
    $(document).ready(function(){
        // Combo Tipo de Produto
        $("select[name='branch_id']").change(function(){
            var branch_id = $("select[name='branch_id'] option:selected").attr('value');
            if (branch_id.length > 0) {
                var urlpath = '<?php echo base_url();?>';
                $("#type_product_id").html("<option value=''>Selecione</option>");
                $.ajax({
                    url: urlpath+'type_products/loadCombo/',
                    type:'POST',
                    data: 'branch_id='+branch_id,
                    dataType: 'json',
                    success: function( data ) {
                        $.each(data, function(i, value) {
                            $('#type_product_id').append($('<option/>', {
                                value: i,
                                text : value 
                            }));
                        });
                    }
                });
            }
	});
        
    });
</script>
<div class="col-md-12">
    <div class="col-md-8">
        <form class="form-horizontal form-group" method="post" action="">
            <fieldset>
            <div class="row col-md-9">
                <legend>
                    Cadastro de entidade parceira
                    <div style="float: right; font-size: 11px; padding-top: 10px;">
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/> 
                    Indica campo obrigatório
                </div>
                </legend>
            </div>
            <div class="clearfix"></div>
                <div class="form-group">
                   <?php 
                       echo form_label('Entidade', 'partner', array('class' => 'col-md-4 control-label'));
                       echo "<div class='col-md-7'>";
                       $attName = array(
                           'name'      => 'partner',
                           'id'        => 'partner',
                           'value'     => isset($dados['partner']) ? $dados['partner'] : set_value('partner'),
                           'class'     => 'form-control input-md requiredField',
                       );
                       echo form_input($attName);
                       echo form_error('name', '<div class="error">', '</div>');
                       echo "</div>"
                    ?>
                   <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
               </div>

               <div class="form-group">
                   <?php 
                       echo form_label('E-mail', 'email', array('class' => 'col-md-4 control-label labelTextareaSmall'));
                       echo "<div class='col-md-7'>";
                       $attEmail = array(
                           'name'      => 'email',
                           'id'        => 'email',
                           'value'     => isset($dados['email']) ? $dados['email'] : set_value('email'),
                           'class'     => 'form-control input-md requiredField',
                           'rows'      => '4',
                       );
                       echo form_textarea($attEmail);
                       echo form_error('email', '<div class="error">', '</div>');
                       echo "</div>"
                    ?>
                   <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
               </div>
            
               <div class="form-group">
                   <?php 
                       echo form_label('Grupo', 'group_name', array('class' => 'col-md-4 control-label'));
                       echo "<div class='col-md-7'>";
                       $default = isset($dados['group_id']) ? $dados['group_id'] : set_value('group_id');
                       echo form_dropdown('group_id', $comboGroup, $default, 'class="form-control"');
                       echo "</div>";
                    ?>
               </div>

                <div class="form-group">
                   <?php echo form_label('Status', 'status', array('class' => 'col-md-4 control-label')); ?>
                    <div class="col-md-3">
                        <?php
                            $arrStatus = array(0 => 'Inativo', 1 => 'Ativo');
                            echo form_dropdown('status', $arrStatus, $dados['status'], 'class="form-control input-small" id="status"'); 
                        ?>
                    </div>
               </div>

               <div style="margin: 30px auto; text-align: center">
                   <a class='btn btn-default' href="<?php echo base_url()."partners/index/";?>">
                        <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                   </a>
                   <button id="save" name="save" class="btn btn-success">Salvar</button>
               </div>
            </fieldset>
        </form>
    </div>
    <div class="col-md-4 alert alert-info" style="margin-top: 50px;" role="alert">
        <b style="text-align: center; display: block">Nota: </b> 
        Para receber os alertas de recall, é necessário informar o(s) e-mail(s)
        separado(s) por vírgulas. <br/>
        <b>Ex: </b>email1@teste.com<b>,</b> email2@teste.com<b>,</b> email3@teste.com
    </div>
</div>