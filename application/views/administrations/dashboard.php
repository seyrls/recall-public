<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load('visualization', '1.0', {'packages':['corechart','bar']});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            
           // chart affected
            var data_affected = google.visualization.arrayToDataTable(
                <?php echo $series_data_affected; ?>
            );
            var options_affected = {
              chart: {
                  title: 'Performance no atendimento das campanhas',
//                  subtitle: 'Produtos Atendidos e Produtos Afetados: 2010-2015',
                },
                bars: 'vertical'
            };
            var chart_affected = new google.charts.Bar(document.getElementById('chart_affected'));
            chart_affected.draw(data_affected, options_affected);
           
        
        // chart status campaign
        var data_status = google.visualization.arrayToDataTable(
            <?php echo $series_data_status; ?>
        );
        var options_status = {
          is3D: true
        };

        var chart_status = new google.visualization.PieChart(document.getElementById('chart_status'));
        chart_status.draw(data_status, options_status);
    }
    $(document).ready(function () {
        $("select[name='supplier_id']").change(function(){
            var supplier_id = $(this).val();
            window.location='<?php echo base_url().'administrations/index/';?>'+supplier_id;
        });
    });
</script>
    
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-12">
                        <div class="col-md-9">
                            <h1 class="page-header">
                                <small> 
                                    Estatísticas de Campanhas de Recall para o fornecedor: 
                                    <b><?php echo $trade_name; ?></b>
                                </small>
                            </h1>
                        </div>
                        <div class="col-md-3" style="margin-top: 20px">
                            <p style="text-align: left">
                                Lista de fornecedores
                                <img src="<?php echo base_url();?>img/help.png" width="12px" style="padding-top: -2px; cursor: pointer"
                                        title="Selecione um fornecedor para ver suas estatísticas."/>
                            </p>
                            <?php
                                $default = ($supplier_id) ? $supplier_id : set_value('supplier_id');
                                echo form_dropdown('supplier_id', $comboSupplier, 
                                $default, 'class="form-control"');
                             ?>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  
                            Os dados das estatísticas referem-se apenas às campanhas
                            de recall que estão com a situação "Publicada",
                            "Publicada com Ressalva" ou "Finalizada".
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-bullhorn fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div style="font-size: 25px; font-weight: bold;">
                                            <?php echo $countRecallPublish; ?>
                                        </div>
                                        <div>Campanhas de Recall Publicadas</div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url().'recalls/lists';?>">
                                <div class="panel-footer">
                                    <span class="pull-left">Detalhar</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-unlock-alt fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div style="font-size: 25px; font-weight: bold;">
                                            <?php echo $countRecallAndamento; ?>
                                        </div>
                                        <div>Campanhas de Recall em Andamento</div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url().'recalls/lists';?>">
                                <div class="panel-footer">
                                    <span class="pull-left">Detalhar</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-map-marker fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div style="font-size: 25px; font-weight: bold;">
                                            <?php echo $totalAffected; ?>
                                        </div>
                                        <div>Produtos Afetados em todo o Brasil</div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url().'reports/affected';?>">
                                <div class="panel-footer">
                                    <span class="pull-left">Detalhar</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-check-square-o fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div style="font-size: 25px; font-weight: bold;">
                                            <?php echo $totalServiced; ?>
                                        </div>
                                        <div>Produtos Atendidos. Percentual: <b><?php echo $indiceServiced; ?></b></div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url().'reports/serviced';?>">
                                <div class="panel-footer">
                                    <span class="pull-left">Detalhar</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading col-md-12">
                                <h3 class="panel-title col-md-9"><i class="fa fa-bar-chart-o fa-fw"></i> 
                                    Situação das campanhas de recall
                                </h3>
                            </div>
                            <div class="clear"></div>
                            <div class="panel-body">
                                 <div id="chart_status" style="height: 350px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading col-md-12">
                                <h3 class="panel-title col-md-9"><i class="fa fa-bar-chart-o fa-fw"></i> 
                                    Produtos afetados/atendidos por ano
                                </h3>
                            </div>
                            <div class="clear"></div>
                            <div class="panel-body">
                                 <div id="chart_affected" style="height: 350px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-4" style="height: 300px">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-fire fa-fw"></i>Tipos de Riscos mais frequentes</h3>
                            </div>
                            <div class="panel-body" style="max-height: 350px; min-height: 240px; overflow-y: scroll;">
                                <div class="list-group">
                                    <?php 
                                        if ($arr_type_risk) : 
                                            foreach ($arr_type_risk as $value) :
                                    ?>
                                                <p class="list-group-item">
                                                    <span class="badge">
                                                        <?php echo $value['total_risk']; ?>
                                                    </span>
                                                    <?php echo $value['type_risk']; ?>
                                                </p>
                                    <?php
                                            endforeach;
                                        else :
                                    ?>
                                                <p class="list-group-item">
                                                    Nenhum ítem encontrado.
                                                </p>
                                    <?php
                                        endif;
                                    ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4" style="height: 300px;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-cogs fa-fw"></i> Setores de atividades mais frequentes</h3>
                            </div>
                            <div class="panel-body" style="max-height: 350px; min-height: 240px; overflow-y: scroll;">
                                <div class="list-group">
                                    <?php 
                                        if ($arr_branch) : 
                                            foreach ($arr_branch as $value) :
                                    ?>
                                                <p class="list-group-item">
                                                    <span class="badge">
                                                        <?php echo $value['total_branch']; ?>
                                                    </span>
                                                    <?php echo $value['branch']; ?>
                                                </p>
                                    <?php
                                            endforeach;
                                        else :
                                    ?>
                                                <p class="list-group-item">
                                                    Nenhum ítem encontrado.
                                                </p>
                                    <?php
                                        endif;
                                    ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4" style="height: 300px">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Valores investidos em planos de mídia</h3>
                            </div>
                            <div class="panel-body" style="max-height: 350px; min-height: 240px; overflow-y: scroll;">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Meio de veiculação</th>
                                                <th>Valor (R$)</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php 
                                                $total = 0;
                                                if ($arr_media) :
                                                    foreach ($arr_media as $value) :
                                                        $total += $value['media_cost_sum'];
                                            ?>
                                                        <tr>
                                                            <td><?php echo $value['origin']; ?></td>
                                                            <td style="text-align: right">
                                                                <?php echo isset($value['media_cost']) ? $value['media_cost'] : 0; ?>
                                                            </td>
                                                        </tr>
                                            <?php
                                                    endforeach;
                                                else :
                                            ?>
                                                        <tr>
                                                            <td>-</td>
                                                            <td>-</td>
                                                        </tr>
                                            <?php
                                                endif;
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr style="background-color: #ddd">
                                                <td><b>Total: </b></td>
                                                <td style="text-align: right">
                                                    <?php echo number_format($total, 2, ',', '.'); ?>
                                                </td>
                                            </tr>
                                        </tfoot>
                                            
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->