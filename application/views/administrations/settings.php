
    <?php
        $attributes = array('class' => 'formSettings', 'id' => 'formSettings');
        echo form_open('administrations/settings', $attributes);
    ?>

    <div class="topo-title-div">
        <h5> Configurações do sistema</h5>
    </div>
    <div class="clear"></div>
    <div class="col-xs-12 dvformulario">
        <div class="col-md-12">
            <div class="form-group">
                <?php 
                    echo form_label('E-mail padrao para envio de alertas', 'email', array('class' => 'col-md-4'));
                    echo "<div class='col-md-5'>";
                    $email = array(
                        'name'      => 'email',
                        'id'        => 'email',
                        'value'     => ($email) ? $email : set_value('email'),
                        'class'     => 'form-control input-xlarge',
                    );
                    echo form_input($email);
                    echo form_error('password', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
            </div>
            <div class="form-group">
                <?php 
                    echo form_label('Usuário', 'user', array('class' => 'col-md-4'));
                    echo "<div class='col-md-5'>";
                    $user = array(
                        'name'      => 'password',
                        'id'        => 'password',
                        'value'     => ($user) ? $user : set_value('user'),
                        'class'     => 'form-control input-xlarge',
                    );
                    echo form_input($user);
                    echo form_error('user', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
            </div>
            <div class="form-group">
                <?php 
                    echo form_label('Senha', 'password', array('class' => 'col-md-4'));
                    echo "<div class='col-md-5'>";
                    $password = array(
                        'name'      => 'password',
                        'id'        => 'password',
                        'value'     => set_value('password'),
                        'class'     => 'form-control input-large',
                    );
                    echo form_password($password);
                    echo form_error('password_new', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
            </div>
        </div>
        <div style="margin: 30px auto; text-align: center">
            <a class='btn btn-success' href="javascript::void(0)" onclick="alterar()">
                Salvar
            </a>
        </div>
    </div>
<?php echo form_close();