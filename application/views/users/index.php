<?php
    //verifica quantidade total de solicitacoes de fornecedores
    $ci = & get_instance();
    $ci->load->model('Function_model');
    $totalRequest = $ci->Function_model->getTotalSupplierRequest();
    $labelRequest = ($totalRequest > 0) ? 'danger' : 'default';
    
?>
<nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>users/index">Usuários</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo base_url(); ?>suppliers">Listar Fornecedores</a></li>
                <li>
                    <a style="float: left" href="<?php echo base_url(); ?>suppliers/requestLists">Listar Solicitações de Cadastro</a>
                    <span style="margin: 15px auto auto -10px; display: block; float: right" class="label label-<?php echo $labelRequest; ?>">
                        <?php echo $totalRequest; ?>
                    </span>
                </li>
            </ul>
        </div>
</nav>