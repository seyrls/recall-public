<?php  
    $booSupplier = ($this->uri->segment(3) == 'supplier') ? TRUE : FALSE;
    $action = $this->uri->segment(2);
    $habilita = ($action == 'insert') ? FALSE : TRUE;
    if (
            $booSupplier || 
            $this->input->post('level_id') == LEVEL_FORNECEDOR ||
            (isset($dados['level']) && $dados['level'] == LEVEL_FORNECEDOR))
    {
        $habilita = TRUE;
    } else {
        $habilita = FALSE;
    }
    
    $city_id = ($this->input->post('city_id')) ? $this->input->post('city_id') : NULL;
    $defaultCity = isset($dados['supplier']['city_id']) ? $dados['supplier']['city_id'] : $city_id;
    
?>

<script>
    $(document).ready(function(){
        <?php if ($habilita || $booSupplier) : ?>
            $('#divSupplier').show();
        <?php else : ?>
            $('#divSupplier').hide();    
        <?php endif; ?>
            
        $("select[name='level_id']").change(function(){
            var level = $("select[name='level_id'] option:selected").attr('value');
            var level_supplier = <?php echo LEVEL_FORNECEDOR; ?>;
            if (level == level_supplier) {
                $('#divSupplier').show();
            } else {
                $('#divSupplier').hide();
            }
    });
        
        $("#cnpj").mask("99.999.999/9999-99");
        $("#zip").mask("99999-999"); 
        $("#commercial_phone").mask("(99) 9999-9999?9");
        $("#fax_phone").mask("(99) 9999-9999?9");
        
        
        $('#status_aguardando').hide();
        $("select[name='status']").change(function(){
            var status = $("select[name='status'] option:selected").attr('value');
            var status_aguardando = <?php echo STATUS_AGUARDANDO; ?>;
            if (status == status_aguardando) {
                $('#status_aguardando').toggle('fast');
            }
    });
        
    })
    
    function salvar() {
        if (confirm('Deseja salvar este usuário? Ao concluir esta operação, será enviada uma senha de acesso para o e-mail cadastrado.')) {
            $('#user').submit();
        }
    }
    function editar() {
        if (confirm('Deseja alterar este usuário?')) {
            $('#user').submit();
        }
    }
</script>



<form class="form-horizontal" method="post" id="user" action="">
    <input name="user_id" type="hidden" 
           value="<?php echo isset($dados['user_id']) ? $dados['user_id'] : NULL;?>">
    
    <input name="supplier_id" type="hidden" 
           value="<?php echo (isset($dados['supplier']['supplier_id'])) ? $dados['supplier']['supplier_id'] : NULL ;?>">
  <fieldset>

    <!-- Form Name -->
    <legend>
            Dados do Usuário
            <div style="float: right; font-size: 11px; padding-top: 10px;">
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/> Indica campo obrigatório
                <img src="<?php echo base_url();?>img/help.png" width="12px" style="margin-left: 10px"/> Indica descrição do campo
            </div>
    </legend>
    
    <div class="form-group">
        <?php 
            echo form_label('Nome', 'username', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            $attUsername = array(
                'name'      => 'username',
                'id'        => 'username',
                'value'     => isset($dados['username']) ? ($dados['username']) : set_value('username'),
                'class'     => 'form-control input-md requiredField',
            );
            echo form_input($attUsername);
            echo form_error('username', '<div class="error">', '</div>');
            echo "</div>"
         ?>
        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
    </div>

    <div class="form-group">
        <?php 
            echo form_label('E-mail', 'email', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            $attEmail = array(
                'name'      => 'email',
                'id'        => 'email',
                'value'     => (isset($dados['email']) && !empty($dados['email'])) ? ($dados['email']) : set_value('email'),
                'class'     => 'form-control input-md requiredField',
            );
            echo form_input($attEmail);
            echo form_error('email', '<div class="error">', '</div>');
            echo "</div>"
         ?>
        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
    </div>
    
    <?php 
        $dadosUser = $this->session->userdata('login'); 
        if ($dadosUser['level'] != LEVEL_FORNECEDOR) : 
    ?>
            <div class="form-group">
                <?php 
                    echo form_label('Perfil', 'name_level', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-3'>";
                    $readonly = $booSupplier ? 'readonly' : '';
                    if ($booSupplier) {
                        $default = LEVEL_FORNECEDOR;
                    } else {
                        $default = (isset($dados['level'])) ? $dados['level'] : set_value('level_id');
                    }
                    echo form_dropdown('level_id', $comboLevel,$default , 'class="form-control requiredField"'.$readonly);
                    echo form_error('level_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>
            <div class="form-group">
                <?php echo form_label('Status', 'status', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-3">
                    <?php
                        $defaultStatus = isset($dados['status']) ? $dados['status'] : 1;
                        echo form_dropdown('status', $comboStatus, $defaultStatus, 'class="form-control input-small requiredField" id="status"'); 
                    ?>
                </div>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                <img src="<?php echo base_url();?>img/help.png" width="12px"
                         title="Apenas os usuários ativos poderão cadastrar uma campanha"/>
                
                <div class="col-md-4 alert alert-info floatRight" id="status_aguardando" role="alert">
                    <b style="text-align: center; display: block">Atenção: </b> 
                    Será enviado uma nova senha aleatória para o e-mail deste usuário,
                    caso seja selecionado a opção de status: Pendente
                </div>
            </div>
    <?php else : ?>
        <input type="hidden" name="level_id" value="<?php echo LEVEL_FORNECEDOR;?>"/>
        <input type="hidden" name="status" value="<?php echo STATUS_ATIVO;?>"/>
    <?php endif; ?>
    
        <div id="divSupplier">

            <legend>Dados da Empresa</legend>

            <div class="form-group">
                <?php 
                    echo form_label('Razão Social', 'corporate_name', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attCorporate = array(
                        'name'      => 'corporate_name',
                        'id'        => 'corporate_name',
                        'value'     => isset($dados['supplier']['corporate_name']) ? ($dados['supplier']['corporate_name']) : set_value('corporate_name'),
                        'class'     => 'form-control input-md requiredField',
                    );
                    echo form_input($attCorporate);
                    echo form_error('corporate_name', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('Nome Fantasia', 'trade_name', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attTrade = array(
                        'name'      => 'trade_name',
                        'id'        => 'trade_name',
                        'value'     => isset($dados['supplier']['trade_name']) ? ($dados['supplier']['trade_name']) : set_value('trade_name'),
                        'class'     => 'form-control input-md requiredField',
                    );
                    echo form_input($attTrade);
                    echo form_error('trade_name', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('CNPJ', 'cnpj', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attCnpj = array(
                        'name'      => 'cnpj',
                        'id'        => 'cnpj',
                        'value'     => isset($dados['supplier']['cnpj']) ? ($dados['supplier']['cnpj']) : set_value('cnpj'),
                        'class'     => 'form-control input-md requiredField',
                    );
                    echo form_input($attCnpj);
                    echo form_error('cnpj', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('Inscrição Estadual', 'state_registration', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attState = array(
                        'name'      => 'state_registration',
                        'id'        => 'state_registration',
                        'value'     => isset($dados['supplier']['state_registration']) ? ($dados['supplier']['state_registration']) : set_value('state_registration'),
                        'class'     => 'form-control input-md',
                        'maxlength' => 20,
                    );
                    echo form_input($attState);
                    echo form_error('state_registration', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('Ramo de atividade', 'branch', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $defaultBranch = isset($dados['supplier']['branch_id']) ? $dados['supplier']['branch_id'] : set_value('branch_id');
                    echo form_dropdown('branch_id', $comboBranch, $defaultBranch, 'class="form-control requiredField"');
                    echo form_error('branch_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    <img src="<?php echo base_url();?>img/help.png" width="12px"
                 title="Selecione a atividade relacionada ao fornecedor"/>
            </div>
            
            <div class="form-group">
                <?php 
                    echo form_label('E-mail(s) com Cópia', 'email', array('class' => 'col-md-4 control-label labelTextareaSmall'));
                    echo "<div class='col-md-5'>";
                    $attEmailCopy = array(
                        'name'      => 'email_copy',
                        'id'        => 'email_copy',
                        'value'     => isset($dados['supplier']['email_copy']) ? $dados['supplier']['email_copy'] : set_value('email_copy'),
                        'class'     => 'form-control input-md',
                        'rows'      => '4',
                    );
                    echo form_textarea($attEmailCopy);
                    echo form_error('email_copy', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/help.png" width="12px"
                 title="Informe os e-mails que deseja receber copias das notificações da campanha, separados por vírgula"/>
            </div>

            <legend>Dados do Endereço da Empresa</legend>

            <div class="form-group">
                <?php 
                    echo form_label('Endereço', 'address', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attAddress = array(
                        'name'      => 'address',
                        'id'        => 'address',
                        'value'     => isset($dados['supplier']['address']) ? ($dados['supplier']['address']) : set_value('address'),
                        'class'     => 'form-control input-md requiredField',
                    );
                    echo form_input($attAddress);
                    echo form_error('address', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('Complemento', 'complement', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attComplement = array(
                        'name'      => 'complement',
                        'id'        => 'complement',
                        'value'     => isset($dados['supplier']['complement']) ? ($dados['supplier']['complement']) : set_value('complement'),
                        'class'     => 'form-control input-md',
                    );
                    echo form_input($attComplement);
                    echo form_error('complement', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('Bairro', 'district', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attDistrict = array(
                        'name'      => 'district',
                        'id'        => 'district',
                        'value'     => isset($dados['supplier']['district']) ? ($dados['supplier']['district']) : set_value('district'),
                        'class'     => 'form-control input-md requiredField',
                    );
                    echo form_input($attDistrict);
                    echo form_error('district', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('UF', 'state', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $defaultUf = isset($dados['supplier']['state_id']) ? $dados['supplier']['state_id'] : set_value('state_id');
                    echo form_dropdown('state_id', $comboState, $defaultUf, 'class="form-control requiredField" id="uf" onChange="display(this.value)"');
                    echo form_error('state_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('Município', 'city_id', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5' id='show_city'>";
                    $city = $defaultCity ? $defaultCity : set_value('city_id');
                    echo form_dropdown('city_id', $comboCity, $city, 'class="form-control requiredField" id="city_id"');
                    echo form_error('city_id', '<div class="error">', '</div>');
                    echo "</div>";
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('CEP', 'zip', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attZip = array(
                        'name'      => 'zip',
                        'id'        => 'zip',
                        'value'     => isset($dados['supplier']['zip']) ? ($dados['supplier']['zip']) : set_value('zip'),
                        'class'     => 'form-control input-md requiredField',
                    );
                    echo form_input($attZip);
                    echo form_error('zip', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('Telefone comercial', 'commercial_phone', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attComercialPhone = array(
                        'name'      => 'commercial_phone',
                        'id'        => 'commercial_phone',
                        'value'     => isset($dados['supplier']['commercial_phone']) ? ($dados['supplier']['commercial_phone']) : set_value('commercial_phone'),
                        'class'     => 'form-control input-md requiredField',
                    );
                    echo form_input($attComercialPhone);
                    echo form_error('commercial_phone', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div class="form-group">
                <?php 
                    echo form_label('Fax', 'fax_phone', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attFaxPhone = array(
                        'name'      => 'fax_phone',
                        'id'        => 'fax_phone',
                        'value'     => isset($dados['supplier']['fax_phone']) ? ($dados['supplier']['fax_phone']) : set_value('fax_phone'),
                        'class'     => 'form-control input-md',
                    );
                    echo form_input($attFaxPhone);
                    echo form_error('fax_phone', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
            </div>
        
    </div>
    <div style="margin: 30px auto; text-align: center">
        <a class="btn btn-default" href="javascript:window.history.go(-1)">Voltar</a>
        <input type="button" value="Salvar" class="btn btn-success" 
               onclick="<?php echo ($action == 'insert') ? 'salvar()' : 'editar()';?>"/>
    </div>
    
  </fieldset>
</form>
