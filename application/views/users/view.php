<script>
    $(document).ready(function(){
        <?php if ($this->session->flashdata('error')) : ?>
            $('#newPassword').show();
        <?php else : ?>
            $('#newPassword').hide();
        <?php endif; ?>
        
        $("#changePassword").click(function() {
            $("#newPassword").toggle("fast");
        });
    });
    function alterar(){
        $('#password').submit();
    }
    function ativar(id,nome) {
        if (confirm('Deseja Ativar o fornecedor '+nome+'? Será enviado uma senha aleatória para acesso ao sistema.')) {
            window.location='<?php echo base_url().'users/activeSupplier/';?>'+id;
        }
    }
</script>

    <div class="topo-title-div">
        <h5> Dados do usuário: <?php echo $user['username']; ?></h5>
    </div>
    <div class="clear"></div>
    <table class="tbSumaryRecall">
        <tr>
            <td>Usuário: </td>
            <td><?php echo (isset($user['username']))? $user['username'] : "-"; ?></td>
        </tr>
        <tr>
            <td>E-mail: </td>
            <td><?php echo (isset($user['email']))? $user['email'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Perfil:</td>
            <td><?php echo (isset($user['name_level']))? $user['name_level'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Status:</td>
            <?php 
                switch ($user['status']) {
                        case STATUS_ATIVO:
                            $status = '<span class="label label-success">Ativo</span>';
                            break;
                        case STATUS_INATIVO:
                            $status = '<span class="label label-danger">Inativo</span>';
                            break;
                        case STATUS_AGUARDANDO:
                            $status = '<span class="label label-warning">Aguardando</span>';
                            break;
                        case STATUS_SOLICITADO:
                            $status = '<span class="label label-primary">Solicitação</span>';
                            break;
                        default:
                            $status = "";
                            break;
                }
            ?>
            <td><?php echo $status; ?>
            </td>
        </tr>
        <?php if ($user['level'] == LEVEL_FORNECEDOR) : ?>
        <tr>
            <td>Razão Social </td>
            <td><?php echo (isset($supplier['corporate_name']))? $supplier['corporate_name'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Nome Fantasia</td>
            <td><?php echo (isset($supplier['trade_name']))? $supplier['trade_name'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Ramo de Atividade</td>
            <td><?php echo (isset($supplier['branch']))? $supplier['branch'] : "-"; ?></td>
        </tr>
        <tr>
            <td>CNPJ</td>
            <td><?php echo (isset($supplier['cnpj']))? $supplier['cnpj'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Número do Registro</td>
            <td><?php echo (isset($supplier['state_registration']))? $supplier['state_registration'] : "-"; ?></td>
        </tr>
        <tr>
            <td>E-mail(s) com Cópia *</td>
            <td>
                <?php echo (isset($supplier['email_copy']))? $supplier['email_copy'] : "-"; ?> 
            </td>
        </tr>
        
        <tr>
            <td>Estado</td>
            <td><?php echo (isset($supplier['state']))? $supplier['state'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Cidade</td>
            <td><?php echo (isset($supplier['city']))? $supplier['city'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Endereço</td>
            <td><?php echo (isset($supplier['address']))? $supplier['address'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Complemento</td>
            <td><?php echo (isset($supplier['complement']))? $supplier['complement'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Bairro</td>
            <td><?php echo (isset($supplier['district']))? $supplier['district'] : "-"; ?></td>
        </tr>
        <tr>
            <td>CEP</td>
            <td><?php echo (isset($supplier['zip']))? $supplier['zip'] : "-"; ?></td>
        </tr>
        <tr>
            <td>Telefone Comercial</td>
            <td><?php echo (isset($supplier['commercial_phone']))? $supplier['commercial_phone'] : "-"; ?></td>
        </tr>
        <tr>
            <td>FAX</td>
            <td><?php echo (isset($supplier['fax_phone']))? $supplier['fax_phone'] : "-"; ?></td>
        </tr>
        <?php endif; ?>
    </table>
    <p>
        <span style="text-align: right; font-size: 11px; font-style: italic">
            * Os e-mails com cópia receberão todas as notificações das campanhas de recall
        </span>
    </p>
    <div style="margin: 30px auto; text-align: center">
        <a class="btn btn-default" href="javascript:window.history.go(-1)">Voltar</a>
        <?php if ($user['status'] == STATUS_SOLICITADO) : ?>
            <a class='btn btn-success' href="javascript::void(0)" 
               onclick="ativar(<?php echo $user['user_id']; ?>,'<?php echo $user['username']; ?>')">
                Ativar Fornecedor
            </a>
        <?php else : 
            
                // dados da sessao do usuario logado
                $session_login = $this->session->userdata('login');
                $level_user = $session_login['level'];

                if (($level_user == LEVEL_ADMINISTRADOR) || ($user['level'] != LEVEL_ADMINISTRADOR)) :
        ?>
                    <a class='btn btn-default' href="javascript::void(0)" id="changePassword">
                        Alterar senha
                    </a>
                    <a class='btn btn-primary' href="<?php echo base_url()."users/edit/".$user['user_id'];?>">
                        Alterar dados
                    </a>
        
        <?php 
                endif; 
            endif; 
        ?>
    </div>
    <div id="newPassword">
        <form name="password" id="password" method="post" action="" class="form-horizontal form-group">
            <div class="col-md-12">
                <div class="form-group">
                    <?php 
                        echo form_label('Senha Atual', 'password', array('class' => 'col-md-4'));
                        echo "<div class='col-md-5'>";
                        $password = array(
                            'name'      => 'password',
                            'id'        => 'password',
                            'value'     => set_value('password'),
                            'class'     => 'form-control input-xlarge',
                        );
                        echo form_password($password);
                        echo form_error('password', '<div class="error">', '</div>');
                        echo "</div>"
                     ?>
                </div>
                <div class="form-group">
                    <?php 
                        echo form_label('Nova senha', 'password_new', array('class' => 'col-md-4'));
                        echo "<div class='col-md-5'>";
                        $passwordNew = array(
                            'name'      => 'password_new',
                            'id'        => 'password_new',
                            'value'     => set_value('password_new'),
                            'class'     => 'form-control input-xlarge',
                        );
                        echo form_password($passwordNew);
                        echo form_error('password_new', '<div class="error">', '</div>');
                        echo "</div>"
                     ?>
                </div>
                <div class="form-group">
                    <?php 
                        echo form_label('Confirmar nova senha', 'confirm_password_new', array('class' => 'col-md-4'));;
                        echo "<div class='col-md-5'>";
                        $passwordConfirm = array(
                            'name'      => 'confirm_password_new',
                            'id'        => 'confirm_password_new',
                            'value'     => set_value('confirm_password_new'),
                            'class'     => 'form-control input-xlarge',
                        );
                        echo form_password($passwordConfirm);
                        echo form_error('confirm_password_new', '<div class="error">', '</div>');
                        echo "</div>"
                     ?>
                </div>
            </div>
            <div style="margin: 30px auto; text-align: center">
                <a class='btn btn-success' href="javascript::void(0)" onclick="alterar()">
                    Confirmar alteração de senha
                </a>
            </div>
        </form>
    </div>