<script>
    $(document).ready(function(){
        $("select[name='level']").change(function(){
            var level = $("select[name='level'] option:selected").attr('value');
            var level_supplier = <?php echo LEVEL_FORNECEDOR; ?>;
            if (level == level_supplier) {
                $('#divSupplier').show();
            } else {
                $('#divSupplier').hide();
            }
	});
        
        // Combo Tipo de Produto
        $("select[name='branch_id']").change(function(){
            var branch_id = $("select[name='branch_id'] option:selected").attr('value');
            if (branch_id.length > 0) {
                var urlpath = '<?php echo base_url();?>';
                $("#type_product_id").html("<option value=''>Selecione</option>");
                $.ajax({
                    url: urlpath+'type_products/loadCombo/',
                    type:'POST',
                    data: 'branch_id='+branch_id,
                    dataType: 'json',
                    success: function( data ) {
                        $.each(data, function(i, value) {
                            $('#type_product_id').append($('<option/>', {
                                value: i,
                                text : value 
                            }));
                        });
                    }
                });
            }
	});
        
    });
</script>

<form class="form-horizontal form-group" method="post" action="">
  <fieldset>

    <!-- Form Name -->
    <legend>Cadastro de entidade parceira</legend>
    
     <!-- Text input-->
    
     
     <div class="form-group">
        <?php 
            echo form_label('Entidade', 'name', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            $attName = array(
                'name'      => 'name',
                'id'        => 'name',
                'value'     => set_value('name'),
                'class'     => 'form-control input-md requiredField',
            );
            echo form_input($attName);
            echo form_error('name', '<div class="error">', '</div>');
            echo "</div>"
         ?>
        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
    </div>
     
    <div class="form-group">
        <?php 
            echo form_label('E-mail', 'email', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            $attEmail = array(
                'name'      => 'email',
                'id'        => 'email',
                'value'     => set_value('email'),
                'class'     => 'form-control input-md requiredField',
            );
            echo form_input($attEmail);
            echo form_error('email', '<div class="error">', '</div>');
            echo "</div>"
         ?>
        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
    </div>
    
    <div class="form-group">
        <?php 
            echo form_label('Setor', 'branch', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            echo form_dropdown('branch_id', $comboBranch, set_value('branch_id'), 'class="form-control"');
            echo "</div>";
         ?>
    </div>
    
    <div class="form-group">
        <?php 
            echo form_label('Tipo de Produto', 'type_product', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            echo form_dropdown('type_product_id', $type_product, set_value('type_product_id'), 'class="form-control input-xlarge" id="type_product_id"');
            echo "</div>";
         ?>
    </div>
    
    <div style="margin: 30px auto; text-align: center">
        <a class='btn btn-default' href="<?php echo base_url()."subscribers/index/";?>">
             <span class="glyphicon glyphicon-chevron-left"></span> Voltar
        </a>
        <button id="save" name="save" class="btn btn-success">Salvar</button>
    </div>
    
  </fieldset>
</form>
