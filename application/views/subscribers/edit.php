<script>
    $(document).ready(function(){
        // Combo Tipo de Produto
        $("select[name='branch_id']").change(function(){
            var branch_id = $("select[name='branch_id'] option:selected").attr('value');
            if (branch_id.length > 0) {
                var urlpath = '<?php echo base_url();?>';
                $("#type_product_id").html("<option value=''>Selecione</option>");
                $.ajax({
                    url: urlpath+'type_products/loadCombo/',
                    type:'POST',
                    data: 'branch_id='+branch_id,
                    dataType: 'json',
                    success: function( data ) {
                        $.each(data, function(i, value) {
                            $('#type_product_id').append($('<option/>', {
                                value: i,
                                text : value 
                            }));
                        });
                    }
                });
            }
	});
        
    });
</script>

<form class="form-horizontal form-group" method="post" action="">
  <fieldset>

    <!-- Form Name -->
    <legend>Cadastro de entidade parceira</legend>
    
     <!-- Text input-->
    
     
     <div class="form-group">
        <?php 
            echo form_label('Entidade', 'name', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            $attName = array(
                'name'      => 'name',
                'id'        => 'name',
                'value'     => $dados['name'],
                'class'     => 'form-control input-md requiredField',
            );
            echo form_input($attName);
            echo form_error('name', '<div class="error">', '</div>');
            echo "</div>"
         ?>
        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
    </div>
     
    <div class="form-group">
        <?php 
            echo form_label('E-mail', 'email', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            $attEmail = array(
                'name'      => 'email',
                'id'        => 'email',
                'value'     => $dados['email'],
                'class'     => 'form-control input-md requiredField',
            );
            echo form_input($attEmail);
            echo form_error('email', '<div class="error">', '</div>');
            echo "</div>"
         ?>
        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
    </div>
    
    <div class="form-group">
        <?php 
            echo form_label('Setor', 'branch', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            $defaultBranch = isset($dados['branch_id']) ? $dados['branch_id'] : set_value('branch_id');
            echo form_dropdown('branch_id', $comboBranch, $defaultBranch, 'class="form-control"');
            echo "</div>";
         ?>
    </div>
    
    <div class="form-group">
        <?php 
            echo form_label('Tipo de Produto', 'type_product', array('class' => 'col-md-4 control-label'));
            echo "<div class='col-md-5'>";
            $defaultType = ($dados['type_product_id']) ? $dados['type_product_id'] : set_value('branch_id');
            echo form_dropdown('type_product_id', $comboTypeProduct, $defaultType, 'class="form-control input-xlarge" id="type_product_id"');
            echo "</div>";
         ?>
    </div>
    
    <div style="margin: 30px auto; text-align: center">
        <a class='btn btn-default' href="<?php echo base_url()."subscribers/index/";?>">
             <span class="glyphicon glyphicon-chevron-left"></span> Voltar
        </a>
        <button id="save" name="save" class="btn btn-success">Salvar</button>
    </div>
    
  </fieldset>
</form>
