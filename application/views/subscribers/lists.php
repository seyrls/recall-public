<script>
$(document).ready(function() {
    $('#example').dataTable();
});
function excluir(id,nome) {
    if (confirm('Deseja excluir a entidade '+nome+'?')) {
        window.location='<?php echo base_url().'subscribers/remove/';?>'+id;
    }
}
</script>
<div>			
    <table class="table table-striped table-bordered" id="example" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 60px">Ação</th>
                <th>Entidade</th>
                <th>E-mail</th>
                <th>Setor</th>
                <th>Tipo de produto</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if ($dados) :
                foreach ($dados as $dado) :
                    $id = $dado['option_id'];
                    $nome = $dado['name'];
                    if ($dado['status'] == STATUS_ATIVO) {
                        $status = '<span class="label label-success">Ativo</span>';
                    } else {
                        $status = '<span class="label label-danger">Inativo</span>';
                    }
                ?>  
                    <tr>
                        <td>
                            <a class="btn btn-sm btn-default" href='<?php echo base_url()."subscribers/edit/".$id; ?>'>
                                <img src="<?php echo base_url()."img/fi-page-edit.svg";?>" width='14px' title='Editar' style='text-align: center'/>
                            </a>
                            <a class="btn btn-sm btn-default" href='javascript:void(o)' onClick="excluir(<?php echo $id.",'".$nome."'"; ?>)">
                                <img src="<?php echo base_url()."img/fi-x.svg";?>" width='14px' title='Excluir' style='text-align: center'/>
                            </a>
                        </td>
                        <td><?php echo $nome; ?></td>
                        <td><?php echo $dado['email']; ?></td>
                        <td><?php echo isset($dado['branch']) ? ($dado['branch']) : 'Todos'; ?></td>
                        <td><?php echo isset($dado['type_product']) ? ($dado['type_product']) : 'Todos'; ?></td>
                        <td><?php echo $status; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>
<div style="margin: 30px auto; text-align: center">
    <a class='btn btn-primary' href="<?php echo base_url(); ?>subscribers/insert">
        Inserir novo parceiro
    </a>
</div>