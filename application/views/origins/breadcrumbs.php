<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
        </button>
            <?php 
                if (isset($caminho)) :
                    foreach ($caminho as $breadcrumbs => $action) :
            ?>
                        <a class="navbar-brand" href="<?php echo base_url().$action;?>">
                            <span class="label label-default"><?php echo $breadcrumbs; ?></span>
                        </a>
            <?php
                    endforeach;
                endif;
            ?>
    </div>
</nav>