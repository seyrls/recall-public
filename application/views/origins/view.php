<script>
$(document).ready(function() {
    $('#example').dataTable();
});

function excluir(id,nome) {
    if (confirm('Deseja excluir o meio de veiculação '+nome+'?')) {
        window.location='<?php echo base_url().'medias/remove/';?>'+id;
    }
}

</script>
<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Mídias</h5>
    </div>
    <table class="table table-striped table-bordered table-grid" id="example" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Ação</th>
                <th>Mídia</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if ($media) :
                foreach ($media as $m) : 
                    $id = $m['media_id'];
                    $nome = $m['media'];
                    $status = ($m['status'] == TRUE) ? '<span class="label label-success">Ativo</span>' : '<span class="label label-danger">Inativo</span>';
            ?>  
                    <tr>
                        <td>
                            <a class="btn btn-sm btn-default" href='<?php echo base_url()."medias/view/".$id; ?>'>
                                <img src="<?php echo base_url()."img/fi-magnifying-glass.svg";?>" width='14px' title='Visualuzar' style='text-align: center'/>
                            </a>
                            <a class="btn btn-sm btn-default" href='<?php echo base_url()."medias/edit/".$id; ?>'>
                                <img src="<?php echo base_url()."img/fi-page-edit.svg";?>" width='14px' title='Editar' style='text-align: center'/>
                            </a>
                            <a class="btn btn-sm btn-default" href='javascript:void(o)' onClick="excluir(<?php echo $id.",'".$nome."'"; ?>)">
                                <img src="<?php echo base_url()."img/fi-x.svg";?>" width='14px' title='Excluir' style='text-align: center'/>
                            </a>
                        </td>
                        <td><?php echo $nome; ?></td>
                        <td><?php echo $status; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif;
            ?>
        </tbody>
    </table>
    <div style="margin: 30px auto; text-align: center">
        <a class='btn btn-default' href="<?php echo base_url();?>origins/index">
             <span class="glyphicon glyphicon-chevron-left"></span> Voltar
        </a>
        <a class='btn btn-primary' href="<?php echo base_url()."medias/insert/".$dados['origin_id'];?>">
             Inserir mídia para <?php echo $dados['origin']; ?>
        </a>
    </div>
    
</div>