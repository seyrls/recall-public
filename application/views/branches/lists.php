<script>
$(document).ready(function() {
    $('#branch').dataTable();
});

function excluir(id,nome) {
    if (confirm('Deseja excluir o setor de atividade '+nome+'?')) {
        window.location='<?php echo base_url().'branches/remove/';?>'+id;
    }
}
</script>
<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Setores de Atividades</h5>
    </div>
    <table class="table table-striped table-bordered" id="branch" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 60px">Ação</th>
                <th>Setor de atividade</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            if ($dados) :
                foreach ($dados as $d) :
                    $status = ($d['status'] == TRUE) ? 
                    '<span class="label label-success">Ativo</span>' : 
                    '<span class="label label-danger">Inativo</span>';
                    $id = $d['branch_id'];
                    $name = $d['branch'];
        ?>
                    <tr>
                        <td>
                            <a class="btn btn-sm btn-default" href='<?php echo base_url()."branches/edit/".$id; ?>'>
                                <img src="<?php echo base_url()."img/fi-page-edit.svg";?>" width='14px' title='Editar' style='text-align: center'/>
                            </a>
                            <a class="btn btn-sm btn-default" href='javascript:void(o)' onClick="excluir(<?php echo $id.",'".$name."'"; ?>)">
                                <img src="<?php echo base_url()."img/fi-x.svg";?>" width='14px' title='Excluir' style='text-align: center'/>
                            </a>
                        </td>
                        <td><?php echo $d['branch']; ?></td>
                        <td><?php echo $status; ?></td>
                    </tr>
        <?php
                endforeach;
            endif;
        ?>
        </tbody>
    </table>
</div>
<div style="margin: 30px auto; text-align: center">
    <a class='btn btn-primary' href="<?php echo base_url(); ?>branches/insert">
        Inserir novo
    </a>
</div>