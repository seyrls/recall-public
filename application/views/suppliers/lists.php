<script>
$(document).ready(function() {
    $('#example').dataTable();
});

function excluir(id,nome) {
    if (confirm('Deseja excluir o usuário '+nome+'?')) {
        window.location='<?php echo base_url().'users/remove/';?>'+id+'/supplier';
    }
}

</script>
<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Fornecedores</h5>
    </div>
    <table class="table table-striped table-bordered" id="example" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 100px">Ação</th>
                <th>Usuário</th>
                <th>CNPJ</th>
                <th>Razão Social</th>
                <th>Nome Fantasia</th>
                <th>UF</th>
                <th>Municipio</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if ($dados) :
                foreach ($dados as $dado) :
                    switch ($dado['status']) {
                        case STATUS_ATIVO:
                            $status = '<span class="label label-success">Ativo</span>';
                            break;
                        case STATUS_INATIVO:
                            $status = '<span class="label label-danger">Inativo</span>';
                            break;
                        case STATUS_AGUARDANDO:
                            $status = '<span class="label label-warning">Aguardando</span>';
                            break;
                        case STATUS_SOLICITADO:
                            $status = '<span class="label label-primary">Solicitação</span>';
                            break;
                        default:
                            $status = "";
                            break;
                    }
                    $id = $dado['user_id'];
                ?>  
                    <tr>
                        <td>
                            <a class="btn btn-sm btn-default" href='<?php echo base_url()."users/view/".$id; ?>'>
                                <img src="<?php echo base_url()."img/fi-magnifying-glass.svg";?>" width='14px' title='Visualizar' style='text-align: center'/>
                            </a>
                            <a class="btn btn-sm btn-default" href='<?php echo base_url()."users/edit/".$id; ?>'>
                                <img src="<?php echo base_url()."img/fi-page-edit.svg";?>" width='14px' title='Editar' style='text-align: center'/>
                            </a>
                            <a class="btn btn-sm btn-default" href='javascript:void(o)' onClick="excluir(<?php echo $id.",'".$dado['username']."'"; ?>)">
                                <img src="<?php echo base_url()."img/fi-x.svg";?>" width='14px' title='Excluir' style='text-align: center'/>
                            </a>
                        </td>
                        <td><?php echo utf8_encode($dado['username']); ?></td>
                        <td><?php echo $dado['cnpj']; ?></td>
                        <td><?php echo utf8_encode($dado['corporate_name']); ?></td>
                        <td><?php echo utf8_encode($dado['trade_name']); ?></td>
                        <td><?php echo $dado['state']; ?></td>
                        <td><?php echo utf8_encode($dado['city']); ?></td>
                        <td><?php echo $status; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>

<div style="margin: 30px auto; text-align: center">
    <a class='btn btn-default' href="<?php echo base_url()."users/index/";?>">
        <span class="glyphicon glyphicon-chevron-left"></span> Voltar
   </a>
    <a class='btn btn-primary' href="<?php echo base_url(); ?>users/insert/supplier">
        Inserir novo fornecedor
    </a>
</div>