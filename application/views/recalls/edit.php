<div class="container">
    <!-- cabeçalho passos -->
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                <p>Campanha</p>
            </div><!-- stepwizard-step -->
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default btn-circle">2</a>
                <p>Produto(s)</p>
            </div><!-- stepwizard-step -->
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default btn-circle">3</a>
                <p>Local de Atendimento</p>
            </div><!-- stepwizard-step -->
            <div class="stepwizard-step">
                <a href="#step-4" type="button" class="btn btn-default btn-circle">4</a>
                <p>Plano de mídia</p>
            </div><!-- stepwizard-step -->
            <div class="stepwizard-step">
                <a href="#step-5" type="button" class="btn btn-default btn-circle">5</a>
                <p>Aviso de risco</p>
            </div><!-- stepwizard-step -->
            <div class="stepwizard-step">
                <a href="#step-6" type="button" class="btn btn-default btn-circle">6</a>
                <p>Processos judiciais</p>
            </div><!-- stepwizard-step -->
            <div class="stepwizard-step">
                <a href="#step-7" type="button" class="btn btn-default btn-circle">7</a>
                <p>Resumo da Campanha</p>
            </div><!-- stepwizard-step -->
        </div><!-- stepwizard-row setup-panel -->
    </div><!-- stepwizard -->
	
    <!-- início form -->
    <form role="form" id="myform" class="form-horizontal" method="post" enctype="multipart/form-data">
        <!-- step 1 -->
        <div class="row setup-content" id="step-1">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <fieldset>

                        <!-- Form Name -->
                        <legend>Dados da Campanha</legend>
                        
                        <!-- Text input-->
                        <div class="form-group">
                                <?php
                                
                                if ($supplier[0]['cnpj'] == ''){
                                    echo '<label class="col-md-4 control-label" for="title">Fornecedor</label>  
                                            <div class="col-md-5">';
                                    echo '<select id="supplier_id" name="supplier_id" class="form-control">';
                                    foreach ($supplier as $sp) {
                                        echo '<option value="'.$sp['id'].'">'.$sp['trade_name'].'</option>';
                                    }
                                    echo "</select>";
                                }else{
                                    echo '<label class="col-md-4 control-label" for="title"></label>  
                                            <div class="col-md-5">';
                                    echo "<input type='hidden' name='supplier_id' value='".$supplier[0]['id']."'>";
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="title">Titulo</label>  
                            <div class="col-md-5">
                                <input id="title" name="title" value="<?=$dados['title'];?>" 
                                       placeholder="Informe o título da campanha" class="form-control input-md" required="" type="text">
                                <span class="help-block">Campo "Título" é obrigatório!</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="protocol">Nº do protocolo</label>  
                            <div class="col-md-4">
                                <input id="protocol" name="protocol" value="<?=$dados['protocol'];?>" 
                                       placeholder="Informe o nº do protocolo" class="form-control input-md" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="start_date">Data de início</label>  
                            <div class="col-md-4">
                                <input id="start_date" name="start_date" value="<?=($dados['start_date']) ? date("d/m/Y", strtotime($dados['start_date']['date'])) : '';?>" 
                                       placeholder="Informe a data de início" class="form-control input-md calendario" required="" type="text">
                                <span class="help-block">Campo "Data de Inicio" é obrigatório!</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="end_date">Data de término</label>  
                            <div class="col-md-4">
                                <input id="end_date" name="end_date" value="<?=($dados['end_date']) ? date("d/m/Y", strtotime($dados['end_date']['date'])) : '';?>" 
                                       placeholder="Informe a data de término" class="form-control input-md calendario" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="description">Descrição</label>
                            <div class="col-md-4">                     
                                <textarea class="form-control" id="description" name="description" required="" 
                                          value="<?=$dados['description'];?>" 
                                          placeholder="Informe a descrição"></textarea>
                                <span class="help-block">Campo "Descrição" é obrigatório</span>
                            </div>
                        </div>

                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="corrective_measure">Medida de correção</label>
                            <div class="col-md-4">                     
                                <textarea class="form-control textarea" id="corrective_measure" name="corrective_measure" required=""
                                          value="<?=$dados['description'];?>" 
                                          placeholder="Informe a medida de correção"></textarea>
                                <span class="help-block">Campo "Medida de correção" é obrigatório</span>
                            </div>
                        </div>

                        <!-- Multiple Radios (inline) -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="report_accident">Relato de acidentes?</label>
                            <div class="col-md-4"> 
                                <label class="radio-inline" for="report_accident-0">
                                    <input name="report_accident" id="report_accident-0" value="1" type="radio">
                                        Sim
                                </label>
                                <label class="radio-inline" for="report_accident-1">
                                    <input name="report_accident" id="report_accident-1" value="0" checked="checked" type="radio">
                                        Não
                                </label>
                            </div>
                        </div>

                    </fieldset>

                    <button class="btn btn-primary btn-lg pull-right" type="submit" id="btn_campaign" name="btn_campaign"><span class="glyphicon glyphicon-plus-sign"></span> Salvar</button>
                    <button class='btn btn-primary nextBtn btn-lg pull-right' type='button' id="btn_next1" name="btn_next1">Próximo <span class='glyphicon glyphicon-chevron-right'></button>

                    <div id="msg1">
                        <div class="alert alert-success" role="alert" id="response1">
                            Item salvo com sucesso!
                        </div>
                    </div>

                </div><!-- col-md-12 -->
            </div><!-- col-xs-12 -->
        </div><!-- row setup-content -->

        <!-- step 2 -->
        <div class="row setup-content" id="step-2">
            <div class="css3-tabstrip">
                <ul><!-- inicio abas -->
                    <li><!-- aba 1 -->
                        <input type="radio" name="css3-tabstrip-0" checked="checked" id="css3-tabstrip-0-0" /><label for="css3-tabstrip-0-0">Produto(s)</label>
                        <div>
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Produto(s)</legend>
                            
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="type_product_id">Tipo do produto</label>
                                            <div class="col-md-4">
                                                <select id="type_product_id" name="type_product_id" class="form-control">
                                                <?php
                                                
                                                    foreach ($type_product as $tp) {
                                                        echo '<option value="'.$tp['id'].'">'.$tp['type_product'].'</option>';
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product">Produto</label>  
                                            <div class="col-md-6">
                                                <input id="product" name="product" placeholder="Informe o nome do produto" class="form-control input-md"  type="text">
                                                <span class="help-block">Campo "Produto" é obrigatório</span>  
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="manufacturer_id">Fabricante</label>
                                            <div class="col-md-4">
                                                <select id="manufacturer_id" name="manufacturer_id" class="form-control">
                                                <?php
                                                    foreach ($manufacturer as $m) {
                                                        echo '<option value="'.$m['id'].'">'.$m['corporate_name'].'</option>';
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="model">Modelo</label>  
                                            <div class="col-md-5">
                                                <input id="model" name="model" placeholder="Informe o modelo do produto" class="form-control input-md"  type="text">
                                                <span class="help-block">Campo "Modelo" é obrigatório</span>  
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="year_manufacture">Ano de fabricação</label>  
                                            <div class="col-md-4">
                                                <input id="year_manufacture" name="year_manufacture" placeholder="Informe o ano de fabricação" class="form-control input-md"  type="text">
                                                <span class="help-block">Campo "Ano de fabricação" é obrigatório</span>  
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="quantity_affected">Quantidade afetada</label>  
                                            <div class="col-md-4">
                                                <input id="quantity_affected" name="quantity_affected" placeholder="Informe a quantidade afetada" class="form-control input-md"  type="text">
                                                <span class="help-block">Campo "Quantidade afetada" é obrigatório</span>  
                                            </div>
                                        </div>

                                        <!-- Image input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="quantity_affected">Imagens do produto</label>  
                                            <div class="col-md-4">
                                                <div class="span9">
                                                      <input type="file" id="file2" name="file2" />
                                                  </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div><!-- col-md-12 -->
                            </div><!-- col-xs-12 -->
                        </div>
                    </li><!-- fim aba 1 -->

                    <!-- aba 2 -->
                    <li>
                        <input type="radio" name="css3-tabstrip-0" id="css3-tabstrip-0-1" /><label for="css3-tabstrip-0-1">Lote do produto</label>
                        <div>
                            <!-- Lote do produto -->
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <legend>Lote do produto</legend>
                                    <table class="table table-bordered table-hover" id="tab_logic">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Nº lote inicial</th>
                                                <th class="text-center">Nº lote final</th>
                                                <th class="text-center">Data inicio de fabricação</th>
                                                <th class="text-center">Data fim de fabricação</th>
                                                <th class="text-center">Validade</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id='addr0'>
                                                <td>1</td>
                                                <td>
                                                    <input name='initial_batch[]' type='text' placeholder='Lote inicial' class='form-control input-md'  />
                                                </td>
                                                <td>
                                                    <input name='final_batch[]' type='text' placeholder='Lote final' class='form-control input-md'>
                                                </td>
                                                <td>
                                                    <input  name='manufacturer_initial_date[]' id="manufacturer_initial_date" type='text' placeholder='Data de início de fabricação' class='form-control input-md calendario'>
                                                </td>
                                                <td>
                                                    <input  name='manufacturer_final_date[]' id='manufacturer_final_date' type='text' placeholder='Data final de fabricação' class='form-control input-md calendario'>
                                                </td>
                                                <td>
                                                    <input  name='expiry_date[]' type='text' placeholder='Data de validade' class='form-control input-md calendario'>
                                                </td>
                                            </tr>
                                            <tr id='addr1'></tr>
                                        </tbody>
                                    </table>
                                    <a id="add_row" class="btn btn-default pull-left"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar item</a><a id='delete_row' class="pull-right btn btn-default"><span class="glyphicon glyphicon-minus-sign"></span> Remover item</a>
                                </div><!-- col-md-12 -->
                            </div><!-- col-xs-12 -->
                        </div>
                    </li><!-- fim aba 2 -->

                    <!-- aba 3 -->
                    <li>
                        <input type="radio" name="css3-tabstrip-0" id="css3-tabstrip-0-2" /><label for="css3-tabstrip-0-2">Quantidade afetada por UF</label>
                        <div>
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <legend>Quantidade afetada por UF</legend>
                                    <table class="table table-bordered table-hover" id="tab_quantity">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">UF</th>
                                                <th class="text-center">Quantidade</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id='pro0'>
                                                <td>1</td>
                                                <td>
                                                    <select id="iduf" name="iduf[]" class="form-control">
                                                        <option value="12">AC</option>
                                                        <option value="27">AL</option>
                                                        <option value="13">AM</option>
                                                        <option value="16">AP</option>
                                                        <option value="29">BA</option>
                                                        <option value="23">CE</option>
                                                        <option value="53">DF</option>
                                                        <option value="32">ES</option>
                                                        <option value="52">GO</option>
                                                        <option value="21">MA</option>
                                                        <option value="31">MG</option>
                                                        <option value="50">MS</option>
                                                        <option value="51">MT</option>
                                                        <option value="15">PA</option>
                                                        <option value="25">PB</option>
                                                        <option value="26">PE</option>
                                                        <option value="22">PI</option>
                                                        <option value="41">PR</option>
                                                        <option value="33">RJ</option>
                                                        <option value="24">RN</option>
                                                        <option value="11">RO</option>
                                                        <option value="14">RR</option>
                                                        <option value="43">RS</option>
                                                        <option value="42">SC</option>
                                                        <option value="28">SE</option>
                                                        <option value="35">SP</option>
                                                        <option value="17">TO</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input name='quantity[]' type='text' placeholder='Quantidade' class='form-control input-md'>
                                                </td>
                                            </tr>
                                            <tr id='pro1'></tr>
                                        </tbody>
                                    </table>
                                    <a id="add_row1" class="btn btn-default pull-left"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar item</a><a id='delete_row1' class="pull-right btn btn-default"><span class="glyphicon glyphicon-minus-sign"></span> Remover item</a>
                                </div><!-- col-md-12 -->
                            </div><!-- col-xs-12 -->
                        </div>
                    </li><!-- fim aba 3 -->

                    <!-- aba 4 -->
                    <li>
                        <input type="radio" name="css3-tabstrip-0" id="css3-tabstrip-0-3" /><label for="css3-tabstrip-0-3">Defeito / Informações Tecnicas</label>
                        <div>
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <fieldset>

                                        <!-- Form Name -->
                                        <legend>Defeito / Informações Tecnicas</legend>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="faulty_part">Componente</label>  
                                            <div class="col-md-6">
                                                <input id="faulty_part" name="faulty_part" placeholder="Informe o componente" class="form-control input-md" required="" type="text">
                                                <span class="help-block">Campo "Componente" é obrigatório</span>  
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="date_faulty">Data da constatação</label>  
                                            <div class="col-md-4">
                                                <input id="date_faulty" name="date_faulty" placeholder="Informe a data da constatação" class="form-control input-md calendario" required="" type="text">
                                                <span class="help-block">Campo "Data da constatação" é obrigatório</span>  
                                            </div>
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="faulty_describe">Descrição</label>
                                            <div class="col-md-4">                     
                                                <textarea class="form-control" id="faulty_describe" name="faulty_describe"></textarea>
                                            </div>
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="faulty">Defeito</label>
                                            <div class="col-md-4">                     
                                                <textarea class="form-control" id="faulty" name="faulty"></textarea>
                                            </div>
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="risk_falty">Risco</label>
                                            <div class="col-md-4">                     
                                                <textarea class="form-control" id="risk_falty" name="risk_falty"></textarea>
                                            </div>
                                        </div>

                                    </fieldset>

                                    <button class="btn btn-primary btn-lg pull-right" type="submit" id="btn_product" name="btn_product"><span class="glyphicon glyphicon-plus-sign"></span> Salvar</button>
                                    <button class='btn btn-primary nextBtn btn-lg pull-right' type='button' id="btn_next2" name="btn_next1">Próximo <span class='glyphicon glyphicon-chevron-right'></button>
                                    <button class="btn btn-primary btn-lg pull-left" type="submit" id="btn_new" name="btn_new"><span class="glyphicon glyphicon-plus-sign"></span> Novo produto</button>

                                    <div id="msg2">
                                        <div class="alert alert-success" role="alert" id="response1">
                                            Item salvo com sucesso!
                                        </div>
                                    </div>

                                    <!-- div para mensagem quando for novo produto -->
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>

                                    <div id="msg">
                                        <div id="response"></div>
                                    </div>

                                </div><!-- col-md-12 -->
                            </div><!-- col-xs-12 -->
                        </div>
                    </li><!-- fim aba 4 -->

                </ul><!-- fim abas  -->
            </div><!-- css3-tabstrip -->
        </div><!-- row setup-content -->

        <!-- step 3 -->
        <div class="row setup-content" id="step-3">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <fieldset>

                        <!-- Form Name -->
                        <legend>Dados do Local de Atendimento</legend>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="customer_service">Local de atendimento</label>  
                            <div class="col-md-6">
                                <input id="customer_service" name="customer_service" placeholder="Informe o local de atendimento" class="form-control input-md" required="" type="text">
                                <span class="help-block">Campo "Local de atendimento" é obrigatório</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="address_service">Endereço</label>  
                            <div class="col-md-6">
                                <input id="address_service" name="address_service" placeholder="Informe o endereço de atendimento" class="form-control input-md" required="" type="text">
                                <span class="help-block">Campo "Local de atendimento" é obrigatório</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="site">Sitio eletrônico</label>  
                            <div class="col-md-5">
                                <input id="site" name="site" placeholder="Informe o sitio eletrônico" class="form-control input-md" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="email">Correio eletrônico</label>  
                            <div class="col-md-5">
                                <input id="email" name="email" placeholder="Informe o endereço eletrônico" class="form-control input-md" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="phone_service">Telefone</label>  
                            <div class="col-md-4">
                                <input id="phone_service" name="phone_service" placeholder="Informe o telefone de atendimento" class="form-control input-md" required="" type="text">
                                <span class="help-block">Campo "Telefone" é obrigatório</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="notice_service">Observação</label>  
                            <div class="col-md-5">
                                <input id="notice_service" name="notice_service" placeholder="Observações (se necessário)" class="form-control input-md" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                    </fieldset>

                    <button class="btn btn-primary btn-lg pull-right" type="submit" id="btn_customer" name="btn_customer"><span class="glyphicon glyphicon-plus-sign"></span> Salvar</button>
                    <button class="btn btn-primary nextBtn btn-lg pull-right" id="btn_next3" type="button" >Próximo <span class="glyphicon glyphicon-chevron-right"></button>
                </div><!-- col-md-12 -->
            </div><!-- col-xs-12 -->
        </div><!-- row setup-content -->

        <!-- step 4 -->
        <div class="row setup-content" id="step-4">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <fieldset>

                        <!-- Form Name -->
                        <legend>Plano de Mídia</legend>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="origin">Meio de veiculação</label>
                            <div class="col-md-4">
                                <select id="origin" name="origin" class="form-control">
                                    <option value="" selected>-- Selecione meio de veiculação --</option>
                                    <?php
                                    foreach ($origin as $o) {
                                        echo '<option value="'.$o['id'].'">'.$o['origin'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="media">Veículo de mídia</label>
                            <div class="col-md-4" id="media">

                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="source">Fonte</label>
                            <div class="col-md-4" id="source">

                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="date_insertion">Data de veiculação</label>  
                            <div class="col-md-4">
                                <input id="date_insertion" name="date_insertion" placeholder="Informe a data de veiculação" class="form-control input-md calendario" required="" type="text">
                                <span class="help-block">Campo "Data de veiculação" é obrigatório</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="total_insertion">Total de inserção</label>  
                            <div class="col-md-1">
                                <input id="total_insertion" name="total_insertion" placeholder="Informa a quantidade de inserções" class="form-control input-md" required="" type="text">
                                <span class="help-block">Obrigatório</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="total_insertion">Horário da inserção</label>  
                            <div class="col-md-1">
                                <input id="time_insertion" name="time_insertion" placeholder="Informa a quantidade de inserções" class="form-control input-md" required="" type="text">
                                <span class="help-block">Obrigatório</span>  
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="singlebutton">Opção</label>
                            <div class="col-md-4">
                                <a id="add_row2" class="btn btn-default pull-left">
                                    <span class="glyphicon glyphicon-plus-sign"></span> Adicionar item
                                </a>
                            </div>
                        </div>

                    </fieldset>
                </div><!-- col-md-12 -->
            </div><!-- col-xs-12 -->

            <!-- Lote do produto -->
            <div class="col-xs-12">
                <div class="col-md-12">
                    <legend>Lote do produto</legend>
                    <table class="table table-bordered table-hover" id="tab_media">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Data de veiculação</th>
                                <th class="text-center">Meio de veiculação</th>
                                <th class="text-center">Veículo de mídia</th>
                                <th class="text-center">Fonte</th>
                                <th class="text-center">Total de inserção</th>
                                <th class="text-center">Horário de inserção</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id='media0'>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr id='media1'></tr>
                        </tbody>
                    </table>
                    <a id='delete_row2' class="pull-right btn btn-default">
                        <span class="glyphicon glyphicon-minus-sign"></span> Remover item
                    </a>                  
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <button class="btn btn-primary btn-lg pull-right" type="submit" id="btn_media" name="btn_media"><span class="glyphicon glyphicon-plus-sign"></span> Salvar</button>
                    <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="btn_next4" >Próximo <span class="glyphicon glyphicon-chevron-right"></button>
                </div><!-- col-md-12 -->
            </div><!-- col-xs-12 -->
        </div><!-- row setup-content -->

        <!-- step 5 -->
        <div class="row setup-content" id="step-5">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <fieldset>

                        <!-- Form Name -->
                        <legend>Aviso de risco</legend>

                        <!-- Textarea -->
                       <div class="form-group">
                            <label class="col-md-4 control-label" for="notice_risk">Mensagem veiculada</label>
                            <div class="col-md-4">                     
                                <textarea class="form-control" id="notoce_risk" name="notice_risk"></textarea>
                            </div>
                        </div>

                    </fieldset>
                    <button class="btn btn-primary btn-lg pull-right" type="submit" id="btn_noticerisk" name="btn_noticerisk"><span class="glyphicon glyphicon-plus-sign"></span> Salvar</button>
                    <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="btn_next5">Próximo <span class="glyphicon glyphicon-chevron-right"></button>
                </div><!-- col-md-12 -->
            </div><!-- col-xs-12 -->
        </div><!-- row setup-content -->

        <!-- step 6 -->
        <div class="row setup-content" id="step-6">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <fieldset>

                        <!-- Form Name -->
                        <legend>Processos Judiciais</legend>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="process_number">Nº do Processo</label>  
                            <div class="col-md-4">
                                <input id="process_number" name="process_number" placeholder="Informe o nº do processo" class="form-control input-md" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="date_process">Data do processo</label>  
                            <div class="col-md-4">
                                <input id="date_process" name="date_process" placeholder="Informa a data do processo" class="form-control input-md calendario" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="staff">Vara</label>  
                            <div class="col-md-5">
                                <input id="staff" name="staff" placeholder="Informe a vara do processo" class="form-control input-md" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="distrito">Comarca</label>  
                            <div class="col-md-5">
                                <input id="distrito" name="distrito" placeholder="Informe a comarca" class="form-control input-md" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="situation">Situação</label>  
                            <div class="col-md-4">
                                <input id="situation" name="situation" placeholder="Informe a situação do processo" class="form-control input-md" type="text">
                                <span class="help-block">Campo opcional</span>  
                            </div>
                        </div>

                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="describe_process">Descrição da ação</label>
                            <div class="col-md-4">                     
                                <textarea class="form-control" id="describe_process" name="describe_process"></textarea>
                            </div>
                        </div>

                    </fieldset>

                    <button class="btn btn-success btn-lg pull-right" type="submit" id="finish" name="finish">Salvar</button>
                </div><!-- col-md-12 -->
            </div><!-- col-xs-12 -->
        </div><!-- row setup-content -->
        <div class="row setup-content" id="step-7">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <fieldset>

                        <!-- Form Name -->
                        <legend>Resumo da Campanha</legend>
                        <h3>Camapanha</h3>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="process_number">Titulo</label>  
                            <div class="col-md-4">
                                <p>1</p>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="process_number">Nº do protocolo</label>  
                            <div class="col-md-4">
                                <p>1</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="date_process">Data de Início</label>  
                            <div class="col-md-4">
                                <p>11/01/1989</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="staff">Data de término</label>  
                            <div class="col-md-5">
                                <p>12/12/2012</p>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="staff">Descrição</label>  
                            <div class="col-md-5">
                                <p>descrição sobre a camapnha</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="distrito">Medida de correção</label>  
                            <div class="col-md-5">
                                <p>Demitir todo mundo</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="situation">Relato de acidentes?</label>  
                            <div class="col-md-4">
                                <p>Não</p>
                            </div>
                        </div>
                        <a href="#">Editar</a>
                        <br clear="both"/>
                        
                        
                        
                        
                        <h3>Produto</h3>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="process_number">Titulo</label>  
                            <div class="col-md-4">
                                <p>1</p>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="process_number">Nº do protocolo</label>  
                            <div class="col-md-4">
                                <p>1</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="date_process">Data de Início</label>  
                            <div class="col-md-4">
                                <p>11/01/1989</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="staff">Data de término</label>  
                            <div class="col-md-5">
                                <p>12/12/2012</p>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="staff">Descrição</label>  
                            <div class="col-md-5">
                                <p>descrição sobre a camapnha</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="distrito">Medida de correção</label>  
                            <div class="col-md-5">
                                <p>Demitir todo mundo</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="situation">Relato de acidentes?</label>  
                            <div class="col-md-4">
                                <p>Não</p>
                            </div>
                        </div>
                        <a href="#">Editar</a>
                        <br clear="both"/>
                        
                        
                        
                        
                    </fieldset>
                </div><!-- col-md-12 -->
            </div><!-- col-xs-12 -->
        </div><!-- row setup-content -->
    </form><!-- fim form -->
</div><!-- div conteiner -->


<!-- script para colocar mascaras nos campos -->
<script>
jQuery(function($){
   $("#time_insertion").mask("99:99",{placeholder:" "});
   $("#year_manufacture").mask("9999",{placeholder:" "});
   
});
</script>

<script type="text/javascript">
    $(document).ready(function(){ 
      $("#file").pekeUpload();
      $("#file2").pekeUpload({theme:'bootstrap', multi:true});
      $("#file3").pekeUpload({theme:'bootstrap', allowedExtensions:"jpeg|jpg|png|gif"});
      $("#file4").pekeUpload({theme:'bootstrap', multi:false});
      $("#file5").pekeUpload({theme:'bootstrap', allowedExtensions:"jpeg|jpg|png|gif", onFileError:function(file,error){alert("error on file: "+file.name+" error: "+error+"")}});
    });
    
    $("#file").pekeUpload();
  </script>
  
  <script type="text/javascript">
    
      
  </script>
  