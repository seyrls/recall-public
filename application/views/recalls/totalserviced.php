<script>
    $(document).ready(function() {
    $('#totalserviced').dataTable();
} );
</script>

<div class="container">			
    <table class="table table-striped table-bordered" id="totalserviced" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>ID</th>
                <th>Quantidade</th>
                <th>Data</th>
                <th>UF</th>
                <th>Produto</th>
                <th>Opção</th>
            </tr>
        </thead>
        <tbody>
        <?php
            foreach ($totalserviced as $ts){
                echo ($ts['id']);
                echo "<tr>
                <td><a href='".base_url()."recalls/edit/{$ts['id']}'>{$ts['id']}</a></td>
                <td>{$ts['quantity']}</td>
                <td>{$ts['date_insert']}</td>
                <td>{$ts['state']}</td>
                <td>{$ts['product']}</td>
                <td></td>

                </tr>";
        }?>
        </tbody>
    </table>
</div>