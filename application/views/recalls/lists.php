<script>
    $(document).ready(function(){
        $('#recall').dataTable();
    });
    function viewRecall(id) {
        $("#formRecall_"+id).submit();
    }
    function serviceds(supplier_id,recall_id) {
        window.location='<?php echo base_url().'serviceds/lists/';?>'+supplier_id+'/'+recall_id;
    }
</script>
<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Campanhas de Recall</h5>
    </div>
    <table class="table table-striped table-bordered table-list" id="recall" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Data de Início</th>
                    <th>Situação</th>
                    <th>Protocolo</th>
                    <?php if ($level != LEVEL_FORNECEDOR) : ?>
                        <th>Fornecedor</th>
                    <?php endif; ?>
                    <th>Total Afetado</th>
                    <th>Total Atendido</th>
                    <th>Índice de Atendimento</th>
                </tr>
            </thead>
            <tbody>
            <?php    
                foreach ($dados as $d) :
                    $start_date = ($d['start_date']) ?  date('d/m/Y', strtotime($d['start_date'])) : '---';
                    $status = (get_status_campaign($d['status_campaign'])) ? 
                            get_status_campaign($d['status_campaign']) : '';
                    
                    $qtd_affected = ($d['total_affected']) ? $d['total_affected'] : 0;
                    $qtd_serviced = ($d['total_serviced']) ? $d['total_serviced'] : 0;
                    
                    $percent = ($qtd_affected > 0) ? (($qtd_serviced*100)/$qtd_affected) : 0;
                    $progress = round($percent, 2).'%';
                    if (substr($progress,0,-1) == 100) {
                        $labelAtendimento = 'label-success';
                    } else{
                        $labelAtendimento = 'label-danger';
                    }
            ?>
                        <tr>
                            <form method="post" action="<?php echo base_url()."recalls/view";?>" 
                                  id="formRecall_<?php echo $d['recall_id']; ?>" 
                                  name="formRecall_<?php echo $d['recall_id']; ?>">
                                <input type="hidden" name="recall" value="<?php echo $d['recall_id']; ?>"/>
                                <td>
                                    <a href="javascript:void(0)" onclick="viewRecall(<?php echo $d['recall_id']; ?>)">
                                        <?php echo $d['title'];?>
                                    </a>
                                </td>
                                <td><?php echo $start_date;?></td>
                                <td>
                                    <?php echo $status;?>
                                </td>
                                <td><?php echo $d['protocol'];?></td>
                                <?php if ($level != LEVEL_FORNECEDOR) : ?>
                                    <td><?php echo $d['trade_name'];?></td>
                                <?php endif; ?>
                                <td><?php echo $qtd_affected;?></td>
                                <td><?php echo $qtd_serviced;?></td>
                                <td>
                                    <?php if (($d['status_campaign'] == STATUS_ID_PUBLICADA) || 
                                              ($d['status_campaign'] == STATUS_ID_PUBLICADA_COM_RESSALVA) || 
                                              ($d['status_campaign'] == STATUS_ID_FINALIZADA)
                                            ) : 
                                    ?>
                                        <a href='javascript:void(0)' onclick='serviceds(<?php echo $d['supplier_id']; ?>,<?php echo $d['recall_id']; ?>)'>
                                            <span class="label <?php echo $labelAtendimento; ?>">
                                                <?php echo $progress; ?>
                                            </span>
                                        </a>
                                    <?php else : ?>
                                            ---
                                    <?php endif; ?>
                                </td>
                            </form>
                        </tr>
                                    
            <?php 
                endforeach; 
            ?>
        </tbody>
    </table>
    <div style="margin: 30px auto; text-align: center">
        <a class='btn btn-primary' href="<?php echo base_url();?>recalls/insertCampaign">
             Inserir campanha de Recall
        </a>
    </div>
</div>