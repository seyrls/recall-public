<script>
    $(document).ready(function(){
        $("input:radio[name=possui_processo]").click(function() {
            var value = $(this).val();
            if (value == 1) {
                $("#processDiv").show();
            } else {
                $("#processDiv").hide();
            }
            $("#btn_process").show();
        });
    });
</script>
<?php
    $attributes = array('class' => 'formProcess', 'id' => 'formProcess', 'enctype'=>'multipart/form-data');
    echo form_open('recalls/insertProcess', $attributes);
?>
        <div class="col-xs-12 dvformulario">
            <div class="title-form">
                <h4>Processos Judiciais</h4>
                <p>
                    <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                    <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                </p>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <?php 
                    
                        if ($this->input->post('possui_processo')) {
                            $ativo = ($this->input->post('possui_processo') == 1) ? TRUE : FALSE;
                            $inativo = ($this->input->post('possui_processo') == 0) ? TRUE : FALSE;
                        } else {
                            $ativo = (isset($dados) && isset($dados['process_number'])) ? TRUE : FALSE;
                            $inativo = (isset($dados['process_number'])) ? FALSE : TRUE;
                        }

                        echo form_label('Possui processos judiciais?', '', array('class' => 'col-md-4 control-label requiredField'));
                        echo "<div class='col-md-5'>";
                        echo '<p style="float: left; padding: 10px 10px 0px 10px;">'.form_radio ('possui_processo', 1,$ativo, $habilita['disabled']).' Sim </p>';
                        echo '<p style="float: left; padding: 10px 0px 0px 10px;">'.form_radio ('possui_processo', 0, $inativo,$habilita['disabled']).' Não </p>';
                        echo form_error('possui_processo', '<div class="error">', '</div>');
                    ?>
                    &ensp;&ensp;&ensp; <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    </div>
                </div>
                <?php $habilitaDiv = (validation_errors()) ? 'display:block' : (isset($dados) && isset($dados['process_number'])) ? 'display:block' : 'display:none'; ?>
                <div id="processDiv" style="<?php echo $habilitaDiv;?>">
                    <div class="form-group">
                        <?php 
                            echo form_label('Nº do Processo', 'process_number', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attProcess = array(
                                'name'      => 'process_number',
                                'id'        => 'process_number',
                                'value'     => isset($dados['process_number']) ? $dados['process_number'] : set_value('process_number'),
                                'class'     => 'form-control input-md requiredField',
                            );
                            if ($habilita['readonly']) {
                                $attProcess['readonly'] = 'readonly';
                            }
                            echo form_input($attProcess);
                            echo form_error('process_number', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    </div>
                    
                    <div class="form-group">
                        <?php 
                            echo form_label('Identificação do consumidor', 'identification', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attIdentification = array(
                                'name'      => 'identification',
                                'id'        => 'identification',
                                'value'     => isset($dados['identification']) ? $dados['identification'] : set_value('identification'),
                                'class'     => 'form-control input-md',
                            );
                            if ($habilita['readonly']) {
                                $attIdentification['readonly'] = 'readonly';
                            }
                            echo form_input($attIdentification);
                            echo form_error('identification', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Data do processo', 'date_process', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-2'>";
                            $attDateProcess = array(
                                'name'      => 'date_process',
                                'id'        => 'date_process',
                                'value'     => !empty($dados['date_process']) ? date("d/m/Y", strtotime($dados['date_process']['date'])) : set_value('date_process'),
                                'class'     => 'form-control input-md calendario_prev',
                                'readonly'  =>'true',
                                'style'     => 'background-color: #fff;'
                            );
                            if ($habilita['readonly']) {
                                $attDateProcess['readonly'] = 'readonly';
                            }
                            echo form_input($attDateProcess);
                            echo form_error('date_process', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Vara', 'staff', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attStaff = array(
                                'name'      => 'staff',
                                'id'        => 'staff',
                                'value'     => isset($dados['staff']) ? $dados['staff'] : set_value('staff'),
                                'class'     => 'form-control input-md',
                            );
                            if ($habilita['readonly']) {
                                $attStaff['readonly'] = 'readonly';
                            }
                            echo form_input($attStaff);
                            echo form_error('staff', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Comarca', 'distrito', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attDistrito = array(
                                'name'      => 'distrito',
                                'id'        => 'distrito',
                                'value'     => isset($dados['distrito']) ? $dados['distrito'] : set_value('distrito'),
                                'class'     => 'form-control input-md',
                            );
                            if ($habilita['readonly']) {
                                $attDistrito['readonly'] = 'readonly';
                            }
                            echo form_input($attDistrito);
                            echo form_error('distrito', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Situação', 'situation', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attSituation = array(
                                'name'      => 'situation',
                                'id'        => 'situation',
                                'value'     => isset($dados['situation']) ? $dados['situation'] : set_value('situation'),
                                'class'     => 'form-control input-md',
                            );
                            if ($habilita['readonly']) {
                                $attSituation['readonly'] = 'readonly';
                            }
                            echo form_input($attSituation);
                            echo form_error('situation', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Descrição da ação', 'describe_process', array('class' => 'col-md-4 control-label', 'style' => 'height: 120px; line-height: 100px;'));
                            echo "<div class='col-md-5'>";
                            $attDescribe_process = array(
                                'name'      => 'describe_process',
                                'id'        => 'describe_process',
                                'value'     => isset($dados['describe_process']) ? $dados['describe_process'] : set_value('describe_process'),
                                'class'     => 'form-control input-md',
                                'rows'      => '4',
                            );
                            if ($habilita['readonly']) {
                                $attDescribe_process['readonly'] = 'readonly';
                            }
                            echo form_textarea($attDescribe_process);
                            echo form_error('describe_process', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="btnNext">
                <a class='btn btn-default'
                    href="<?php echo base_url();?>recalls/insertRisk">
                     <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                </a>

                <?php if ($habilita['habilitaBotao']) : ?>
                    <button class="btn btn-success" type="submit" id="btn_process" name="btn_process">
                         Salvar
                    </button>
                <?php endif; ?>

                <?php if ($time_line > TIME_LINE_PROCESS) : ?>
                    <a class='btn btn-default'
                        href="<?php echo base_url();?>recalls/view">
                        Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                <?php endif; ?>
            </div>
        </div>
<?php echo form_close();