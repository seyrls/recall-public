<?php $totMedia = count($media); ?>
<script>
    $(document).ready(function(){
        
        $("#form_media_cost").maskMoney({symbol: '', showSymbol:true, thousands:'.', decimal:',', symbolStay: true});
        
        somaCost();
        
        var k = <?php echo ($totMedia) ? ($totMedia) : 1; ?>;
        $("#add_row_media").click(function(){
            
            var b_way = $("#origin").find('option:selected').text();
            var b_type = $("#media").find('option:selected').text();
            var b_source = $("#source").find('option:selected').text();
            var b_source_val = $("#source").find('option:selected').val();
            var b_total = $("#total_insertion").val();
            var b_date = $("#date_insertion").val();
            var b_insertion = $("#date_insertion_end").val();
            var b_cost = $("#form_media_cost").val();
            
            if (b_way === "" || b_way === "-- Selecione Meio de veiculaçao --") {
                alert('É necessário selecionar um Meio de veiculação');
                return false;
            }
            if (b_type === "" || b_type === "-- Selecione Veículo de mídia --") {
                alert('É necessário selecionar um Veículo de Mídia');
                return false;
            }
            if (b_source === "") {
                alert('É necessário selecionar uma Fonte');
                return false;
            }
            if (b_date === "") {
                alert('O campo Data de veiculação é obrigatório');
                return false;
            }
            if (b_insertion === "") {
                alert('O campo Data final da inserção é obrigatório');
                return false;
            }
            if (b_total === "") {
                alert('O campo Total de inserção é obrigatório');
                return false;
            }
            if (b_cost === "") {
                alert('O campo Custo de inserção é obrigatório');
                return false;
            }
            if (b_total.length > 11) {
                alert('O campo Total de inserção não deve conter mais de 11 dígitos');
                return false;
            }
            if (b_cost.length > 13) {
                alert('O campo Custo da inserção não deve conter mais de 13 dígitos');
                return false;
            }
            if (!compareDate(b_date,b_insertion)) {
                alert("A data final de veiculação não pode ser menor do que a data inicial.");
                return false;
            }
                
            
            $('#noMedia').remove();
            $('#media'+k).html(
                "<td><input name='origin[]' title='"+b_way+"' value='"+b_way+"' type='text' class='form-control input-md requiredField' readonly></td>"+
                "<td><input name='media[]' title='"+b_type+"' value='"+b_type+"' type='text' class='form-control input-md requiredField' readonly></td>"+
                "<td><input name='source[]' title='"+b_source+"' value='"+b_source+"' type='text' class='form-control input-md requiredField' readonly><input name='b_source[]' value='"+b_source_val+"' type='hidden'></td>"+
                "<td><input name='date_insertion[]' title='"+b_date+"' value='"+b_date+"' type='text' class='form-control input-md requiredField' readonly/> </td>"+
                "<td><input name='date_insertion_end[]' title='"+b_insertion+"' value='"+b_insertion+"' type='text' class='form-control input-md requiredField' readonly></td>"+
                "<td><input name='total_insertion[]' title='"+b_total+"' value='"+b_total+"' type='text' class='form-control input-md requiredField' readonly></td>"+
                "<td><input name='media_cost[]' title='"+b_cost+"' value='"+b_cost+"' type='text' class='form-control input-md requiredField' readonly></td>"+
                "<td><a href='javascript:void(0)' onclick='removeItemMedia("+(k+1)+")' title='Remover Item'  class=' requiredFieldremoveItemColumn'><img src='<?php echo base_url();?>img/fi-x.svg' width='20px'></a></td>"
            );

            $("#date_insertion").val("");
            $("#origin").val("");
            $("#media").val("");
            $("#source").val("");
            $("#media").text("");
            $("#source").text("");
            $("#total_insertion").val("");
            $("#date_insertion_end").val("");
            $("#form_media_cost").val("");
            $('#tab_media').append('<tr id="media'+(k+1)+'"></tr>');
            k++;
            somaCost();
        });

        $("#delete_row_media").click(function(){
            if(k>0){
                $("#media"+(k-1)).html('');
                k--;
            }
        });
    });
    
    
    Number.prototype.pad = function(size) {
      var s = String(this);
      while (s.length < (size || 2)) {s = "0" + s;}
      return s;
    }
    
    function compareDate(datepickerBegin, datepickerEnd) {
        // transforma a data inicial para o formato americano
        var data_ini_Br = datepickerBegin;
        var d_1  = data_ini_Br.substring(0, 2);
        var m_1  = data_ini_Br.substring(3, 5);
        var y_1  = data_ini_Br.substring(6, 10);
        var h_1  = data_ini_Br.substring(11, 13);
        var mi_1 = data_ini_Br.substring(14, 16);
        var data_ini_ingles = y_1+"/"+m_1+"/"+d_1+" "+h_1+":"+mi_1;
        
        // transforma a data inicial em objeto
        var data1    = new Date(data_ini_ingles);
        var minutos1 = data1.getMinutes();
        var hora1    = data1.getHours();
        var dia1     = data1.getDate();
        var mes1     = data1.getMonth();
        var ano1     = data1.getFullYear();
        
        // concatena os valores em uma unica variavel, sem retirar os (zeros a esquerda) com a funcao "pad"
        var data_inicial = ano1.pad(4)+''+mes1.pad()+''+dia1.pad()+''+hora1.pad()+''+minutos1.pad();
        
        // transforma a data final para o formato americano
        var data_fim_Br = datepickerEnd;
        var d_2  = data_fim_Br.substring(0, 2);
        var m_2  = data_fim_Br.substring(3, 5);
        var y_2  = data_fim_Br.substring(6, 10);
        var h_2  = data_fim_Br.substring(11, 13);
        var mi_2 = data_fim_Br.substring(14, 16);
        var data_fim_ingles = y_2+"/"+m_2+"/"+d_2+" "+h_2+":"+mi_2;
        
        // transforma a data final em objeto
        var data2    = new Date(data_fim_ingles);
        var minutos2 = data2.getMinutes();
        var hora2    = data2.getHours();
        var dia2     = data2.getDate();
        var mes2     = data2.getMonth();
        var ano2     = data2.getFullYear();
        
        // concatena os valores em uma unica variavel, sem retirar os (zeros a esquerda) com a funcao "pad"
        var data_final = ano2.pad(4)+''+mes2.pad()+''+dia2.pad()+''+hora2.pad()+''+minutos2.pad();
        
        // faz a magica para verificar se a data final eh maior que a data inicial
        if (parseInt(data_final) >= parseInt(data_inicial)){
            return true;
        } else {
            return false;
        }
    }
    
    function somaCost(){
        var soma_cost = 0;
        var result = 0;
        $('#somaCusto span').text("");
        $('input[name^="media_cost"]').each(function(i,v){
            var valor = parseFloat($(this).val().replace(".","").replace(".","").replace(",","."));
            soma_cost += parseFloat(valor);
        });
        
        result = numeroParaMoeda(soma_cost);
        
        $('#somaCusto span').append(result);
        $("input[name='somaCusto']").val(soma_cost);
    }
    
    function numeroParaMoeda(n, c, d, t)
    {
        c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    }
    
    function removeItemMedia(k, id) {
        var rowCount = $('#tab_media tr').length;
        if (rowCount > 3){
            $('#media'+(k-1)+'').remove();
            somaCost();
            if (id) {
                $("#tab_media").append("<input type='hidden' name='deleteMedia[]' value='"+id+"'/>");
            }
        } else {
            $("#div_date_insertion").val("");
            $("#div_origin").val("");
            $("#div_media").val("");
            $("#div_source").val("");
            $("#div_total_insertion").val("");
            $("#div_date_insertion_end").val("");
            $("#div_media_cost").val("");
            alert('É necessário inserir pelo menos um plano de média.');
        }
    }
    
    function addMediaFile() {
        $("#addFile").append("<tr><td><input type='file' name='userfile[]'/></td><td><img src='<?php echo base_url();?>img/fi-x.svg' width='20px' onClick='removeFile(this);' title='Exclur Arquivo' style='cursor:pointer'/></td><td></td></tr>");
    }
    function removeFile(element,id) {
        var tr = element.parentNode.parentNode;
        tr.parentNode.removeChild(tr);
        if (id) {
            $("#addFile").append("<input type='hidden' name='deleteFile[]' value='"+id+"'/>");
        }
    }
    function downloadFile(file_id){
        var urlpath = '<?php echo base_url();?>';
        window.location.href=""+urlpath+"recalls/downloadFile/"+file_id;
    }
    function addMediaLink() {
        $("#addLink").append("<tr><td><label style='float: left; padding: 7px;'>Url:</label><div class='col-md-2'><input type='text' name='http' value='http://' readonly class='form-control input-mini'/></div><div class='col-md-8' style='margin-left: -30px !important'><input type='text' name='url[]' value='' class='form-control'></div> </td><td><img src='<?php echo base_url();?>img/fi-x.svg' width='20px' onClick='removeLink(this);' title='Exclur Arquivo' style='cursor:pointer'/></td><td></td></tr>");
    }
    function removeLink(element,id) {
        var tr = element.parentNode.parentNode;
        tr.parentNode.removeChild(tr);
        if (id) {
            $("#addLink").append("<input type='hidden' name='deleteLink[]' value='"+id+"'/>");
        }
    }
    function popup() {
        var urlpath = $('body').data('baseurl');
        var media_id = $('#media').find(":selected").val();
        var url = urlpath+'sources/insertPopup/'+media_id;


        var w = 700;
        var h = 670;
        var left = Number((screen.width/2)-(w/2));
        var tops = Number((screen.height/2)-(h/2));
        window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
    }
</script>
<?php
    $attributes = array('class' => 'formMedia', 'id' => 'formMedia', 'enctype'=>'multipart/form-data');
    echo form_open('recalls/insertMedia', $attributes);
?>
    <div class="row setup-content1" id="step-2">
        <div class="css3-tabstrip">
            <ul>
                <li>
                    <input type="radio" name="css3-tabstrip-0" checked="checked" id="css3-tabstrip-0-1" />
                    <label for="css3-tabstrip-0-1" class="labelAba">Novo Plano de Mídia</label>
                    <div class="dvformulario">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <div class="title-form">
                                    <h4>Plano de Mídia</h4>
                                    <p>
                                        <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                                        <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <?php
                                        echo form_label('Meio de veiculação', 'origin', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-6'>";
                                        $selecione = '';
                                        echo form_dropdown('origin', $origins, '', 'class="form-control" id="origin"'.$habilita['disabled']);
                                        echo "</div>";
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>

                                <div class="form-group">
                                    <?php
                                        echo form_label('Veículo de mídia', 'media', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-6' id='media'>";
                                        echo "</div>";
                                     ?>
                                    <img id="mediaReq" class="hide" src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="source">Fonte</label>
                                    <div class="col-md-6" id="source">

                                    </div>
                                    <img id="fonteReq" class="hide" src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                    <a href="javascript:void(0);"  class='hide' id="addSource"  onclick="popup()">
                                        <img src='<?php echo base_url();?>img/i_add.png' width='20px' title='Adicionar Fonte'>
                                    </a>
                                </div>

                                <div class="form-group">
                                    <?php 
                                        echo form_label('Data de veiculação', 'date_insertion', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-3'>";
                                        $attDateInsertion = array(
                                            'name'      => 'date_insertion',
                                            'id'        => 'date_insertion',
                                            'value'     => set_value('date_insertion'),
                                            'class'     => 'form-control input-md calendario requiredField',
                                            'readonly'  =>'readonly',
                                            'style'     => 'background-color: #fff;'
                                        );
                                        if ($habilita['readonly']) {
                                            $attDateInsertion['style'] = 'background-color: #eee';
                                        }
                                        echo form_input($attDateInsertion);
                                        echo "</div>"
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>
                                
                                <div class="form-group">
                                    <?php 
                                        echo form_label('Data final de veiculação', 'date_insertion_end', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-3'>";
                                        $attTime_insertion = array(
                                            'name'      => 'date_insertion_end',
                                            'id'        => 'date_insertion_end',
                                            'value'     => set_value('date_insertion_end'),
                                            'class'     => 'form-control input-md requiredField calendario',
                                            'readonly'  =>'readonly',
                                            'style'     => 'background-color: #fff;'
                                        );
                                        if ($habilita['readonly']) {
                                            $attTime_insertion['style'] = 'background-color: #eee';
                                        }
                                        echo form_input($attTime_insertion);
                                        echo "</div>"
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>

                                <div class="form-group">
                                    <?php 
                                        echo form_label('Total de inserção', 'total_insertion', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-3'>";
                                        $attTotal_insertion = array(
                                            'name'      => 'total_insertion',
                                            'id'        => 'total_insertion',
                                            'value'     => set_value('total_insertion'),
                                            'class'     => 'form-control input-md requiredField numeric',
                                        );
                                        if ($habilita['readonly']) {
                                            $attTotal_insertion['readonly'] = 'readonly';
                                        }
                                        echo form_input($attTotal_insertion);
                                        echo "</div>"
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>
                                
                                <div class="form-group">
                                    <?php 
                                        echo form_label('Custo da inserção', 'form_media_cost', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-3'>";
                                        $attMedia_cost = array(
                                            'name'      => 'form_media_cost',
                                            'id'        => 'form_media_cost',
                                            'value'     => set_value('form_media_cost'),
                                            'class'     => 'form-control input-md requiredField',
                                        );
                                        if ($habilita['readonly']) {
                                            $attMedia_cost['readonly'] = 'readonly';
                                        }
                                        echo form_input($attMedia_cost);
                                        echo "</div>"
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>
                                <div class="clear"></div>
                                <?php if ($habilita['habilitaBotao']) : ?>
                                <div style="text-align: center; margin-top: 20px;">
                                        <a id="add_row_media" class="btn btn-primary">
                                            <span class="glyphicon glyphicon-plus-sign"></span> Adicionar Meio de Veiculação
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-3 alert alert-info" role="alert">
                                <b style="text-align: center; display: block">Nota: </b> De acordo com o Decreto 2000/201-20, Portaria X, 
                                é necessário informar pelo menos um plano de mídia
                                dos tipos: TV, Rádio e Jornal para divulgar na campanha de Recall.
                            </div>
                            <div class="col-md-12">
                                <div class="title-form">
                                    <h5>Plano(s) de mídia adicionado(s)</h5>
                                </div>
                                <table class="table table-bordered table-hover tbHeader2" id="tab_media">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Meio de veiculação</th>
                                            <th class="text-center">Veículo de mídia</th>
                                            <th class="text-center">Fonte</th>
                                            <th class="text-center">Data</th>
                                            <th class="text-center">Data Fim</th>
                                            <th class="text-center">Total de inserção</th>
                                            <th class="text-center">Custo</th>
                                            <th class="text-center">Ação</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td colspan="7" id="somaCusto" style="text-align:right">
                                                <b>Custo Total: </b>
                                                <span class='label label-warning' id="valorMonetario"></span>
                                                <input type="hidden" name="somaCusto"/>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php 
                                            if ($media) : 
                                                foreach($media as $key => $value) :
                                        ?>
                                                    <tr id='media<?php echo $key;?>'>
                                                        <input type="hidden" name="id_media_plan[]" value="<?php echo isset($value['id_media_plan']) ? $value['id_media_plan'] : ''; ?>"/>
                                                        <?php echo form_error('id_media_plan[]', '<div class="error">', '</div>'); ?>
                                                        <td style="width: 35px;">
                                                            <?php $origin =  isset($value['origin']) ? $value['origin'] : '';?>
                                                            <input name='origin[]' id="div_origin" title="<?=$origin; ?>" value='<?=$origin; ?>' type='text' 
                                                            readonly class='form-control input-md requiredField  floatLeft'/>
                                                            <?php echo form_error('origin[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td>
                                                            <?php $media =  isset($value['media']) ? $value['media'] : '';?>
                                                            <input name='media[]' id="div_media" title="<?=$media; ?>" value='<?=$media; ?>' type='text' 
                                                                   readonly class='form-control input-md requiredField  floatLeft'/>
                                                            <?php echo form_error('media[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td>
                                                            <?php $source = isset($value['source']) ? $value['source'] : '';?>
                                                            <input name='source[]' id="div_source" title='<?=$source; ?>' value='<?=$source; ?>' type='text' 
                                                            readonly class='form-control input-md requiredField  floatLeft'/>
                                                            <?php $b_source =  isset($value['b_source']) ? $value['b_source'] : '';?>
                                                            <input type="hidden" name="b_source[]" value="<?=$b_source; ?>"/>
                                                            <?php echo form_error('source[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td>
                                                            <?php $date_insertion = (isset($value['date_insertion']))? date("d/m/Y", strtotime($value['date_insertion']['date'])) : '';?>
                                                            <input  name='date_insertion[]' id="div_date_insertion" title='<?=$date_insertion; ?>' value='<?=$date_insertion; ?>' type='text' 
                                                                    readonly class='form-control input-md requiredField' style="width: 110px">
                                                            <?php echo form_error('date_insertion[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td>
                                                            <?php $date_insertion_end = isset($value['date_insertion_end']) ? date("d/m/Y", strtotime($value['date_insertion_end']['date'])) : '';?>
                                                            <input name='date_insertion_end[]' id="div_date_insertion_end" title='<?=$date_insertion_end; ?>' value='<?=$date_insertion_end; ?>' type='text' 
                                                            readonly class='form-control input-md requiredField  floatLeft' style="width: 110px"/>
                                                            <?php echo form_error('date_insertion_end[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td>
                                                            <?php $total_insertion = isset($value['total_insertion']) ? $value['total_insertion'] : '';?>
                                                            <input name='total_insertion[]' id="div_total_insertion" title='<?=$total_insertion; ?>' value='<?=$total_insertion; ?>' type='text' 
                                                            readonly class='form-control input-md requiredField  floatLeft'/>
                                                            <?php echo form_error('total_insertion[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td>
                                                            <?php $media_cost =  isset($value['media_cost']) ? $value['media_cost'] : '';?>
                                                            <input name='media_cost[]' id="div_media_cost" title='<?=$media_cost; ?>' value='<?=$media_cost; ?>' type='text' 
                                                            readonly class='form-control input-md requiredField  floatLeft'/>
                                                            <?php echo form_error('media_cost[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($habilita['habilitaBotao']) : ?>
                                                                <a href="javascript:void(0);" onclick="removeItemMedia(<?php echo ($key+1);?>,<?php echo $value['id_media_plan'];?>)" class="removeItemColumn" title='Remover Item'>
                                                                    <img src="<?php echo base_url();?>img/fi-x.svg" width="20px">
                                                                </a>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                            <?php endforeach; ?>
                                                    <tr id='media<?php echo ($totMedia);?>'>
                                            <?php else : ?>
                                                    <tr id='media0'>
                                                        <td colspan="8" class="noFound" id="noMedia">Nenhum plano de mídia cadastrado</td>
                                                    </tr>
                                                    <tr id='media1'></tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                        <div style="text-align:center;">
                            <a class='btn btn-default'
                                href="<?php echo base_url();?>recalls/insertLocation">
                                 <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                            </a>
                            <label for="css3-tabstrip-0-2" class="btn btn-default">
                                Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                            </label>
                        </div>
                    </div>
                </li>
                <li>
                    <input type="radio" name="css3-tabstrip-0" id="css3-tabstrip-0-2" />
                    <label for="css3-tabstrip-0-2" class="labelAba">Anexar Arquivos e Links</label>
                    <div class="dvformulario">
                        <div class="col-xs-12">
                            <div class="col-md-10">
                                <div class="title-form">
                                    <h4>Arquivos / Links Anexados</h4>
                                    <p>
                                        <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                                        <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                                    </p>
                                </div>
                                <table class="table table-bordered table-hover tbHeader" id="addFile">
                                    <thead>
                                        <tr>
                                            <th>
                                                Arquivos
                                            </th>
                                            <th>
                                                Ação
                                            </th>
                                            <th>
                                                <?php if ($habilita['habilitaBotao']) : ?>
                                                    <button class="btn btn-primary btn-sm" type="button" onclick="addMediaFile()">
                                                        <span class="glyphicon glyphicon-plus-sign"></span> Adicionar
                                                    </button>
                                                <?php endif; ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($files) : 
                                            foreach ($files as $f => $file) :
                                    ?>
                                            <tr>
                                                <td>
                                                    <?php 
                                                        $fileName = explode('/', $file['file_path']);
                                                        $id_file = $file['id_file'];
                                                        echo "<a href='javascript:void()' onClick='downloadFile(".$id_file.")'>".end($fileName)."</a>";
                                                    ?>
                                                </td>
                                                <td style="width: 5%!important ">
                                                    <?php if ($habilita['habilitaBotao']) : ?>
                                                        <img src='<?php echo base_url();?>img/fi-x.svg' width='20px'
                                                         onClick='removeFile(this,<?php echo $file['id_file']; ?>);' title='Exclur Arquivo' style='cursor:pointer'/>
                                                    <?php endif; ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                    <?php   endforeach; ?>
                                        <?php else : ?>
                                            <tr id="noFile">
                                                <td>
                                                    <input type='file' name='userfile[]'/>
                                                    <?php echo form_error('userfile[]', '<div class="error">', '</div>'); ?>
                                                </td>
                                                <td style="width: 5%!important ">
                                                    <?php if ($habilita['habilitaBotao']) : ?>
                                                        <img src='<?php echo base_url();?>img/fi-x.svg' width='20px'
                                                         onClick='removeFile(this);' title='Exclur Arquivo' style='cursor:pointer'/>
                                                    <?php endif; ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                    <?php
                                        endif;
                                    ?>
                                    </tbody>
                                </table>
                                
                                <table class="table table-bordered table-hover tbHeader" id="addLink">
                                    <thead>
                                        <tr>
                                            <th>
                                                Sites Relacionados
                                            </th>
                                            <th>
                                                Ação
                                            </th>
                                            <th>
                                                <?php if ($habilita['habilitaBotao']) : ?>
                                                    <button class="btn btn-primary btn-sm" type="button" onclick="addMediaLink()">
                                                        <span class="glyphicon glyphicon-plus-sign"></span> Adicionar
                                                    </button>
                                                <?php endif; ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($links) : 
                                            foreach ($links as $l => $link) :
                                    ?>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo $link['url']; ?>" target="blank">
                                                        <?php echo $link['url']; ?>
                                                    </a>
                                                </td>
                                                <td style="width: 5%!important ">
                                                    <?php if ($habilita['habilitaBotao']) : ?>
                                                        <img src='<?php echo base_url();?>img/fi-x.svg' width='20px'
                                                         onClick='removeLink(this,<?php echo $link['id_link']; ?>);' title='Exclur Link' style='cursor:pointer'/>
                                                    <?php endif; ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <?php else : ?>
                                            <tr id="noLink">
                                                <td>
                                                    <label style='float: left; padding: 7px;'>Url:</label>
                                                    <div class='col-md-2'>
                                                        <input type="text" name="http" value="http://" readonly class="form-control input-mini"/>
                                                    </div>
                                                    <div class='col-md-8' style="margin-left: -30px !important">
                                                        <?php
                                                            $url = array(
                                                                'name'      => 'url[]',
                                                                'value'     => set_value('url'),
                                                                'class'     => 'form-control',
                                                            );
                                                            echo form_input($url);
                                                        ?>
                                                    </div>
                                                </td>
                                                <td style="width: 5%!important ">
                                                    <?php if ($habilita['habilitaBotao']) : ?>
                                                        <img src='<?php echo base_url();?>img/fi-x.svg' width='20px'
                                                         onClick='removeLink(this);' title='Exclur Arquivo' style='cursor:pointer'/>
                                                    <?php endif; ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                    <?php
                                        endif;
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                            <div class="btnNext">
                                <label for="css3-tabstrip-0-1" class="btn btn-default">
                                    <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                                </label>

                                <?php if ($habilita['habilitaBotao']) : ?>
                                    <button class="btn btn-success" type="submit" 
                                            id="btn_media" name="btn_media">Salvar
                                    </button>
                                <?php endif; ?>

                                <?php if ($time_line > TIME_LINE_MEDIA) : ?>
                                    <a class='btn btn-default'
                                        href="<?php echo base_url();?>recalls/insertRisk">
                                        Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                <?php endif; ?>
                            </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
<?php echo form_close();
