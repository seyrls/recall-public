<?php 
    # total de lote
    $totBatch = count($batch);
    
    # total de quantidades afetadas
    $totAffected = count($affecteds);
?>
<script>
    $(document).ready(function(){
        $("input[name='year_manufacture']").mask("9999");
        
        $("select[name='branch_id']").change(function(){
            var branch_id = $("select[name='branch_id'] option:selected").attr('value');
            $("#type_product_id").html("<option value=''>selecione</option>");
            if (branch_id.length > 0) {
                var urlpath = '<?php echo base_url();?>';
                $("#type_product_id").removeAttr('disabled');
                $.ajax({
                    url: urlpath+'type_products/loadCombo/',
                    type:'POST',
                    data: 'branch_id='+branch_id,
                    dataType: 'json',
                    success: function( data ) {
                        $.each(data, function(i, value) {
                            $('#type_product_id').append($('<option/>', {
                                value: i,
                                text : value 
                            }));
                        });
                    }
                });
            }
	});
        
        
       //script para criação dinamica dos campos da tabela de lot
        $("#add_row_lote").click(function(){
            var i = ($('#tab_logic tr').length - 1);
            $('#tab_logic').append("<tr id='addr"+(i)+"'><td><input name='initial_batch[]' type='text' class='form-control requiredField input-md'/><input type='hidden' name='batch_id[]'/></td>"+
            "<td><input name='final_batch[]' type='text' class='form-control input-md requiredField'></td>"+
            "<td><input name='manufacturer_initial_date[]' type='text' class='form-control input-md requiredField calendario' readOnly='readOnly' style='background-color: #fff;'></td>"+
            "<td><input name='manufacturer_final_date[]' type='text' class='form-control input-md requiredField calendario' readOnly='readOnly' style='background-color: #fff;'></td>"+
            "<td><input name='expiry_date[]' type='text'  class='form-control input-md calendario' readOnly='readOnly' style='background-color: #fff;'></td>"+
            "<td><a href='javascript:void(0)' onclick='removeItemLote("+(i)+")' title='Remover Item'  class='removeItemColumn'><img src='<?php echo base_url();?>img/fi-x.svg' width='20px'></a></td></tr>"
            );
    
            // inserir plugin de calendario nos campos data para lote
            $('.calendario').datepicker({
              changeMonth: true,
              changeYear: true,
              dateFormat: "dd/mm/yy"
            });
            //i++;
        });

        var total = 0;
        
        $("#quantity_0").blur(function(){
            if ((!isNaN($(this).val())) && ($(this).val().length != 0) && ($(this).attr('readonly') != 'readonly'))  {
                total +=parseInt($(this).val());
                $(this).attr("readonly",true);
                $('input[name="quantity_affected"]').val(total);
            }
        });

        // Adicionar quantidades afetadas
        var a = ($('#tab_quantity tr').length - 2);
        $("#add_row_qtd_uf").click(function(){
            var lastAffected = $(".affected:last").val();
            var lastState = $(".iduf:last").val();
            
            if (typeof lastAffected === 'undefined') {
                lastAffected = a;
            }
            
            if (lastState == 0) {
                alert('Favor selecione um estado.');
                return false;
            }
            if (lastAffected.length == 0) {
                alert('Favor informe a quantidade do estado selecionado.');
                return false;
            }
            
            $('#tab_quantity').append("<tr id='pro"+a+"'><td><select name='iduf[]' id='iduf_"+a+"' class='form-control iduf'>"+
                "<?php foreach ($comboState as $s => $state) : ?>"+
                    "<option value='<?php echo $s;?>'><?php echo $state;?></option>"+
                "<?php endforeach; ?>"+
                "</select></td>"+
                "<td><input name='quantity[]' id='quantity_"+a+"' onblur='alteraTotal("+a+")' type='text' class='form-control input-md affected numeric'/></td>"+
                "<td><a href='javascript:void(0);'  onclick='removeItemQuantidade("+(a+1)+")' class='removeItemColumn' title='Remover Item'><img src='<?php echo base_url();?>img/fi-x.svg' width='20px'></a></td>"+
                "<td></td></tr>"
            );
            
            $("#quantity_"+a).blur(function(){
                if ((!isNaN($(this).val())) && ($(this).val().length != 0) && ($(this).attr('readonly') != 'readonly')){
                    total +=parseInt($(this).val());
                }
            });
            
            $("#quantity_"+(a-1)).attr("readonly",true);
            a++;
       });
    });
    
    function alteraTotal(key){
        var totalAtual = parseInt($("input[name='quantity_affected']").val());
        var totalAlterado = 0;
        var tdQuantity = parseInt($("#quantity_"+key).val());
        if ((!isNaN(tdQuantity)) && (tdQuantity.length != 0) && ($("#quantity_"+key).attr('readonly') != 'readonly'))  {
            totalAlterado = (totalAtual+tdQuantity);
            $('input[name="quantity_affected"]').val(totalAlterado);
            $("#quantity_"+(key)).attr("readonly",true);
        }
    }
    
    function removeItemLote(i,id) {
        var rowCount = $('#tab_logic tr').length;
        if (rowCount > 2){
            $('#addr'+(i)+'').remove();
            $("#tab_logic").append("<input type='hidden' name='deleteBatch[]' value='"+id+"'/>");
        } else {
            alert('É necessário cadastrar pelo menos um lote.');
        }
    }
    function removeItemQuantidade(i, id) {
        var rowCount = $('#tab_quantity tr').length;
        if (id) {
            $("#tab_quantity").append("<input type='hidden' name='deleteAffected[]' value='"+id+"'/>");
        }
        var quantity = ($("#quantity_"+(i-1)).val() > 0) ? $("#quantity_"+(i-1)).val() : 0 ;
        var total = $("input[name='quantity_affected']").val();
        var novoTotal = (total - quantity);
        
        $("input[name='quantity_affected']").val(novoTotal);
        $('#pro'+(i-1)+'').remove();
    }
    
    function addFile() {
        $("#addImage").append("<tr><td><input type='file' name='userfile[]'/></td><td><img src='<?php echo base_url();?>img/fi-x.svg' onClick='excluirImg(this);' title='Exclur Imagem' style='cursor:pointer; width: 20px;'/></td><td></td></tr>");
    }
    function excluirImg(element,id) {
        var tr = element.parentNode.parentNode; // Get the input's parent <tr>
        tr.parentNode.removeChild(tr);          // Remove the <tr> from it's parent.
        $("#addImage").append("<input type='hidden' name='deleteimg[]' value='"+id+"'/>");
    }
    
</script>

<?php
    $attributes = array('class' => 'formProduct', 'id' => 'formProduct', 'enctype'=>'multipart/form-data');
    $hidden = array('product_id' => (isset($product['product_id'])) ? $product['product_id'] : '');
    echo form_open('recalls/insertProduct', $attributes,$hidden);
?>
    <div class="row setup-content1" id="step-2">
        <div class="css3-tabstrip">
            <ul>
                <li>
                    <input type="radio" name="css3-tabstrip-0" checked="checked" id="css3-tabstrip-0-1" />
                    <label for="css3-tabstrip-0-1" class="labelAba">Novo produto</label>
                    <div class="dvformulario">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <div class="title-form">
                                    <h4>Produto(s)</h4>
                                    <p>
                                        <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                                        <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                                    </p>
                                </div>
                                
                                <div class="form-group">
                                    <?php 
                                        echo form_label('Setor de Atividade', 'branch', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-5'>";
                                        $defaultBranch = isset($product['branch_id']) ? $product['branch_id'] : set_value('branch_id');
                                        echo form_dropdown('branch_id', $comboBranch, $defaultBranch, 'class="form-control requiredField"');
                                        echo form_error('branch_id', '<div class="error">', '</div>');
                                        echo "</div>"
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>
                                
                                <div class="form-group">
                                <?php 
                                    echo form_label('Tipo do produto', 'type_product_id', array('class' => 'col-md-4 control-label'));
                                    echo "<div class='col-md-5'>";
                                    $defaultType = isset($product['type_product_id']) ? $product['type_product_id'] : set_value('type_product_id');
                                    echo form_dropdown('type_product_id', $comboTypeProduct, $defaultType, 'class="form-control requiredField" id="type_product_id"');
                                    echo form_error('type_product_id', '<div class="error">', '</div>');
                                    echo "</div>"
                                 ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>
                                
                                <div class="form-group">
                                    <?php 
                                        echo form_label('Nome do produto', 'product', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-5'>";
                                        $attProduct = array(
                                            'name'      => 'product',
                                            'id'        => 'product',
                                            'value'     => isset($product['product']) ? $product['product'] : set_value('product'),
                                            'class'     => 'form-control input-md requiredField',
                                        );
                                        if ($habilita['readonly']) {
                                            $attProduct['readonly'] = 'readonly';
                                        }
                                        echo form_input($attProduct);
                                        echo form_error('product', '<div class="error">', '</div>');
                                        echo "</div>"
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>

                                <div class="form-group">
                                    <?php 
                                        echo form_label('Fabricante', 'manufacturer_id', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-5'>";
                                        $defaultManufact = isset($product['manufacturer_id']) ? $product['manufacturer_id'] : set_value('manufacturer_id');
                                        echo form_dropdown('manufacturer_id', $comboManufacturer, $defaultManufact, 'class="form-control requiredField"');
                                        echo "</div>";
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>
                                
                                <div class="form-group">
                                    <?php 
                                        echo form_label('Pais de origem', 'country_id', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-5'>";
                                        $defaultCountry = isset($product['country_id']) ? $product['country_id'] : set_value('country_id');
                                        echo form_dropdown('country_id', $comboCountry, $defaultCountry, 'class="form-control requiredField"');
                                        echo "</div>";
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>
                                
                                <div class="form-group">
                                    <?php 
                                        echo form_label('Pais de exportação', 'country_export_id', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-5'>";
                                        $defaultCountryExport = isset($product['country_export_id']) ? $product['country_export_id'] : set_value('country_export_id');
                                        echo form_dropdown('country_export_id', $comboCountry, $defaultCountryExport, 'class="form-control"');
                                        echo "</div>";
                                     ?>
                                </div>

                                <div class="form-group">
                                    <?php 
                                        echo form_label('Modelo', 'model', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-5'>";
                                        $attModel = array(
                                            'name'      => 'model',
                                            'id'        => 'model',
                                            'value'     => (isset($product['model'])) ? $product['model'] : set_value('model'),
                                            'class'     => 'form-control input-md requiredField',
                                        );
                                        if ($habilita['readonly']) {
                                            $attModel['readonly'] = 'readonly';
                                        }
                                        echo form_input($attModel);
                                        echo form_error('model', '<div class="error">', '</div>');
                                        echo "</div>"
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>

                                <div class="form-group">
                                    <?php 
                                        echo form_label('Ano de fabricação', 'year_manufacture', array('class' => 'col-md-4 control-label'));
                                        echo "<div class='col-md-2'>";
                                        $attYear = array(
                                            'name'      => 'year_manufacture',
                                            'id'        => 'year_manufacture',
                                            'value'     => (isset($product['year_manufacture'])) ? $product['year_manufacture'] : set_value('year_manufacture'),
                                            'class'     => 'form-control input-md requiredField',
                                        );
                                        if ($habilita['readonly']) {
                                            $attYear['readonly'] = 'readonly';
                                        }
                                        echo form_input($attYear);
                                        echo form_error('year_manufacture', '<div class="error">', '</div>');
                                        echo "</div>"
                                     ?>
                                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                </div>

                              
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="userfile">Imagens do produto</label>
                                    <div class="col-md-5">
                                        <table class="tbHeader" id="addImage">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Imagem
                                                    </th>
                                                    <th>
                                                        Ação
                                                    </th>
                                                    <th>
                                                        <?php if ($habilita['habilitaBotao']) : ?>
                                                            <button class="btn btn-primary btn-sm" type="button" onclick="addFile()">
                                                                <span class="glyphicon glyphicon-plus-sign"></span> Adicionar
                                                            </button>
                                                        <?php endif; ?>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php if (isset($product['image'])) :
                                                    foreach ($product['image'] as $image) : 
                                            ?>
                                                    <tr>
                                                        <td>
                                                            <img src="<?php echo $image['path'];?>" width="50px"/>
                                                        </td>
                                                        <td>
                                                            <?php if ($habilita['habilitaBotao']) : ?>
                                                                <img src='<?php echo base_url();?>img/fi-x.svg' onClick='excluirImg(this,"<?php echo basename($image['id']);?>");' title='Exclur Imagem' style='cursor:pointer; width: 20px;'/>
                                                             <?php endif; ?>
                                                                <input type='hidden' name='userfile[]' value="<?php echo basename($image['path']); ?>"/>
                                                                <?php echo form_error('userfile[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                            <?php   endforeach; 
                                                else :
                                            ?>
                                                    <tr>
                                                        <td>
                                                            <input type='file' <?php echo $habilita['disabled'];?> name='userfile[]'/>
                                                            <?php echo form_error('userfile[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($habilita['habilitaBotao']) : ?>
                                                                <img src='<?php echo base_url();?>img/fi-x.svg' onClick='excluirImg(this);' title='Exclur Imagem' style='cursor:pointer; width: 20px;'/>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                                                        </td>
                                                    </tr>
                                            <?php
                                                endif;
                                            ?>
                                                    <tr>
                                                        <td id='apagar'></td>
                                                    </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="text-align:center;">
                            <a class='btn btn-default'
                                href="<?php echo base_url();?>recalls/insertInfo">
                                 <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                            </a>
                            <label for="css3-tabstrip-0-2" class="btn btn-default">
                                Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                            </label>
                        </div>
                    </div>
                </li><!-- fim aba 1 -->

                <!-- aba 2 -->
                <li>
                    <input type="radio" name="css3-tabstrip-0" id="css3-tabstrip-0-2" />
                    <label for="css3-tabstrip-0-2" class="labelAba">Lote do produto</label>
                    <div>
                        <!-- Lote do produto -->
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <div class="title-form">
                                    <h4>Lote do produto</h4>
                                    <p>
                                        <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                                        <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                                    </p>
                                </div>
                                <table class="table table-bordered table-hover tbLoteProduct" id="tab_logic">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Nº lote inicial</th>
                                            <th class="text-center">Nº lote final</th>
                                            <th class="text-center">Data inicio de fabricação</th>
                                            <th class="text-center">Data fim de fabricação</th>
                                            <th class="text-center">Validade</th>
                                            <th class="text-center">Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 

                                        $count = count(set_value("initial_batch"));
                                        if ($count > 0 && validation_errors()) {
                                            for ($i=0; $i<$count; $i++) :
                                        ?>
                                        
                                        <tr id='addr<?=$i;?>'>
                                            <td>
                                                <?php 
                                                $attBatch = array(
                                                    'name'      => 'initial_batch['.$i.']',
                                                    'value'     => set_value("initial_batch[".$i."]"),
                                                    'class'     => 'form-control input-md requiredField  floatLeft',
                                                );
                                                if ($habilita['readonly']) {
                                                    $attBatch['readonly'] = 'readonly';
                                                }
                                                echo form_input($attBatch);
                                                ?>
                                                <input type="hidden" name="batch_id[]" value="<?php echo isset($bat['batch_id']) ? $bat['batch_id'] : ''; ?>"/>
                                                <?php echo form_error('initial_batch['.$i.']', '<div class="error">', '</div>'); ?>
                                            </td>
                                            <td>
                                                <?php 
                                                    $attBatchFim = array(
                                                        'name'      => 'final_batch['.$i.']',
                                                        'value'     => set_value("final_batch[".$i."]"),
                                                        'class'     => 'form-control input-md requiredField  floatLeft',
                                                    );
                                                    if ($habilita['readonly']) {
                                                        $attBatchFim['readonly'] = 'readonly';
                                                    }
                                                    echo form_input($attBatchFim);
                                                    echo form_error('final_batch['.$i.']', '<div class="error">', '</div>'); 
                                                ?>
                                            </td>
                                            <td>
                                                <?php 
                                                    $attDateIni = array(
                                                        'name'      => 'manufacturer_initial_date['.$i.']',
                                                        'value'     => set_value("manufacturer_initial_date[".$i."]"),
                                                        'class'     => 'form-control input-md requiredField calendario',
                                                        'readonly'  =>'readonly',
                                                        'style'     => 'background-color: #fff;'
                                                    );
                                                    if ($habilita['readonly']) {
                                                        $attDateExp['style'] = 'background-color: #eee';
                                                    }

                                                    echo form_input($attDateIni);
                                                    echo form_error('manufacturer_initial_date['.$i.']', '<div class="error">', '</div>'); 
                                                ?>
                                            </td>
                                            <td>
                                                <?php 
                                                    $attDateFim = array(
                                                        'name'      => 'manufacturer_final_date['.$i.']',
                                                        'value'     => set_value("manufacturer_final_date[".$i."]"),
                                                        'class'     => 'form-control input-md requiredField calendario',
                                                        'readonly'  =>'readonly',
                                                        'style'     => 'background-color: #fff;'
                                                    );
                                                    if ($habilita['readonly']) {
                                                        $attDateExp['style'] = 'background-color: #eee';
                                                    }

                                                    echo form_input($attDateFim);
                                                    echo form_error('manufacturer_final_date['.$i.']', '<div class="error">', '</div>'); 
                                                ?>
                                            </td>
                                            <td>
                                                <?php 
                                                    $attDateExp = array(
                                                        'name'      => 'expiry_date['.$i.']',
                                                        'value'     => set_value("expiry_date[".$i."]"),
                                                        'class'     => 'form-control input-md calendario',
                                                        'readonly'  =>'readonly',
                                                        'style'     => 'background-color: #fff;'
                                                    );
                                                    if ($habilita['readonly']) {
                                                        $attDateExp['style'] = 'background-color: #eee';
                                                    }

                                                    echo form_input($attDateExp);
                                                    echo form_error('expiry_date['.$i.']', '<div class="error">', '</div>'); 
                                                ?>
                                            </td>
                                            <td>
                                                <?php if ($habilita['habilitaBotao']) : ?>
                                                    <a href="javascript:void(0);" onclick="removeItemLote(<?=($i);?>)" class="removeItemColumn" title='Remover Item'>
                                                        <img src="<?php echo base_url();?>img/fi-x.svg" width="20px">
                                                    </a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <?php
                                            endfor;
                                        } else {
                                            if ($batch) :
                                                foreach ($batch as $kb => $bat) : 
                                        ?>
                                                    <tr id='addr<?=$kb;?>'>
                                                        <td>
                                                            <?php 
                                                            $attBatch = array(
                                                                'name'      => 'initial_batch[]',
                                                                'value'     => isset($bat['initial_batch']) ? $bat['initial_batch'] : '',
                                                                'class'     => 'form-control input-md requiredField  floatLeft',
                                                            );
                                                            if ($habilita['readonly']) {
                                                                $attBatch['readonly'] = 'readonly';
                                                            }
                                                            echo form_input($attBatch);
                                                            ?>
                                                            <input type="hidden" name="batch_id[]" value="<?php echo isset($bat['batch_id']) ? $bat['batch_id'] : ''; ?>"/>
                                                            <?php echo form_error('initial_batch[]', '<div class="error">', '</div>'); ?>
                                                        </td>
                                                        <td>
                                                            <?php 
                                                                $attBatchFim = array(
                                                                    'name'      => 'final_batch[]',
                                                                    'value'     => isset($bat['final_batch']) ? $bat['final_batch'] : '',
                                                                    'class'     => 'form-control input-md requiredField  floatLeft',
                                                                );
                                                                if ($habilita['readonly']) {
                                                                    $attBatchFim['readonly'] = 'readonly';
                                                                }
                                                                echo form_input($attBatchFim);
                                                                echo form_error('final_batch[]', '<div class="error">', '</div>'); 
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php 
                                                                $attDateIni = array(
                                                                    'name'      => 'manufacturer_initial_date[]',
                                                                    'value'     => (isset($bat['manufacturer_initial_date'])) ? date("d/m/Y", strtotime($bat['manufacturer_initial_date']['date'])) : '',
                                                                    'class'     => 'form-control input-md requiredField calendario',
                                                                    'readonly' =>'readonly',
                                                                    'style'     => 'background-color: #fff;'
                                                                );
                                                                if ($habilita['readonly']) {
                                                                    $attDateExp['style'] = 'background-color: red !important';
                                                                }
                                                                echo form_input($attDateIni);
                                                                echo form_error('manufacturer_initial_date[]', '<div class="error">', '</div>'); 
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php 
                                                                $attDateFim = array(
                                                                    'name'      => 'manufacturer_final_date[]',
                                                                    'value'     => (isset($bat['manufacturer_final_date'])) ? date("d/m/Y", strtotime($bat['manufacturer_final_date']['date'])) : '',
                                                                    'class'     => 'form-control input-md requiredField calendario',
                                                                    'readonly'  => 'readonly',
                                                                    'style'     => 'background-color: #fff;'
                                                                );
                                                                if ($habilita['readonly']) {
                                                                    $attDateExp['style'] = 'background-color: #eee';
                                                                }
                                                                echo form_input($attDateFim);
                                                                echo form_error('manufacturer_final_date[]', '<div class="error">', '</div>'); 
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php 
                                                                $attDateExp = array(
                                                                    'name'      => 'expiry_date[]',
                                                                    'value'     => (isset($bat['expiry_date'])) ? date("d/m/Y", strtotime($bat['expiry_date']['date'])) : '',
                                                                    'class'     => 'form-control input-md requiredField calendario',
                                                                    'readonly'  =>'readonly',
                                                                    'style'     => 'background-color: #fff;'
                                                                );
                                                                if ($habilita['readonly']) {
                                                                    $attDateExp['style'] = 'background-color: #eee';
                                                                }
                                                                echo form_input($attDateExp);
                                                                echo form_error('expiry_date[]', '<div class="error">', '</div>'); 
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($habilita['habilitaBotao']) : ?>
                                                                <a href="javascript:void(0);" onclick="removeItemLote(<?php echo ($kb);?>,<?php echo $bat['batch_id']; ?>)" class="removeItemColumn" title='Remover Item'>
                                                                    <img src="<?php echo base_url();?>img/fi-x.svg" width="20px">
                                                                </a>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                        <?php else : ?>
                                            <tr id='addr0'>
                                                <td>
                                                    <?php 
                                                    $attBatch = array(
                                                        'name'      => 'initial_batch[]',
                                                        'value'     => set_value("initial_batch[]"),
                                                        'class'     => 'form-control input-md requiredField  floatLeft',
                                                    );
                                                    if ($habilita['readonly']) {
                                                        $attBatch['readonly'] = 'readonly';
                                                    }
                                                    echo form_input($attBatch);
                                                    ?>
                                                    <input type="hidden" name="batch_id[]" value="<?php echo isset($bat['batch_id']) ? $bat['batch_id'] : ''; ?>"/>
                                                    <?php echo form_error('initial_batch[]', '<div class="error">', '</div>'); ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $attBatchFim = array(
                                                            'name'      => 'final_batch[]',
                                                            'value'     => set_value("final_batch[]"),
                                                            'class'     => 'form-control input-md requiredField  floatLeft',
                                                        );
                                                        if ($habilita['readonly']) {
                                                            $attBatchFim['readonly'] = 'readonly';
                                                        }
                                                        echo form_input($attBatchFim);
                                                        echo form_error('final_batch[]', '<div class="error">', '</div>'); 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $attDateIni = array(
                                                            'name'      => 'manufacturer_initial_date[]',
                                                            'value'     => set_value("manufacturer_initial_date[]"),
                                                            'class'     => 'form-control input-md requiredField calendario',
                                                            'readonly'  =>'readonly',
                                                            'style'     => 'background-color: #fff;'
                                                        );
                                                        if ($habilita['readonly']) {
                                                            $attDateExp['style'] = 'background-color: #eee';
                                                        }
                                                        
                                                        echo form_input($attDateIni);
                                                        echo form_error('manufacturer_initial_date[]', '<div class="error">', '</div>'); 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $attDateFim = array(
                                                            'name'      => 'manufacturer_final_date[]',
                                                            'value'     => set_value("manufacturer_final_date[]"),
                                                            'class'     => 'form-control input-md requiredField calendario',
                                                            'readonly'  =>'readonly',
                                                            'style'     => 'background-color: #fff;'
                                                        );
                                                        if ($habilita['readonly']) {
                                                            $attDateExp['style'] = 'background-color: #eee';
                                                        }
                                                        
                                                        echo form_input($attDateFim);
                                                        echo form_error('manufacturer_final_date[]', '<div class="error">', '</div>'); 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $attDateExp = array(
                                                            'name'      => 'expiry_date[]',
                                                            'value'     => set_value("expiry_date[]"),
                                                            'class'     => 'form-control input-md calendario',
                                                            'readonly'  =>'readonly',
                                                            'style'     => 'background-color: #fff;'
                                                        );
                                                        if ($habilita['readonly']) {
                                                            $attDateExp['style'] = 'background-color: #eee';
                                                        }
                                                        
                                                        echo form_input($attDateExp);
                                                        echo form_error('expiry_date[]', '<div class="error">', '</div>'); 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php if ($habilita['habilitaBotao']) : ?>
                                                        <a href="javascript:void(0);" onclick="removeItemLote(0)" class="removeItemColumn" title='Remover Item'>
                                                            <img src="<?php echo base_url();?>img/fi-x.svg" width="20px">
                                                        </a>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php 
                                            endif; 
                                            }
                                        ?>
                                    </tbody>
                                </table>
                                <?php if ($habilita['habilitaBotao']) : ?>
                                    <button class="btn btn-primary btn-sm floatRight" type="button" id="add_row_lote">
                                        <span class="glyphicon glyphicon-plus-sign"></span> Adicionar
                                    </button>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="btnNext">
                            <label for="css3-tabstrip-0-1" class="btn btn-default">
                                <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                            </label>
                            <label for="css3-tabstrip-0-3" class="btn btn-default">
                                Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                            </label>
                        </div>
                    </div>
                </li><!-- fim aba 2 -->

                <!-- aba 3 -->
                <li>
                    <input type="radio" name="css3-tabstrip-0" id="css3-tabstrip-0-3" />
                    <label for="css3-tabstrip-0-3" class="labelAba">Quantidade afetada por UF</label>
                    <div>
                        <div class="col-xs-12">
                                <div class="title-form">
                                    <h4>Quantidade afetada por UF</h4>
                                    <p>
                                        <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                                        <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                                    </p>
                                </div>
                                <div class="col-md-10">
                                <table class="table tbHeader table-bordered table-hover" id="tab_quantity">
                                    <thead>
                                        <tr>
                                            <th>UF</th>
                                            <th>Quantidade</th>
                                            <th>Ação</th>
                                            <th>
                                                <?php if ($habilita['habilitaBotao']) : ?>
                                                    <button class="btn btn-primary btn-sm" type="button" id="add_row_qtd_uf">
                                                        <span class="glyphicon glyphicon-plus-sign"></span> Adicionar
                                                    </button>
                                                <?php endif; ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $countQnt = count(set_value("quantity"));
                                            $totalAffected = 0;
                                            if ($countQnt > 0 && validation_errors()) :
                                                $totalAffected = array_sum($this->input->post('quantity'));
                                                for ($q=0; $q<$countQnt; $q++) :
                                        ?>
                                            <tr id='pro<?php echo ($q); ?>'>
                                                <td>
                                                    <?php echo form_dropdown('iduf[]', $comboState, set_value('iduf['.$q.']'), 'class="form-control iduf" '.$habilita['disabled']); ?>
                                                    <input type="hidden" name="affected_id[]" value="<?=set_value('affected_id['.$q.']'); ?>"/>
                                                    <?php echo form_error('affected_id['.$q.']', '<div class="error">', '</div>'); ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $attQuantity = array(
                                                            'name'      => 'quantity['.$q.']',
                                                            'id'        => 'quantity_'.$q,
                                                            'value'     => set_value('quantity['.$q.']'),
                                                            'class'     => 'form-control input-md requiredField numeric',
                                                            //'readonly'  => 'readonly',
                                                            'onblur'    => 'alteraTotal('.$q.')'
                                                        );
                                                        echo form_input($attQuantity);
                                                        echo form_error('quantity['.$q.']', '<div class="error">', '</div>'); 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php if ($habilita['habilitaBotao']) : ?>
                                                        <a href="javascript:void(0);" 
                                                           onclick="removeItemQuantidade(<?=($q+1);?>)" 
                                                           class="removeItemColumn" title='Remover Item'>
                                                            <img src="<?php echo base_url();?>img/fi-x.svg" width="20px">
                                                        </a>
                                                    <?php endif; ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <?php 
                                                endfor; 
                                            else :
                                                if ($affecteds) : 
                                                    $totalAffected = $product['quantity_affected'];
                                            ?>
                                            <?php foreach ($affecteds as $ka => $affected) : ?>
                                            <tr id='pro<?php echo ($ka); ?>'>
                                                <td>
                                                    <?php echo form_dropdown('iduf[]', $comboState, $affected['iduf'], 'class="form-control iduf" '.$habilita['disabled']); ?>
                                                    <input type="hidden" name="affected_id[]" value="<?php echo isset($affected['affected_id']) ? $affected['affected_id'] : ''; ?>"/>
                                                    <?php echo form_error('affected_id[]', '<div class="error">', '</div>'); ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $attQuantity = array(
                                                            'name'      => 'quantity[]',
                                                            'id'        => 'quantity_'.$ka,
                                                            'value'     => ($affected['quantity']) ? $affected['quantity'] : set_value('quantity[]'),
                                                            'class'     => 'form-control input-md requiredField numeric',
                                                            'readonly'  => 'readonly',
                                                            'onblur'    => 'alteraTotal('.$ka.')'
                                                        );
                                                        echo form_input($attQuantity);
                                                        echo form_error('quantity[]', '<div class="error">', '</div>'); 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php if ($habilita['habilitaBotao']) : ?>
                                                        <a href="javascript:void(0);" 
                                                           onclick="removeItemQuantidade(<?php echo ($ka+1);?>,<?php echo $affected['affected_id']; ?>)" 
                                                           class="removeItemColumn" title='Remover Item'>
                                                            <img src="<?php echo base_url();?>img/fi-x.svg" width="20px">
                                                        </a>
                                                    <?php endif; ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php endforeach; ?>
                                            <tr id='pro<?=($totAffected);?>'>
                                        <?php else : ?>
                                            <tr id='pro0'>
                                                <td>
                                                    <?php echo form_dropdown('iduf[]', $comboState, set_value('iduf[]'), 'class="form-control iduf requiredField"  id="iduf_0"'); ?>
                                                    <?php echo form_error('iduf[]', '<div class="error">', '</div>'); ?>
                                                </td>
                                                <td>
                                                    <input name='quantity[]' id="quantity_0" type='text' class='form-control numeric input-md affected requiredField'/>
                                                    <?php echo form_error('quantity[]', '<div class="error">', '</div>'); ?>
                                                </td>
                                                <td>
                                                    <?php if ($habilita['habilitaBotao']) : ?>
                                                        <a href="javascript:void(0);" onclick="removeItemQuantidade(0)" class="removeItemColumn" title='Remover Item'>
                                                            <img src="<?php echo base_url();?>img/fi-x.svg" width="20px">
                                                        </a>
                                                    <?php endif; ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr id="pro1"></tr>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <tr id='pro1'></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                            <td id="somaAfetados" class="totalRows">
                                                <span> Total: </span>
                                                <?php
                                                    $attQuantityAffected = array(
                                                        'name'      => 'quantity_affected',
                                                        'value'     => $totalAffected,
                                                        'class'     => 'form-control floatRight',
                                                        'readonly'  => 'readonly',
                                                        'style'     => 'width: 80px; margin-left: 10px;'
                                                    );
                                                    echo form_input($attQuantityAffected);
                                                    echo form_error('quantity_affected', '<div class="error">', '</div>'); 
                                                ?>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                      </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="btnNext">
                            <label for="css3-tabstrip-0-2" class="btn btn-default">
                                <span class="glyphicon glyphicon-chevron-left"></span> Voltar</label>
                            <?php if ($time_line > TIME_LINE_PRODUCT) : ?>
                                    <a class='btn btn-default' class="novoProduto" 
                                        href="<?php echo base_url();?>recalls/listsProducts">
                                        Visualizar Produtos
                                    </a>
                                    <a class='btn btn-default' 
                                        href="<?php echo base_url(); ?>recalls/insertLocation">
                                        Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                            <?php endif; ?>
                            <?php if ($habilita['habilitaBotao']) : ?>
                                <button class="btn btn-success" type="submit" id="btn_product" name="btn_product">
                                    Salvar
                                </button>
                            <?php endif; ?>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    <?php echo form_close(); ?>
</div>