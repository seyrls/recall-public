<script>
    $(document).ready(function(){
        
       $("input[name='protocol']").mask("99999.999999/9999-99");
        
       $("input:radio[name=report_accident]").click(function() {
            var value = $(this).val();
            if (value == 1) {
                $("#showAccident").show();
            } else {
                $("#showAccident").hide();
            }
        });
    });
</script>    
    <?php
        $attributes = array('class' => 'formCampaign', 'id' => 'formCampaign', 'enctype'=>'multipart/form-data');
        echo form_open('recalls/insertCampaign', $attributes);
    ?>
        <input type="hidden" name="id" value="<?=isset($recall['id']) ? $recall['id'] : NULL;?>"/>
            <div class="col-xs-12 dvformulario">
                <div class="title-form">
                    <h4>Dados da Campanha</h4>
                    <p>
                        <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                        <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                    </p>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?php 
                            echo form_label('Fornecedor', 'trade_name', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            if (($user['level'] == LEVEL_ADMINISTRADOR) || ($user['level'] == LEVEL_OPERADOR)){
                                $supplier_id = ($this->input->post('supplier_id')) ? $this->input->post('supplier_id') : $supplier['supplier_id'];
                                $default = ($supplier_id) ? $supplier_id : '';
                                echo form_dropdown('supplier_id', $comboSupplier, $default, 'class="form-control requiredField"'.$habilita['disabled']);
                            } else {
                                $arrSupplier = array(
                                    'name'      => 'trade_name',
                                    'id'        => 'trade_name',
                                    'value'     => ($supplier['trade_name']) ? $supplier['trade_name']  : set_value('trade_name'),
                                    'class'     => 'form-control input-md requiredField',
                                    'readonly'  =>'true',
                                );
                                echo form_input($arrSupplier);
                                echo "<input type='hidden' name='supplier_id' value='".$supplier['supplier_id']."'";
                            }
                            echo form_error('supplier_id', '<div class="error">', '</div>');
                            echo "</div>";
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo form_label('Titulo', 'title', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attTitle = array(
                                'name'      => 'title',
                                'id'        => 'title',
                                'value'     => ($recall) ? $recall['title'] : set_value('title'),
                                'class'     => 'form-control input-md requiredField',
                            );
                            if ($habilita['readonly']) {
                                $attTitle['readonly'] = 'readonly';
                            }
                            
                            
                            echo form_input($attTitle);
                            echo form_error('title', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        <img src="<?php echo base_url();?>img/help.png" width="12px"
                             title="Informe um titulo no campo ao lado para esta campanha"/>
                    </div>

                    <?php if ($user['level'] == LEVEL_ADMINISTRADOR) : ?>
                        <div class="form-group">
                            <?php 
                                echo form_label('Nº do protocolo', 'protocol', array('class' => 'col-md-4 control-label'));
                                echo "<div class='col-md-5'>";
                                $attProtocol = array(
                                    'name'      => 'protocol',
                                    'id'        => 'protocol',
                                    'value'     => ($recall) ? $recall['protocol'] : set_value('protocol'),
                                    'class'     => 'form-control input-md',
                                );
                                if ($habilita['readonly']) {
                                    $attProtocol['readonly'] = 'readonly';
                                }
                                echo form_input($attProtocol);
                                echo form_error('protocol', '<div class="error">', '</div>');
                                echo "</div>"
                             ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($recall['start_date']['date'])) : ?>
                        <div class="form-group">
                            <?php 
                                echo form_label('Data de início', 'start_date', array('class' => 'col-md-4 control-label'));
                                echo "<div class='col-md-2'>";
                                $start_date = array(
                                    'name'      => 'start_date',
                                    'id'        => 'start_date',
                                    'value'     => (isset($recall['start_date']['date'])) ? date("d/m/Y", strtotime($recall['start_date']['date'])) : '',
                                    'class'     => 'form-control input-md',
                                    'readonly' => TRUE
                                );
                                echo form_input($start_date);
                                echo "</div>"
                             ?>
                            <img src="<?php echo base_url();?>img/help.png" width="12px"
                                 title="A data de início será registrada automaticamente após o envio da campanha"/>
                        </div>
                    <?php endif; ?>
                    
                    <?php if (isset($recall['end_date']['date'])) : ?>
                        <div class="form-group">
                            <?php 
                                echo form_label('Data de término', 'end_date', array('class' => 'col-md-4 control-label'));
                                echo "<div class='col-md-2'>";
                                $end_date = array(
                                    'name'      => 'end_date',
                                    'id'        => 'end_date',
                                    'value'     => (isset($recall['end_date']['date'])) ? date("d/m/Y", strtotime($recall['end_date']['date'])) : '',
                                    'class'     => 'form-control input-md',
                                    'readonly' => TRUE
                                );
                                echo form_input($end_date);
                                echo "</div>"
                             ?>
                        </div>
                    <?php endif; ?>

                    <div class="form-group">
                        <?php 
                            echo form_label('Descrição', 'description', array('class' => 'col-md-4 control-label', 'style' => 'height: 120px; line-height: 100px;'));
                            echo "<div class='col-md-5'>";
                            $description = array(
                                'name'      => 'description',
                                'id'        => 'description',
                                'value'     => ($recall) ? $recall['description'] : set_value('description'),
                                'class'     => 'form-control input-md requiredField',
                                'rows'      => '4',
                            );
                            
                            if ($habilita['readonly']) {
                                $description['readonly'] = 'readonly';
                            }
                            
                            echo form_textarea($description);
                            echo form_error('description', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        <img src="<?php echo base_url();?>img/help.png" width="12px"class="infoCampo" 
                             title='Informe no campo ao lado os detalhes sobre a campanha desejada.'/>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Medida de correção', 'corrective_measure', array('class' => 'col-md-4 control-label labelTextarea'));
                            echo "<div class='col-md-5'>";
                            $corrective_measure = array(
                                'name'      => 'corrective_measure',
                                'id'        => 'corrective_measure',
                                'value'     => ($recall) ? $recall['corrective_measure'] : set_value('corrective_measure'),
                                'class'     => 'form-control input-md requiredField',
                                'rows'      => '4',
                            );
                            if ($habilita['readonly']) {
                                $corrective_measure['readonly'] = 'readonly';
                            }
                            echo form_textarea($corrective_measure);
                            echo form_error('corrective_measure', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    </div>

                    <div class="form-group">
                        <?php
                        
                            $report_yes = FALSE;
                            $report_no = TRUE;
                            
                            if ($this->input->post('report_accident')) {
                                $report_yes = ($this->input->post('report_accident') == 1) ? TRUE : FALSE;
                                $report_no = ($this->input->post('report_accident') == 0) ? TRUE : FALSE;
                            } else if ($recall) {
                                $report_yes = ($recall['report_accident'] == 1) ? TRUE : FALSE;
                                $report_no = ($recall['report_accident'] == 0) ? TRUE : FALSE;
                            }
                            echo form_label('Relato de acidentes?', 'report_accident', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            echo '<div style="float: left; padding: 10px;">'.form_radio ('report_accident', 1, $report_yes,$habilita['disabled']).' Sim </div>';
                            echo '<div style="float: left; padding: 10px;">'.form_radio ('report_accident', 0, $report_no,$habilita['disabled']).' Não </div>';
                            echo form_error('report_accident', '<div class="error">', '</div>');
                            echo "</div>";
                        ?>
                    </div>
                    <?php 
                        if ($report_yes) {
                            $booShow  = TRUE;
                        } else {
                            $booShow  = FALSE;
                        }
                    ?>
                    <div class="form-group" id="showAccident" style="<?php echo ($booShow) ? 'display:block' : 'display:none';?>">
                        <?php 
                            echo form_label('Descrição do acidente', 'reason_accident', array('class' => 'col-md-4 control-label labelTextarea'));
                            echo "<div class='col-md-5'>";
                            $reason_accident = array(
                                'name'      => 'reason_accident',
                                'id'        => 'reason_accident',
                                'value'     => ($recall) ? $recall['reason_accident'] : set_value('reason_accident'),
                                'class'     => 'form-control input-md requiredField',
                                'rows'      => '4',
                            );
                            if ($habilita['readonly']) {
                                $reason_accident['readonly'] = 'readonly';
                            }
                            echo form_textarea($reason_accident);
                            echo form_error('reason_accident', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    </div>
                </div>
            
                <div class="clear"></div>
                <div class="btnNext">
                    <?php if ($habilita['habilitaBotao']) : ?>
                        <button class="btn btn-success" type="submit" id="campaign" name="submit" value="campaign">
                            <span class="glyphicon glyphicon-plus-sign"></span> Salvar
                        </button>
                    <?php endif; ?>
                    <?php if ($this->session->userdata('recall')) : ?>
                        <a class='btn btn-default' href="<?php echo base_url(); ?>recalls/insertInfo">
                            Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
<?php echo form_close();