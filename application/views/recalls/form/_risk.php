<?php if (!$habilita['habilitaBotao']) :  ?>
    <script>
        $(document).ready(function(){
            $("#message").attr("disabled", "disabled");
        });
    </script>
<?php endif; ?>
<script>
    function addFile() {
        $("#addFile").append("<tr><td><input type='file' name='userfile[]'/></td><td><img src='<?php echo base_url();?>img/fi-x.svg' width='20px' onClick='removeFile(this);' title='Exclur Arquivo' style='cursor:pointer'/></td><td></td></tr>");
    }
    function removeFile(element,id) {
        var tr = element.parentNode.parentNode;
        tr.parentNode.removeChild(tr);
        if (id) {
            $("#addFile").append("<input type='hidden' name='deleteFile[]' value='"+id+"'/>");
        }
    }
    function downloadFile(file_id){
        var urlpath = '<?php echo base_url();?>';
        window.location.href=""+urlpath+"recalls/downloadFile/"+file_id;
    }
</script>
        
    
    
<?php
    $aviso_risco = ($this->input->post('notice_risk')) ? $this->input->post('notice_risk') : '';
    $notice_risk = ($notice_risk) ? $notice_risk : '';
    $attributes = array('class' => 'formRisk', 'id' => 'formRisk', 'enctype'=>'multipart/form-data');
    echo form_open('recalls/insertRisk', $attributes);
?>
        <div class="col-xs-12 dvformulario">
            <div class="title-form">
                <h4>Aviso de risco</h4>
                <p>
                    <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                    <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                </p>
                <p style="font-size: 14px">
                    Informe no editor de texto abaixo a MENSAGEM VEICULADA
                </p>
            </div>
            <div class="col-md-12">
                <div id="editor">
                    <textarea id="message" name="notice_risk">
                        <?php echo ($aviso_risco) ? $aviso_risco : $notice_risk; ?>
                    </textarea>
                    <?php echo form_error('notice_risk', '<div class="error">', '</div>'); ?>
                    <?php echo display_ckeditor($ckeditor); ?>
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                </div>
                
                
                
                <table class="table table-bordered table-hover tbHeader" id="addFile" style="margin-top: 30px;">
                    <thead>
                        <tr>
                            <th>
                                Arquivos anexados à Mensagem Veiculada
                            </th>
                            <th>
                                Ação
                            </th>
                            <th>
                                <?php if ($habilita['habilitaBotao']) : ?>
                                    <button class="btn btn-primary btn-sm" type="button" onclick="addFile()">
                                        <span class="glyphicon glyphicon-plus-sign"></span> Adicionar
                                    </button>
                                <?php endif; ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if ($files) : 
                            foreach ($files as $f => $file) :
                    ?>
                            <tr>
                                <td>
                                    <?php 
                                        $fileName = explode('/', $file['file_path']);
                                        $id_file = $file['id_file'];
                                        echo "<a href='javascript:void()' onClick='downloadFile(".$id_file.")'>".end($fileName)."</a>";
                                    ?>
                                </td>
                                <td style="width: 5%!important ">
                                    <?php if ($habilita['habilitaBotao']) : ?>
                                        <img src='<?php echo base_url();?>img/fi-x.svg' width='20px'
                                         onClick='removeFile(this,<?php echo $file['id_file']; ?>);' title='Exclur Arquivo' style='cursor:pointer'/>
                                    <?php endif; ?>
                                </td>
                                <td></td>
                            </tr>
                    <?php   endforeach; ?>
                        <?php else : ?>
                            <tr id="noFile">
                                <td>
                                    <input type='file' name='userfile[]'/>
                                    <?php echo form_error('userfile[]', '<div class="error">', '</div>'); ?>
                                </td>
                                <td style="width: 5%!important ">
                                    <?php if ($habilita['habilitaBotao']) : ?>
                                        <img src='<?php echo base_url();?>img/fi-x.svg' width='20px'
                                         onClick='removeFile(this);' title='Exclur Arquivo' style='cursor:pointer'/>
                                    <?php endif; ?>
                                </td>
                                <td></td>
                            </tr>
                    <?php
                        endif;
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="clear"></div>
            <hr/>
            <div class="btnNext">
                <a class='btn btn-default'
                    href="<?php echo base_url();?>recalls/insertMedia">
                     <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                </a>
                
                <?php if ($habilita['habilitaBotao']) : ?>
                    <button class="btn btn-success" type="submit" id="btn_noticerisk" name="btn_noticerisk">
                         Salvar
                    </button>
                <?php endif; ?>
                
                <?php if ($time_line > TIME_LINE_RISK) : ?>
                    <a class='btn btn-default'
                        href="<?php echo base_url();?>recalls/insertProcess">
                        Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                <?php endif; ?>
            </div>
        </div>
<?php 
    echo form_close();