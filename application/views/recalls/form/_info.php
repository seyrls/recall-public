    <?php
        $attributes = array('class' => 'formInfo', 'id' => 'formInfo', 'enctype'=>'multipart/form-data');
        echo form_open('recalls/insertInfo', $attributes);
    ?>
            <div class="col-xs-12 dvformulario">
                <div class="title-form">
                    <h4>Informações Técnicas / Defeitos</h4>
                    <p>
                        <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                        <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                    </p>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?php 
                            echo form_label('Componente', 'faulty_part', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attFaulty_part = array(
                                'name'      => 'faulty_part',
                                'id'        => 'faulty_part',
                                'value'     => ($dados['faulty_part']) ? $dados['faulty_part'] : set_value('faulty_part'),
                                'class'     => 'form-control input-md requiredField',
                            );
                            if ($habilita['readonly']) {
                                $attFaulty_part['readonly'] = 'readonly';
                            }
                            echo form_input($attFaulty_part);
                            echo form_error('faulty_part', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        <img src="<?php echo base_url();?>img/help.png" width="12px"
                             title="Informe o componente defeituoso."/>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Data da constatação', 'date_faulty', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-2'>";
                            $attDate_faulty = array(
                                'name'      => 'date_faulty',
                                'id'        => 'date_faulty',
                                'value'     => ($dados['date_faulty']) ? date("d/m/Y", strtotime($dados['date_faulty']['date'])) : set_value('date_faulty'),    
                                'class'     => 'form-control input-md calendario_prev requiredField',
                                'readonly'  =>'readonly',
                                'style'     => 'background-color: #fff;'
                            );
                            if ($habilita['readonly']) {
                                $attDate_faulty['style'] = 'background-color: #eee';
                            }
                            
                            echo form_input($attDate_faulty);
                            echo form_error('date_faulty', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Defeito', 'faulty', array('class' => 'col-md-4 control-label labelTextareaSmall'));
                            echo "<div class='col-md-5'>";
                            $attFaulty = array(
                                'name'      => 'faulty',
                                'id'        => 'faulty',
                                'value'     => ($dados['faulty']) ? $dados['faulty'] : set_value('faulty'),
                                'class'     => 'form-control input-md requiredField',
                                'rows'      => '3',
                            );
                            if ($habilita['readonly']) {
                                $attFaulty['readonly'] = 'readonly';
                            }
                            echo form_textarea($attFaulty);
                            echo form_error('faulty', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        <img src="<?php echo base_url();?>img/help.png" width="12px"
                             title="Apresente a descrição pormenorizada do defeito constatado."/>
                    </div>
                    
                    <div class="form-group">
                        <?php 
                            echo form_label('Tipo de risco', 'type_risk', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $type_risk_id = ($this->input->post('type_risk_id')) ? $this->input->post('type_risk_id') : $dados['type_risk_id'];
                            $default = ($type_risk_id) ? $type_risk_id : '';
                            echo form_dropdown('type_risk_id', $comboTypeRisk, $default, 'class="form-control requiredField"'.$habilita['disabled']);
                            echo form_error('type_risk_id', '<div class="error">', '</div>');
                            echo "</div>";
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        <img src="<?php echo base_url();?>img/help.png" width="12px"
                             title="Selecione o risco predominante"/>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Descrição do risco', 'risk_faulty', array('class' => 'col-md-4 control-label labelTextareaSmall'));
                            echo "<div class='col-md-5'>";
                            $attRisk = array(
                                'name'      => 'risk_faulty',
                                'id'        => 'risk_faulty',
                                'value'     => ($dados['risk_faulty']) ? $dados['risk_faulty'] : set_value('risk_faulty'),
                                'class'     => 'form-control input-md requiredField',
                                'rows'      => '3',
                            );
                            if ($habilita['readonly']) {
                                $attRisk['readonly'] = 'readonly';
                            }
                            echo form_textarea($attRisk);
                            echo form_error('risk_faulty', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        <img src="<?php echo base_url();?>img/help.png" width="12px"
                             title="Apresente a descrição pormenorizada dos riscos envolvidos ao consumidor"/>
                    </div>
                    
                    <div class="form-group">
                        <?php 
                            echo form_label('Implicações do risco', 'faulty_describe', array('class' => 'col-md-4 control-label labelTextareaSmall'));
                            echo "<div class='col-md-5'>";
                            $attFaulty_describe = array(
                                'name'      => 'faulty_describe',
                                'id'        => 'faulty_describe',
                                'placeholder' => 'Ex: Morte, Lesão, Fratura, Torção, Queimadura...',
                                'value'     => ($dados['faulty_describe']) ? $dados['faulty_describe'] : set_value('faulty_describe'),
                                'class'     => 'form-control input-md requiredField',
                                'rows'      => '3',
                            );
                            if ($habilita['readonly']) {
                                $attFaulty_describe['readonly'] = 'readonly';
                            }
                            echo form_textarea($attFaulty_describe);
                            echo form_error('faulty_describe', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        <img src="<?php echo base_url();?>img/help.png" width="12px"
                             title="Informe as consequências do risco."/>
                    </div>
                    
                </div>
                <div class="clear"></div>
                <div class="btnNext">
                    <a class='btn btn-default'
                        href="<?php echo base_url();?>recalls/editCampaign">
                         <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                    </a>
                    
                    <?php if ($habilita['habilitaBotao']) : ?>
                        <button class="btn btn-success" type="submit" id="info" name="submit">
                            <span class="glyphicon glyphicon-plus-sign"></span> Salvar
                        </button>
                    <?php endif; ?>
                    
                    <?php if ($dados['faulty_part']) : ?>
                        <a class='btn btn-default' 
                            href="<?php echo base_url(); ?>recalls/listsProducts">
                            Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
<?php echo form_close();