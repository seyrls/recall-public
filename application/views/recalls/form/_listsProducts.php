<script>
    function editarProduto(id) {
        $("#formProduct_"+id).submit();
    }
    function excluirProduto(product_id, produto) {   
        var rowCount = $('.product').length;
        if (rowCount < 2) {
            alert('É necessário cadastrar pelo menos um produto.');
            return false;
        } else {
            if (confirm("Deseja excluir o produto "+produto+" ?")) {
                if (product_id) {
                    var urlpath = $('body').data('baseurl');
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: urlpath+"recalls/removeProduct/"+product_id,
                        data: { product_id: product_id} ,
                        success: function(data) {
                            alert(data);
                            location.reload();
                        },
                        error: function(result) {
                            alert("Error");
                        }
                    });
                }
            }
        }
    }
</script>

    <div class="css3-tabstrip formContent">
        <ul>
            <li>
                <input type="radio" name="css3-tabstrip-0" checked="checked" id="css3-tabstrip-0-0" />
                <label for="css3-tabstrip-0-0" class="labelAba">Produto(s) cadastrado(s)</label>
                <?php if ($product) : ?>
                    <div id="produtos" class="classDisabled">
                        <div id="listarProduto">
                            <?php foreach ($product as $key => $value) : 
                                    $attributes = array('id' => 'formProduct', 'id' => 'formProduct_'.$value['product_id']);
                                    $hidden = array('edit' => $value['product_id']);
                                    echo form_open('recalls/insertProduct', $attributes, $hidden);
                                    $id = $value['product_id'];
                                    $produto = (strlen($value['product']) > 25) ? substr($value['product'], 0, 25).'...' : $value['product'];
                            ?>
                                        <div class="product">
                                            <?php if ($habilita['habilitaBotao']) : ?>
                                                <div class="linksDivProduto">
                                                    <a href="javascript:void(0)" onclick="editarProduto(<?php echo $id;?>);">
                                                        <img src="<?php echo base_url();?>img/i_edit_product.png" title="Editar Produto"width="18px"/>
                                                    </a>
                                                    <a href="javascript:void(0)" onclick="excluirProduto(<?php echo $id;?>,'<?php echo $value['product'];?>')">
                                                        <img src="<?php echo base_url();?>img/i_remove.png" title="Excluir produto" width="18px"/>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                            <p>Produto: <b><?php echo $produto; ?></b> <br/>
                                                Fabricante: <b><?php echo $value['corporate_name']; ?></b><br/>
                                            Pais de origem: <b><?php echo $value['country']; ?></b></p>
                                            <?php if (isset($value['image'])) : ?>
                                                <?php foreach ($value['image'] as $image) : ?>
                                                    <ul>
                                                        <li><img src="<?php echo $image;?>"/></li>
                                                    </ul>
                                                <?php endforeach; ?>
                                            <?php else : ?>
                                                    <img src="<?php echo base_url();?>img/default_product.png" class="imgProductDefault"/>
                                            <?php endif; ?>
                                        </div>
                            <?php 
                                    echo form_close();
                                endforeach; 
                            ?>
                        </div>
                        <div class="clear"></div>
                        <div class="btnNext">
                            <a class='btn btn-default'
                                href="<?php echo base_url();?>recalls/insertInfo">
                                 <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                            </a>
                            <?php if ($habilita['habilitaBotao']) : ?>
                                <a class='btn btn-success' class="novoProduto" 
                                   href="<?php echo base_url();?>recalls/insertProduct">
                                    <span class="glyphicon glyphicon-plus-sign"></span> Adicionar Produto
                                </a>
                            <?php endif; ?>
                            <a class='btn btn-default' 
                               href="<?php echo base_url();?>recalls/insertLocation">
                                Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            </li>
        </ul>
    </div>