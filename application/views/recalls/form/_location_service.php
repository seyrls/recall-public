<script>
    $(document).ready(function(){
        var phone_service = $("input[name='phone_service']").val();
        if (phone_service.substring(0, 4) === '0800') {
            $("input[name='phone_service']").mask("9999 999 9999")
            $('input:radio[name="type_phone"][value="0800"]').prop('checked', true);
        } else {
            $("input[name='phone_service']").mask("(99) 9999-9999?9");
            $('input:radio[name="type_phone"][value="local"]').prop('checked', true);
        }
        $("input:radio[name=type_phone]").click(function() {
            var value = $(this).val();
            if (value == '0800') {
                $("input[name='phone_service']").mask("9999 999 9999");
            } else {
                $("input[name='phone_service']").mask("(99) 9999-9999?9");
            }
        });
    });
    
</script>

<?php
    $attributes = array('class' => 'formLocation', 'id' => 'formLocation', 'enctype'=>'multipart/form-data');
    echo form_open('recalls/insertLocation', $attributes);
?>
<div class="row setup-contentTESTE" id="step-3">
        <!-- step 3 -->
            <div class="col-xs-12 dvformulario">
                <div class="title-form">
                    <h4>Dados do Local de Atendimento</h4>
                    <p>
                        <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                        <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                    </p>
                </div>
                <div class="col-md-12">

                    <div class="form-group">
                        <?php 
                            echo form_label('Local de atendimento', 'customer_service', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attCustomer = array(
                                'name'      => 'customer_service',
                                'id'        => 'customer_service',
                                'value'     => set_value('customer_service'),
                                'value'     => ($dados['customer_service']) ? $dados['customer_service'] : set_value('customer_service'),
                                'class'     => 'form-control input-md requiredField',
                            );
                            if ($habilita['readonly']) {
                                $attCustomer['readonly'] = 'readonly';
                            }
                            echo form_input($attCustomer);
                            echo form_error('customer_service', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    </div>
                    
                    <div class="form-group">
                        <?php 
                            echo form_label('Endereço', 'address_service', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attAddress = array(
                                'name'      => 'address_service',
                                'id'        => 'address_service',
                                'value'     => ($dados['address_service']) ? $dados['address_service'] : set_value('address_service'),
                                'class'     => 'form-control input-md',
                            );
                            if ($habilita['readonly']) {
                                $attAddress['readonly'] = 'readonly';
                            }
                            echo form_input($attAddress);
                            echo form_error('address_service', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                    </div>
                    
                    <div class="form-group">
                        <?php 
                            echo form_label('Sitio eletrônico', 'site', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attSite = array(
                                'name'      => 'site',
                                'id'        => 'site',
                                'value'     => ($dados['site']) ? $dados['site'] : set_value('site'),
                                'placeholder' => 'http://',
                                'class'     => 'form-control input-md',
                            );
                            if ($habilita['readonly']) {
                                $attSite['readonly'] = 'readonly';
                            }
                            echo form_input($attSite);
                            echo form_error('site', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/help.png" width="12px"
                             title="Informe o protocolo http:// ou https:// ao inserir o sítio eletrônico"/>
                    </div>
                    
                    <div class="form-group">
                        <?php 
                            echo form_label('Correio eletrônico', 'email', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-5'>";
                            $attEmail = array(
                                'name'      => 'email',
                                'id'        => 'email',
                                'value'     => ($dados['email']) ? $dados['email'] : set_value('email'),
                                'class'     => 'form-control input-md',
                            );
                            if ($habilita['readonly']) {
                                $attEmail['readonly'] = 'readonly';
                            }
                            echo form_input($attEmail);
                            echo form_error('email', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Telefone', 'phone_service', array('class' => 'col-md-4 control-label'));
                            echo "<div class='col-md-3'>";
                            $attPhone = array(
                                'name'      => 'phone_service',
                                'id'        => 'phone_service',
                                'value'     => ($dados['phone_service']) ? $dados['phone_service'] : set_value('phone_service'),
                                'class'     => 'form-control input-small requiredField',
                            );
                            if ($habilita['readonly']) {
                                $attPhone['readonly'] = 'readonly';
                            }
                            echo form_input($attPhone);
                            echo form_error('phone_service', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                        <div class="col-md-2">
                            <input type="radio" name="type_phone" value="local"/>
                            Local <br/>
                            <input type="radio" name="type_phone" value="0800"/>
                            0800
                        </div>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo form_label('Observação', 'notice_service', array('class' => 'col-md-4 control-label labelTextareaSmall'));
                            echo "<div class='col-md-5'>";
                            $attNotice = array(
                                'name'      => 'notice_service',
                                'id'        => 'notice_service',
                                'value'     => ($dados['notice_service']) ? $dados['notice_service'] : set_value('notice_service'),
                                'class'     => 'form-control input-md',
                                'rows'      => '3',
                            );
                            if ($habilita['readonly']) {
                                $attNotice['readonly'] = 'readonly';
                            }
                            echo form_textarea($attNotice);
                            echo form_error('notice_service', '<div class="error">', '</div>');
                            echo "</div>"
                         ?>
                    </div>
                </div><!-- col-md-12 -->
                <div class="clear"></div>
                <div class="btnNext">
                    <a class='btn btn-default'
                        href="<?php echo base_url();?>recalls/listsProducts">
                         <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                    </a>
                    
                    <?php if ($habilita['habilitaBotao']) : ?>
                        <button class="btn btn-success" type="submit" id="btn_customer" name="btn_customer">Salvar</button>
                    <?php endif; ?>
                        
                    <?php if ($time_line > TIME_LINE_LOCATION) : ?>
                        <a class='btn btn-default' 
                            href="<?php echo base_url();?>recalls/insertMedia">
                            Próximo <span class="glyphicon glyphicon-chevron-right"></span>
                         </a>
                    <?php endif; ?>
                </div>
            </div>
        
    <?php echo form_close(); ?>
        
</div>