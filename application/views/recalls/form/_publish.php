<script>
    $(document).ready(function(){
       $(".checkGroup").click(function() {
            if ($(this).prop("checked")) {
                $("#addGroup").append("<input type='hidden' name='idGroup[]' value='"+$(this).val()+"'/>");
            }
        });
    });
</script>


<?php if ((!$protocol) && ($status != STATUS_ID_PENDENCIA_CADASTRO)) :  ?>
    <script>
        $(document).ready(function(){
            $("#message").attr("disabled", "disabled");
            $("input[name='protocol']").mask("99999.999999/9999-99");
        });
    </script>
<?php endif; 
    $attributes = array('class' => 'formPublish', 'id' => 'formPublish', 'enctype'=>'multipart/form-data');
    echo form_open($action, $attributes);
?>

    <input type='hidden' name='mailto' value="<?php echo $mailto; ?>"/>
    <input type='hidden' name='mailcopy' value="<?php echo $mailcopy; ?>"/>

        <div class="col-xs-12 dvformulario">
            <div class="title-form">
                <h4><?php echo $titulo; ?></h4>
                <p style="font-size: 14px">
                    Envio de e-mail
                </p>
            </div>
            <div class="col-md-12">
                <?php 
                if ($status != STATUS_ID_PENDENCIA_CADASTRO) :
                    if ($protocol) :
                        echo '<span class="label label-primary"> Nº do protocolo: '.$protocol.'</span>';
                    else :
                 ?>
                <div class="form-group">
                    <?php 
                        echo form_label('Nº do protocolo', 'protocol', array('class' => 'col-md-4 control-label'));
                        echo "<div class='col-md-3'>";
                        $attProtocol = array(
                            'name'      => 'protocol',
                            'id'        => 'protocol',
                            'value'     => set_value('protocol'),
                            'class'     => 'form-control input-md requiredField',
                        );
                        echo form_input($attProtocol);
                        echo form_error('protocol', '<div class="error">', '</div>');
                        echo "</div>"
                     ?>
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    <button class="btn btn-success btn-sm" type="submit">
                        <span class="glyphicon glyphicon-plus-sign"></span> Salvar
                    </button>
                </div>
            </div>
                <?php endif; ?>
            <?php endif; ?>
                <div class="clear"></div>
                <div class="dvSumaryRecall">
                    <div class="tituloSumary <?php echo $classTitulo; ?>">
                        <h4><?php echo $titulo; ?></h4>
                    </div>
                    <div class="alert <?php echo $classNote; ?>" role="alert">
                        <b style="text-align: center; display: block">Aviso: </b>
                        <?php echo $txtNote; ?> <br/>
                    </div>
                    <div class="clear"></div>
                    
                    <div id="editor" style="margin-top: 10px;">
                        <textarea id="message" name="message">
                            <?php echo ($message) ? $message : ''; ?>
                        </textarea>
                        <?php echo form_error('message', '<div class="error">', '</div>'); ?>
                        <?php echo display_ckeditor($ckeditor); ?>
                        <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                    </div>
                </div>
                
                <?php if ($status == STATUS_ID_PUBLICADA_COM_RESSALVA) : ?>
                    <div class="form-group">
                       <?php 
                           echo form_label('Motivo da ressalva', 'reasonReservation', array('class' => 'col-md-4 control-label', 'style' => 'height: 120px; line-height: 100px;'));
                            echo "<div class='col-md-6'>";
                            $description = array(
                                'name'      => 'reasonReservation',
                                'id'        => 'reasonReservation',
                                'value'     => set_value('reasonReservation'),
                                'class'     => 'form-control input-md requiredField',
                                'rows'      => '5',
                            );
                            echo form_textarea($description);
                            echo form_error('reasonReservation', '<div class="error">', '</div>');
                            echo "</div>"
                        ?>
                       <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                       <img src="<?php echo base_url();?>img/help.png" width="12px"class="infoCampo" 
                                 title='Esse motivo será exibido no site'/>
                   </div>
                <?php endif; ?>
                
                <?php if ($status == STATUS_ID_PENDENCIA_CADASTRO) : ?>
                        <table class="table table-bordered table-hover tbHeader" id="addFile" style="margin-top: 30px;">
                            <thead>
                                <tr>
                                    <th>
                                        Arquivos anexados à mensagem de devolução da campanha
                                    </th>
                                    <th>
                                        Ação
                                    </th>
                                    <th>
                                        <button class="btn btn-primary btn-sm" type="button" onclick="addFile()">
                                            <span class="glyphicon glyphicon-plus-sign"></span> Adicionar
                                        </button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if ($files) : 
                                    foreach ($files as $f => $file) :
                            ?>
                                    <tr>
                                        <td>
                                            <?php 
                                                $fileName = explode('/', $file['file_path']);
                                                $id_file = $file['id_file'];
                                                echo "<a href='javascript:void()' onClick='downloadFile(".$id_file.")'>".end($fileName)."</a>";
                                            ?>
                                        </td>
                                        <td style="width: 5%!important ">
                                            <img src='<?php echo base_url();?>img/fi-x.svg' width='20px'
                                             onClick='removeFile(this,<?php echo $file['id_file']; ?>);' title='Exclur Arquivo' style='cursor:pointer'/>
                                        </td>
                                        <td></td>
                                    </tr>
                            <?php   endforeach; ?>
                                <?php else : ?>
                                    <tr id="noFile">
                                        <td>
                                            <input type='file' name='userfile[]'/>
                                            <?php echo form_error('userfile[]', '<div class="error">', '</div>'); ?>
                                        </td>
                                        <td style="width: 5%!important ">
                                            <img src='<?php echo base_url();?>img/fi-x.svg' width='20px'
                                             onClick='removeFile(this);' title='Exclur Arquivo' style='cursor:pointer'/>
                                        </td>
                                        <td></td>
                                    </tr>
                            <?php
                                endif;
                            ?>
                            </tbody>
                        </table>
                    <?php else :  ?>
                        <div class="clear"></div>
                        <div class="col-md-12 row">
                            <div class="col-md-8">
                                <table class="col-md-5 table table-bordered table-hover"
                                       style=" margin-top: 30px; margin-left: -20px">
                                    <thead>
                                        <tr>
                                            <th>Grupos Parceiros</th>
                                            <th style="text-align: center">Enviar Alerta</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if ($groups): 
                                                foreach ($groups as $group) :
                                        ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $group['name']; ?>
                                                        </td>
                                                        <td style="text-align: center">
                                                            <input type="checkbox" class="checkGroup" 
                                                                   value="<?php echo $group['group_id']; ?>"/>
                                                        </td>
                                                    </tr>
                                        <?php
                                                endforeach;
                                            endif;
                                        ?>
                                    </tbody>
                                </table>
                                <div id="addGroup"></div>
                            </div>
                            <div class="col-md-4 alert alert-info" role="alert" style="margin-top: 30px;">
                                <b style="text-align: center; display: block">Aviso: </b> 
                                Marque na tabela ao lado os grupos de empresas parceiras
                                que deseja enviar os alertas de recall dessa campanha.
                            </div>
                        </div>
                    <?php endif; ?>
                    
            </div>
            <div class="clear"></div>
            <?php if (($protocol) || ($status == STATUS_ID_PENDENCIA_CADASTRO)) : ?>
                <div class="btnNext">
                    <a class='btn btn-default' href="<?php echo base_url();?>recalls/view">
                        <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                   </a>
                    <button class="btn btn-success" type="submit">Salvar</button>
                </div>
            <?php else : ?>
                <div class="alert alert-danger" role="alert" style="width: 60%; text-align: center; display: block; margin: auto;">
                    <p><b>Atenção: </b>
                        Esta campanha só poderá ser publicada após o preenchimento 
                        do número do protocolo no campo acima. <br/>
                    </p>
                </div>
            <?php endif; ?>
        </div>
<?php echo form_close();