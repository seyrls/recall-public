<script>
    $(document).ready(function(){
        $(".enviarCampanha").click(function(){
            if (confirm('Deseja enviar a campanha para análise?')) {
                window.location='<?php echo base_url()?>recalls/sendRecall';
            }
        });
        $("#publicar").click(function(){
            window.location='<?php echo base_url()?>recalls/publishApprove';
        });
        $("#publicarRessalva").click(function(){
            window.location='<?php echo base_url()?>recalls/publishApproveReservation';
        });
        $("#devolver").click(function(){
            window.location='<?php echo base_url()?>recalls/publishDisapprove';
        });
        
        $("#publicarSemRessalva").click(function(){
            if (confirm('Deseja alterar a situação dessa campaha para publicada?')) {
                window.location='<?php echo base_url()?>recalls/publishApproveNotReservation';
            }
        });
        
        $('.hideProduct').each(function(i) {
            $('#showBatch_'+i).click(function(event) {
                if ($('#txtShowBatch_'+i).text() == 'Detalhar') {
                    $('#txtShowBatch_'+i).text('Ocultar');
                } else {
                    $('#txtShowBatch_'+i).text('Detalhar');
                }
                event.preventDefault();
                $('img:visible', this).hide().siblings().show();
                $('#dvProductsView_'+i).toggle($('img:visible').is('.upBatch_'+i));
            });
            
            $('#showAffected_'+i).click(function(event) {
                if ($('#txtShowAffected_'+i).text() == 'Detalhar') {
                    $('#txtShowAffected_'+i).text('Ocultar');
                } else {
                    $('#txtShowAffected_'+i).text('Detalhar');
                }
                event.preventDefault();
                $('img:visible', this).hide().siblings().show();
                $('#dvAffected_'+i).toggle($('img:visible').is('.upAffected_'+i));
            });
        });
        $(window).scroll(function(){
            if ($(window).scrollTop() < 300) {
                $('html, body').find('.botao-topo').removeClass('botao-topo-exibe');
                $('html, body').find('.botao-topo').addClass('displayNoneImportant');
            }
            if ($(window).scrollTop() > 300) {
                $('html, body').find('.botao-topo').removeClass('displayNoneImportant');
                $('html, body').find('.botao-topo').addClass('botao-topo-exibe');
            }
        });
        
        $('.botao-flutuante').click(function () {
            var target = $('#wrapper').offset().top - 100;
            $('html, body').animate({scrollTop:target}, 500);
            return false;
        });

        $('.botao-topo .botao-flutuante').hover(function()
            {
                $(this).parent().find('.texto-info-box').addClass('texto-info-box-exibe');
            },
            function()
            {
                $(this).parent().find('.texto-info-box').removeClass('texto-info-box-exibe');
            }
        );
    });
    function downloadFile(file_id){
        var urlpath = '<?php echo base_url();?>';
        window.location.href=""+urlpath+"recalls/downloadFile/"+file_id;
    }
    function imprimir(){
        print();
    }
    
    function arquivar(recall_id){
        if (confirm("Deseja inativar esta campanha de recall?")) {
            var urlpath = '<?php echo base_url();?>';
            window.location.href=""+urlpath+"recalls/arquivar/"+recall_id;
        }
    }
    
    function desarquivar(recall_id){
        if (confirm("Deseja ativar esta campanha de recall?")) {
            var urlpath = '<?php echo base_url();?>';
            window.location.href=""+urlpath+"recalls/desarquivar/"+recall_id;
        }
    }
</script>
<style>
.maxSize img{
    width:100%;
    max-width: 100%;
}
</style>

<div class="col-xs-12 dvformulario">
    <div class="title-form">
        <h4>Enviar Campanha</h4>
        <p style="font-size: 14px">
            Confira os dados da campanha e clique no botão abaixo para poder finalizá-la.
        </p>
        <div style="float:right; margin-top: -50px;">
            <?php 
                // opcao para ativar/inativar campanha de recall
                if ($dados['level'] != LEVEL_FORNECEDOR) :
                    if (($dados['campaign']['status_campaign']) == STATUS_ID_PUBLICADA) : 
            ?>
                        <a href="javascript::void(0);" onclick="arquivar(<?php echo $dados['campaign']['id'];?>);" 
                           style="float:left; margin-right: 10px; ">
                            <img src="<?php echo base_url();?>img/arquivar.png" 
                                 width="24px;" title="Inativar Campanha"/>
                        </a>
            <?php 
                    elseif (($dados['campaign']['status_campaign']) == STATUS_ID_ARQUIVADO) : 
            ?>
                        <a href="javascript::void(0);" onclick="desarquivar(<?php echo $dados['campaign']['id'];?>);" 
                           style="float:left; margin-right: 10px; ">
                            <img src="<?php echo base_url();?>img/desarquivar.png" 
                                 width="24px;" title="Ativar Campanha"/>
                        </a>
            <?php 
                    endif; 
                endif;
            ?>
            <a href="javascript::void(0);" onclick="imprimir();" class="noPrint">
                <img src="<?php echo base_url();?>img/i_print.png" width="24px;" title="Imprimir"/>
            </a>
        </div>
    </div>
    
    <?php if (($dados['campaign']['status_campaign']) == STATUS_ID_PENDENCIA_CADASTRO) : ?>
        <div class="alert alert-danger" role="alert">
            
            <span class="sr-only">Error:</span>
            <b style="text-align: center; display: block;">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                &nbsp; Mensagem de devolução de campanha: </b><br/>
            <?php 
                echo (isset($dados['campaign']['reason_disapprove']))? $dados['campaign']['reason_disapprove'] : "-"; 
                if (isset($dados['disapproveFiles'])) :
                    echo '<br/><b>Arquivo(s) anexado(s):</b> <br/>';
                    foreach ($dados['disapproveFiles'] as $disapproveFile) :
                        $disapproveFileName = explode('/', $disapproveFile['file_path']);
                        $id_disapprove_file = $disapproveFile['id_file'];
                        echo "<a href='javascript:void()' style='margin-right: 20px'
                              onClick='downloadFile(".$id_disapprove_file.")'>".end($disapproveFileName)."</a>";
                    endforeach; 
                endif; 
            ?>
      </div>
    <?php endif; ?>
    
    <div class="dvSumaryRecall">
        <div class="tituloSumary">
            <h4>Dados da Campanha</h4>
            <?php if (($dados['campaign']['status_campaign'] == STATUS_ID_EM_CADASTRAMENTO) || 
                    ($dados['campaign']['status_campaign'] == STATUS_ID_PENDENCIA_CADASTRO)) : ?>
                <a href="<?php echo base_url(); ?>recalls/editCampaign">
                    <img src="<?php echo base_url();?>img/i_edit.svg" width="20px;" title="Editar Dados da Camapnha"/>
                </a>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
        <table class="tbSumaryRecall">
            <tr>
                <td>Titulo: </td>
                <td><?php echo (isset($dados['campaign']['title']))? $dados['campaign']['title'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Situação atual: </td>
                <td><?php echo (isset($dados['status']))? $dados['status'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Protocolo:</td>
                <td><?php echo (isset($dados['campaign']['protocol']))? $dados['campaign']['protocol'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Data Início:</td>
                <td><?php echo (isset($dados['campaign']['start_date']['date']))? "<span class='label label-default'>".date("d/m/Y", strtotime($dados['campaign']['start_date']['date']))."</span>" : ''; ?></td>
            </tr>
            <tr>
                <td>Data Fim:</td>
                <td><?php echo (isset($dados['campaign']['end_date']['date'])) ? "<span class='label label-default'>".date("d/m/Y", strtotime($dados['campaign']['end_date']['date']))."</span>" : ''; ?></td>
            </tr>
            <tr>
                <td>Descrição:</td>
                <td><?php echo (isset($dados['campaign']['description']))? $dados['campaign']['description'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Meidica de Correção:</td>
                <td><?php echo (isset($dados['campaign']['corrective_measure']))? $dados['campaign']['corrective_measure'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Relato de Acidentes:</td>
                <td><?php echo (isset($dados['campaign']['report_accident'])) ? 
                    (($dados['campaign']['report_accident'] == 0) ? 'Nao' : 
                    "Sim. Motivo: ".$dados['campaign']['reason_accident']) : "-"; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="dvSumaryRecall">
        <div class="tituloSumary">
            <h4>Informações Técnicas / Defeitos</h4>
            <?php if (($dados['campaign']['status_campaign'] == STATUS_ID_EM_CADASTRAMENTO) || 
                    ($dados['campaign']['status_campaign'] == STATUS_ID_PENDENCIA_CADASTRO)) : ?>
                <a href="<?php echo base_url(); ?>recalls/insertInfo">
                    <img src="<?php echo base_url();?>img/i_edit.svg" width="20px;" title="Editar dados das informações Técnicas"/>
                </a>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
        <table class="tbSumaryRecall">
            <tr>
                <td>Componente:</td>
                <td><?php echo (isset($dados['info']['faulty_part']))? $dados['info']['faulty_part'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Data da constatação:</td>
                <td><?php echo (isset($dados['info']['date_faulty']['date']))? date("d/m/Y", strtotime($dados['info']['date_faulty']['date'])) : '-'; ?></td>
            </tr>
            <tr>
                <td>Defeito:</td>
                <td><?php echo (isset($dados['info']['faulty']))? $dados['info']['faulty'] : "-"; ?></td>
            </tr>
            
            
            <tr>
                <td>Tipo de risco:</td>
                <td><?php echo (isset($dados['info']['type_risk']))? $dados['info']['type_risk'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Descrição do risco:</td>
                <td><?php echo (isset($dados['info']['risk_faulty']))? $dados['info']['risk_faulty'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Implicações do risco:</td>
                <td><?php echo (isset($dados['info']['faulty_describe']))? $dados['info']['faulty_describe'] : "-"; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="dvSumaryRecall">
        <div class="tituloSumary">
            <h4>Produtos</h4>
            <?php if (($dados['campaign']['status_campaign'] == STATUS_ID_EM_CADASTRAMENTO) || 
                    ($dados['campaign']['status_campaign'] == STATUS_ID_PENDENCIA_CADASTRO)) : ?>
                <a href="<?php echo base_url(); ?>recalls/listsProducts">
                    <img src="<?php echo base_url();?>img/i_edit.svg" width="20px;" title="Editar Dados dos Produtos"/>
                </a>
            <?php endif;?>
        </div>
        <div class="clear"></div>
        <table class="tbSumaryRecall">
            <?php 
                if ($dados['product']) :
                    foreach ($dados['product'] as $kp => $produto) : 
            ?>
            <tr>
                <td colspan="2" class="mergeCel"><b><?php echo $produto['product']; ?></b></td>
            </tr>
            <tr>
                <td>Setor:</td>
                <td><?php echo (isset($produto['branch']))? $produto['branch'] : '-'; ?></td>
            </tr>
            <tr>
                <td>Tipo de Produto:</td>
                <td><?php echo (isset($produto['type_product']))? $produto['type_product'] : '-'; ?></td>
            </tr>
            <tr>
                <td>Fabricante:</td>
                <td><?php echo (isset($produto['corporate_name']))? $produto['corporate_name'] : '-'; ?></td>
            </tr>
            <tr>
                <td>País de origem</td>
                <td><?php echo (isset($produto['country']))? $produto['country'] : '-'; ?></td>
            </tr>
            <tr>
                <td>País de exportação</td>
                <td><?php echo (isset($produto['countryExport']))? $produto['countryExport']->getCountry() : '-'; ?></td>
            </tr>
            <tr>
                <td>Modelo:</td>
                <td><?php echo (isset($produto['model']))? $produto['model'] : '-'; ?></td>
            </tr>
            <tr>
                <td>Ano de fabricação:</td>
                <td><?php echo (isset($produto['year_manufacture']))? $produto['year_manufacture'] : '-'; ?></td>
            </tr>
            <tr>
                <td>Imagens:</td>
                <?php  if (isset($produto['image'])) : ?>
                <td> 
                    <?php foreach ($produto['image'] as $image) : ?>
                    <img src="<?php echo $image;?>" width="50px"/>
                    <?php endforeach; ?>
                </td>
                <?php else : ?>
                <td> - </td>
                <?php endif; ?>
            </tr>
            <tr>
                <td>
                    Quantidade Afetada:
                    <span class="label label-warning">
                        <?php echo (isset($produto['quantity_affected']))? $produto['quantity_affected'] : '-'; ?>
                    </span>
                </td>
                <td>
                <?php if ($produto['affected'][$kp]) : ?>
                    <div id="hideAffected_<?php echo $kp;?>" class="hideProduct">
                        
                        <div id="showAffected_<?php echo $kp;?>">
                            <img src="<?php echo base_url();?>img/fi-plus.svg" name='mostrar' title="Detalhar Quantidades Afetadas"/>
                            <span id="txtShowAffected_<?php echo $kp;?>">Detalhar</span>
                            <img src="<?php echo base_url();?>img/fi-minus.svg" class="upAffected_<?php echo $kp;?>" name='hide' style="display:none" title="Ocultar Quantidades Afetadas"/>
                        </div>

                    </div>
                    <div class="clear"></div>
                    <div id="dvAffected_<?php echo $kp;?>" class="dvProductsView displayNone">
                        <table class="table table-bordered table-hover tbProductView tbAffectedView">
                            <thead>
                                <tr>
                                    <th class="text-center">Estado(s)</th>
                                    <th class="text-center">Quantidade</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php foreach ($produto['affected'][$kp] as $affected) : ?>
                                <tr>
                                    <td><?php echo $affected['state_name']; ?></td>
                                    <td><?php echo $affected['quantity']; ?></td>
                                </tr>
                        <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else :
                        echo '-';
                    endif;
                ?>
                </td>
            </tr>
            <tr>
                <td>Lote do produto:</td>
                <td>
                <?php if ($produto['batch'][$kp]) : ?>
                    <div id="hideProduct_<?php echo $kp;?>" class="hideProduct">
                        <div id="showBatch_<?php echo $kp;?>">
                            <img src="<?php echo base_url();?>img/fi-plus.svg" title="Detalhar Lote"/>
                            <span id="txtShowBatch_<?php echo $kp;?>">Detalhar</span>
                            <img src="<?php echo base_url();?>img/fi-minus.svg" class="upBatch_<?php echo $kp;?>" style="display:none" title="Ocultar Lote"/>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div id="dvProductsView_<?php echo $kp;?>" class="dvProductsView displayNone">
                        <table class="table table-bordered table-hover tbProductView">
                            <thead>
                                <tr>
                                    <th class="text-center">Nº lote inicial</th>
                                    <th class="text-center">Nº lote final</th>
                                    <th class="text-center">Data início</th>
                                    <th class="text-center">Data fim</th>
                                    <th class="text-center">Validade</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php foreach ($produto['batch'][$kp] as $batch) : ?>
                                <tr>
                                    <td><?php echo $batch['initial_batch']; ?></td>
                                    <td><?php echo $batch['final_batch']; ?></td>
                                    <td><?php echo (isset($batch['manufacturer_initial_date'])) ? date("d/m/Y", strtotime($batch['manufacturer_initial_date']['date'])) : '-'; ?></td>
                                    <td><?php echo (isset($batch['manufacturer_final_date'])) ? date("d/m/Y", strtotime($batch['manufacturer_final_date']['date'])) : '-'; ?></td>
                                    <td><?php echo (isset($batch['expiry_date'])) ? date("d/m/Y", strtotime($batch['expiry_date']['date'])) : '-'; ?></td>
                                </tr>
                        <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else :
                        echo '-';
                    endif;
                ?>
                </td>
            </tr>
            <?php 
                    endforeach; 
                endif;
            ?>
        </table>
    </div>
    
    <div class="dvSumaryRecall">
        <div class="tituloSumary">
            <h4>Locais de Atendimento</h4>
            <?php if (($dados['campaign']['status_campaign'] == STATUS_ID_EM_CADASTRAMENTO) || 
                    ($dados['campaign']['status_campaign'] == STATUS_ID_PENDENCIA_CADASTRO)) : ?>
                <a href="<?php echo base_url(); ?>recalls/insertLocation">
                    <img src="<?php echo base_url();?>img/i_edit.svg" width="20px;" title="Editar Dados do Local de Atendimento"/>
                </a>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
        <table class="tbSumaryRecall">
            <tr>
                <td>Local de Atendimento:</td>
                <td><?php echo (isset($dados['location']['customer_service']))? $dados['location']['customer_service'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Sitio eletrônico:</td>
                <td><?php echo (isset($dados['location']['site']))? $dados['location']['site'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Correio eletrônico:</td>
                <td><?php echo (isset($dados['location']['email']))? $dados['location']['email'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Endereço:</td>
                <td><?php echo (isset($dados['location']['address_service']))? ($dados['location']['address_service']) : "-"; ?></td>
            </tr>
            <tr>
                <td>Telefone:</td>
                <td><?php echo (isset($dados['location']['phone_service']))? '('.
                        substr($dados['location']['phone_service'], 0,2).') '.substr($dados['location']['phone_service'], 2) : "-"; ?></td>
            </tr>
            <tr>
                <td>Observação:</td>
                <td><?php echo (isset($dados['location']['notice_service']))? $dados['location']['notice_service'] : "-"; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="dvSumaryRecall">
        <div class="tituloSumary">
            <h4>Plano(s) de Mídia</h4>
            <?php if (($dados['campaign']['status_campaign'] == STATUS_ID_EM_CADASTRAMENTO) || 
                    ($dados['campaign']['status_campaign'] == STATUS_ID_PENDENCIA_CADASTRO)) : ?>
                <a href="<?php echo base_url(); ?>recalls/insertMedia">
                    <img src="<?php echo base_url();?>img/i_edit.svg" width="20px;" title="Editar Dados do Plano de Midia"/>
                </a>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
        <?php 
            if (isset($dados['media'])) :
                $costTotal = 0;
                foreach ($dados['media'] as $key => $media) : 
                    if ($media['media_cost_sum']) {
                        $costTotal += $media['media_cost_sum'];
                    }
        ?>
                    <table class="tbSumaryRecall">
                        <tr>
                            <td colspan="2" class="mergeCel"><b>Plano de Mídia - <?php echo ($key+1); ?></b></td>
                        </tr>
                        <tr>
                            <td>Meio de veiculação: </td>
                            <td><?php echo (isset($media['origin'])) ? ($media['origin']) : "-"; ?></td>
                        </tr>
                        <tr>
                            <td>Veículo de mídia: </td>
                            <td><?php echo (isset($media['media']))? ($media['media']) : "-"; ?></td>
                        </tr>
                        <tr>
                            <td>Fonte: </td>
                            <td><?php echo (isset($media['source']))? ($media['source']) : "-"; ?></td>
                        </tr>
                        <tr>
                            <td>Data de veiculação: </td>
                            <td><?php echo (isset($media['date_insertion']['date']))? date("d/m/Y", strtotime($media['date_insertion']['date'])) : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Total de inserção: </td>
                            <td><?php echo (isset($media['total_insertion']))? $media['total_insertion'] : "-"; ?></td>
                        </tr>
                        <tr>
                            <td>Horário da inserção: </td>
                            <td><?php echo (isset($media['time_insertion']))? $media['time_insertion'] : "-"; ?></td>
                        </tr>
                        <tr>
                            <td>Custo da inserção: </td>
                            <td><?php echo (isset($media['media_cost'])) ? 'R$ '.$media['media_cost'] : "-"; ?></td>
                        </tr>
                    </table>
                <?php endforeach; ?>
                    <table class="tbSumaryRecall">
                        <tr style="border: 1px solid #ddd;">
                            <td style="background-color: #eee">Custo Total de Inserção:</td>
                            <td>
                                <?php
                                    echo "R$ <span class='label label-warning'>".number_format($costTotal,2,',','.')."</span>";
                                ?>
                            </td>
                        </tr>
                        <tr style="border: 1px solid #ddd;">
                            <td style="background-color: #eee">Arquivos de Midia:</td>
                            <?php if (isset($dados['mediaFiles'])) : ?>
                            <td> 
                                <?php 
                                    foreach ($dados['mediaFiles'] as $mediaFile) :
                                        $mediaFileName = explode('/', $mediaFile['file_path']);
                                        $id_media_file = $mediaFile['id_file'];
                                        echo "<a href='javascript:void()' style='margin-right: 10px' onClick='downloadFile(".$id_media_file.")'>".end($mediaFileName)."</a>";
                                    endforeach; 
                                ?>
                            </td>
                            <?php else : ?>
                            <td> - </td>
                            <?php endif; ?>
                        </tr>
                        <tr style="border: 1px solid #ddd;">
                            <td style="background-color: #eee">Links Relacionados:</td>
                            <?php if (isset($dados['links'])) : ?>
                            <td> 
                                <?php foreach ($dados['links'] as $link) : ?>
                                    <a href="<?php echo $link['url']; ?>" target="blank" style="margin-right: 10px">
                                        <?php echo $link['url']; ?>
                                    </a>
                                <?php endforeach; ?>
                            </td>
                            <?php else : ?>
                            <td> - </td>
                            <?php endif; ?>
                        </tr>
                    </table>
                <?php else : ?>
                <table class="tbSumaryRecall">
                    <tr>
                        <td>Meio de veiculação: </td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Veículo de mídia: </td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Fonte: </td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Data de veiculação: </td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Total de inserção: </td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Horário da inserção: </td>
                        <td>-</td>
                    </tr>
                </table>        
        <?php
            endif;
        ?>
    </div>
    
    <div class="dvSumaryRecall">
        <div class="tituloSumary">
            <h4>Aviso de Risco</h4>
            <?php if (($dados['campaign']['status_campaign'] == STATUS_ID_EM_CADASTRAMENTO) || 
                    ($dados['campaign']['status_campaign'] == STATUS_ID_PENDENCIA_CADASTRO)) : ?>
                <a href="<?php echo base_url(); ?>recalls/insertRisk">
                    <img src="<?php echo base_url();?>img/i_edit.svg" width="20px;" title="Editar Dados de Aviso de Risco"/>
                </a>
            <?php endif; ?>
        </div>
        <div class="bgWhite maxSize">
            <?php echo (isset($dados['notice_risk']))? $dados['notice_risk'] : "-"; ?>
        </div>
        <table class="tbSumaryRecall">
            <tr style="border: 1px solid #ddd;">
                <td style="background-color: #eee">Arquivos anexados:</td>
                <?php if (isset($dados['riskFiles'])) : ?>
                <td> 
                    <?php 
                        foreach ($dados['riskFiles'] as $riskFile) :
                            $riskFileName = explode('/', $riskFile['file_path']);
                            $id_risk_file = $riskFile['id_file'];
                            echo "<a href='javascript:void()' style='margin-right: 10px' onClick='downloadFile(".$id_risk_file.")'>".end($riskFileName)."</a>";
                        endforeach; 
                    ?>
                </td>
                <?php else : ?>
                <td> - </td>
                <?php endif; ?>
            </tr>
    </table>
    </div>
    
    <div class="dvSumaryRecall">
        <div class="tituloSumary">
            <h4>Processos Judiciais</h4>
            <?php if (($dados['campaign']['status_campaign'] == STATUS_ID_EM_CADASTRAMENTO) || 
                    ($dados['campaign']['status_campaign'] == STATUS_ID_PENDENCIA_CADASTRO)) : ?>
                <a href="<?php echo base_url(); ?>recalls/insertProcess">
                    <img src="<?php echo base_url();?>img/i_edit.svg" width="20px;" title="Editar Dados dos Processos Judiciais"/>
                </a>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
        <table class="tbSumaryRecall">
            <tr>
                <td>Número do Processo:</td>
                <td><?php echo (isset($dados['process']['process_number']))? $dados['process']['process_number'] : '-'; ?></td>
            </tr>
            <tr>
                <td>Identificação do consumidor:</td>
                <td><?php echo (isset($dados['process']['identification']))? $dados['process']['identification'] : '-'; ?></td>
            </tr>
            <tr>
                <td>Data do Processo:</td>
                <td><?php echo (isset($dados['process']['date_process']['date']))? date("d/m/Y", strtotime($dados['process']['date_process']['date'])) : '-'; ?></td>
            </tr>
            <tr>
                <td>Vara:</td>
                <td><?php echo (isset($dados['process']['staff']))? $dados['process']['staff'] : '-'; ?></td>
            </tr>
            <tr>
                <td>Comarca:</td>
                <td><?php echo (isset($dados['process']['distrito']))? $dados['process']['distrito'] : '-'; ?></td>
            </tr>
            <tr>
                <td>Situação:</td>
                <td><?php echo (isset($dados['process']['situation']))? $dados['process']['situation'] : '-'; ?></td>
            </tr>
            <tr>
                <td>Descrição:</td>
                <td><?php echo (isset($dados['process']['describe_process']))? $dados['process']['describe_process'] : '-'; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="dvSumaryRecall">
        <div class="tituloSumary">
            <h4>Dados do Fornecedor</h4>
            <?php if (($dados['campaign']['status_campaign'] == STATUS_ID_EM_CADASTRAMENTO) || 
                    ($dados['campaign']['status_campaign'] == STATUS_ID_PENDENCIA_CADASTRO)) : ?>
                <?php if ($dados['level'] == LEVEL_FORNECEDOR) : ?>
                    <a href="<?php echo base_url()?>users/view/minha-conta">
                        <img src="<?php echo base_url();?>img/i_edit.svg" width="20px;" title="Editar Dados do Fornecedor"/>
                    </a>
                <?php else : ?>
                    <a href="<?php echo base_url()."users/edit/".$dados['supplier']['user_id'] ;?>">
                        <img src="<?php echo base_url();?>img/i_edit.svg" width="20px;" title="Editar Dados do Fornecedor"/>
                    </a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
        <table class="tbSumaryRecall">
            <tr>
                <td>Usuário Responsável:</td>
                <td><?php echo (isset($dados['supplier']['username']))? $dados['supplier']['username'] : "-"; ?></td>
            </tr>
            <tr>
                <td>E-mail do responsável:</td>
                <td><?php echo (isset($dados['supplier']['email']))? $dados['supplier']['email'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Razão Social:</td>
                <td><?php echo (isset($dados['supplier']['corporate_name']))? $dados['supplier']['corporate_name'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Nome Fantasia:</td>
                <td><?php echo (isset($dados['supplier']['trade_name']))? $dados['supplier']['trade_name'] : "-"; ?></td>
            </tr>
            <tr>
                <td>CNPJ</td>
                <td><?php echo (isset($dados['supplier']['cnpj']))? $dados['supplier']['cnpj'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Inscrição Estadual:</td>
                <td><?php echo (isset($dados['supplier']['state_registration']))? $dados['supplier']['state_registration'] : "-"; ?></td>
            </tr>
            <tr>
                <td>E-mails com Cópia:</td>
                <td><?php echo (isset($dados['supplier']['email_copy']))? $dados['supplier']['email_copy'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Endereço:</td>
                <td><?php echo (isset($dados['supplier']['address']))? ($dados['supplier']['address']) : "-"; ?></td>
            </tr>
            <tr>
                <td>Complemento:</td>
                <td><?php echo (isset($dados['supplier']['complement']))? $dados['supplier']['complement'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Bairro:</td>
                <td><?php echo (isset($dados['supplier']['district']))? $dados['supplier']['district'] : "-"; ?></td>
            </tr>
            <tr>
                <td>CEP:</td>
                <td><?php echo (isset($dados['supplier']['zip']))? $dados['supplier']['zip'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Telefone Comercial:</td>
                <td><?php echo (isset($dados['supplier']['commercial_phone']))? $dados['supplier']['commercial_phone'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Fax:</td>
                <td><?php echo (isset($dados['supplier']['fax_phone']))? $dados['supplier']['fax_phone'] : "-"; ?></td>
            </tr>
            <tr>
                <td>Cidade:</td>
                <td><?php echo (isset($dados['supplier']['city']))? ($dados['supplier']['city']) : "-"; ?></td>
            </tr>
        </table>
    </div>
    <div class="clear"></div>
    <?php if (($dados['campaign']['status_campaign']) == STATUS_ID_EM_CADASTRAMENTO) : ?>
        <div class="btnNext">
            <a class='btn btn-success btn-lg enviarCampanha' href="javascript:void(0)">
                Enviar Campanha
            </a>
        </div>
    <?php elseif (($dados['campaign']['status_campaign']) == STATUS_ID_PENDENCIA_CADASTRO) : ?>
        <div class="btnNext">
            <a class='btn btn-success btn-lg enviarCampanha' href="javascript:void(0)">
                Reenviar Campanha
            </a>
        </div>
    <?php endif; ?>
    
    <?php if ((($dados['campaign']['status_campaign']) == STATUS_ID_AGUARDANDO_PUBLICACAO) && 
                    (($dados['level'] == LEVEL_ADMINISTRADOR) || ($dados['level'] == LEVEL_OPERADOR))) : ?>
    <div class="btnNext" style="padding-top: 10px;">
        <a class='btn btn-danger btn-small' id="devolver" href="javascript:void(0)">
            Devolver Campanha
        </a>
        <a class='btn btn-warning btn-small' id="publicarRessalva" href="javascript:void(0)">
            Publicar Campanha com Ressalva
        </a>
        <a class='btn btn-success btn-small' id="publicar" href="javascript:void(0)">
            Publicar Campanha
        </a>
    </div>
    <?php elseif ((($dados['campaign']['status_campaign']) == STATUS_ID_PUBLICADA_COM_RESSALVA) && 
                    (($dados['level'] == LEVEL_ADMINISTRADOR) || ($dados['level'] == LEVEL_OPERADOR))) : ?>
        <div class="btnNext" style="padding-top: 10px;">
            <a class='btn btn-success btn-small' id="publicarSemRessalva" href="javascript:void(0)">
                Alterar situação da campanha para Publicada
            </a>
        </div>
    <?php endif; ?>
    
</div>

<div class="botao-topo displayNoneImportant">
    <div class="botao-flutuante">
        <img width="40px" src="<?php echo base_url();?>img/i_up_topo.png" title="Voltar para o topo">
    </div>
    <div class="texto-info-box">
            <p>Topo</p>
    </div>				
</div>