<?php 
    $url = $this->uri->segment(2, 0);
    
    $timeLine = TIME_LINE_CAMPAIGN;
    if ($this->session->userdata('recall')) {
        $recall = $this->doctrine->em->getRepository('Entities\Recall')
                       ->find($this->session->userdata('recall'));
    }
    
    if (isset($recall)) :
        $timeLine = $recall->getTimeLine();
?>
        <div class="msg-warnig">
            <p>Cadastro da campanha de recall: <b><?php echo $recall->getTitle(); ?></b></p>
        </div>
<?php endif; ?>

<table class="timeLine">
        <tr>
            <?php 
                $cssCampaignAtual = "";
                if (($url == "editCampaign") || ($url == "insertCampaign")) {
                    $cssCampaignAtual = 'atual';
                }
                if ($timeLine > TIME_LINE_CAMPAIGN) {
                    $cssCampaignAtivo = 'ativo';
                } elseif ($timeLine == TIME_LINE_CAMPAIGN){
                    $cssCampaignAtivo = "aguardando";
                } else {
                    $cssCampaignAtivo = "inativo";
                }
            ?>
            <td class="<?php echo $cssCampaignAtual.' '.$cssCampaignAtivo;?>">
                <?php if ($timeLine >= TIME_LINE_CAMPAIGN) : ?>
                    <a href="javascript:void(0)" onClick="window.location='<?php echo base_url()?>recalls/editCampaign'">
                        Campanha
                    </a>
                <?php else : ?>
                    <p>Campanha</p>
                <?php endif; ?>
            </td>
                
            
            <?php 
                $cssInfoAtual = ($url == "insertInfo") ? 'atual' : '';
                if ($timeLine > TIME_LINE_INFO) {
                    $cssInfoAtivo = 'ativo';
                } elseif ($timeLine == TIME_LINE_INFO){
                    $cssInfoAtivo = "aguardando";
                } else {
                    $cssInfoAtivo = "inativo";
                }
                
            ?>
            <td class="<?php echo $cssInfoAtual.' '.$cssInfoAtivo;?>">
                <?php if ($timeLine >= TIME_LINE_INFO) : ?>
                    <a href="javascript:void(0)" onClick="window.location='<?php echo base_url()?>recalls/insertInfo'">
                        Informações <br/> Técnicas
                    </a>
                <?php else : ?>
                    <p> Informações <br/> Técnicas </p>
                <?php endif; ?>
            </td>

            
            <?php
                $cssProdutoAtual = "";
                if (($url == "listsProducts") || ($url == "insertProduct")) {
                    $cssProdutoAtual = 'atual';
                }
                
                if ($timeLine > TIME_LINE_PRODUCT) {
                    $cssProdutoAtivo = 'ativo';
                } elseif ($timeLine == TIME_LINE_PRODUCT){
                    $cssProdutoAtivo = "aguardando";
                } else {
                    $cssProdutoAtivo = "inativo";
                }
            ?>
            <td class="<?php echo $cssProdutoAtual.' '.$cssProdutoAtivo;?>">
                <?php if ($timeLine >= TIME_LINE_PRODUCT) : ?>
                    <a href="javascript:void(0)" onClick="window.location='<?php echo base_url()?>recalls/listsProducts'">
                        Produtos
                    </a>
                <?php else : ?>
                    <p> Produtos </p>
                <?php endif; ?>
            </td>
                
            <?php 
                $cssLocationAtual = ($url == "insertLocation") ? 'atual' : '';
                if ($timeLine > TIME_LINE_LOCATION) {
                    $cssLocationAtivo = 'ativo';
                } elseif ($timeLine == TIME_LINE_LOCATION){
                    $cssLocationAtivo = "aguardando";
                } else {
                    $cssLocationAtivo = "inativo";
                }
            ?>
            <td class="<?php echo $cssLocationAtual.' '.$cssLocationAtivo;?>">
                <?php if ($timeLine >= TIME_LINE_LOCATION) : ?>
                    <a href="javascript:void(0)" onClick="window.location='<?php echo base_url()?>recalls/insertLocation'">
                        Local de <br/> Atendimento
                    </a>
                <?php else : ?>
                    <p> Local de <br/> Atendimento </p>
                <?php endif; ?>
            </td>
            
            <?php 
                $cssMediaAtual = ($url == "insertMedia") ? 'atual' : '';
                if ($timeLine > TIME_LINE_MEDIA) {
                    $cssMediaAtivo = 'ativo';
                } elseif ($timeLine == TIME_LINE_MEDIA){
                    $cssMediaAtivo = "aguardando";
                } else {
                    $cssMediaAtivo = "inativo";
                }
            ?>
            <td class="<?php echo $cssMediaAtual.' '.$cssMediaAtivo;?>">
                <?php if ($timeLine >= TIME_LINE_MEDIA) : ?>
                    <a href="javascript:void(0)" onClick="window.location='<?php echo base_url()?>recalls/insertMedia'">
                        Plano de <br/> Mídia
                    </a>
                <?php else : ?>
                    <p> Plano de <br/> Mídia </p>
                <?php endif; ?>
            </td>
            
            <?php 
                $cssRiskAtual = ($url == "insertRisk") ? 'atual' : '';
                if ($timeLine > TIME_LINE_RISK) {
                    $cssRiskAtivo = 'ativo';
                } elseif ($timeLine == TIME_LINE_RISK){
                    $cssRiskAtivo = "aguardando";
                } else {
                    $cssRiskAtivo = "inativo";
                }
            ?>
            <td class="<?php echo $cssRiskAtual.' '.$cssRiskAtivo;?>">
                <?php if ($timeLine >= TIME_LINE_RISK) : ?>
                    <a href="javascript:void(0)" onClick="window.location='<?php echo base_url()?>recalls/insertRisk'">
                        Aviso de <br/> Risco
                    </a>
                <?php else : ?>
                    <p>Aviso de <br/> Risco</p>
                <?php endif; ?>
            </td>
            
            <?php 
                $cssProcessAtual = ($url == "insertProcess") ? 'atual' : '';
                if ($timeLine > TIME_LINE_PROCESS) {
                    $cssProcessAtivo = 'ativo';
                } elseif ($timeLine == TIME_LINE_PROCESS){
                    $cssProcessAtivo = "aguardando";
                } else {
                    $cssProcessAtivo = "inativo";
                }
            ?>
            <td class="<?php echo $cssProcessAtual.' '.$cssProcessAtivo;?>">
                <?php if ($timeLine >= TIME_LINE_PROCESS) : ?>
                    <a href="javascript:void(0)" onClick="window.location='<?php echo base_url()?>recalls/insertProcess'">
                        Processos <br/> Judiciais
                    </a>
                <?php else : ?>
                    <p>Processos <br/> Judiciais</p>
                <?php endif; ?>
            </td>
            
            
            <?php 
                $cssFinishAtual = ($url == "view") ? 'atual' : '';
                if ($timeLine > TIME_LINE_SEND_CAMPAIGN) {
                    $cssFinishAtivo = 'ativo';
                } elseif ($timeLine == TIME_LINE_SEND_CAMPAIGN){
                    $cssFinishAtivo = "aguardando";
                } else {
                    $cssFinishAtivo = "inativo";
                }
            ?>
            <td class="<?php echo $cssFinishAtivo.' '.$cssFinishAtual;?>">
                <?php if ($timeLine >= TIME_LINE_SEND_CAMPAIGN) : ?>
                    <a href="javascript:void(0)" onClick="window.location='<?php echo base_url()?>recalls/view'">
                        Enviar <br/> Campanha
                    </a>
                <?php else : ?>
                    <p> Enviar <br/> Campanha </p>
                <?php endif; ?>
            </td>
        </tr>
    </table>