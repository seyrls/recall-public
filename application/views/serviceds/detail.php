<script>
    $(document).ready(function(){
        $('#close').click(function(){
           window.close();
        });
        
        $(".popup").on('blur',function(){
            window.close();
        });
    });
</script>
<div class="popup" style="padding: 20px">
    <p class="alert alert-info"> 
        Produto: <?php echo $product; ?> <br/>
        Estado: <?php echo $state_name; ?> <br/>
        Total Afetado: <?php echo $qtd_total_uf; ?> 
    </p>
    <div class="topo-title">
        <h5>
            Histórico de registro
        </h5>
    </div>
    <table class="table table-striped table-bordered table-grid" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Data de inserção</th>
                <th>Quantidade Atendida</th>
                <th>Progresso</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if (isset($dados)) :
                $total = 0;
                foreach ($dados as $key => $value) : 
                    
                    $qtd = (int)$value['quantity'];
                    $date_insert = date("d/m/Y H:i", strtotime($value['date_insert'][0]));

                    $total += $qtd;
                    $percent = (($qtd*100)/$qtd_total_uf);
                    $progress = round($percent, 2).'%';
            ?>  
                    <tr>
                        <td><?php echo $date_insert; ?></td>
                        <td><?php echo $qtd; ?></td>
                        <td><?php echo $progress; ?></td>
                    </tr>
                <?php 
                    endforeach; 
                        $percent_total  = (($total*100)/$qtd_total_uf);
                        $progress_total = round($percent_total, 2).'%';
                ?>
            <?php 
                else : 
                    $total = 0;
                    $progress_total = 0;
            ?>
                    <tr>
                        <td colspan="3">
                            Nenhum registro encontrado
                        </td>
                    </tr>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr style="background-color: #ccc;">
                <td>Total:</td>
                <td>
                   <?php echo $total; ?>
                </td>
                <td>
                   <?php echo $progress_total; ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <div style="text-align: center">
        <a href="javascript:void(0)"  id="close" class="btn btn-primary">Fechar</a>
    </div>
</div>
