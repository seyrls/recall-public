    <?php
        $attributes = array('class' => 'formServiced', 'id' => 'formServiced');
        echo form_open('serviceds/edit/'.$supplier_id.'/'.$recall_id, $attributes);
    ?>
        
            <div class="col-xs-12 dvformulario">
                <div class="title-form">
                    <h4>Editar Índice de Atendimento</h4>
                    <p>
                        <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
                        <img src="<?php echo base_url();?>img/help.png" width="22px" style="padding-left: 10px;"/> indica descrição do campo
                    </p>
                </div>
                <div class="col-md-12">
                    
                    <?php if (!empty($serviced[0])) : ?>
                    
                    <h4>Campanha: <b><?php echo $serviced[0][0]['title']; ?></b></h4>
                    
                    <?php foreach ($serviced as $key => $value) : ?>
                        
                        <h5>
                            Produto: <b><?php echo $value[$key]['product']; ?> </b>
                        </h5>
                    
                        <div class="clear"></div>
                        
                        <div class="topo-title">
                            <h5> Lista de quantidade de itens afetados/atendidos por estado</h5>
                        </div>
                        <table class="table table-striped table-bordered tbCenter" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Quantidade de ítens afetados</th>
                                    <th>Quantidade atendida</th>
                                    <th>Lançar quantidade</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                
                                    $total_affected = 0;
                                    $total_serviced = 0;
                                    foreach ($value as $k => $v) :    
                                        $qtd_affected =  $v['quantity_affected'];
                                        $qtd_serviced = $v['quantity_serviced'];
                                        
                                        $product_id = $value[$key]['product_id']; 
                                        $qtd_max = ((int)$qtd_affected - $qtd_serviced);
                                ?>
                                <input type="hidden" name="product_id[]" value="<?php echo $value[$key]['product_id']; ?>"/>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <?php 
                                                echo "<div class='col-md-12'>";
                                                $attState = array(
                                                    'name'      => 'state_name[]',
                                                    'value'     => $v['state_name'],
                                                    'class'     => 'form-control input-md',
                                                    'disabled'  => 'true',
                                                );

                                                echo form_input($attState);
                                                echo form_error('state_name[]', '<div class="error">', '</div>');
                                                echo "</div>"
                                             ?>
                                            <input type="hidden" name="state_id[]" value="<?php echo $v['state_id']; ?>"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <?php 
                                                echo "<div class='col-md-7'>";
                                                $attQuantityAffected = array(
                                                    'name'      => 'quantity_affected[]',
                                                    'value'     => $qtd_affected,
                                                    'class'     => 'form-control input-md',
                                                    'disabled'  => 'true',
                                                );

                                                echo form_input($attQuantityAffected);
                                                echo form_error('quantity_affected[]', '<div class="error">', '</div>');
                                                echo "</div>";
                                             ?>
                                            <input type="hidden" name="affected_id[]" value="<?php echo $v['affected_id']; ?>"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <?php 
                                                echo "<div class='col-md-7'>";
                                                $attQuantityServiced = array(
                                                    'name'      => 'quantity_serviced[]',
                                                    'value'     => $qtd_serviced,
                                                    'class'     => 'form-control input-md',
                                                    'disabled'  => 'true',
                                                );

                                                echo form_input($attQuantityServiced);
                                                echo form_error('quantity_serviced[]', '<div class="error">', '</div>');
                                                echo "</div>";
                                             ?>
                                            <input type="hidden" name="serviced_id[]" value="<?php echo $v['serviced_id']; ?>"/>
                                        </div>
                                    </td>
                                    
                                    <td>
                                        <div class="form-group">
                                            <?php echo "<div class='col-md-12'>"; ?>
                                                <input type="number" name="quantity_serviced_new[]" 
                                                   value="<?php echo set_value('quantity_serviced'); ?>"
                                                   min="<?php echo '-'.$qtd_serviced; ?>" 
                                                   max="<?php echo $qtd_max; ?>"
                                                   onkeypress='validate_only_number(event)'
                                                   class="form-control input-md">                                                
                                            <?php
                                                echo form_error('quantity_serviced_new[]', '<div class="error">', '</div>');
                                                echo "</div>";
                                             ?>
                                        </div>
                                    </td>
                                </tr>
                    <?php 
                        
                            $total_affected = $total_affected + $qtd_affected;
                            $total_serviced = $total_serviced + $qtd_serviced;
                    
                        endforeach; 
                    ?>
                            </tbody>
                            <tfoot>
                                <tr style="background-color: #ccc;">
                                    <td><b>Total:</b></td>
                                    <td>
                                        <input type="hidden" name="total_affected" value="<?php echo $total_affected; ?>"/>
                                        <b><?php echo $total_affected; ?></b>
                                    </td>
                                    <td>
                                        <input type="hidden" name="total_serviced" value="<?php echo $total_serviced; ?>"/>
                                        <b><?php echo $total_serviced; ?></b>
                                    </td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
            <?php 
                    endforeach;
                endif;
            ?>
                </div>
            </div>
            <div class="clear"></div>
                <div class="btnNext">
                    <a class="btn btn-default" href="<?php echo base_url().'serviceds/lists/'.$supplier_id.'/'.$recall_id;?>">
                        Voltar
                    </a>
                    <button class="btn btn-success" type="submit">
                        <span class="glyphicon glyphicon-plus-sign"></span> Salvar
                    </button>
                </div>
<?php echo form_close();