<script>
    $(document).ready(function(){
        
        totalPercentual();
        
        // combo recall
        $("select[name='supplier_id']").change(function(){
            var supplier_id = $("select[name='supplier_id'] option:selected").attr('value');
            $("#recall_id").html("");
            if (supplier_id.length > 0) {
                var urlpath = '<?php echo base_url();?>';
                $("#recall_id").removeAttr('disabled');
                $.ajax({
                    url: urlpath+'recalls/loadCombo/',
                    type:'POST',
                    data: 'supplier_id='+supplier_id,
                    dataType: 'json',
                    success: function( data ) {
                        $.each(data, function(i, value) {
                            $('#recall_id').append($('<option/>', {
                                value: i,
                                text : value 
                            }));
                        });
                    }
                });
            }
	});
    });
    
    function totalPercentual() {
        // soma recall
        var total_affected = 0;
        var total_serviced = 0;
        
        $('input[name^="total_affected"]').each(function(i,v){
            total_affected += parseFloat($(this).val());
        });
        $('input[name^="total_serviced"]').each(function(i,v){
            total_serviced += parseFloat($(this).val());
        }); 

        var percent  = ((total_serviced*100)/total_affected);
        var progress = (percent);
        var total = (progress).toFixed(2); // equal to 6.69
        var span = "";
        if (total === '100.00') {
            total = parseInt(total);
            var span = "<span class='label label-success'>"+total+"%</span>";
            $('#btnAdd').hide();
        } else {
            var span = "<span class='label label-danger'>"+total+"%</span>";
        }
        $('#total').append("<progress value='"+total_serviced+"' max='"+total_affected+"'></progress>&nbsp;"+span);
    }
    
    function popup(product_id,state_id,qtd_total) {
        var urlpath = $('body').data('baseurl');
        var url = urlpath+'serviceds/detail/'+product_id+'/'+state_id+'/'+qtd_total;
        
        var w = 500;
        var h = 500;
        var left = Number((screen.width/2)-(w/2));
        var tops = Number((screen.height/2)-(h/2));
        window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
    }
</script>    

<?php
    $attributes = array('class' => 'formServiceds', 'id' => 'formServiceds');
    echo form_open('serviceds/lists', $attributes);
?>
<div class="col-xs-12 dvformulario">
        <div class="title-form row">
            <h4>Índice de Atendimento dos produtos afetados</h4>
            <p>
                <img src="<?php echo base_url();?>img/required.gif"/> indica campo obrigatório
            </p>
        </div>
        <div class="row" style="padding: 10px;">
            <div class="col-md-12">
                <div class="form-group">
                    <?php 
                        echo form_label('Fornecedor', 'trade_name', array('class' => 'col-md-4 control-label'));
                        echo "<div class='col-md-7'>";
                        if (($user['level'] == LEVEL_ADMINISTRADOR) || ($user['level'] == LEVEL_OPERADOR)){
                            $default = isset($supplier['supplier_id']) ? $supplier['supplier_id'] : set_value('supplier_id');
                            echo form_dropdown('supplier_id', $comboSupplier, $default, 'class="form-control requiredField"');
                        } else {
                            $arrSupplier = array(
                                'name'      => 'trade_name',
                                'id'        => 'trade_name',
                                'value'     => ($supplier['trade_name']) ? $supplier['trade_name']  : set_value('trade_name'),
                                'class'     => 'form-control input-md requiredField',
                                'readonly'  =>'true',
                            );
                            echo form_input($arrSupplier);
                            echo "<input type='hidden' name='supplier_id' value='".$supplier['supplier_id']."'";
                        }
                        echo form_error('supplier_id', '<div class="error">', '</div>');
                        echo "</div>";
                     ?>
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                </div>
                <div class="form-group">
                    <?php 
                        echo form_label('Campanha de Recall', 'recall', array('class' => 'col-md-4 control-label'));
                        echo "<div class='col-md-7'>";
                        $defaultRecall = isset($recall['recall_id']) ? $recall['recall_id'] : set_value('recall_id');
                        echo form_dropdown('recall_id', $comboRecall, $defaultRecall, 'class="form-control requiredField" id="recall_id"');
                        echo form_error('recall_id', '<div class="error">', '</div>');
                        echo "</div>"
                     ?>
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
                </div>
            </div>
        </div>
    
        <div class="clear"></div>
        
        <div style="margin: 30px auto; text-align: center">
            <button class="btn btn-primary" type="submit" id="report">
                 Pesquisar
            </button>
        </div>
        <?php echo form_close(); ?>
        <hr/>
        <div class="clear"></div>
        <div class="col-md-12">
        <?php if (!empty($serviced[0])) :    ?>
            <div class="row">
                <div class="col-md-8" style="margin-left:20px;">
                    <div class="alert alert-info">
                        <p>
                            <b>Campanha de Recall: </b>
                            <?php echo $serviced[0][0]['title']; ?>
                        </p>
                        <p>
                            <b> Índice de atendimento: </b>
                            <span id="total"></span>
                        </p>
                        
                    </div>
                    
                </div>
                <div class="col-md-2" id="btnAdd" style="float: right;margin-right: 20px;">
                    <a class="btn btn-primary"
                        href="<?php echo base_url().'serviceds/edit/'.$supplier['supplier_id'].'/'.$serviced[0][0]['recall_id']; ?>">
                         Adicionar Atendimento
                    </a>
                </div>
            </div>

            <div class="clear"></div>
            
            <?php foreach ($serviced as $key => $value) : ?>
            
                <div class="datailServiced">
                    <div class="tituloSumary">
                        <h4>
                            <?php echo $value[$key]['product']; ?> 
                        </h4>
                    </div>
                    <div class="clear"></div>
                        <table class="table table-striped table-bordered tbCenter" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Data da última inserção</th>
                                    <th>Quantidade de ítens afetados</th>
                                    <th>Quantidade total atendida</th>
                                    <th>Histórico</th>
                                    <th style="width: 250px">Progresso (%)</th>
                                </tr>
                            </thead>
                            <tbody>

                    <?php 
                            $total_affected = 0;
                            $total_serviced = 0;
                        foreach ($value as $k => $v) : 
                            $qtd_affected =  $v['quantity_affected'];
                            $qtd_serviced = $v['quantity_serviced'];
                            $percent = (($qtd_serviced*100)/$qtd_affected);
                            $progress = round($percent, 2).'%';
                            $date = ($v['date_insert']) ? $v['date_insert'] : '-';
                    ?>
                            <tr>
                                <td><?php echo $v['state']; ?></td>
                                <td><?php echo $date; ?></td>
                                <td><?php echo ($v['quantity_affected']) ? $v['quantity_affected'] : 0; ?></td>
                                <td><?php echo ($v['quantity_serviced']) ? $v['quantity_serviced'] : 0; ?></td>
                                <td>
                                    <a href="javascript:void(0)" 
                                       onclick="popup(<?php echo $value[$key]['product_id'].','.$v['state_id'];?>,<?php echo $qtd_affected; ?>)">
                                        <i class="fa fa-history" title="Detalhar"></i>
                                    </a>
                                </td>
                                <td style="width: 200px;">
                                    <div style="float: left">
                                        <progress value="<?php echo $qtd_serviced; ?>" max="<?php echo $qtd_affected; ?>"></progress>
                                    </div>
                                    <div style="float:right;">
                                        <span class="label label-default"><?php echo $progress; ?></span>
                                    </div>
                                </td>
                            </tr>
                    <?php 
                            $total_affected = $total_affected + $qtd_affected;
                            $total_serviced = $total_serviced + $qtd_serviced;
                            endforeach;
                    ?>
                        </tbody>
                        <tfoot>
                            <tr style="background-color: #ccc;">
                                <td><b>Total:</b></td>
                                <td></td>
                                <td>
                                    <input type="hidden" name="total_affected" value="<?php echo $total_affected; ?>"/>
                                    <b><?php echo $total_affected; ?></b>
                                </td>
                                <td>
                                    <input type="hidden" name="total_serviced" value="<?php echo $total_serviced; ?>"/>
                                    <b><?php echo $total_serviced; ?></b>
                                </td>
                                <td></td>
                                <td style="width: 200px;">
                                    <div style="float: left">
                                        <progress value="<?php echo $total_serviced; ?>" max="<?php echo $total_affected; ?>"></progress>
                                    </div>
                                    <div style="float:right;">
                                        <?php 
                                            $percent_total  = (($total_serviced*100)/$total_affected);
                                            $progress_total = round($percent_total, 2).'%';
                                            if (substr($progress_total,0,-1) == 100) {
                                                $labelAtendimento = 'label-success';
                                            } else{
                                                $labelAtendimento = 'label-danger';
                                            }
                                        ?>
                                        <span class="label <?php echo $labelAtendimento; ?>">
                                            <?php echo $progress_total; ?>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            
                <div class="clear"></div>
    <?php 
            endforeach;
        endif; 
    ?>
        </div>
    </div>