<div class="container">
    <form class="form-horizontal" method="post" action="">
        <fieldset>
            
            <div class="row col-md-9">
                <legend>
                    Dados da Mídia
                    <div style="float: right; font-size: 11px; padding-top: 10px;">
                    <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/> 
                    Indica campo obrigatório
                </div>
                </legend>
            </div>
            <div class="clearfix"></div>
            
            <div class="form-group">
                <?php 
                    echo form_label('Meio de veiculação', 'origin', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attOrigin = array(
                        'name'      => 'origin',
                        'id'        => 'origin',
                        'value'     => ($dados) ? $dados['origin'] : set_value('origin'),
                        'class'     => 'form-control input-md',
                        'readOnly'  => TRUE
                    );
                    echo form_input($attOrigin);
                    echo form_error('origin', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <input type="hidden" name="origin_id" value="<?php echo $dados['origin_id']; ?>"/>
                <img src="<?php echo base_url();?>img/required.gif"/>
            </div>
            
            <div class="form-group">
                <?php 
                    echo form_label('Mídia', 'media', array('class' => 'col-md-4 control-label'));
                    echo "<div class='col-md-5'>";
                    $attMedia = array(
                        'name'      => 'media',
                        'id'        => 'media',
                        'value'     => ($dados) ? $dados['media'] : set_value('media'),
                        'class'     => 'form-control input-md',
                    );
                    echo form_input($attMedia);
                    echo form_error('media', '<div class="error">', '</div>');
                    echo "</div>"
                 ?>
                <img src="<?php echo base_url();?>img/required.gif" class="requiredField"/>
            </div>

            <div style="margin: 30px auto; text-align: center">
                <a class='btn btn-default' href="<?php echo base_url()."origins/view/".$dados['origin_id'];?>">
                     <span class="glyphicon glyphicon-chevron-left"></span> Voltar
                </a>
                <button id="save" name="save" class="btn btn-success">Salvar</button>
            </div>
        </fieldset>
    </form>
</div>