<script>
	$(document).ready(function() {
    $('#example').dataTable();
} );
</script>

<div  class="topo">
    <div class="topo-title">
        <h5> Lista de Mídias</h5>
    </div>
    <table class="table table-striped table-bordered" id="example" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>ID</th>
                <th>Mídia</th>
                <th>Status</th>
                <th>Meio de veiculação</th>
                <th>Opção</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        foreach ($dados as $d){
            $status = ($d['status'] == TRUE) ? '<span class="label label-success">Ativo</span>' : '<span class="label label-danger">Inativo</span>';
            echo "<tr>"
            . "<td><a href='".base_url()."medias/edit/{$d['id']}'>{$d['id']}</a></td>"
            . "<td>{$d['media']}</td>"
            . "<td>{$status}</td>"
            . "<td>{$d['origin']}</td>"
            . "<td><a href='".base_url()."medias/edit/{$d['id']}'>Editar</a></td>"
            . "</tr>";
        }?>
        </tbody>
    </table>
</div>