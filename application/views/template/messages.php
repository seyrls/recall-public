<?php if ($this->session->flashdata('success')) : ?>
    <div class="flash-messages">
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    </div>
<?php elseif ($this->session->flashdata('error')) : ?>
    <div class="flash-messages">
        <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    </div>
<?php elseif ($this->session->flashdata('sendCampaign')) : ?>
    <div class="flash-messages">
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('sendCampaign'); ?>
        </div>
    </div>
<?php endif; ?>