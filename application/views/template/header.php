<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
   	<title>Recall</title>

        <link href="<?php echo base_url();?>img/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
   	
        <script src="<?php echo base_url();?>js/jquery.js"></script>
        <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
   	<script src="<?php echo base_url();?>js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>js/dataTables.bootstrap.js"></script>
        <script src="<?php echo base_url();?>js/combo.js"></script>
        <script src="<?php echo base_url();?>js/wizard.js"></script>
        <script src="<?php echo base_url();?>js/jquery.maskedinput.js"></script>
        <script src="<?php echo base_url();?>js/autopost.js"></script>
        <script src="<?php echo base_url();?>js/jquery-ui.js"></script>
        <script src="<?php echo base_url();?>js/jquery.ui.timepicker.js"></script>
        <script src="<?php echo base_url();?>js/functions.js"></script>
        <script src="<?php echo base_url();?>js/jquery.maskMoney.js"></script>
        
        <link rel="stylesheet" href="<?php echo base_url();?>css/calendario.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/dataTables.bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/dashboard.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/tabs.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/style.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/timeLine.css" />
        
        <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/sb-admin.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/font-awesome.min.css" />
        
    </head>
    
    <body data-baseurl="<?php echo base_url(); ?>">

    <?php $arr = $this->session->userdata('login');?>
        
    <div id="wrapper">
            <div style="text-align: center; margin: 0px; padding-top: 10px;">
                <span class="label label-danger" style="padding: 10px; font-size: 14px !important;">
                    ATENÇÃO! OS DADOS CADASTRADOS NESSE AMBIENTE DE HOMOLOGAÇÃO SERÃO UTILIZADOS EM PRODUÇÃO! Não será possível enviar e-mail neste momento.
                </span>
            </div>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>">SISTEMA NACIONAL DE ALERTAS DE RECALL</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
                        <?php echo $arr['username']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url();?>users/view/minha-conta">
                                <i class="fa fa-fw fa-user"></i> Minha Conta
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo base_url();?>logins/logout">
                                <i class="fa fa-fw fa-power-off"></i> Sair
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                
                <ul class="nav navbar-nav side-nav">
                    <?php if ($arr['level'] == LEVEL_ADMINISTRADOR) : ?>
                    <li class="active">
                        <a href="<?php echo base_url().'administrations';?>">
                            <i class="fa fa-fw fa-dashboard"></i> Administração
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#menu_report">
                            <i class="fa fa-fw fa-bar-chart-o"></i> Relatórios <i class="fa fa-fw fa-caret-down"></i>
                        </a>
                        <ul id="menu_report" class="collapse">
                            <li>
                                <a href="<?php echo base_url().'reports/recall';?>">
                                    Campanhas de Recall
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'reports/affected';?>">
                                    Produtos Afetados
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'reports/serviced';?>">
                                    Índice de Atendimento
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'reports/risk';?>">
                                    Tipos de Risco
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php if ($arr['level'] != LEVEL_FORNECEDOR) : ?>
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#menu_user">
                            <i class="fa fa-fw fa-user"></i> Usuários <i class="fa fa-fw fa-caret-down"></i>
                        </a>
                        <ul id="menu_user" class="collapse">
                            <li>
                                <a href="<?php echo base_url().'users';?>">
                                    Todos
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'suppliers';?>">
                                    Fornecedores
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#menu_group">
                            <i class="fa fa-fw fa-users"></i>
                            Parceiros <i class="fa fa-fw fa-caret-down"></i>
                        </a>
                        <ul id="menu_group" class="collapse">
                            <li>
                                <a href="<?php echo base_url().'groups/lists';?>">
                                    Grupos Parceiros
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'partners/lists';?>">
                                    Entidades Parceiras
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#menu_branch">
                            <i class="fa fa-fw fa-cogs"></i> Ramos de Atividades <i class="fa fa-fw fa-caret-down"></i>
                        </a>
                        <ul id="menu_branch" class="collapse">
                            <li>
                                <a href="<?php echo base_url().'branches';?>">
                                    Setores
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'type_products/lists';?>">
                                    Tipos de Produtos
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li>
                        <a href="<?php echo base_url().'manufacturers';?>">
                            <i class="fa fa-fw fa-truck"></i> Fabricantes
                        </a>
                    </li>
                    
                    <li>
                        <a href="<?php echo base_url().'origins';?>">
                            <i class="fa fa-fw fa-globe"></i>
                            Meios de veiculação</a>
                    </li>
                    <?php endif; ?>
                    <li>
                        <a href="<?php echo base_url().'serviceds/lists';?>">
                            <i class="fa fa-fw fa-check"></i> Índice de Atendimento
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url().'recalls';?>">
                            <i class="fa fa-fw fa-edit"></i> Campanhas de Recall
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
         <div id="page-wrapper">
             <div class="container-fluid" style="padding: 20px;">