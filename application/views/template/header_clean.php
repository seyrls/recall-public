<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
   	<title>Recall</title>

        <link href="<?php echo base_url();?>img/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
   	
        <script src="<?php echo base_url();?>js/jquery.js"></script>
        <script src="<?php echo base_url();?>js/jsapi.js"></script>
        <link rel="stylesheet" href="<?php echo base_url();?>css/style.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
        
    </head>
    
    <body data-baseurl="<?php echo base_url(); ?>">