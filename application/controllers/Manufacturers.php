<?php

class Manufacturers extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        
        /**
         * Vefifica se o usuário está logado
         * Se não estiver, redireciona para a página de login
         */
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function index() {
        $this->lists();
    }

    function lists() {
        $dados = array();
        $arrManufacturer = $this->doctrine->em->getRepository('Entities\Manufacturer')
                                ->findBy(array(),array('corporateName' => 'ASC'));
        $manufacturer = new Entities\Manufacturer;
        $dados['dados'] = $manufacturer->getArrayAllManufacturer($arrManufacturer);
        
        $this->load->view('template/header');
        $this->load->view('manufacturers/index');
        $this->load->view('template/messages');
        $this->load->view('manufacturers/lists',$dados);
        $this->load->view('template/footer');
    }

    function insert() {
        
        if ($this->input->post()) {
            $this->save();
        }
        $this->load->view('template/header');
        $this->load->view('manufacturers/index');
        $this->load->view('template/messages');
        $this->load->view('manufacturers/insert');
        $this->load->view('template/footer');
    }

    function save() {
        if ($this->form_validation->run('manufacturers')){
            $obj = new Entities\Manufacturer;
            $obj->setCorporateName($this->input->post('corporate_name'));
            $obj->setTradeName($this->input->post('trade_name'));
            $obj->setStatus($this->input->post('status'));
            $this->doctrine->em->persist($obj);
            $this->doctrine->em->flush();
            $this->session->set_flashdata('success', MSG_SUCCESS);
            redirect('manufacturers/lists');
        }
    }
    
    function edit($id) {
        if (!$id) {
            redirect('manufacturers/lists');
        }
        if ($this->input->post()) {
            $this->update();
        }
        
        $dados = array();
        $arrManufacturer = $this->doctrine->em->getRepository('Entities\Manufacturer')->findById($id);
        $manufacturer = new Entities\Manufacturer;
        $dados['dados'] = $manufacturer->getArrayManufacturer($arrManufacturer);
        
        $this->load->view('template/header');
        $this->load->view('manufacturers/index');	
        $this->load->view('template/messages');
        $this->load->view('manufacturers/insert',$dados);
        $this->load->view('template/footer');
    }
    
    function update() {
        if ($this->form_validation->run('manufacturers')){
            $manufacturer = $this->doctrine->em->getRepository('Entities\Manufacturer')
                            ->find($this->input->post('manufacturer_id'));
            if ($manufacturer) {
                $manufacturer->setCorporateName($this->input->post('corporate_name'));
                $manufacturer->setTradeName($this->input->post('trade_name'));
                $manufacturer->setStatus($this->input->post('status'));
                $this->doctrine->em->persist($manufacturer);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR);
            }
            redirect('manufacturers/lists');
        }
    }

    function loadCombo(){
        $manufacturer = $this->doctrine->em->getRepository('Entities\Manufacturer')->findAll();

        foreach($manufacturer as $m){
            echo "<option value='".$m->getId."'>" . $m->getCorporateName . "</option>";
        }
    }
    
    function remove($id){
        if (!$id) {
            redirect('manufacturers/lists');
        }
        $product = $this->doctrine->em->getRepository('Entities\Product')
                        ->findBy(array('manufacturer' => $id));
        if ($product) {
            $this->session->set_flashdata('error', MSG_ERROR_REMOVE_RECALL_EXISTS);
            redirect('manufacturers/lists');
        } else {
            $manufacturer = $this->doctrine->em->getRepository('Entities\Manufacturer')->find($id);
            if ($manufacturer) {
                $this->doctrine->em->remove($manufacturer);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_REMOVE);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
            }
        }
        redirect('manufacturers/lists');
    }
}
