<?php

class Groups extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        //$this->load->database(); // necessario para utilizar a funcao is_unique na validacao
        
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function index() {
        $this->lists();
    }

    function lists() {
        $dados = array();
        $group = new \Entities\Group;
        $arrGroup = $this->doctrine->em->getRepository('Entities\Group')
                         ->findBy(array(),array('name' => 'ASC'));
        $dados['dados'] = $group->getArrayAllGroup($arrGroup);
        $this->load->view('template/header');
        $this->load->view('groups/index');
        $this->load->view('template/messages');
        $this->load->view('groups/lists',$dados);			
        $this->load->view('template/footer');
    }

    function insert() {
        if ($this->input->post()) {
            if ($this->form_validation->run('group')){
                try {
                    $group = new Entities\Group;
                    $group->setName($this->input->post('name'));
                    $group->setStatus($this->input->post('status'));
                    $this->doctrine->em->persist($group);
                    $this->doctrine->em->flush();
                    $this->session->set_flashdata('success', MSG_SUCCESS);
                    redirect('groups/index');
                } catch (Exception $ex) {
                    $this->session->set_flashdata('error', MSG_ERROR);
                }
            }
        }
        $this->load->view('template/header');
        $this->load->view('groups/index');
        $this->load->view('template/messages');
        $this->load->view('groups/insert');
        $this->load->view('template/footer');
    }

    function edit($id) {
        if (!$id) {
            redirect('groups/index');
        }
        if ($this->input->post()) {
            $this->update($id);
        }
        $dados = array();
        $group = new Entities\Group;
        $arrGroup = $this->doctrine->em->getRepository('Entities\Group')
                            ->findBy(array('id' => $id));
        $dados['dados'] = $group->getArrayGroup($arrGroup);
        $this->load->view('template/header');
        $this->load->view('groups/index');
        $this->load->view('template/messages');
        $this->load->view('groups/insert',$dados);
        $this->load->view('template/footer');
    }

    private function update($id) {
        $group = $this->doctrine->em->getRepository('Entities\Group')->find($id);
        if ($group){
            if ($this->form_validation->run('group')){
                try {
                    $group->setName($this->input->post('name'));
                    $group->setStatus($this->input->post('status'));
                    $this->doctrine->em->persist($group);
                    $this->doctrine->em->flush();
                    $this->session->set_flashdata('success', MSG_SUCCESS_ALTERADO);
                } catch (Exception $ex) {
                    $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
                }
                redirect('groups/lists');
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
        }
    }
    
    function remove($id){
        $group = $this->doctrine->em
                ->getRepository('Entities\Group')
                ->find($id);
        if ($group) {
            $groupPartner = $this->doctrine->em
                ->getRepository('Entities\GroupPartner')
                ->findBy(array('group' => $id));
            if ($groupPartner) {
                $this->session->set_flashdata('error', MSG_ERROR_REMOVE_GROUP_EXISTS);
            } else {
                $this->doctrine->em->remove($group);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS);
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
        }
        redirect('groups/index');
    }
    
    function view($id) {
        if (!$id) {
            redirect('groups/index');
        }
        $dados = array();
        $arrPartner = array();
        $arrGroupPartner = $this->doctrine->em->getRepository('Entities\GroupPartner')
                            ->findBy(array('group' => $id));
        if ($arrGroupPartner) {
            $partner  = new Entities\Partner;
            foreach ($arrGroupPartner as $key => $gp) {
                $arrPartner[$key] = $this->doctrine->em->getRepository('Entities\Partner')
                            ->find($gp->getPartner()->getId());
                $dados['partner'][$key] = $partner->getArrayPartner($arrPartner);
            }
        }
            
        $arrGroup = $this->doctrine->em->getRepository('Entities\Group')
                        ->find($id);
        $dados['group'] = array(
            'group_id' => $arrGroup->getId(),
            'name' => ($arrGroup->getName()),
        );
        
        $this->load->view('template/header');
        $this->load->view('groups/index');
        $this->load->view('template/messages');
        $this->load->view('groups/view',$dados);
        $this->load->view('template/footer');
    }
}