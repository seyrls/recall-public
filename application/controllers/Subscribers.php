<?php

class Subscribers extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        //$this->load->database(); // necessario para utilizar a funcao is_unique na validacao
        
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function index() {
        $this->lists();
    }

    function lists() {
        
        $dql = "
            SELECT  DISTINCT o.id as option_id,
                    s.email,
                    s.name,
                    s.status,
                    b.id as branch_id,
                    b.branch,
                    tp.id as type_product_id,
                    tp.typeProduct as type_product
            FROM 
                    Entities\Subscriber s
            INNER JOIN 
                    Entities\Option o WITH s.id = o.subscriber
            LEFT JOIN
                    Entities\Branch b WITH b.id = o.branch
            LEFT JOIN
                    Entities\TypeProduct tp WITH tp.id = o.type_product
            ORDER BY 
                    s.name ASC
            ";
        $sql = $this->doctrine->em->createQuery($dql);
        $dados = array();
        if ($sql->getResult()) {
            foreach ($sql->getResult() as $key => $value) {
                $dados['dados'][$key] = $value;
            }
        }
        
        $this->load->view('template/header');
        $this->load->view('subscribers/index');
        $this->load->view('template/messages');
        $this->load->view('subscribers/lists',$dados);			
        $this->load->view('template/footer');
    }

    function insert() {
        if ($this->input->post()) {
            $this->save();
        }
        $branch                     = new Entities\Branch;
        $arrBranch                  = $this->doctrine->em->getRepository('Entities\Branch')
                                            ->findBy(array('status' => TRUE),array('branch' => 'ASC'));
        $data['comboBranch']        = $branch->getComboBranch($arrBranch);
        $data['type_product']       = array('' => 'Todos');
        
        $this->load->view('template/header');
        $this->load->view('subscribers/index');
        $this->load->view('template/messages');
        $this->load->view('subscribers/insert', $data);
        $this->load->view('template/footer');
    }

    function save() {
        $subscriber = new Entities\Subscriber;
        if ($this->form_validation->run('subscriber')){
            $this->doctrine->em->getConnection()->beginTransaction();
            try {
                    $subscriber->setStatus(STATUS_ATIVO);
                    $subscriber->setName($this->input->post('name'));
                    $subscriber->setEmail($this->input->post('email'));
                    $this->doctrine->em->persist($subscriber);
                    $this->doctrine->em->flush();
                    $this->saveOptions($subscriber);
                    $this->session->set_flashdata('success', MSG_SUCCESS_ALTERADO);
                    $this->doctrine->em->getConnection()->commit();
                } catch (Exception $ex) {
                    $this->doctrine->em->getConnection()->rollback();
                    $this->session->set_flashdata('error', MSG_ERROR);
                }
            redirect('subscribers/index');
        }
    }
    
    function saveOptions($subscribers) {
        $option = new Entities\Option;
        $option->setSubscriber($subscribers);
        $type_product = $this->doctrine->em
                ->getRepository('Entities\TypeProduct')
                ->find($this->input->post('type_product_id'));
        if ($type_product) {
            $option->setTypeProduct($type_product);
        }
        $branch = $this->doctrine->em
                ->getRepository('Entities\Branch')
                ->find($this->input->post('branch_id'));
        if ($branch) {
            $option->setBranch($branch);
        }
        $this->doctrine->em->persist($option);
        $this->doctrine->em->flush();
    }

    function edit($id) {
        if (!$id) {
            redirect('subscribers/index');
        }
        if ($this->input->post()) {
            $this->update($id);
        }
        
        $dados = array();
        $option = new \Entities\Option;
        $arrOptions = $this->doctrine->em->getRepository('Entities\Option')
                            ->findBy(array('id' => $id));
        $dados['dados']   = $option->getArrayOptions($arrOptions);
        
        $branch                     = new Entities\Branch;
        $arrBranch                  = $this->doctrine->em->getRepository('Entities\Branch')
                                            ->findBy(array('status' => TRUE),array('branch' => 'ASC'));
        $dados['comboBranch']       = $branch->getComboBranch($arrBranch);
        
        $branch_id = isset($dados['dados']['branch_id']) ? $dados['dados']['branch_id'] : NULL;
        $typeProduct                = new Entities\TypeProduct;
        $arrTypeProduct             = $this->doctrine->em->getRepository('Entities\TypeProduct')
                                            ->findBy(array('branch' => $branch_id),array('typeProduct' => 'ASC'));
        $dados['comboTypeProduct']  = $typeProduct->getComboTypeProduct($arrTypeProduct);
        
        $this->load->view('template/header');
        $this->load->view('subscribers/index');
        $this->load->view('template/messages');
        $this->load->view('subscribers/edit',$dados);
        $this->load->view('template/footer');
    }

    private function update($id) {
        $option = $this->doctrine->em->getRepository('Entities\Option')
                        ->find($id);
        if ($option->getSubscriber()){
            $this->doctrine->em->getConnection()->beginTransaction();
            try {
                $subscriber = $this->doctrine->em->getRepository('Entities\Subscriber')
                        ->find($option->getSubscriber()->getId());
                
                $subscriber->setName($this->input->post('name'));
                $subscriber->setEmail($this->input->post('email'));
                $this->doctrine->em->persist($subscriber);
                
                $option->setSubscriber($subscriber);
                
                if ($this->input->post('branch_id')) {
                    $branch = $this->doctrine->em->getRepository('Entities\Branch')
                        ->find($this->input->post('branch_id'));
                    $option->setBranch($branch);
                }
                if ($this->input->post('type_product_id')) {
                    $typeProduct = $this->doctrine->em->getRepository('Entities\TypeProduct')
                        ->find($this->input->post('type_product_id'));
                    $option->setTypeProduct($typeProduct);
                }
                $this->doctrine->em->persist($option);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_ALTERADO);
                $this->doctrine->em->getConnection()->commit();
            } catch (Exception $ex) {
                var_dump($ex->getMessage()); die;
                $this->doctrine->em->getConnection()->rollback();
                $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
        }
        redirect('subscribers/lists');
    }
    
    private function saveSupplier($supplier,$subscriber_id) {
        $branch = $this->doctrine->em->getRepository('Entities\Branch')->find($this->input->post('branch_id'));
        $city = $this->doctrine->em->getRepository('Entities\City')->find($this->input->post('city_id'));
        $subscriber = $this->doctrine->em->getRepository('Entities\Subscriber')->find($subscriber_id);
        $supplier->setCnpj($this->input->post('cnpj'));
        $supplier->setCorporateName($this->input->post('corporate_name'));
        $supplier->setTradeName($this->input->post('trade_name'));
        $supplier->setStateRegistration($this->input->post('state_registration'));
        $supplier->setAddress($this->input->post('address'));
        $supplier->setComplement($this->input->post('complement'));
        $supplier->setDistrict($this->input->post('district'));
        $supplier->setZip($this->input->post('zip'));
        $supplier->setCommercialPhone($this->input->post('commercial_phone'));
        $supplier->setFaxPhone($this->input->post('fax_phone'));
        $supplier->setCity($city);
        $supplier->setBranch($branch);
        $supplier->setSubscriber($subscriber);
        $this->doctrine->em->persist($supplier);
        $this->doctrine->em->flush();
    }
    
    function remove($id){
        redirect('subscribers/index');
    }
}