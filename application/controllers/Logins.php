<?php

class Logins extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('send_email');
        $this->load->library('form_validation');
    }

    function index() {
        if ($this->input->post()) {
            $this->login();
        }
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('logins/index');
        $this->load->view('portal-padrao/footer');
    }

    function login() {
        if ($this->form_validation->run('login')){
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            
            $user = $this->doctrine->em->getRepository('Entities\User')
                        ->findBy(array('email'=> $email, 'password' => $password));
            if ($user) {
                
                if ($user[0]->getStatus() == STATUS_AGUARDANDO) {
                    $this->session->set_flashdata('error', MSG_ERROR_SENHA_NAO_ATIVADA);
                    redirect('logins/index');
                }
                $arr = array(
                    'id' =>$user[0]->getId(),
                    'username' => $user[0]->getUsername(),
                    'level' => $user[0]->getLevel()->getLevel(),
                    'email' => $user[0]->getEmail(),
                );
                $this->session->set_userdata('login',$arr);
                redirect(base_url().'administrations');
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_USUARIO_NAO_ENCONTRADO);
                redirect('logins/index');   
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('logins/index');
        }
    }

    function forgotPassword(){
        $dados = array();
        
        if ($this->input->post()) {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
            if ($this->form_validation->run() == TRUE){
                $user = $this->doctrine->em->getRepository('Entities\User')
                                    ->findOneBy(array('email' => $this->input->post('email')));
                if ($user) {
                    $this->doctrine->em->getConnection()->beginTransaction();
                    // gerar senha aleatoria
                    $numeros = 0123456789;
                    $letras = "abcdefghijklmnopqrstuvyxwz";
                    $password = str_shuffle($numeros);
                    $password .= str_shuffle($letras);
                    $senha = substr(str_shuffle($password),0,6);
                    
                    $mailCripto = urlencode(base64_encode($user->getEmail()));
                    
                    $message = "Olá, ".$user->getUserName().'.<br/>';
                    $message .= "Sua senha temporária para acessar o sistema de recall é: <b>".$senha."</b><br/><br/>";
                    $message .= "É necessário alterá-la 
                                <a href='".base_url('principal/ativarConta/'.$mailCripto)."'>clicando aqui</a>
                                para acessar o sistema.<br/><br/>";
                    $message .= "Atenciosamente, <br/> SENACON";
                    $mailto  = $user->getEmail();
                    $subject = "Recuperar Senha do Sistema de Recall";
                    
                    try {
                        $user->setPassword($senha);
                        $user->setStatus(STATUS_AGUARDANDO);
                        send_email($message,$mailto,$subject);
                        $this->doctrine->em->persist($user);
                        $this->doctrine->em->flush();
                       $this->doctrine->em->getConnection()->commit();
                       $this->session->set_flashdata('success', MSG_SUCCESS_NEW_PASSWORD);
                       redirect('logins/forgotPassword');
                    } catch (Exception $ex) {
                        $this->doctrine->em->getConnection()->rollback();
                        $this->session->set_flashdata('error', MSG_ERROR);
                    }
                } else {
                    $this->session->set_flashdata('error', 'Ops! Email não encontrado.');
                    redirect('logins/forgotPassword');
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('logins/forgotPassword');
            }
        }
        
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('logins/forgot_password',$dados);
        $this->load->view('portal-padrao/footer');
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
    /*
     * Acessar Conta Fornecedor
     */
    
    function accountSupplier() {
        if ($this->input->post()) {
            $this->loginSupplier();
        }
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('logins/login_supplier');
        $this->load->view('portal-padrao/footer');
    }
    
    /*
     * Login Fornecedor
     */
    function loginSupplier() {
        if ($this->form_validation->run('login')){
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            
            $user = $this->doctrine->em->getRepository('Entities\User')
                        ->findBy(array('email'=> $email, 'password' => $password));
            if ($user) {
                if ($user[0]->getStatus() == STATUS_AGUARDANDO) {
                    $this->session->set_flashdata('error', MSG_ERROR_SENHA_NAO_ATIVADA);
                    redirect('logins/accountSupplier');
                }
                $arr = array(
                    'id' =>$user[0]->getId(),
                    'username' => $user[0]->getUsername(),
                    'level' => $user[0]->getLevel()->getLevel(),
                    'email' => $user[0]->getEmail(),
                );
                $this->session->set_userdata('login',$arr);
                redirect(base_url().'administrations');
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_USUARIO_NAO_ENCONTRADO);
                redirect('logins/accountSupplier');   
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('logins/accountSupplier');
        }
    }
}
