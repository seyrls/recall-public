<?php

class Recalls extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->helper( array('url','form','audit','functions','send_email') );
        $this->load->library('form_validation');
        /**
         * Vefifica se o usuário está logado
         * Se não estiver, redireciona para a página de login
         */
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
        
    }

    /*
     * Redireciona para a listagem de campanhas
     */
    function index() {
        $this->lists();
    }
    
    /*
     * Listar campanhas de recall
     */
    function lists() {
        $this->load->helper('status');
        $user = $this->session->userdata('login');
        $dados = array();
        $dados['level'] = $user['level'];
        
        if ($user['level'] < LEVEL_FORNECEDOR){
            $dados['dados'] = $this->sqlRecall();
        }else{
            
            $supplier = $this->doctrine->em
                                    ->getRepository('Entities\Supplier')
                                    ->findBy(array('user' => $user['id']));
            $supplier_id = $supplier[0]->getId();
            $dados['dados'] = $this->sqlRecall($supplier_id);
        }
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/lists',$dados);
        $this->load->view('template/footer');
    }
        
    /**
     * Editar dados da campanha
     */
    function editCampaign(){
        if (!$this->session->userdata('recall')) {
            redirect('recalls/insertCampaign');
        }
        $dados = array();
        $recall = $this->doctrine->em->getRepository('Entities\Recall')
                                     ->find($this->session->userdata('recall'));
        $user = $this->session->userdata('login');
        $dados['user'] = $user;
        $dados['recall'] = $recall->getArrayCampaign($recall);
        
        // se for administrador ou operador do sistema mostra todos os Fornecedores
        if (($user['level'] == LEVEL_ADMINISTRADOR) || ($user['level'] == LEVEL_OPERADOR)){
            $supplier = new Entities\Supplier();
            $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')->findAll();
            $dados['comboSupplier'] = $supplier->getComboSupplier($arrSupplier);
            $dados['supplier'] = array(
                'supplier_id' => $recall->getSupplier()->getid(),
                'trade_name' => $recall->getSupplier()->getTradeName()
            );
        }else{ // se nao, mostra apenas o nome da empresa
            if ($user['id']) {
                $arrSupplier = $this->doctrine->em
                                           ->getRepository('Entities\Supplier')
                                           ->findBy(array('user' => $user['id']));
                $dados['supplier'] = array(
                    'supplier_id' => $arrSupplier[0]->getId(),
                    'trade_name' => $arrSupplier[0]->getTradeName(),
                );
            }
        }
        
        $dados['habilita'] = $this->showFieldsByStatus($recall->getStatusCampaign());
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/form/_campaign',$dados);
        $this->load->view('template/footer');
    }
    
    /*
     * Inserir dados da campanha 
     */
    function insertCampaign(){
        
        $formOk = TRUE;
        
        if ($this->input->post()) {
            if ($this->form_validation->run('campaign')){
                $id_recall = !empty($this->input->post('id')) ? $this->input->post('id') : NULL;
                if ($id_recall) {
                    $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id_recall);
                    if ($recall->getTitle() != $this->input->post('title')) {
                        $this->form_validation->set_rules('title', 'Título', 'is_unique[recalls.title]');
                    }
                } else {
                    $this->form_validation->set_rules('title', 'Título', 'is_unique[recalls.title]');
                }
                if ($this->input->post('report_accident') && empty($this->input->post('reason_accident'))) {
                    $this->form_validation->set_rules('reason_accident', 'Descrição do acidente', 'required');
                }
                if ($this->form_validation->run() == FALSE) {
                    $formOk = FALSE;
                }
                $id = $this->session->userdata('recall');
                $this->doctrine->em->getConnection()->beginTransaction();
                if ($formOk) {
                    try {
                        $recall = $this->saveCampaign($this->input->post(),$id);
                        $this->session->set_userdata('recall',$recall->getId());
                        $this->session->set_flashdata('success', MSG_SUCCESS);
                        $this->doctrine->em->getConnection()->commit();
                        redirect('recalls/insertInfo');
                    }
                    catch(Exception $err){
                        $this->doctrine->em->getConnection()->rollback();
                        throw $err;
                    }
                }
            }
        } else {
            $this->session->unset_userdata('recall');
            $this->session->unset_userdata('product');
        }
        
        $dados = array();
        $dados['recall'] = array();
        
        $user = $this->session->userdata('login');
        $dados['user'] = $user;
        
        if (($user['level'] == LEVEL_ADMINISTRADOR) || ($user['level'] == LEVEL_OPERADOR)){
            $supplier = new Entities\Supplier();
            $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')
                    ->findBy(array(), array('corporateName' => 'ASC'));
            $dados['comboSupplier'] = $supplier->getComboSupplier($arrSupplier);
            $dados['supplier'] = array(
                'supplier_id' => ''
            );
        }else{
            if ($user['id']) {
                $arrSupplier = $this->doctrine->em
                                           ->getRepository('Entities\Supplier')
                                           ->findBy(array('user' => $user['id']));
                $dados['supplier'] = array(
                    'supplier_id' => $arrSupplier[0]->getId(),
                    'trade_name' => $arrSupplier[0]->getTradeName(),
                );
            }
        }
        
        $dados['habilita'] = $this->showFieldsByStatus(NULL);

        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/form/_campaign',$dados);
        $this->load->view('template/footer');
    }
    
    /*
     * salvar a campanha do recall
     * return Object
     */
    private function saveCampaign($dados = array(),$id = null) {
        if (empty($dados)) {
            return false;
        } else {
            if (!$id) {
                $recall = new Entities\Recall;
            } else {
                $recall = $this->doctrine->em->getRepository('Entities\Recall')
                                             ->find($id);
            }
            if ($dados['supplier_id']) {
                $supplier = $this->doctrine->em->getRepository('Entities\Supplier')
                                           ->find($dados['supplier_id']);
            }
            $recall->setTitle($dados['title']);
            $protocol = str_replace(array('.','/','-'), '', $dados['protocol']);
            $recall->setProtocol($protocol);
            $recall->setDescription($dados['description']);
            $recall->setCorrectiveMeasure($dados['corrective_measure']);
            $accidentReport = (int)$dados['report_accident'];
            $recall->setReportAccident($accidentReport);
            $reasonAccident = ($accidentReport) ? $dados['reason_accident'] : NULL;
            $recall->setReasonAccident($reasonAccident);
            $recall->setSupplier($supplier);
            $recall->setDateInsert(new \DateTime());
            $recall->setStatusCampaign(STATUS_ID_EM_CADASTRAMENTO);
            if (!$recall->getTimeLine()) {
                $recall->setTimeLine(TIME_LINE_INFO);
            }
            
            $this->doctrine->em->persist($recall);
            $this->doctrine->em->flush();
            
            return $recall;
        }
    }
    
    /*
     * Inserir informacaoes gerais da campanha
     */
    function insertInfo(){
        $id = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        if (!$recall) {
            redirect('recalls/insertCampaign');
        }
        if ($recall->getTimeLine() < TIME_LINE_INFO) {
            redirect('recalls/insertCampaign');
        }
        if ($this->input->post()) {
            if ($this->form_validation->run('info')){
                $this->doctrine->em->getConnection()->beginTransaction();
                try {
                    $product = $this->doctrine->em->getRepository('Entities\Product')->findBy(array('recall' => $id));
                    $recall = $this->saveInfo($this->input->post(),$id);
                    $this->session->set_flashdata('success', MSG_SUCCESS);
                    $this->doctrine->em->getConnection()->commit();
                    if ($product) {
                        redirect('recalls/listsProducts');
                    } else {
                        redirect('recalls/InsertProduct');
                    }
                }
                catch(Exception $err){
                    $this->doctrine->em->getConnection()->rollback();
                    throw $err;
                }
            }
        }
        
        $typeRisk = new Entities\TypeRisk();
        $arrTypeRisk = $this->doctrine->em->getRepository('Entities\TypeRisk')
                            ->findBy(array('status' => TRUE), array('typeRisk' => 'ASC'));
        $dados['comboTypeRisk'] = $typeRisk->getComboTypeRisk($arrTypeRisk);
        $dados['type_risk'] = array(
            'type_risk_id' => ''
        );
        
        $dados['dados'] = $recall->getArrayInfoRecall($recall);
        $dados['time_line'] = $recall->getTimeLine();
        $dados['habilita'] = $this->showFieldsByStatus($recall->getStatusCampaign());
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/form/_info',$dados);
        $this->load->view('template/footer');
    }
    
    // persistir no banco de dados as informacoes da campanha
    private function saveInfo($dados = array(),$id = null) {
        if (empty($dados) or (!$id)) {
            return false;
        } else {
            $recall = $this->doctrine->em->getRepository('Entities\Recall')
                                             ->find($id);
            $recall->setFaultyPart($dados['faulty_part']);
            $date_faulty = ($dados['date_faulty']) ? implode("-",array_reverse(explode("/",$dados['date_faulty']))) : NULL;
            $recall->setDateFaulty($date_faulty);
            $recall->setFaultyDescribe($dados['faulty_describe']);
            $recall->setFaulty($dados['faulty']);
            $recall->setRiskFaulty($dados['risk_faulty']);
            if ($recall->getTimeLine() == TIME_LINE_INFO) {
                $recall->setTimeLine(TIME_LINE_PRODUCT);
            }
            
            if ($dados['type_risk_id']) {
                $typeRisk = $this->doctrine->em->getRepository('Entities\TypeRisk')
                                           ->find($dados['type_risk_id']);
                $recall->setTypeRisk($typeRisk);
            }
            
            $this->doctrine->em->persist($recall);
            $this->doctrine->em->flush();
            return $recall;
        }
    }
    function check_batch() {
        return true;
    }
    
    /*
     * Metodo para inserir / alterar os dados de um produto
     */
    function insertProduct(){
        $dados = array();
        $idRecall = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($idRecall);
        if (!$recall) {
            redirect('recalls/insertCampaign');
        }
        // salva na sessao product o id do produto alterado
        if ($this->input->post('edit')) {
            $this->session->set_userdata('product',$this->input->post('edit'));
        } else {
            $this->session->unset_userdata('product');
        }
        
        // verifica se ja existe um produto na sessao
        $product_id = ($this->session->userdata('product')) ? $this->session->userdata('product') : $this->input->post('product_id');
        
        if ($product_id) {
            $product            = $this->doctrine->em->getRepository('Entities\Product')->find($product_id);
            $dados['product']   = $this->getArrayOneProduct($product);
            $arrBatch           = $this->doctrine->em->getRepository('Entities\Batch')->findBy(array('product' => $product_id));
            $arrAffected        = $this->doctrine->em->getRepository('Entities\Affected')->findBy(array('product' => $product_id));
        } else {
            $dados['product']   = array();
            $arrAffected        = array();
            $arrBatch           = array();
        }
        
        if (($this->input->post()) && (!$this->input->post('edit'))) {
            
            // validacao de todas as abas do produto
            $formOk = $this->validacaoProduto();
            
            if ($formOk) {
                $this->doctrine->em->getConnection()->beginTransaction();
                try {
                    $product = $this->saveProduct($this->input->post(),$idRecall);
                    $this->session->set_flashdata('success', MSG_SUCCESS);
                    $this->doctrine->em->getConnection()->commit();
                    $this->session->unset_userdata('product');
                    redirect('recalls/listsProducts');
                }
                catch(Exception $err){
                    $this->doctrine->em->getConnection()->rollback();
                    throw $err;
                }
            } else {
                $this->session->set_flashdata('error', MSG_ERROR);
            }
        }
        
        $batch          = new Entities\Batch;
        $affected       = new Entities\Affected;
        $branch         = new Entities\Branch;
        $state          = new Entities\State;
        $country        = new Entities\Country;
        $manufacturer   = new Entities\Manufacturer;
        $type_product   = new Entities\TypeProduct;
        
        $arrBranch          = $this->doctrine->em->getRepository('Entities\Branch')
                                ->findBy(array('status' => TRUE),array('branch' => 'ASC'));
        
        $arrState           = $this->doctrine->em->getRepository('Entities\State')
                                ->findBy(array(),array('stateName' => 'ASC'));
        
        $arrCountry         = $this->doctrine->em->getRepository('Entities\Country')
                                ->findBy(array(),array('country' => 'ASC'));
        
        $arrManufacturer    = $this->doctrine->em->getRepository('Entities\Manufacturer')
                                ->findBy(array('status' => TRUE),array('corporateName' => 'ASC'));
        
        
        $branch_id = isset($dados['product']['branch_id']) ? 
                            $dados['product']['branch_id'] : 
                            $this->input->post('branch_id');
        if ($branch_id) {
            $arrTypeProduct = $this->doctrine->em->getRepository('Entities\TypeProduct')
                            ->findBy(array('branch' => $branch_id),array('typeProduct' => 'ASC'));
        } else {
            $arrTypeProduct = array();
        }
        
        $dados['comboBranch']       = $branch->getComboBranch($arrBranch);
        $dados['comboTypeProduct']  = $type_product->getComboTypeProduct($arrTypeProduct);
        $dados['comboState']        = $state->getComboState($arrState);
        $dados['comboManufacturer'] = $manufacturer->getComboManufacturer($arrManufacturer);
        $dados['comboCountry']      = $country->getComboCountry($arrCountry);
        $dados['batch']             = $batch->getArrayAllBatch($arrBatch);
        $dados['affecteds']         = $affected->getArrayAllAffected($arrAffected);
        $dados['habilita']          = $this->showFieldsByStatus($recall->getStatusCampaign());
        $dados['time_line']         = $recall->getTimeLine();
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/form/_product',$dados);
        $this->load->view('template/footer');
    }
    
    /*
     * Funcao para validar todos os campos (separados) do produto de recall.
     * @ return boolean
     */
    private function validacaoProduto(){
        // validacao informacoes gerais do produto
        $this->form_validation->set_rules('branch_id', 'Ramo da Atividade', 'required');
        $this->form_validation->set_rules('type_product_id', 'Tipo do Produto', 'required');
        $this->form_validation->set_rules('country_id', 'País de origem', 'required');
        $this->form_validation->set_rules('product', 'Produto', 'required|max_length[150]');
        $this->form_validation->set_rules('manufacturer_id', 'Fabricante', 'required');
        $this->form_validation->set_rules('model', 'Modelo', 'required|max_length[45]');
        $this->form_validation->set_rules('year_manufacture', 'Ano de fabricação', 'required|numeric|max_length[4]|check_ano_valid');
        $this->form_validation->set_rules('quantity_affected', 'Quantidade afetada', 'required|numeric');

        // validacao para os lotes do produto
        $count = count($this->input->post('final_batch'));
        for ($i=0; $i<$count; $i++) :
            $this->form_validation->set_rules('initial_batch['.$i.']', 'Lote inicial', 'required|max_length[45]');
            $this->form_validation->set_rules('final_batch['.$i.']', 'Lote final', 'required|max_length[45]');
            $this->form_validation->set_rules('manufacturer_initial_date['.$i.']', 'Data inicio de fabricação', 'required|check_date_valid');
            $this->form_validation->set_rules('manufacturer_final_date['.$i.']', 'Data fim de fabricação', 'required|check_date_valid');
            
            // verifica se a data inicial eh maior ou igual a data final
            if ((!empty($this->input->post('manufacturer_initial_date')[$i])) && (!empty($this->input->post('manufacturer_final_date')[$i]))) {
                $dt_valid = dif_datas($this->input->post('manufacturer_initial_date')[$i], $this->input->post('manufacturer_final_date')[$i]);
                if (!$dt_valid) {
                    $this->form_validation->set_rules('manufacturer_final_date['.$i.']', 'Data fim de fabricação', 'check_dt_ini_menor_igual_dt_fim');
                }
            }
            if (!empty($this->input->post('expiry_date')[$i])) {
                $this->form_validation->set_rules('expiry_date['.$i.']', 'Validade', 'check_date_valid');
            }
        endfor;
        
        // validacao quantidade afetada
        $countQtd = count($this->input->post('quantity'));
        for ($q=0; $q<$countQtd; $q++) :
            $this->form_validation->set_rules('quantity['.$q.']', 'Quantidade', 'required|numeric');
            $this->form_validation->set_rules('iduf['.$q.']', 'UF', 'required');
        endfor;
        
        // validacao para o campo imagem do produto
        if ((empty($_FILES['userfile']['name'][0])) && (!$this->input->post('userfile'))){
            $this->form_validation->set_rules('userfile[]', 'Imagem', 'required');
        }

        // verifica se ocorreu algum erro em uma das validacoes acima
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    /*
     * lista todos os productos cadastrados
     */
    function listsProducts() {
        $idRecall = $this->session->userdata('recall');
        if ($idRecall) {
            $product = $this->doctrine->em->getRepository('Entities\Product')->findBy(array('recall' => $idRecall));
            $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($idRecall);
            
            $dados['product'] = array();
            if ($product) {
                $dados['product'] = $this->getArrayAllProduct($product);
            } else {
                redirect('recalls/insertProduct');
            }
            
            $dados['habilita']      = $this->showFieldsByStatus($recall->getStatusCampaign());
            
            $this->load->view('template/header');
            $this->load->view('recalls/index');
            $this->load->view('template/messages');
            $this->load->view('recalls/timeLineForm');
            $this->load->view('recalls/form/_listsProducts',$dados);
            $this->load->view('template/footer');   
        } else {
            redirect('recalls/insertProduct');
        }
    }
    
    /*
     * Salva os dados do produto do recall
     * return Object
     */
    private function saveProduct($dados = array(), $idRecall = null) {
        
        if (empty($dados) || (is_null($idRecall))) {
            return false;
        } else {
            if ($dados['product_id']) {
                $product = $this->doctrine->em
                ->getRepository('Entities\Product')
                ->find($dados['product_id']);
            }else {
                $product = new Entities\Product;
            }
            
            $type_product = $this->doctrine->em
                ->getRepository('Entities\TypeProduct')
                ->find($this->input->post('type_product_id'));
            
            $country = $this->doctrine->em
                ->getRepository('Entities\Country')
                ->find($this->input->post('country_id'));
            
            $countryExport = $this->doctrine->em
                ->getRepository('Entities\Country')
                ->find($this->input->post('country_export_id'));

            $manufacturer = $this->doctrine->em
                ->getRepository('Entities\Manufacturer')
                ->find($this->input->post('manufacturer_id'));

            $recall = $this->doctrine->em
                ->getRepository('Entities\Recall')
                ->find($idRecall);
            
            $product->setProduct($this->input->post('product'));
            $product->setModel($this->input->post('model'));
            $product->setYearManufacture($this->input->post('year_manufacture'));
            $product->setQuantityAffected($this->input->post('quantity_affected'));
            $product->setTypeProduct($type_product);
            $product->setCountry($country);
            $product->setCountryExport($countryExport);
            
            $product->setManufacturer($manufacturer);
            $product->setRecall($recall);
            
            if ($recall->getTimeLine() == TIME_LINE_PRODUCT) {
                $recall->setTimeLine(TIME_LINE_LOCATION);
                $this->doctrine->em->persist($recall);
            }
            
            $this->doctrine->em->persist($product);
            $this->doctrine->em->flush();
            
            $this->saveImages($product);
            $type = UPLOAD_IMAGE; // tipo de do arquivo para fazer upload
            $this->do_upload($type,$product->getId());
            $this->saveBatch($product);
            $this->saveAffecteds($product);

            return $product;
        }
    }
    
    function insertLocation() {
        
        $dados = array();
        $msg = NULL;
        
        $id = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        
        if (!$recall) {
            redirect('recalls/insertCampaign');
        }
        
        if ($this->input->post()) {
            if ($this->form_validation->run('location')){
                $this->doctrine->em->getConnection()->beginTransaction();
                try {
                    $recall = $this->saveLocation($this->input->post(),$id);
                    $this->session->set_flashdata('success', MSG_SUCCESS);
                    $this->doctrine->em->getConnection()->commit();
                    redirect('recalls/insertMedia');
                }
                catch(Exception $err){
                    $this->doctrine->em->getConnection()->rollback();
                    throw $err;
                }
            } else{
                $msg['message_error'] = validation_errors();
            }
        }
        
        $dados['dados']     = $recall->getArrayLocationRecall($recall);
        $dados['time_line'] = $recall->getTimeLine();
        $dados['habilita']  = $this->showFieldsByStatus($recall->getStatusCampaign());

        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm',$msg);
        $this->load->view('recalls/form/_location_service',$dados);
        $this->load->view('template/footer');
    }
    
    private function saveLocation($dados = array(),$id = null) {
        
        if (empty($dados) || is_null($id)) {
            return false;
        } else {
            $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        }
        
        $recall->setCustomerService($dados['customer_service']);
        $recall->setAddressService($dados['address_service']);
        $recall->setSite($dados['site']);
        $recall->setEmail($dados['email']);
        $phone = str_replace(array('(',')',' ','-'), '', $dados['phone_service']);
        $recall->setPhoneService($phone);
        $recall->setNoticeService($dados['notice_service']);
        
        if ($recall->getTimeLine() == TIME_LINE_LOCATION) {
            $recall->setTimeLine(TIME_LINE_MEDIA);
        }
        
        $this->doctrine->em->persist($recall);
        $this->doctrine->em->flush();

        return $recall;
    }
    
    function insertMedia() {
        $dados = array();
        $msg = '';
        $id = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')
                                    ->find($id);
        
        if ($recall->getTimeLine() < TIME_LINE_MEDIA) {
            redirect('recalls/insertLocation');
        }
        
        $arrMedia = $this->doctrine->em->getRepository('Entities\MediaPlan')
                                    ->findBy(array('recall' => $id));
        
        $files = $this->doctrine->em->getRepository('Entities\File')
                                    ->findBy(array('recall' => $id, 'belongsTo' => FILE_BELONGS_TO_MEDIA_PLAN));
        
        $links = $this->doctrine->em->getRepository('Entities\Link')
                                    ->findBy(array('recall' => $id));
        
        if ($this->input->post()) {
            if ($this->form_validation->run('media')){
                $this->doctrine->em->getConnection()->beginTransaction();
                try {
                    $media = $this->saveMediaPlan($this->input->post(),$id);
                    if (($_FILES) || ($this->input->post('deleteFile'))) {
                        $this->saveRecallFile($id, FILE_BELONGS_TO_MEDIA_PLAN);
                    }
                    if (($this->input->post('deleteLink')) || ($this->input->post('url'))) {
                        $this->saveLink($id);
                    }
                    $this->session->set_flashdata('success', MSG_SUCCESS);
                    $this->doctrine->em->getConnection()->commit();
                    redirect('recalls/insertRisk',$media);
                }
                catch(Exception $err){
                    $this->doctrine->em->getConnection()->rollback();
                    throw $err;
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('recalls/insertMedia');
            }
        }
        $media = new Entities\MediaPlan;
        $dados['origins']   = $this->getComboAllOrigin();
        $dados['media']     = $media->getArrayMedia($arrMedia);
        $dados['files']     = $this->getArrayFiles($files);
        $dados['links']     = $this->getArrayLinks($links);
        $dados['time_line'] = $recall->getTimeLine();
        $dados['habilita']  = $this->showFieldsByStatus($recall->getStatusCampaign());
        
        
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm',$msg);
        $this->load->view('recalls/form/_media',$dados);
        $this->load->view('template/footer');
    }
    
    private function saveMediaPlan($dados = array(),$id = null) {
        
        $arrMedia = ($this->input->post('deleteMedia')) ? $this->input->post('deleteMedia') : array();
        if ($arrMedia) {
            foreach ($arrMedia as $media) {
                $this->removeMediaPlanById($media);
            }
        }
        
        $total = count($dados['b_source']); //total de itens na tabela
        $date_insertion = $dados['date_insertion'];
        $arrSource = $dados['b_source'];
        $total_insertion = $dados['total_insertion'];
        $date_insertion_end = $dados['date_insertion_end'];
        $media_cost = $dados['media_cost'];
        $arrIdMedia = isset($dados['id_media_plan']) ? $dados['id_media_plan'] : array();
        $recall = $this->doctrine->em
                ->getRepository('Entities\Recall')
                ->find($id);
        
        $media_cost = str_replace('.', '', $media_cost);
        $media_cost = str_replace(',', '.', $media_cost);
        
        for ($i=0; $i < $total; $i++) {
            if ((!empty($arrIdMedia)) && isset($arrIdMedia[$i])) {
                $modelMedia = $this->doctrine->em
                    ->getRepository('Entities\MediaPlan')
                    ->find($arrIdMedia[$i]);
                $mediaplan = ($modelMedia) ? $modelMedia : new Entities\MediaPlan;
            } else {
                $mediaplan = new Entities\MediaPlan;
            }
            
            $source = $this->doctrine->em
                ->getRepository('Entities\Source')
                ->find($arrSource[$i]);
        
            // date e hora insercao
            $dt1 = substr($date_insertion[$i],0,10);
            $hr1 = substr($date_insertion[$i],11,14);
            $dt_insertion = implode("-",array_reverse(explode("/",$dt1))).$hr1;
            
            // date e hora insercao final
            $dt2 = substr($date_insertion_end[$i],0,10);
            $hr2 = substr($date_insertion_end[$i],11,14);
            $dt_insertion_end = implode("-",array_reverse(explode("/",$dt2))).$hr2;
            
            $mediaplan->setDateInsertion($dt_insertion);
            $mediaplan->setDateInsertionEnd($dt_insertion_end);
            $mediaplan->setRecall($recall);
            $mediaplan->setSource($source);
            $mediaplan->setMediaCost($media_cost[$i]);
            $mediaplan->setTotalInsertion($total_insertion[$i]);
            
            if ($recall->getTimeLine() == TIME_LINE_MEDIA) {
                $recall->setTimeLine(TIME_LINE_RISK);
                $this->doctrine->em->persist($recall);
            }
            
            $this->doctrine->em->persist($mediaplan);
            $this->doctrine->em->flush();
        }
        return $mediaplan;
    }
    
    /*
     * Remove todas as midias referentes ao recall
     */
    private function removeMediaPlan($recall_id = null) {
        $mediaPlan = $this->doctrine->em
                    ->getRepository('Entities\MediaPlan')
                    ->findBy(array('recall' => $recall_id));
        if ($mediaPlan) {
            foreach ($mediaPlan as $media) {
                try {
                    $this->doctrine->em->remove($media);
                    $this->doctrine->em->flush();
                }
                catch(Exception $err){
                    $this->doctrine->em->getConnection()->rollback();
                    throw $err;
                }
            }
        }
    }
    /*
     * Remove midia por id_media_plan
     */
    private function removeMediaPlanById($id) {
        $mediaPlan = $this->doctrine->em
                    ->getRepository('Entities\MediaPlan')
                    ->find($id);
        if ($mediaPlan) {
            $this->doctrine->em->remove($mediaPlan);
            $this->doctrine->em->flush();
        }
    }
    /*
     * Salva os links de Midia
     */
    private function saveLink($recall_id) {
        // remove os arquivos deletados pelo usuario
        $arrLinks = ($this->input->post('deleteLink')) ? $this->input->post('deleteLink') : array();
        if ($arrLinks) {
            foreach ($arrLinks as $link) {
                $this->removeLinkById($link);
            }
        }
        if (in_array(!null,$this->input->post('url'))) {
            $count = ($this->input->post('url')) ? count($this->input->post('url')) : 0;
            $url = $this->input->post('url');
            $arrIdLink = $this->input->post('id_link');
            $recall = $this->doctrine->em
                        ->getRepository('Entities\Recall')
                        ->find($recall_id);

            for ($i=0; $i<$count; $i++){
                if ($arrIdLink[$i]) {
                    $modelLink = $this->doctrine->em
                        ->getRepository('Entities\Link')
                        ->find($arrIdLink[$i]);
                    $link = ($modelLink) ? $modelLink : new Entities\Link;
                }else {
                    $link = new Entities\Link;
                }

                $http = 'http://';
                $protocolo = substr($url[$i], 0,7);
                if ($protocolo != $http) {
                    $url[$i] = 'http://'.$url[$i];
                }

                $link->setUrl($url[$i]);
                $link->setRecall($recall);

                $this->doctrine->em->persist($link);
                $this->doctrine->em->flush();
            }
        }
    }
    
    /*
     * Remove arquivo especifico
     */
    private function removeFileById($id) {
        $file = $this->doctrine->em
                    ->getRepository('Entities\File')
                    ->find($id);
        if ($file) {
            $this->doctrine->em->remove($file);
            $this->doctrine->em->flush();
        }
    }
    /*
     * Remove link especifico
     */
    private function removeLinkById($id) {
        $link = $this->doctrine->em
                    ->getRepository('Entities\Link')
                    ->find($id);
        if ($link) {
            $this->doctrine->em->remove($link);
            $this->doctrine->em->flush();
        }    
    }
    
    function insertRisk(){
        $dados = array();
        $id = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        if (!$recall) {
            redirect('recalls/insertCampaign');
        }
        
        if (!empty($this->input->post()) || !empty($_FILES)) {
            if ($this->form_validation->run('risk')){
                if (max($_FILES['userfile']['size']) > MAX_UPLOAD_FILE_SIZE) {
                    $this->session->set_flashdata('error', MSG_UPLOAD_ACIMA_PERMITIDO);
                } else {
                    $this->doctrine->em->getConnection()->beginTransaction();
                    try {
                        $this->saveRisk($recall);
                        $this->saveRecallFile($recall->getId(),FILE_BELONGS_TO_RISK_ALERT);
                        $this->session->set_flashdata('success', MSG_SUCCESS);
                        $this->doctrine->em->getConnection()->commit();
                        redirect('recalls/insertProcess',$recall);
                    }
                    catch(Exception $err){
                        $this->doctrine->em->getConnection()->rollback();
                        throw $err;
                    }
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
            }
        }
        
        $dados['ckeditor'] = $this->getEditor(400);
        
        $files = $this->doctrine->em->getRepository('Entities\File')
                                    ->findBy(array('recall' => $id, 'belongsTo' => FILE_BELONGS_TO_RISK_ALERT));
        
        $dados['files']         = $this->getArrayFiles($files);
        $dados['notice_risk']   = $recall->getNoticeRisk();
        $dados['time_line']     = $recall->getTimeLine();
        $dados['habilita']      = $this->showFieldsByStatus($recall->getStatusCampaign());
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/form/_risk',$dados);
        $this->load->view('template/footer');
    }
    
    function saveRisk($recall){
        if ($recall) {
            $recall->setNoticeRisk($this->input->post('notice_risk'));
            if ($recall->getTimeLine() == TIME_LINE_RISK) {
                $recall->setTimeLine(TIME_LINE_PROCESS);
            }
            $this->doctrine->em->persist($recall);
            $this->doctrine->em->flush();
        }
    }
    
    function saveRecallFile($recall_id, $belongs_to){
        if (!$recall_id) {
            $recall_id = $this->session->userdata('recall');
        }
       // remove os arquivos deletados pelo usuario
        $arrFiles = ($this->input->post('deleteFile')) ? $this->input->post('deleteFile') : array();
        if ($arrFiles) {
            foreach ($arrFiles as $file) {
                $this->removeFileById($file);
            }
        }
        $files = $_FILES['userfile']['name'];
        $count = count($_FILES['userfile']['name']);
        $arrIdFile = $this->input->post('id_file');
        $recall = $this->doctrine->em
                    ->getRepository('Entities\Recall')
                    ->find($recall_id);
        for ($i=0; $i<$count; $i++){
            if ($files[$i]) {
                if ($arrIdFile[$i]) {
                    $modelFile = $this->doctrine->em
                        ->getRepository('Entities\File')
                        ->find($arrIdFile[$i]);
                    $file = ($modelFile) ? $modelFile : new Entities\File;
                } else {
                    $file = new Entities\File;
                }
                $fileName = str_replace(" ","_",preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim($files[$i]))));
                $file->setFilePath(base_url().'uploads/'.$recall_id.'/'.$fileName);
                
                $file->setBelongsTo($belongs_to);
                $file->setRecall($recall);
                $type = UPLOAD_FILE; // tipo de do arquivo para fazer upload
                $this->do_upload($type);
                $this->doctrine->em->persist($file);
                $this->doctrine->em->flush();
            }
        }
    }
    
    function insertProcess() {
        $dados['dados'] = array();
        $id = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        if (!$recall) {
            redirect('recalls/insertCampaign');
        }
        if ($this->input->post()) {
            if ($this->form_validation->run('process')){
                $formOk = TRUE;
                if ($this->input->post('possui_processo') == TRUE) {
                    $this->form_validation->set_rules('process_number', 'Nº do Processo', 'required|max_length[60]');
                    $this->form_validation->set_rules('identification', 'Identificação do consumidor', 'max_length[100]');
                    $this->form_validation->set_rules('date_process', 'Data do processo', 'check_date_valid');
                    $this->form_validation->set_rules('staff', 'Vara', 'max_length[45]');
                    $this->form_validation->set_rules('distrito', 'Comarca', 'max_length[45]');
                    $this->form_validation->set_rules('situation', 'Situação', 'max_length[45]');
                    $this->form_validation->set_rules('describe_process', 'Descrição da ação', 'max_length[100]');
                    if ($this->form_validation->run() == FALSE) {
                        $formOk = FALSE;
                    }
                }
                if ($formOk) {
                    $this->doctrine->em->getConnection()->beginTransaction();
                    try {
                        $this->saveProcess($this->input->post(),$recall);
                        $this->session->set_flashdata('success', MSG_SUCCESS);
                        $this->doctrine->em->getConnection()->commit();
                        redirect('recalls/view');
                    }
                    catch(Exception $err){
                        $this->doctrine->em->getConnection()->rollback();
                        throw $err;
                    }
                }
            }
        }
        $dados['dados']     = $this->getArrayProcess($recall);
        $dados['time_line'] = $recall->getTimeLine();
        $dados['habilita']  = $this->showFieldsByStatus($recall->getStatusCampaign());
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/form/_process',$dados);
        $this->load->view('template/footer');
    }
    
    private function saveProcess($dados = array(), $recall){
        $modelProcess = $this->doctrine->em->getRepository('Entities\Process')
                                      ->findBy(array('recall' => $recall));

        $process = ($modelProcess) ? current($modelProcess) : new Entities\Process;
        
        $possui_processo = $dados['possui_processo'];
        
        # caso o usuario nao tenha processo, seta os campos = null
        $process_number     = ($possui_processo == 1) ? $dados['process_number'] : NULL;
        $identification     = ($possui_processo == 1) ? $dados['identification'] : NULL;
        $date_process       = ($possui_processo == 1) ? $dados['date_process'] : NULL;
        $describe_process   = ($possui_processo == 1) ? $dados['describe_process'] : NULL;
        $distrito           = ($possui_processo == 1) ? $dados['distrito'] : NULL;
        $situation          = ($possui_processo == 1) ? $dados['situation'] : NULL;
        $staff              = ($possui_processo == 1) ? $dados['staff'] : NULL;

        $process->setProcessNumber($process_number);
        $process->setIdentification($identification);
        $dt = ($date_process) ? implode("-",array_reverse(explode("/",$date_process))) : NULL;
        $process->setDateProcess($dt);
        $process->setDescribeProcess($describe_process);
        $process->setDistrito($distrito);
        $process->setRecall($recall);
        $process->setSituation($situation);
        $process->setStaff($staff);
        
        if ($recall->getTimeLine() == TIME_LINE_PROCESS) {
            $recall->setTimeLine(TIME_LINE_SEND_CAMPAIGN);
            $this->doctrine->em->persist($recall);
        }

        $this->doctrine->em->persist($process);
        $this->doctrine->em->flush();

        return $process;
    }
    
    function view() {
        
        $dados = array();
        $this->load->helper('status');
        
        if ($this->input->post("recall")) {
            $recall_id = $this->input->post('recall');
            $this->session->set_userdata('recall',$recall_id);
            $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($recall_id);
            $timeLinhe = $recall->getTimeLine();
            if ($timeLinhe < TIME_LINE_SEND_CAMPAIGN) {
                $this->redirectByTimeLine($timeLinhe);
            }
        }
        
        $id         = $this->session->userdata('recall');
        if (!$id) {
            redirect('recalls/index');
        }
        $recall         = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        $arrMedia          = $this->doctrine->em->getRepository('Entities\MediaPlan')->findBy(array('recall' => $id));
        $mediaFile      = $this->doctrine->em->getRepository('Entities\File')->findBy(array('recall' => $id, 'belongsTo' => FILE_BELONGS_TO_MEDIA_PLAN));
        $riskFile       = $this->doctrine->em->getRepository('Entities\File')->findBy(array('recall' => $id, 'belongsTo' => FILE_BELONGS_TO_RISK_ALERT));
        $disapproveFile = $this->doctrine->em->getRepository('Entities\File')->findBy(array('recall' => $id, 'belongsTo' => FILE_BELONGS_TO_MESSAGE_DISAPPROVE_RECALL));
        $links          = $this->doctrine->em->getRepository('Entities\Link')->findBy(array('recall' => $id));
        $product        = $this->doctrine->em->getRepository('Entities\Product')->findBy(array('recall' => $id));
        $user           = $this->session->userdata('login');
        
        $media = new Entities\MediaPlan;
        
        $dados['dados']['level']            = $user['level'];
        $dados['dados']['campaign']         = $recall->getArrayCampaign($recall);
        $dados['dados']['info']             = $recall->getArrayInfoRecall($recall);
        $dados['dados']['product']          = $this->getArrayAllProduct($product);
        $dados['dados']['location']         = $recall->getArrayLocationRecall($recall);
        $dados['dados']['media']            = $media->getArrayMedia($arrMedia);
        $dados['dados']['mediaFiles']       = $this->getArrayFiles($mediaFile);
        $dados['dados']['riskFiles']        = $this->getArrayFiles($riskFile);
        $dados['dados']['disapproveFiles']  = $this->getArrayFiles($disapproveFile);
        $dados['dados']['links']            = $this->getArrayLinks($links);
        $dados['dados']['notice_risk']      = $recall->getNoticeRisk();
        $dados['dados']['process']          = $this->getArrayProcess($recall);
        $dados['dados']['supplier']         = $this->getArraySupplierByRecall($recall);
        
        // pega a situacao atual da campanha
        $status_campaign = $recall->getStatusCampaign();
        $dados['dados']['status'] = get_status_campaign($status_campaign);
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/view',$dados);
        $this->load->view('template/footer');
    }
    
    function getArraySupplierByRecall($recall) {
        $dados = array();
        if ($recall) {
            $dados = array(
                'cnpj'                  => ($recall->getSupplier()->getCnpj()) ? mask('##.###.###/####-##',$recall->getSupplier()->getCnpj()) : $recall->getSupplier()->getCnpj(),
                'corporate_name'        => $recall->getSupplier()->getCorporateName(),
                'trade_name'            => $recall->getSupplier()->getTradeName(),
                'state_registration'    => $recall->getSupplier()->getStateRegistration(),
                'email_copy'            => $recall->getSupplier()->getEmailCopy(),
                'address'               => $recall->getSupplier()->getAddress(),
                'complement'            => $recall->getSupplier()->getComplement(),
                'district'              => $recall->getSupplier()->getDistrict(),
                'zip'                   => ($recall->getSupplier()->getZip()) ? mask("#####-###",$recall->getSupplier()->getZip()) : $recall->getSupplier()->getZip(),
                'commercial_phone'      => ($recall->getSupplier()->getCommercialPhone()) ? ((strlen($recall->getSupplier()->getCommercialPhone()) == 10) ? mask('(##) ####-####', $recall->getSupplier()->getCommercialPhone()) : mask('(##) ####-#####', $recall->getSupplier()->getCommercialPhone())) : $recall->getSupplier()->getCommercialPhone(),
                'fax_phone'             => ($recall->getSupplier()->getFaxPhone()) ? ((strlen($recall->getSupplier()->getFaxPhone()) == 10) ? mask('(##) ####-####', $recall->getSupplier()->getFaxPhone()) : mask('(##) ####-#####', $recall->getSupplier()->getFaxPhone())) : $recall->getSupplier()->getFaxPhone(),
                'city'                  => $recall->getSupplier()->getCity()->getCity(),
                'user_id'               => $recall->getSupplier()->getUser()->getId(),
                'email'                 => $recall->getSupplier()->getUser()->getEmail(),
                'username'              => $recall->getSupplier()->getUser()->getUsername(),
            );
        }
        return $dados;
    }
    
    /*
     * Redireciona para a action referente a time line
     */
    function redirectByTimeLine($timeLine) {
        switch ($timeLine) {
            case TIME_LINE_CAMPAIGN:
                redirect('recalls/editCampaign');
                break;
            case TIME_LINE_INFO:
                redirect('recalls/insertInfo');
                break;
            case TIME_LINE_PRODUCT:
                redirect('recalls/listsProducts');
                break;
            case TIME_LINE_LOCATION:
                redirect('recalls/insertLocation');
                break;
            case TIME_LINE_MEDIA:
                redirect('recalls/insertMedia');
                break;
            case TIME_LINE_RISK:
                redirect('recalls/insertRisk');
                break;
            case TIME_LINE_PROCESS:
                redirect('recalls/insertProcess');
                break;
            case TIME_LINE_SEND_CAMPAIGN:
                redirect('recalls/view');
                break;
            case TIME_LINE_ENVIADA:
                redirect('recalls/view');
                break;
            default:
                redirect('recalls/insertCampaign');
                break;
        }
    }
    
    function publishApprove() {
        $dados = array();
        $id = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        $idUser = $recall->getSupplier()->getUser()->getId();
        
        $userSupplier = $this->doctrine->em->getRepository('Entities\User')->findById($idUser);
        $emailSupplier = ($userSupplier[0]->getEmail()) ? $userSupplier[0]->getEmail() : NULL;
        
        $supplier = $this->doctrine->em->getRepository('Entities\Supplier')->findBy(array('user' => $idUser));
        $emailSupplierCopy = ($supplier[0]->getEmailCopy()) ? $supplier[0]->getEmailCopy() : NULL;
        
        $this->verificaPermissaoPublicacao($recall);
        $arrGroup = $this->doctrine->em->getRepository('Entities\Group')
                                    ->findBy(array('status' => STATUS_ATIVO),array('name' => 'ASC'));
        $group = new \Entities\Group;
        $dados['groups'] = $group->getArrayAllGroup($arrGroup);
        $acion = "recalls/publishApprove";
        
        if ($this->input->post('protocol')) {
            $this->saveProtocol($recall,$this->input->post('protocol'),$acion); 
        }
        if ($this->input->post('message')) {
            if ($this->input->post('idGroup')) {
                $mailPartners = $this->getEmailPartners($this->input->post('idGroup'));
            } else {
                $mailPartners = array();
            }
            $mailSubscriber = $this->getEmailSubscribers($recall);
            $arrMailTo = array_merge($mailPartners,$mailSubscriber);
            $arrMail = array_unique($arrMailTo);
            
            // adiciona email do fornecedor
            array_push($arrMail, $emailSupplier);
            
            // transforma array de email em string
            $mailto = implode(',', $arrMail);
            
            // caso nao tenha nenhum e-mail cadastrado (grupo parceiro nem pessoa fisica)
            if (!$mailto) {
                $mailto = EMAIL_FROM_TESTE;
            }
            $mailcopy = $this->input->post('mailcopy');
            $this->approveRecall($recall,$this->input->post('message'),STATUS_ID_PUBLICADA,$mailto,$mailcopy);
        }
        $dados['message']       = $this->getMessagePublish($recall);
        $dados['status']        = STATUS_ID_PUBLICADA;
        $dados['titulo']        = "Publicar campanha";
        $dados['classTitulo']   = "bgPublish";
        $dados['classNote']     = "alert-success";
        $dados['txtNote']       = "Após a publicação com resalva, a campanha será publicada no portal
                                   e todos os consumidores cadastrados receberão a mensagem abaixo. <br/>
                                   <strong>Lembre-se de informar o número do protocolo antes de Publicar a Campanha.</strong>";
        $dados['mailto']        = $emailSupplier;
        $dados['mailcopy']      = $emailSupplierCopy;
        $dados['ckeditor'] = $this->getEditor();
        $dados['action'] = $acion;
        $dados['protocol'] = $recall->getProtocol();
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/form/_publish',$dados);
        $this->load->view('template/footer');
    }
    
    function publishApproveReservation() {
        $id = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        $idUser = $recall->getSupplier()->getUser()->getId();
        
        $userSupplier = $this->doctrine->em->getRepository('Entities\User')->findById($idUser);
        $emailSupplier = ($userSupplier[0]->getEmail()) ? $userSupplier[0]->getEmail() : NULL;
        
        $supplier = $this->doctrine->em->getRepository('Entities\Supplier')->findBy(array('user' => $idUser));
        $emailSupplierCopy = ($supplier[0]->getEmailCopy()) ? $supplier[0]->getEmailCopy() : NULL;
        
        $this->verificaPermissaoPublicacao($recall);
        $arrGroup = $this->doctrine->em->getRepository('Entities\Group')
                                    ->findBy(array('status' => STATUS_ATIVO),array('name' => 'ASC'));
        $group = new \Entities\Group;
        $dados['groups'] = $group->getArrayAllGroup($arrGroup);
        
        $acion = "recalls/publishApproveReservation";

        if ($this->input->post('protocol')) {
            $this->saveProtocol($recall,$this->input->post('protocol'),$acion);
        }
        
        if ($this->input->post('message')) {
            if ($this->form_validation->run('reason_reservation')){
                $recall->setReasonReservation($this->input->post('reasonReservation'));
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('recalls/publishApproveReservation');
            }
            if ($this->input->post('idGroup')) {
                $mailPartners = $this->getEmailPartners($this->input->post('idGroup'));
            } else {
                $mailPartners = array();
            }
            $mailSubscriber = $this->getEmailSubscribers($recall);
            $arrMailTo = array_merge($mailPartners,$mailSubscriber);
            $arrMail = array_unique($arrMailTo);
            
            // adiciona email do fornecedor
            array_push($arrMail, $emailSupplier);
            
            // transforma array de email em string
            $mailto = implode(',', $arrMail);
            
            // caso nao tenha nenhum e-mail cadastrado (grupo parceiro nem pessoa fisica)
            if (!$mailto) {
                $mailto = EMAIL_FROM_TESTE;
            }
            $mailcopy = $this->input->post('mailcopy');
            $this->approveRecall($recall,$this->input->post('message'),STATUS_ID_PUBLICADA_COM_RESSALVA,$mailto,$mailcopy);
        }

        $dados['message']       = $this->getMessagePublish($recall);
        $dados['status']        = STATUS_ID_PUBLICADA_COM_RESSALVA;
        $dados['titulo']        = "Publicar campanha com ressalva";
        $dados['classTitulo']   = "bgReservation";
        $dados['classNote']     = "alert-warning";
        $dados['txtNote']       = "Após a publicação com resalva, a campanha será publicada no portal
                                   e todos os consumidores cadastrados receberão a mensagem abaixo.<br/>
                                   Porém, o fornecedor deverá corrigir a pendência identificada pela SENACON.<br/>
                                   Essa mensagem também será enviada para o(s) e-mail(s): ".$emailSupplier.','.$emailSupplierCopy;
        $dados['mailto']        = $emailSupplier;
        $dados['mailcopy']      = $emailSupplierCopy;
        $dados['ckeditor']      = $this->getEditor();
        $dados['action']        = $acion;
        $dados['protocol']      = ($recall->getProtocol()) ? $recall->getProtocol() : NULL;
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/form/_publish',$dados);
        $this->load->view('template/footer');
    }
    
    function publishDisapprove() {
        $id = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        $idUser = $recall->getSupplier()->getUser()->getId();
        
        $userSupplier = $this->doctrine->em->getRepository('Entities\User')->findById($idUser);
        $emailSupplier = ($userSupplier[0]->getEmail()) ? $userSupplier[0]->getEmail() : NULL;
        
        $supplier = $this->doctrine->em->getRepository('Entities\Supplier')->findBy(array('user' => $idUser));
        $emailSupplierCopy = ($supplier[0]->getEmailCopy()) ? $supplier[0]->getEmailCopy() : NULL;
        
        $this->verificaPermissaoPublicacao($recall);
        if ($this->input->post()) {
            $this->form_validation->set_rules('message', 'Devolver campanha para correção', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('error', MSG_ERROR_CAMPO_MENSAGEM_OBRIGATORIO);
                } else {
                    $mailto = $this->input->post('mailto');
                    $mailcopy = $this->input->post('mailcopy');
                    $this->disapproveRecall($recall,$this->input->post('message'),$mailto, $mailcopy);   
                }
        }
                
        
        $files = $this->doctrine->em->getRepository('Entities\File')
                                    ->findBy(array('recall' => $id, 'belongsTo' => FILE_BELONGS_TO_MESSAGE_DISAPPROVE_RECALL));
        $dados['message']           = $recall->getReasonDisapprove();
        $dados['files']             = $this->getArrayFiles($files);
        $dados['status']            = STATUS_ID_PENDENCIA_CADASTRO;
        $dados['titulo']            = "Devolver campanha para correção";
        $dados['classTitulo']       = "bgPublishReturned";
        $dados['classNote']         = "alert-danger";
        $dados['txtNote']           = "Após devolver a campanha, o fornecedor deverá corrigí-la 
                                   de acordo com as observações descritas no campo abaixo. <br/>  
                                   <strong>Essa mensagem será enviada apenas para o(s) e-mail(s) do fornecedor:
                                   ".$emailSupplier.','.$emailSupplierCopy."</strong>";
        $dados['protocol']          = ($recall->getProtocol()) ? $recall->getProtocol() : NULL;
        $dados['mailto']            = $emailSupplier;
        $dados['mailcopy']          = $emailSupplierCopy;
        $dados['ckeditor'] = $this->getEditor(300);
        $dados['action'] = "recalls/publishDisapprove";
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/timeLineForm');
        $this->load->view('recalls/form/_publish',$dados);
        $this->load->view('template/footer');
    }
    
    function publishApproveNotReservation() {
        $id = $this->session->userdata('recall');
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        $recall->setStatusCampaign(STATUS_ID_PUBLICADA);
        $this->doctrine->em->persist($recall);
        $this->doctrine->em->flush();
        $this->session->set_flashdata('sendCampaign', MSG_APPROVE_RECALL);
        redirect('recalls/index');
    }
    
    function arquivar($recall_id) {
        $id = $this->session->userdata('recall');
        if ($id == $recall_id) {
            $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
            $recall->setStatusCampaign(STATUS_ID_ARQUIVADO);
            $this->doctrine->em->persist($recall);
            $this->doctrine->em->flush();
            $this->session->set_flashdata('success', MSG_ARQUIVADA);
        } else {
            $this->session->set_flashdata('error', MSG_ERROR);
        }
        redirect('recalls/view');
    }
    
    function desarquivar($recall_id) {
        $id = $this->session->userdata('recall');
        if ($id == $recall_id) {
            $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
            $recall->setStatusCampaign(STATUS_ID_PUBLICADA);
            $this->doctrine->em->persist($recall);
            $this->doctrine->em->flush();
            $this->session->set_flashdata('success', MSG_SUCCESS);
        } else {
            $this->session->set_flashdata('error', MSG_ERROR);
        }
        redirect('recalls/view');
    }
    
    private function getEmailSubscribers($recall_id){
        $products = $this->doctrine->em->getRepository('Entities\Product')
                             ->findBy(array('recall' => $recall_id));
        
        $product        = new \Entities\Product;
        $arrResult      = $product->getBranchTypeProduct($products);

        $branch         = implode(",", $arrResult['branch_id']);
        $typeProduct    = implode(",", $arrResult['type_product_id']);
        
        $andWhere = "";
        
        if ($branch) {
            $andWhere .= " or op.branch in(".$branch.")";
        }
        
        if ($typeProduct) {
            $andWhere .= " or op.type_product in(".$typeProduct.")";
        }
        
        $dql = "select 
                                distinct(su.email)
                from 
                                Entities\Subscriber su
                inner join
                                Entities\Option op WITH su.id = op.subscriber
                where 
                                op.flag_all = ".STATUS_ATIVO."
                $andWhere";

        $query = $this->doctrine->em->createQuery($dql);
        $dados = $query->getResult();
        $arrEmail = array();
        if ($dados) {
            foreach ($dados as $key => $value) {
                $arrEmail[$key] = $value['email'];
            }
        }
        return $arrEmail;
    }
    
    function getEmailPartners($arrGroup) {
        
        if ($arrGroup) {
            $group_id = implode(',', $arrGroup);
        } else {
            return false;
        }
        $dql = "
            SELECT  
                    DISTINCT p.email
            FROM 
                    Entities\Partner p
            INNER JOIN
                    Entities\GroupPartner gp WITH p.id = gp.partner
            INNER JOIN
                    Entities\Group g WITH g.id = gp.group
            WHERE
                    g.id in(".$group_id.")
            ";
            
        $query = $this->doctrine->em->createQuery($dql);
        $dados = $query->getResult();
        
        $arrEmail = array();
        
        if ($dados) {
            foreach ($dados as $key => $value) {
                $arrEmail[$key] = $value['email'];
            }
        }
        return $arrEmail;
    }
    
    private function saveProtocol($recall,$protocol,$action) {
        if ($this->form_validation->run('protocol_number')){
            try {
                $protocolo = str_replace(array('.','/','-'), '', $protocol);
                $recall->setProtocol($protocolo);
                $this->doctrine->em->persist($recall);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS);
                redirect($action);
            }
            catch(Exception $err){
                throw $err;
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($action);
        }
    }
    
    function getEditor($height = 600){
        $this->load->helper('ckeditor'); 
        $dados = array(
            'id'   => 'message',
            'path' => 'js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width'   => "100%",
                'height'  => $height."px",
            ),
        );
        return $dados;
    }
    
    private function verificaPermissaoPublicacao($recall) {
        $user = $this->session->userdata('login');
        if (($recall->getTimeLine() != TIME_LINE_ENVIADA) && 
            (($user['level'] != LEVEL_ADMINISTRADOR) || 
            ($user['level'] != LEVEL_OPERADOR))) 
        {
            redirect('recalls/index');
        }
    }
    
    function getTableBatch($arrBatch) {
        if ($arrBatch) {
            $batches = '<table class="table table-bordered table-hover tbProductView">
                            <thead>
                                <tr>
                                    <th class="text-center">Nº lote inicial</th>
                                    <th class="text-center">Nº lote final</th>
                                    <th class="text-center">Data início</th>
                                    <th class="text-center">Data fim</th>
                                    <th class="text-center">Validade</th>
                                </tr>
                            </thead>
                        <tbody>';
            foreach ($arrBatch as $batch) {
                $dateIni = (isset($batch["manufacturer_initial_date"])) ? date("d/m/Y", strtotime($batch["manufacturer_initial_date"]["date"])) : "-";
                $dateFim = (isset($batch["manufacturer_final_date"])) ? date("d/m/Y", strtotime($batch["manufacturer_final_date"]["date"])) : "-";
                $dateExp = (isset($batch["expiry_date"])) ? date("d/m/Y", strtotime($batch["expiry_date"]["date"])) : "-";
                $batches .= '<tr>
                                <td> '.$batch["initial_batch"].'</td>
                                <td> '.$batch["final_batch"].'</td>
                                <td> '.$dateIni.' </td>
                                <td> '.$dateFim.' </td>
                                <td> '.$dateExp.' </td>
                            </tr>';
            }
            $batches .= '</tbody>
                    </table>';
        }
        return $batches;
    }
    
    function getMessagePublish($recall) {
        $message = "";
        if ($recall) {
            $data = (array) $recall->getStartDate();
            $startDate = getDataExtenso($data['date']);
            $products = $this->doctrine->em->getRepository('Entities\Product')
                             ->findBy(array('recall' => $recall->getId()));
            $arrProducts = $this->getArrayAllProduct($products);
            $url = base_url();
            $message .= '<p><strong>Alerta de Recall:
            <a href="'.$url.'principal/detailRecall/'.$recall->getId().'">'.$recall->getTitle().' </a></strong></p>
            <p>A Secretaria Nacional do Consumidor (Senacon) do Ministério da Justiça
            informa que a empresa <strong>'.$recall->getSupplier()->getCorporateName().' </strong> protocolou 
            Campanha de Chamamento, <strong>'.$recall->getDescription().'</strong>,
            com início em '.$startDate.', do(s) produto(s): <br/><br/>';
            
            $numAffected = 0;
            $arrBatch  = array();
            
            if ($arrProducts) {
                foreach ($arrProducts as $kp => $produto) {
                    
                    $message .= "Nome: <strong>".$produto['product']."</strong>, <br/>
                        Ramo: <strong>".$produto['branch']."</strong>, <br/>
                        Tipo do produto: <strong>".$produto['type_product']."</strong>, <br/>
                        Ano de fabricação: <strong>".$produto['year_manufacture']."</strong>, <br/>
                        Modelo: <strong>".$produto['model']."</strong>, <br/>
                        Fabricante: <strong>".$produto['corporate_name']."</strong>.
                    ";
//                    if ($produto['image']) {
//                        foreach ($produto['image'] as $image) :
//                            $message .= "<img src='".$image."' width='60px' style='margin: 5px 10px 5px 0px'/>";
//                        endforeach;
//                    }
                    $numAffected = $produto['quantity_affected']++;
                    $arrBatch = $produto['batch'][$kp]++;
                    $batch = $this->getTableBatch($arrBatch);
                    $numAffectedExt = valor_por_extenso($numAffected,FALSE);
                    $message .= ''.$batch.'
                    Total afetado pelo Brasil: <strong>'.$numAffected.'</strong>
                    <hr/>';
                }
            }
            
            $message .= '
            <p>Quanto aos riscos à saúde e à segurança, a empresa destacou que 
            foi constatada: <strong>'.$recall->getFaulty().'</strong>, 
            que possui o risco: <strong>'.$recall->getRiskFaulty().'</strong>. <br/>
            a medidade de correção encontrada para solucionar o risco, é : 
            '.$recall->getCorrectiveMeasure().'</p> 
            
            <p>O Código de Defesa do Consumidor determina que o fornecedor repare 
            ou troque o produto defeituoso a qualquer momento e de forma gratuita. 
            Se houver dificuldade, a recomendação é procurar um dos órgãos de 
            proteção e defesa do consumidor.</p>

            <p>Mais informações podem ser obtidas junto à empresa, por meio do telefone,
            <strong>'.$recall->getPhoneService().'</strong> ou pelo Local de atendimento: 
            '.$recall->getCustomerService().'. 
            Detalhes sobre a Campanha de Chamamento também estão disponíveis 
            no site do Ministério da Justiçaa - <a href="http://portal.mj.gov.br/recall/" 
            target="_blank">http://portal.mj.gov.br/recall/</a>.&nbsp;</p>';
            
            $message .= 'Se você não deseja mais receber nossos e-mails, cancele sua inscrição
                        <a href="'.  base_url('principal/viewSubscriber/').'">aqui</a>. ';
        }
        return $message;
    }
    
    
    /*
     * Enviar campanha para analise
    */
    function sendRecall() {
        $idRecall = $this->session->userdata('recall');
        $recall   = $this->doctrine->em->getRepository('Entities\Recall')->find($idRecall);
        try {
            $action = ACTION_SEND_RECALL.$idRecall;
            log_audit($action,'recall/sendRecall');
            $recall->setStartDate(new \DateTime());
            $recall->setTimeLine(TIME_LINE_ENVIADA);
            $recall->setStatusCampaign(STATUS_ID_AGUARDANDO_PUBLICACAO);
            $this->doctrine->em->persist($recall);
            $this->doctrine->em->flush();
            $this->session->set_flashdata('sendCampaign', MSG_SEND_RECALL);
            redirect('recalls/index');
        } catch (Exception $err){
                throw $err;
        }
    }
    
    /*
     * Aprovar campanha
    */
    function approveRecall($recall, $message, $status, $mailto, $mailcopy = NULL) {
        if (($recall) && ($message) && ($status)) {
            try {
                $action = ACTION_APPROVE_RECALL.$recall->getId();
                log_audit($action,'recall/approveRecall');
                $recall->setStatusCampaign($status);
                $mailfrom = EMAIL_FROM_SENACON;
                $subject = "Alerta de Recall: ".$recall->getTitle();
                
                // envia o email na funcao send_email (helper)
                send_email($message,$mailto,$subject,$mailfrom,$mailcopy);
                
                
                $this->doctrine->em->persist($recall);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('sendCampaign', MSG_APPROVE_RECALL);
                redirect('recalls/index');
            } catch (Exception $err){
                    throw $err;
            }
        }
    }
    /*
     * Devolver campanha
    */
    private function disapproveRecall($recall, $message, $mailto, $mailcopy = NULL) {
        if (($recall) && ($message)) {
            try {
                $action = ACTION_DISAPPROVE_RECALL.$recall->getId();
                log_audit($action,'recall/disapproveRecall');
                $recall->setStatusCampaign(STATUS_ID_PENDENCIA_CADASTRO);
                $recall->setReasonDisapprove($message);
                $this->saveRecallFile($recall->getId(),FILE_BELONGS_TO_MESSAGE_DISAPPROVE_RECALL);
                $subject = "Pendência na campanha de Recall: ".$recall->getTitle();
                $mailfrom = EMAIL_FROM_SENACON;
                
                // envia o email na funcao send_email (helper)
                send_email($message,$mailto,$subject,$mailfrom,$mailcopy);
                
                $this->doctrine->em->persist($recall);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('sendCampaign', MSG_DISAPPROVE_RECALL);
                redirect('recalls/index');
            } catch (Exception $err){
                    throw $err;
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            $this->view();
        }
    }
    /*
     * Salva os lotes dos produtos
     */
    private function saveBatch($product) {
        if (!$product) {
            return false;
        }
        
        // exclusao dos lotes excluidos pelo usuario
        $arrBatch = ($this->input->post('deleteBatch')) ? $this->input->post('deleteBatch') : array();
        if ($arrBatch) {
            foreach ($arrBatch as $batch) {
                $this->removeBatchById($batch);
            }
        }
        
        $total = count($this->input->post('initial_batch'));
        $initial_batch = $this->input->post('initial_batch');
        $final_batch = $this->input->post('final_batch');
        $manufacture_initial_date = $this->input->post('manufacturer_initial_date');
        $manufacture_final_date = $this->input->post('manufacturer_final_date');
        $expiry_date = $this->input->post('expiry_date');
        $arrBatch_id = $this->input->post('batch_id');

        for($i=0; $i<$total; $i++) {
            if ($arrBatch_id[$i]) {
                $modelBatch = $this->doctrine->em
                    ->getRepository('Entities\Batch')
                    ->find($arrBatch_id[$i]);
                $batch = ($modelBatch) ? $modelBatch : new Entities\Batch;
            } else {
                $batch = new Entities\Batch;
            }
            
            $batch->setInitialBatch($initial_batch[$i]);
            $batch->setFinalBatch($final_batch[$i]);

            $date1 = implode("-",array_reverse(explode("/",$manufacture_initial_date[$i])));
            $batch->setManufactureInitialDate($date1);
            
            $date2 = implode("-",array_reverse(explode("/",$manufacture_final_date[$i])));
            $batch->setManufactureFinalDate($date2);
            
            if (trim($expiry_date[$i]) == ""){
                $batch->setExpiryDate(NULL);
                
            }else{
                $date3 = implode("-",array_reverse(explode("/",$expiry_date[$i])));
                $batch->setExpiryDate($date3);
            }

            $batch->setProduct($product);

            $this->doctrine->em->persist($batch);
            $this->doctrine->em->flush();
        }
    }
    
    /*
     * Remove o lote do produto pelo id do lote
     */
    private function removeBatchById($id = null) {
        $batch = $this->doctrine->em
                ->getRepository('Entities\Batch')
                ->find($id);
        if ($batch) {
            $this->doctrine->em->remove($batch);
            $this->doctrine->em->flush();
        }
    }
    
    /*
     * Remove todos os lotes do produto
     */
    private function removeAllBatch($product_id = null) {
        $batch = $this->doctrine->em
                ->getRepository('Entities\Batch')
                ->findBy(array('product' => $product_id));
        
        if ($batch) {
            foreach ($batch as $bat) {
                $this->doctrine->em->remove($bat);
                $this->doctrine->em->flush();
            }
        }
    }
    
    private function saveAffecteds($product) {
        if (!$product) {
            return false;
        }
        
        // exclusao dos itens afetados excluidos pelo usuario
        $arrAffected = ($this->input->post('deleteAffected')) ? $this->input->post('deleteAffected') : array();
        if ($arrAffected) {
            foreach ($arrAffected as $aff) {
                $this->removeAffectedById($aff);
            }
        }
        
        $quantity = $this->input->post('quantity');
        $arrQuantity = array_filter($quantity);
        
        $uf = $this->input->post('iduf');
        $arrUf = array_filter($uf);
        
        $arrAffected_id = $this->input->post('affected_id');
        $total = count($arrQuantity);

        for ($i=0; $i < $total; $i++) {
            if ($arrAffected_id[$i]) {
                $modelAffected = $this->doctrine->em->getRepository('Entities\Affected')
                                                    ->find($arrAffected_id[$i]);
                $affected = ($modelAffected) ? $modelAffected : new Entities\Affected;
            } else {
                $affected = new Entities\Affected;
            }
            if (!empty($arrQuantity[$i]) && !empty($arrUf[$i])) {
                $affected->setQuantity($arrQuantity[$i]);
                $state = $this->doctrine->em->getRepository('Entities\State')->find($arrUf[$i]);
                $affected->setState($state);
                $affected->setProduct($product);
            }
            $this->doctrine->em->persist($affected);
            $this->doctrine->em->flush();
        }
    }
    
    /*
     * Remove todas as quantidades afetadas por prouto
     */
    private function removeQuantityAffected($product_id = null) {
        $affected = $this->doctrine->em
                ->getRepository('Entities\Affected')
                ->findBy(array('product' => $product_id));
        if ($affected) {
            foreach ($affected as $aff) {
                $this->doctrine->em->remove($aff);
                $this->doctrine->em->flush();
            }
        }
        
    }
    /*
     * Remove quantidade afetada por id
     */
    private function removeAffectedById($id) {
        $affected = $this->doctrine->em
                ->getRepository('Entities\Affected')
                ->find($id);
        if ($affected) {
            $this->doctrine->em->remove($affected);
            $this->doctrine->em->flush();
        }
    }

    private function saveFaulty() {
        $recall1 = $this->doctrine->em
            ->getRepository('Entities\Recall')
            ->find($this->session->userdata('recall'));
        
        $recall1->setFaultyPart($this->input->post('faulty_part'));
        $dt = implode("-",array_reverse(explode("/",$this->input->post('date_faulty'))));
        $recall1->setDateFaulty($dt);
        $recall1->setFaultyDescribe($this->input->post('faulty_describe'));
        $recall1->setFaulty($this->input->post('faulty'));
        $recall1->setRiskFaulty($this->input->post('risk_falty'));
        
        $this->doctrine->em->persist($recall1);
        $this->doctrine->em->flush();
    }
    
    /*
     *  Salvar todas as imagens do produto no diretorio: uploads/(id_recall)/(id_product)
     */
    private function saveImages($product) {
        $arrImages = ($this->input->post('deleteimg')) ? $this->input->post('deleteimg') : array();
        if ($arrImages) {
            foreach ($arrImages as $image) {
                $this->removeImageById($image);
            }
        }
        if ($_FILES) {
            $files = $_FILES['userfile']['name'];
            $count = count($_FILES['userfile']['name']);

            for ($i=0; $i<$count; $i++){
                if ($files[$i]) {
                    $images = new Entities\Image;
                    $nome_imagem = str_replace(array(' '), '_', $files[$i]);
                    $path = base_url().'uploads/'.$this->session->userdata('recall').'/'.$product->getId().'/'.$nome_imagem;
                    $images->setPath($path);
                    $images->setProduct($product);
                    $this->doctrine->em->persist($images);
                    $this->doctrine->em->flush();
                }
            }
        }
    }
    
    function do_upload($type = UPLOAD_ALL,$product_id = NULL){
        if ($_FILES) {
            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            for($i=0; $i<$cpt; $i++){
                $nome_arquivo = str_replace(" ","_",preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim($files['userfile']['name'][$i]))));
                $_FILES['userfile']['name']= $nome_arquivo;
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];
                $config = $this->set_upload_options($type,$product_id);
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload();
            }
        }
    }
    
    /*
     * Configuracao dos arquivos para upload
     * return array com as propriedades do arquivo
     */
    private function set_upload_options($type = UPLOAD_ALL,$product_id = NULL){

        $folderRecall  = $this->session->userdata('recall');
        $folderProduct = $product_id;
        
        if (($type == UPLOAD_IMAGE) && (isset($product_id))) {
            $pathToUpload = FCPATH.'uploads/'.$folderRecall.'/'.$folderProduct.'/';
        } else {
            $pathToUpload = FCPATH.'uploads/'.$folderRecall.'/';
        }
        if (!file_exists($pathToUpload)){
            mkdir($pathToUpload, 0777,TRUE);
        }
        $config = array();
        if ($type == UPLOAD_IMAGE) {
            $config['upload_path']   = $pathToUpload;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']      = '10000';
            $config['file_size']      = '10000';
            $config['overwrite']     = FALSE;
        } elseif ($type == UPLOAD_FILE) {
            $config['upload_path']   = $pathToUpload;
            $config['allowed_types'] = 'doc|docx|xls|xlsx|odt|ods|pdf|mp3|txt|avi|gif|jpg|png|jpeg';
            $config['max_size']      = '10000';
            $config['file_size']      = '10000';
            $config['overwrite']     = FALSE;
        } else {
            $config['upload_path']   = FCPATH.'uploads/';
            $config['allowed_types'] = 'doc|docx|xls|xlsx|odt|ods|pdf|mp3|txt|avi|gif|jpg|png|jpeg';
            $config['max_size']      = '10000';
            $config['file_size']      = '10000';
            $config['overwrite']     = FALSE;
        }
        return $config;
    }
    
    /*
     * Remove todas as imagens do produto
     */
    private function removeAllImagesProduct($product) {
        $image = $this->doctrine->em
                    ->getRepository('Entities\Image')
                    ->findBy(array('product' => $product));
        if ($image) {
            foreach ($image as $value) {
                $this->doctrine->em->remove($value);
                $this->doctrine->em->flush();
            }
        }
    }
    /*
     * Remove imagem especifica
     */
    private function removeImageById($id) {
        $image = $this->doctrine->em
                    ->getRepository('Entities\Image')
                    ->find($id);
        if ($image) {
            $this->doctrine->em->remove($image);
            $this->doctrine->em->flush();
        }
    }
    
    function totalServiced($id) {
        $recall = $this->doctrine->em
                    ->getRepository('Entities\Recall')
                    ->find($id);
        
        $product = $this->doctrine->em
                    ->getRepository('Entities\Product')
                    ->findBy(array('recall' => $recall));
        
        $totalserviced = $this->doctrine->em
                    ->getRepository('Entities\TotalServiced')
                    ->findBy(array('product' => $product),
                            array('dateInsert' => 'group by')
                        );
        
        $dados['totalserviced'] = array();
        
        foreach ($totalserviced as $ts){
            $dados['totalserviced'][] = array(
                'id' => $ts->getId(),
                'quantity' => $ts->getQuantity(),
                'date_insert' => $ts->getDateInsert(),
                'product' => $ts->getProduct()->getProduct(),
                'state' => $ts->getState()->getState()
                );
        }
        
        $this->load->view('template/header');
        $this->load->view('recalls/index');
        $this->load->view('template/messages');
        $this->load->view('recalls/totalserviced', $dados);
        $this->load->view('template/footer');
    }
    
    /*
     * Remove os Total serviced
     */
    private function removeTotalServiced($product_id = null) {
        $total_serviced = $this->doctrine->em
                    ->getRepository('Entities\TotalServiced')
                    ->findBy(array('product' => $product_id));
        
        if ($total_serviced) {
            foreach ($total_serviced as $total) {
                $this->doctrine->em->remove($total);
                $this->doctrine->em->flush();
            }
        }
    }
    
    
    
    /*
     * Metodo para retornar um array com todos os produtos de uma recall
     * return Array
     */    
    function getArrayAllProduct($product) {
        
        if ($product) {
            foreach ($product as $key => $value) {
                $dados[$key] = array(
                    'product_id'        => $value->getId(),
                    'product'           => $value->getProduct(),
                    'model'             => $value->getModel(),
                    'year_manufacture'  => $value->getYearManufacture(),
                    'quantity_affected' => $value->getQuantityAffected(),
                    'type_product_id'   => $value->getTypeProduct()->getId(),
                    'type_product'      => ($value->getTypeProduct()->getTypeProduct()),
                    'branch_id'         => $value->getTypeProduct()->getBranch()->getId(),
                    'branch'            => ($value->getTypeProduct()->getBranch()->getBranch()),
                    'country'           => ($value->getCountry()->getCountry()),
                    'countryExport'     => ($value->getCountryExport()),
                    'corporate_name'    => $value->getManufacturer()->getCorporateName(),
                    'manufacturer_id'   => $value->getManufacturer()->getId(),
                );
                
                $image = $this->doctrine->em
                    ->getRepository('Entities\Image')
                    ->findBy(array('product' => $value));
                if ($image) {
                    $arrImg = array();
                    foreach($image as $k => $v) {
                        $arrImg[$k] = $v->getPath();
                    }
                    $dados[$key]['image'] = $arrImg;
                }
                
                $batch = $this->doctrine->em->getRepository('Entities\Batch')
                                        ->findBy(array('product' => $value));
                $arrBatch = array();
                if ($batch) {
                    foreach($batch as $kb => $vb) {
                        $arrBatch[$key][] = array(
                            'batch_id'                  => $vb->getid(),
                            'initial_batch'             => $vb->getInitialBatch(),
                            'final_batch'               => $vb->getFinalBatch(),
                            'manufacturer_initial_date' => ($vb->getManufactureInitialDate()) ? 
                                                           (array)$vb->getManufactureInitialDate() : NULL,
                            'manufacturer_final_date'   => ($vb->getManufactureFinalDate()) ? 
                                                           (array)$vb->getManufactureFinalDate() : null,
                            'expiry_date'               => ($vb->getExpiryDate()) ? 
                                                           (array)$vb->getExpiryDate() : null,
                        );
                    }
                    $dados[$key]['batch'] = $arrBatch;
                }

                $affected = $this->doctrine->em->getRepository('Entities\Affected')
                                               ->findBy(array('product' => $value));
                $arrAffected = array();
                if ($affected) {
                    foreach($affected as $ka => $va) {
                        $arrAffected[$key][] = array(
                            'affected_id'   => $va->getid(),
                            'quantity'      => $va->getQuantity(),
                            'state_name'    => ($va->getState()->getStateName()),
                        );
                    }
                    $dados[$key]['affected'] = $arrAffected;
                }
            }
        }
        
        return $dados;
    }
    
    /*
     * Metodo para retornar um array com um produto determinado
     * return Array
     */    
    function getArrayOneProduct($product) {
        
        $dados = array();
        if ($product) {
            $dados = array(
                'product_id'        => $product->getId(),
                'product'           => $product->getProduct(),
                'model'             => $product->getModel(),
                'year_manufacture'  => $product->getYearManufacture(),
                'quantity_affected' => $product->getQuantityAffected(),
                'branch_id'         => $product->getTypeProduct()->getBranch()->getId(),
                'type_product'      => $product->getTypeProduct()->getTypeProduct(),
                'type_product_id'   => $product->getTypeProduct()->getId(),
                'country'           => $product->getCountry()->getCountry(),
                'country_id'        => $product->getCountry()->getId(),
                'country_export'    => ($product->getCountryExport()) ? $product->getCountryExport()->getCountry() : NULL,
                'country_export_id' => ($product->getCountryExport()) ? $product->getCountryExport()->getId() : NULL,
                'corporate_name'    => $product->getManufacturer()->getCorporateName(),
                'manufacturer_id'   => $product->getManufacturer()->getId(),
            );
            $image = $this->doctrine->em
                    ->getRepository('Entities\Image')
                    ->findBy(array('product' => $product));
            if ($image) {
                $arr = array();
                foreach($image as $k => $v) {
                    $arr[$k]['id'] = $v->getId();
                    $arr[$k]['path'] = $v->getPath();
                }
                $dados['image'] = $arr;
            }
        }
        
        return $dados;
    }
    
    /*
     * retorna um array com os arquivos de um recall
     */
    function getArrayFiles($files) {
        
        $dados = array();
        if ($files) {
            foreach($files as $key => $value) {
                $dados[$key]['id_file'] = $value->getId();
                $dados[$key]['file_path'] = $value->getFilePath();
            }
        }
        return $dados;
    }
    
    /*
     * retorna um array com os links de um recall
     */
    function getArrayLinks($links) {
        
        $dados = array();
        if ($links) {
            foreach($links as $key => $value) {
                $dados[$key]['id_link'] = $value->getId();
                $dados[$key]['url']     = $value->getUrl();
            }
        }
        return $dados;
    }
    
    /*
     * retorna um array com os dados do processo do recall
     */
    function getArrayProcess($recall) {
        $dados = array();
        $process = $this->doctrine->em
                    ->getRepository('Entities\Process')
                    ->findBy(array('recall' => $recall));
        if ($process) {
            foreach($process as $value) {
                $dados['process_number']   = $value->getProcessNumber();
                $dados['identification']   = $value->getIdentification();
                $dados['date_process']     = $value->getDateProcess() ? (array) $value->getDateProcess() : "";
                $dados['staff']            = $value->getStaff();
                $dados['distrito']         = $value->getDistrito();
                $dados['situation']        = $value->getSituation();
                $dados['describe_process'] = $value->getDescribeProcess();
            }
        }
        return $dados;
    }
    
    function comboMedia ($id){
        $query = $this->doctrine->em->getRepository('Entities\Media')
                                ->findBy(
                                   array('origin'=> $id), 
                                   array('media' => 'ASC')
                                 );

        $html = '<select id="media" name="media" class="form-control">';
        $html .= '<option value="">-- Selecione Veículo de mídia --</option>';
                                
        foreach ($query as $q){
            $html .='<option value="'.$q->getId().'">'.($q->getMedia()).'</option>';            
        }
        $html .='</select>';

        echo $html;
    }

    function comboSource ($id){
        $query = $this->doctrine->em->getRepository('Entities\Source')
                                ->findBy(
                                   array('media'=> $id), 
                                   array('source' => 'ASC')
                                 );

        $html = '<select id="source" name="source" class="form-control">';
                                
        foreach ($query as $q){
            $html .='<option value="'.$q->getId().'">'.($q->getSource()).'</option>';
                            
        }
        $html .='</select>';

        echo $html;
    }
    
    /*
     * retorna um array com todos os Paises
     */
    function getComboCountry() {
        $country = $this->doctrine->em->getRepository('Entities\Country')->findAll();
        $dados = array();
        if ($country) {
            foreach ($country as $value) {
                $dados[$value->getId()] = ($value->getCountry());
            }
        }
        return $dados;
    }
    
    function getComboAllOrigin() {
        $origins = $this->doctrine->em->getRepository('Entities\Origin')
                ->findBy(array('status' => TRUE),array('origin'=>'ASC'));
        $dados = array(
            '' => '-- Selecione Meio de veiculaçao --'
        );
        if ($origins) {
            foreach ($origins as $value) {
                $dados[$value->getId()] = ($value->getOrigin());
            }
        }
        return $dados;
    }
    
    /*
     * Habilita campos de acordo com o status da campanha
     * return Array
     */
    function showFieldsByStatus($status_campaign = NULL) {
        $dados = array();
        if ($status_campaign) {
            if (($status_campaign == STATUS_ID_EM_CADASTRAMENTO) || 
                ($status_campaign == STATUS_ID_PENDENCIA_CADASTRO)) 
            {
                $dados = array(
                    'readonly'     => FALSE,
                    'disabled'      => "",
                    'habilitaBotao' => TRUE
                );
            } else {
                $dados = array(
                    'readonly'     => TRUE,
                    'disabled'      => "disabled",
                    'habilitaBotao' => FALSE
                );
            }
        } else {
            $dados = array(
                'readonly'     => FALSE,
                'disabled'      => "",
                'habilitaBotao' => TRUE
            );
        }
        return $dados;
    }
    
    /*
     * remove o produto e todos seus dependentes
     */
    function removeProduct($id) {
        if ($id) {
            $product = $this->doctrine->em
                            ->getRepository('Entities\Product')->find($id);
            $idRecall = $this->session->userdata('recall');
            if ($product) {
                try {
                    $this->doctrine->em->getConnection()->beginTransaction();
                    $action = ACTION_REMOVE_PRODUCT.$id.' id_recall: '.$idRecall;
                    log_audit($action,'recall/removeProduct');
                    $this->removeTotalServiced($id);
                    $this->removeAllImagesProduct($product);
                    $this->removeAllBatch($id);
                    $this->removeQuantityAffected($id);
                    $this->doctrine->em->remove($product);
                    $this->doctrine->em->flush();
                    $this->doctrine->em->getConnection()->commit();
                    echo json_encode('Produto removido com sucesso.');
                }
                catch(Exception $err){
                    echo json_encode('Error.');
                    $this->doctrine->em->getConnection()->rollback();
                    throw $err;
                }
            }
        }
        die;
    }
    
    function downloadFile($file_id){
        if ($file_id) {
            $file = $this->doctrine->em->getRepository('Entities\File')->find($file_id);
            if ($file) {
                $this->load->helper('download');
                $path = $file->getFilePath();
                $name = end(explode('/',$path));
                $data = file_get_contents($path);
                //Make sure that php_openssl.dll or php_openssl.so is enabled.
                force_download($name, $data);
            }
        }
        die;
    }
    
    /*
     * Carregar combo com as campanhas de recall que possuirem status = Publicada, 
     * Publicada com ressalva ou Finalizada
     */
    function loadCombo(){
        $supplier_id = $this->input->post('supplier_id');
        if ($supplier_id) {
            
            $dados = array();
            
            $dql = "
            SELECT DISTINCT (r.id),
                r.title
            FROM 
                    Entities\Recall r
            INNER JOIN 
                    Entities\Supplier s WITH s.id = r.supplier
            WHERE 
                    r.supplier = ".$supplier_id."
            AND 
                    r.statusCampaign in(".STATUS_ID_PUBLICADA.",".STATUS_ID_PUBLICADA_COM_RESSALVA.",".STATUS_ID_FINALIZADA.")
            ORDER BY 
                    r.title ASC
            ";
            
            $sql = $this->doctrine->em->createQuery($dql);
            $result = $sql->getResult();
            if ($result) {
                foreach($result as $value){
                    $dados[$value["id"]] = (utf8_encode($value["title"]));
                }
                echo json_encode($dados);
            } else {
                echo json_encode(array(''=>'selecione'));
            }
        } else {
            echo json_encode(array('msg' => 'err'));
        }
        die;
    }
    
    /*
     * Sql com os dados do recall (query nativa do MYSQL necessaria para utilizar select in select
     */
    private function sqlRecall($supplier_id = NULL){
        
        $this->load->database();
        
        $where = "";
        if ($supplier_id) {
            $where = "where su.id = ".$supplier_id."";
        }
        $sql = "select 
                                r.id as recall_id,
                                r.title,
                                r.protocol,
                                r.start_date,
                                r.status_campaign,
                                su.id as supplier_id,
                                su.trade_name,
                                (select 
                                        sum(quantity)
                                  from 
                                        affecteds a 
                                  where 
                                        product_id in ( select 
                                                                id 
                                                        from 
                                                                products
                                                        where 
                                                                recall_id = r.id )
                                 )as total_affected,
                                (select 
                                        sum(quantity) 
                                  from 
                                        total_serviceds t 
                                  where 
                                        product_id in ( select 
                                                                id 
                                                        from 
                                                                products
                                                        where 
                                                                recall_id = r.id )
                                 )as total_serviced
                from 
                                recalls r
                inner join 
                                suppliers su on r.supplier_id = su.id
                left join 
                                products p on p.recall_id = r.id
                {$where}
                    
                GROUP BY 
                                r.id
                order by 
                                r.date_insert DESC;
                 ";                

        $query = $this->db->query($sql);
        $result = $query->result();
        $dados = array();
        
        foreach ($result as $key => $value) {
            $dados[$key] = array(
                'recall_id'         => $value->recall_id,
                'title'             => $value->title,
                'protocol'          => $value->protocol,
                'start_date'        => $value->start_date,
                'status_campaign'   => $value->status_campaign,
                'supplier_id'       => $value->supplier_id,
                'trade_name'        => $value->trade_name,
                'total_affected'    => $value->total_affected,
                'total_serviced'    => $value->total_serviced
            );
        }
        
        return $dados;
    }
}