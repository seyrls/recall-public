<?php

class Medias extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function index() {
            $this->lists();
    }

    
    function insert($media_id) {
        if ($this->input->post()) {
            $this->save();
        }
        $dados['dados'] = array();
        $arrOrigin = $this->doctrine->em->getRepository('Entities\Origin')
                ->findById($media_id);
        if (!$arrOrigin) {
            redirect('origins/index');
        }
        
        $origin = new Entities\Origin;
        $dados['dados'] = $origin->getArrayOrigin($arrOrigin);
        $dados['dados']['media'] = NULL;
        
        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
            $dados['dados']['origin'] => 'origins/view/'.$dados['dados']['origin_id'],
            'Inserir' => 'medias/insert/'.$media_id,
        );
        
        $this->load->view('template/header');
        $this->load->view('origins/breadcrumbs',$breadcrumbs);
        $this->load->view('template/messages');
        $this->load->view('medias/insert', $dados);
        $this->load->view('template/footer');
    }

    function save() {
        $this->form_validation->set_rules('media', 'Mídia', 'required|max_length[20]');
        if ($this->form_validation->run() == TRUE) {
            $media = new Entities\Media;
            $origin = $this->doctrine->em->getRepository('Entities\Origin')
                            ->find($this->input->post('origin_id'));
            $media->setMedia($this->input->post('media'));
            $media->setOrigin($origin);
            $media->setStatus(TRUE);
            $this->doctrine->em->persist($media);
            $this->doctrine->em->flush();
            if (!is_null($media->getId())) {
                $this->session->set_flashdata('success', MSG_SUCCESS);
            }else{
                $this->session->set_flashdata('error', MSG_ERROR);
            }
            redirect('origins/view/'.$this->input->post('origin_id'));
        }
    }

    
    function edit($id) {
        if (!$id) {
            redirect('medias/index');
        }
        if ($this->input->post()) {
            $this->update();
        }
        $arrMedia = $this->doctrine->em->getRepository('Entities\Media')->findById($id);
        
        if (!$arrMedia) {
            redirect('medias/index');
        }
        $dados['dados'] = array();
        $media = new Entities\Media;
        $dados['dados'] = $media->getArrayMedia($arrMedia);
        
        $origin = new Entities\Origin;
        $arrOrigin  = $this->doctrine->em->getRepository('Entities\Origin')->findAll();
        $dados['comboOrigin'] = $origin->getComboOrigin($arrOrigin);
        
        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
            $dados['dados']['origin'] => 'origins/view/'.$dados['dados']['origin_id'],
            'Editar '.$dados['dados']['media'] => 'medias/edit/'.$id,
        );
        
        $this->load->view('template/header');
        $this->load->view('origins/breadcrumbs',$breadcrumbs);
        $this->load->view('template/messages');
        $this->load->view('medias/edit', $dados);
        $this->load->view('template/footer');
    }
    
    private function update() {
        $this->form_validation->set_rules('media', 'Mídia', 'required|max_length[20]');
        if ($this->form_validation->run() == TRUE) {
            $media = $this->doctrine->em->getRepository('Entities\Media')
                            ->find($this->input->post('media_id'));
            $origin = $this->doctrine->em->getRepository('Entities\Origin')
                            ->find($this->input->post('origin_id'));
            if (($media) && ($origin)) {
                try {
                    $media->setMedia($this->input->post('media'));
                    $media->setStatus($this->input->post('status'));
                    $media->setOrigin($origin);
                    $this->doctrine->em->persist($media);
                    $this->doctrine->em->flush();
                    $this->session->set_flashdata('success', MSG_SUCCESS_ALTERADO);
                } catch (Exception $ex) {
                    $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
                }
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
            }
            redirect('origins/view/'.$this->input->post('origin_id'));
        }
    }
    
    function view($id) {
        if (!$id) {
            redirect('origins/index');
        }
        $dados  = array();
        $media  = new Entities\Media;
        $source = new Entities\Source;
        $arrMedia  = $this->doctrine->em->getRepository('Entities\Media')
                                        ->findBy(array('id' => $id));
        if (!$arrMedia) {
            redirect('origins/index');
        }
        $dados['media']   = $media->getArrayMedia($arrMedia);
        $arrSource        = $this->doctrine->em->getRepository('Entities\Source')
                                                ->findBy(array('media' => $id));
        $dados['source']  = $source->getArrayAllSource($arrSource);

        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
            $dados['media']['origin'] => 'origins/view/'.$dados['media']['origin_id'],
            $dados['media']['media'] => 'medias/view/'.$id,
        );
        
        $this->load->view('template/header');
        $this->load->view('origins/breadcrumbs',$breadcrumbs);
        $this->load->view('template/messages');
        $this->load->view('medias/view',$dados);
        $this->load->view('template/footer');
    }
    
    function remove($id){
        $media = $this->doctrine->em
                ->getRepository('Entities\Media')
                ->find($id);
        
        $origin_id = $media->getOrigin()->getId();
        
        $source = $this->doctrine->em
                ->getRepository('Entities\Source')
                ->findBy(array('media' => $id));
        if ((!$source) && ($media)) {
                $this->doctrine->em->remove($media);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_REMOVE);
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
        }
        redirect('origins/view/'.$origin_id);
    }
}