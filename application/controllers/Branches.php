<?php

class Branches extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function index() {
        $this->lists();
    }

    function lists() {
        
        $branch     = new Entities\Branch;
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')->findBy(array(),array('branch' => 'ASC'));
        $dados['dados'] = $branch->getArrayAllBranch($arrBranch);
        $this->load->view('template/header');
        $this->load->view('branches/index');
        $this->load->view('template/messages');
        $this->load->view('branches/lists',$dados);
        $this->load->view('template/footer');
    }

    function insert() {
        
        if ($this->input->post()) {
            $this->save();
        }
        
        $this->load->view('template/header');
        $this->load->view('branches/index');
        $this->load->view('template/messages');
        $this->load->view('branches/insert');
        $this->load->view('template/footer');
    }

    function save() {
        $this->form_validation->set_rules('branch', 'Setor de Atividade', 'required|max_length[70]');
        if ($this->form_validation->run() == TRUE) {
            $branch = new Entities\Branch;
            $branch->setBranch($this->input->post('branch'));
            $branch->setStatus($this->input->post('status'));
            $this->doctrine->em->persist($branch);
            $this->doctrine->em->flush();
            $this->session->set_flashdata('success', MSG_SUCCESS);
            redirect('branches/lists');
        }
    }

    function edit($id) {
        if (!$id) {
            redirect('branches/lists');
        }
        
        if ($this->input->post()) {
            $this->update();
        }
        
        $dados = array();
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')->findById($id);
        $branch = new Entities\Branch;
        $dados['dados'] = $branch->getArrayBranch($arrBranch);

        $this->load->view('template/header');
        $this->load->view('branches/index');
        $this->load->view('template/messages');
        $this->load->view('branches/insert', $dados);
        $this->load->view('template/footer');
    }

    function update() {
        $this->form_validation->set_rules('branch', 'Setor de Atividade', 'required|max_length[70]');
        if ($this->form_validation->run() == TRUE) {
            $branch = $this->doctrine->em->getRepository('Entities\Branch')
                              ->find($this->input->post('branch_id'));
            if ($branch) {
                $branch->setBranch($this->input->post('branch'));
                $branch->setStatus($this->input->post('status'));
                $this->doctrine->em->persist($branch);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR);
            }
            redirect('branches/lists');
        }
    }
    
    function remove($id){
        if (!$id) {
            redirect('branches/lists');
        }
        $type_product = $this->doctrine->em->getRepository('Entities\TypeProduct')
                        ->findBy(array('branch' => $id));
        
        $supplier = $this->doctrine->em->getRepository('Entities\Supplier')
                        ->findBy(array('branch' => $id));
        
        if ($type_product || $supplier) {
            $this->session->set_flashdata('error', MSG_ERROR_REMOVE_RECALL_EXISTS);
            redirect('branches/lists');
        } else {
            $branch = $this->doctrine->em->getRepository('Entities\Branch')->find($id);
            if ($branch) {
                $this->doctrine->em->remove($branch);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_REMOVE);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
            }
        }
        redirect('branches/lists');
    }
}