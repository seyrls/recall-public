<?php

class Users extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database(); // necessario para utilizar a funcao is_unique na validacao
        $this->load->helper('send_email');
        $this->load->library('form_validation');
        
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function index() {
        $this->lists();
    }

    function lists() {
        $dados = array();
        $dados['dados'] = $this->sqlAllUsers();
        $this->load->view('template/header');
        $this->load->view('users/index');
        $this->load->view('template/messages');
        $this->load->view('users/lists',$dados);            
        $this->load->view('template/footer');
    }
    
    /*
     * Pega todos os usuarios do sistema menos as solicitacoes de fornecedor
     */
    function sqlAllUsers() {
        $dados = array();
        $dql = "
                SELECT DISTINCT (u.id) as user_id,
                    u.username,
                    u.email,
                    l.level,
                    l.name as name_level,
                    u.status,
                    s.corporateName as corporate_name
                FROM 
                        Entities\User u
                INNER JOIN 
                        Entities\Level l WITH u.level = l.id
                LEFT JOIN
                        Entities\Supplier s WITH u.id = s.user
                WHERE 
                        u.status not in (".STATUS_SOLICITADO.")
                ORDER BY 
                        l.name DESC 
                ";
        $sql = $this->doctrine->em->createQuery($dql);
        if (count($sql->getResult()) > 0) {
            $dados = $sql->getResult();
        }
        return $dados;
    }

    function insert() {
        
        if ($this->input->post()) {
            if ($this->input->post('level_id') == LEVEL_FORNECEDOR) {
                if ($this->form_validation->run('new_supplier_user')){
                    $this->save();
                }
            } else {
                if ($this->form_validation->run('new_user')){
                    $this->save();
                }
            }
        }
        $level = new Entities\Level;
        $arrLevel = $this->doctrine->em->getRepository('Entities\Level')
                        ->findBy(array(),array('level' =>'ASC'));
        $dados['comboLevel'] = $level->getComboLevel($arrLevel);

        $branch = new Entities\Branch;
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')
                                        ->findBy(array('status' => TRUE));
        $dados['comboBranch'] = $branch->getComboBranch($arrBranch);
        
        $state = new Entities\State;
        $arrState = $this->doctrine->em->getRepository('Entities\State')
                                    ->findBy(array(),array('state' =>'ASC'));
        $dados['comboState'] = $state->getComboState($arrState);
        
        $dados['comboStatus'] = array(
            '1' => 'Ativo',
            '0' => 'Inativo',
        );
        
        $statePost = ($this->input->post('state_id')) ? $this->input->post('state_id') : NULL;
        if ($statePost) {
            $city = new Entities\City;
            $arrCity = $this->doctrine->em->getRepository('Entities\City')
                                    ->findBy(array('state' => $statePost),array('city' =>'ASC'));
            $dados['comboCity'] = $city->getComboCity($arrCity);
        } else {
            $dados['comboCity'] = array('' => 'selecione');
        }
        
        $this->load->view('template/header');
        $this->load->view('users/index');
        $this->load->view('template/messages');
        $this->load->view('users/insert', $dados);
        $this->load->view('template/footer');
    }

    function save() {
        $user = new Entities\User;
        $this->doctrine->em->getConnection()->beginTransaction();
        
        // gerar senha aleatoria
        $numeros = 0123456789;
        $letras = "abcdefghijklmnopqrstuvyxwz";
        $password = str_shuffle($numeros);
        $password .= str_shuffle($letras);
        $senha = substr(str_shuffle($password),0,6);
        
        try {
                $user->setPassword($senha);
                $user->setStatus(STATUS_AGUARDANDO);
                $user->setUsername($this->input->post('username'));
                $user->setEmail($this->input->post('email'));
                $level = $this->doctrine->em->getRepository('Entities\Level')
                        ->find($this->input->post('level_id'));
                $user->setLevel($level);
                $this->doctrine->em->persist($user);
                $this->doctrine->em->flush();
                if ($this->input->post('level_id') == LEVEL_FORNECEDOR) {
                    $supplier = new Entities\Supplier;
                    $this->saveSupplier($supplier,$user->getId());
                }
                
                // enviar e-mail com a nova senha
                $this->sendEmailCadastro($user,$senha);
                
                $this->session->set_flashdata('success', MSG_SUCCESS);
                $this->doctrine->em->getConnection()->commit();
        } catch (Exception $ex) {
            $this->doctrine->em->getConnection()->rollback();
            $this->session->set_flashdata('error', MSG_ERROR);
        }
        redirect('users/index');
    }

            
    function edit($id) {
        if (!$id) {
            redirect('users/index');
        }
        
        $arrLogin = $this->session->userdata('login');
        $user_id = $arrLogin['id'];
        $level_id = $arrLogin['level'];
        
        if ($level_id == LEVEL_FORNECEDOR) {
            if ($id != $user_id) {
                redirect('users/view/minha-conta');
            }
        }
        
        if ($this->input->post()) {
            $user = $this->doctrine->em->getRepository('Entities\User')
                        ->find($this->input->post('user_id'));
            if ($user->getEmail() != $this->input->post('email')) {
                $this->form_validation->set_rules('email', 'E-mail', 'is_unique[users.email]');
                if ($this->form_validation->run() == FALSE)
                {
                    $this->session->set_flashdata('error', validation_errors());
                    redirect('users/edit/'.$id);
                }
            }
            
            if ($this->input->post('level_id') == LEVEL_FORNECEDOR) {
                if ($this->form_validation->run('supplier_user')){
                    $this->update($user);
                }
            } else {
                if ($this->form_validation->run('user')){
                    $this->update($user);
                }
            }
        }
        $dados = array();
        $user = new Entities\User;
        $level = new Entities\Level;
        $arrUser = $this->doctrine->em->getRepository('Entities\User')->findById($id);
        if ($level_id == LEVEL_ADMINISTRADOR) {
            $arrLevel = $this->doctrine->em->getRepository('Entities\Level')->findAll();
        } else {
            $arrLevel = $this->doctrine->em->getRepository('Entities\Level')
                        ->findBy(array('level' => array(LEVEL_FORNECEDOR, LEVEL_OPERADOR)));
        }
        
        $dados['dados'] = $user->getArrayUser($arrUser);
        $dados['comboLevel'] = $level->getComboLevel($arrLevel);
        $dados['comboStatus'] = array(
            STATUS_ATIVO => 'Ativo',
            STATUS_INATIVO => 'Inativo',
            STATUS_AGUARDANDO => 'Pendente',
        );
        
        $branch = new Entities\Branch;
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')
                                        ->findBy(array('status' => TRUE));
        $dados['comboBranch'] = $branch->getComboBranch($arrBranch);
        $supplier = new Entities\Supplier;
        $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')
                                          ->findBy(array('user' => $dados['dados']['user_id']));

        $dados['dados']['supplier'] = $supplier->getArraySupplier($arrSupplier);

        $state = new Entities\State;
        $arrState = $this->doctrine->em->getRepository('Entities\State')
                                    ->findBy(array(),array('state' =>'ASC'));
        $dados['comboState'] = $state->getComboState($arrState);

        $city = new Entities\City;
        if (isset($dados['dados']['supplier']['state_id'])) {
            $arrCity = $this->doctrine->em->getRepository('Entities\City')
                                    ->findBy(array('state' => $dados['dados']['supplier']['state_id']),
                                            array('city' =>'ASC'));
        } else {
            $arrCity = array();
        }
        
        $dados['comboCity'] = $city->getComboCity($arrCity);
        
        $this->load->view('template/header');
        $this->load->view('users/index');
        $this->load->view('template/messages');
        $this->load->view('users/insert',$dados);
        $this->load->view('template/footer');
    }

    private function update($user) {
        if ($user){
            $this->doctrine->em->getConnection()->beginTransaction();
            try {
                if ($this->input->post('level_id') == LEVEL_FORNECEDOR) {
                    $objSupplier = $this->doctrine->em->getRepository('Entities\Supplier')
                                 ->find($this->input->post('supplier_id'));
                    $supplier = $objSupplier ? $objSupplier : new Entities\Supplier;
                    $this->saveSupplier($supplier,$user);
                }
                $user->setUsername($this->input->post('username'));
                $user->setEmail($this->input->post('email'));
                $level = $this->doctrine->em->getRepository('Entities\Level')
                        ->find($this->input->post('level_id'));
                $user->setLevel($level);
                $user->setStatus($this->input->post('status'));
                
                // caso o usuario altere o status para Aguardando (pendente), 
                // sera enviado uma nova senha aleatoria
                if ($this->input->post('status') == STATUS_AGUARDANDO) {
                    $numeros = 0123456789;
                    $letras = "abcdefghijklmnopqrstuvyxwz";
                    $password = str_shuffle($numeros);
                    $password .= str_shuffle($letras);
                    $senha = substr(str_shuffle($password),0,6);
                    $user->setPassword($senha);
                    $this->sendEmailCadastro($user,$senha);
                }
                
                $this->doctrine->em->persist($user);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_ALTERADO);
                $this->doctrine->em->getConnection()->commit();
            } catch (Exception $ex) {
                $this->doctrine->em->getConnection()->rollback();
                $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
        }
        if ($this->uri->segment(4) == 'supplier') {
            redirect('suppliers/lists');
        } else {
            redirect('users/lists');
        }
    }
    
    /*
     * Verifica dentro de um array de e-mails se possui algum email invalido.
     * Se possuir, retorna FALSE.
     */
    public function email_supplier_check($strEmail) {
        if ($strEmail) {
            $arrEmail = explode(',', $strEmail);
            foreach ($arrEmail as $mail) {
                $verifica = trim($mail);
                if (!(filter_var($verifica, FILTER_VALIDATE_EMAIL))) {
                    $this->form_validation->set_message('email_supplier_check', 'Este campo possui e-mails inválidos.');
                    return FALSE;
                }
            }
        }
        return TRUE;
    }
    
    private function saveSupplier($supplier,$user_id) {
        
        $emailCopy = $this->input->post('email_copy');
        if ($emailCopy) {
            $this->email_supplier_check($emailCopy);
        }
        $branch = $this->doctrine->em->getRepository('Entities\Branch')->find($this->input->post('branch_id'));
        $city = $this->doctrine->em->getRepository('Entities\City')->find($this->input->post('city_id'));
        $user = $this->doctrine->em->getRepository('Entities\User')->find($user_id);

        $cnpj = str_replace(array('.','/','-'), '', $this->input->post('cnpj'));
        $commercial_phone = str_replace(array('(',')','-',' '), '', $this->input->post('commercial_phone'));
        $fax_phone = str_replace(array('(',')','-',' '), '', $this->input->post('fax_phone'));
        $zip = str_replace(array('-','.'), '', $this->input->post('zip'));

        $supplier->setCnpj($cnpj);
        $supplier->setCorporateName($this->input->post('corporate_name'));
        $supplier->setTradeName($this->input->post('trade_name'));
        $supplier->setStateRegistration($this->input->post('state_registration'));
        $supplier->setEmailCopy($this->input->post('email_copy'));
        $supplier->setAddress($this->input->post('address'));
        $supplier->setComplement($this->input->post('complement'));
        $supplier->setDistrict($this->input->post('district'));
        $supplier->setZip($zip);
        $supplier->setCommercialPhone($commercial_phone);
        $supplier->setFaxPhone($fax_phone);
        $supplier->setCity($city);
        $supplier->setBranch($branch);
        $supplier->setUser($user);
        $this->doctrine->em->persist($supplier);
        $this->doctrine->em->flush();
    }
    
    function remove($id){
        
        $action = ($this->uri->segment(4) == 'supplier') ? 
                    'suppliers' : 'users';
        
        $user = $this->doctrine->em
                ->getRepository('Entities\User')
                ->find($id);
        if ($user) {
            $this->doctrine->em->getConnection()->beginTransaction();
            try {
                if ($user->getLevel()->getLevel() == LEVEL_FORNECEDOR) {
                    $supplier = $this->doctrine->em
                                     ->getRepository('Entities\Supplier')
                                     ->findBy(array('user' => $id));
                    
                    if ($supplier) {
                        $recall = $this->doctrine->em
                                     ->getRepository('Entities\Recall')
                                     ->findBy(array('supplier' => $supplier[0]->getId()));
                        if ($recall) {
                            $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
                            redirect($action.'/index');
                        } else {
                            $this->doctrine->em->remove($supplier[0]);
                            $this->doctrine->em->flush();
                        }
                    }
                }
                
                $this->doctrine->em->remove($user);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_REMOVE);
                $this->doctrine->em->getConnection()->commit();
                redirect('users/index');
            } catch (Exception $exc) {
                $this->doctrine->em->getConnection()->rollback();
                $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
        }
        redirect('users/index');
    }
    
    function view($id = NULL) {
        $dados = array();
        if ($this->uri->segment(3) == 'minha-conta') {
            $arrLogin = $this->session->userdata('login');
            $user_id = $arrLogin['id'];
            $action = 'minha-conta';
        } elseif (is_numeric($id)){
            $user_id = $id;
            $action = $id;
        } else {
            redirect('administrations');
        }

        $arrUser = $this->doctrine->em->getRepository('Entities\User')
                         ->findById($user_id);
        $user = new Entities\User;
        $dados['user'] = $user->getArrayUser($arrUser);
        if ($dados['user']['level'] == LEVEL_FORNECEDOR) {
            $supplier = new \Entities\Supplier;
            $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')
                         ->findBy(array('user' => $user_id));
            $dados['supplier'] = $supplier->getArraySupplier($arrSupplier);
        }
        
        if ($this->input->post()) {
            $this->changePassword($arrUser,$action);
        }
        
        $this->load->view('template/header');
        if ($action != 'minha-conta') {
            $this->load->view('users/index');
        }
        $this->load->view('template/messages');
        $this->load->view('users/view',$dados);
        $this->load->view('template/footer');
    }
    
    function changePassword($user,$action) {
        if ($this->form_validation->run('alter_password')){
            $senhaAtual = $user[0]->getPassword();
            $senhaForm = md5($this->input->post('password'));
            if ($senhaAtual == $senhaForm) {
                $user[0]->setPassword($this->input->post('password_new'));
                $user[0]->setStatus(STATUS_ATIVO);
                $this->doctrine->em->persist($user[0]);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_SENHA_ALTERADA);
                redirect('users/view/'.$action);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_USUARIO_SENHA_NAO_CONFERE);
                redirect('users/view/'.$action);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('users/view/'.$action);
        }
        
        $this->load->view('template/header');
        $this->load->view('users/index');
        $this->load->view('template/messages');
        $this->load->view('users/change_password');
        $this->load->view('template/footer');
    }
    
    private function sendEmailCadastro($user, $senha) {
            // encriptografando email
            $mailCripto = urlencode(base64_encode($user->getEmail()));

            $message = "Olá, ".$user->getUserName().'.<br/>';
            $message .= "Bem vindo(a) ao SISTEMA NACIONAL DE ALERTAS DE RECALL. <br/>";
            $message .= "Para acessá-lo, é necessário ativar a sua conta 
                        <a href='".base_url('principal/ativarConta/'.$mailCripto)."'>clicando aqui</a>
                        e informar a senha: <b>".$senha."</b>.<br/><br/>";
            $message .= "Atenciosamente, <br/> SENACON";
            $mailto  = $user->getEmail();
            $subject = "Cadastro Sistema de Recall";
            if (send_email($message,$mailto,$subject)) {
                return TRUE;
            } else {
                return FALSE;
            }
    }
    
    public function activeSupplier($user_id) {
        if ($user_id) {
            $user = $this->doctrine->em->getRepository('Entities\User')->find($user_id);
            $numeros = 0123456789;
            $letras = "abcdefghijklmnopqrstuvyxwz";
            $password = str_shuffle($numeros);
            $password .= str_shuffle($letras);
            $senha = substr(str_shuffle($password),0,6);
            $user->setPassword($senha);
            $user->setStatus(STATUS_AGUARDANDO);
            if ($this->sendEmailCadastro($user,$senha)) {
                $this->doctrine->em->persist($user);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_SENHA_NAO_ATIVADA);
            }
        }
        redirect('users/view/'.$user_id);
    }
}