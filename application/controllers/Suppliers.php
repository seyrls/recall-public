<?php

class Suppliers extends CI_Controller {

    function __construct() {
        parent::__construct();
       if (!($this->session->userdata('login'))){
           redirect(base_url(). 'logins');
       }
    }

    function index() {
            $this->lists();
    }

    function lists() {
        
        $dados['dados'] = $this->sqlAllSuppliers();
        
        $this->load->view('template/header');
        $this->load->view('users/index');
        $this->load->view('template/messages');
        $this->load->view('suppliers/lists',$dados);			
        $this->load->view('template/footer');
    }
    
    function requestLists() {
        
        $dados['dados'] = $this->sqlAllSuppliers(TRUE);
        
        $this->load->view('template/header');
        $this->load->view('users/index');
        $this->load->view('template/messages');
        $this->load->view('suppliers/request_lists',$dados);		
        $this->load->view('template/footer');
    }
    
    /*
     * Pega todos os fornecedores do sistema, (solicitados ou nao)
     */
    function sqlAllSuppliers($solicitado = FALSE) {
        $dados = array();
        
        $where = "";
        if ($solicitado) {
            $where = "WHERE u.status = ".STATUS_SOLICITADO."";
        } else {
            $where = "WHERE u.status not in (".STATUS_SOLICITADO.")";
        }
        $dql = "
                SELECT  u.id as user_id,
                        u.username,
                        u.status,
                        s.cnpj,
                        s.corporateName as corporate_name,
                        s.tradeName as trade_name,
                        st.state,
                        c.city
                FROM 
                        Entities\Supplier s
                INNER JOIN
                        Entities\User u WITH u.id = s.user
                INNER JOIN
                        Entities\City c WITH c.id = s.city
                INNER JOIN
                        Entities\State st WITH st.id = c.state
                        
                {$where}
                    
                ORDER BY 
                        u.username DESC
                ";
        $sql = $this->doctrine->em->createQuery($dql);
        if (count($sql->getResult()) > 0) {
            foreach ($sql->getResult() as $key => $value) {
                $dados[$key] = array(
                    'user_id'           => $value['user_id'],
                    'username'          => ($value['username']),
                    'status'            => $value['status'],
                    'cnpj'              => $value['cnpj'],
                    'corporate_name'    => ($value['corporate_name']),
                    'trade_name'        => ($value['trade_name']),
                    'state'             => $value['state'],
                    'city'              => ($value['city']),
                );
            }
        }
        return $dados;
    }

    function save() {
        $supplier = new Entities\Supplier;
        $branch = $this->doctrine->em->getRepository('Entities\Branch')->find($this->input->post('branch_id'));
        $city = $this->doctrine->em->getRepository('Entities\City')->find($this->input->post('city_id'));

        $msg['mensagem'] = null;

        $supplier->setCnpj($this->input->post('cnpj'));
        $supplier->setCorporateName($this->input->post('corporate_name'));
        $supplier->setTradeName($this->input->post('trade_name'));
        $supplier->setStateRegistration($this->input->post('state_registration'));
        $supplier->setAddress($this->input->post('address'));
        $supplier->setComplement($this->input->post('complement'));
        $supplier->setDistrict($this->input->post('district'));
        $supplier->setZip($this->input->post('zip'));
        $supplier->setCommercialPhone($this->input->post('commercial_phone'));
        $supplier->setFaxPhone($this->input->post('fax_phone'));
        $supplier->setCity($city);
        $supplier->setBranch($branch);

        $this->doctrine->em->persist($supplier);
        $this->doctrine->em->flush();

        if (!is_null($supplier->getId())) {
                $msg['mensagem'] = "Registro inserido/alterado com sucesso!";
                $this->lists($msg);
        }else{
                $msg['mensagem'] = "Erro ao inserir/alterar registro!";
                $this->lists($msg);
        }
    }

    function edit($id) {
        $supplier = $this->doctrine->em->getRepository('Entities\Supplier')->findAll($id);
        $state = $this->doctrine->em->getRepository('Entities\State')->findBy(array(),array('state' =>'ASC'));
        $branch = $this->doctrine->em->getRepository('Entities\Branch')->findBy(array(),array('branch' =>'ASC'));

        $dados['dados'] = array();

        foreach($supplier as $su){
            $dados['dados'] = array(
                'id' => $su->getId(),
                'cnpj' => $su->getCnpj(),
                'corporate_name' => $su->getCorporateName(),
                'trade_name' => $su->getTradeName(),
                'state_registration' => $su->getStateRegistration(),
                'address' => $su->getAddress(),
                'complement' => $su->getComplement(),
                'district' => $su->getDistrict(),
                'zip' => $su->getZip(),
                'commercial_phone' => $su->getCommercialPhone(),
                'fax_phone' => $su->getFaxPhone()
            );
        }

        foreach($branch as $b) {
            $dados['branch'][] = array(
                'id' => $b->getId(),
                'branch' => $b->getBranch()
            );
        }

        foreach($state as $s) {
            $dados['state'][] = array(
                'id' => $s->getId(),
                'state' => $s->getState()
            );
        }

        $this->load->view('template/header');
        $this->load->view('users/index');
        $this->load->view('template/messages');
        $this->load->view('suppliers/edit',$dados);
        $this->load->view('template/footer');
    }

    function update($id) {
        $supplier = $this->doctrine->em->getRepository('Entities\Supplier')->find($id);
        $branch = $this->doctrine->em->getRepository('Entities\Branch')->find($this->input->post('branch_id'));
        $city = $this->doctrine->em->getRepository('Entities\City')->find($this->input->post('city_id'));

        $msg['mensagem'] = null;

        $supplier->setCnpj($this->input->post('cnpj'));
        $supplier->setCorporateName($this->input->post('corporate_name'));
        $supplier->setTradeName($this->input->post('trade_name'));
        $supplier->setStateRegistration($this->input->post('state_registration'));
        $supplier->setAddress($this->input->post('address'));
        $supplier->setComplement($this->input->post('complement'));
        $supplier->setDistrict($this->input->post('district'));
        $supplier->setZip($this->input->post('zip'));
        $supplier->setCommercialPhone($this->input->post('commercial_phone'));
        $supplier->setFaxPhone($this->input->post('fax_phone'));
        $supplier->setCity($city);
        $supplier->setBranch($branch);

        $this->doctrine->em->flush();

        if (!is_null($supplier->getId())) {
            $msg['mensagem'] = "Registro inserido/alterado com sucesso!";
            $this->lists($msg);
        }else{
            $msg['mensagem'] = "Erro ao inserir/alterar registro!";
            $this->lists($msg);
        }
    }
}