<?php

class Serviceds extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');

        /**
         * Vefifica se o usuário está logado
         * Se não estiver, redireciona para a página de login
         */
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function lists() {
        $dados = array();
        $user = $this->session->userdata('login');
        $dados['user'] = $user;
        $supplier_id = $this->uri->segment(3);
        $recall_id = $this->uri->segment(4);
        $recall = new Entities\Recall;
        
        // verifica se o campanha a ser editada pertence ao forncedor correto.
        if ($user['level'] == LEVEL_FORNECEDOR){
            if ($supplier_id) {
                $this->verifyUser($user['id'], $supplier_id, $recall_id);
            }
            $arrSupplier = $this->doctrine->em
                                          ->getRepository('Entities\Supplier')
                                          ->findBy(array('user' => $user['id']));
            if ($arrSupplier) {
                $dados['supplier'] = array(
                    'supplier_id' => $arrSupplier[0]->getId(),
                    'trade_name' => ($arrSupplier[0]->getTradeName()),
                );
                $arrRecall = $this->doctrine->em->getRepository('Entities\Recall')
                                    ->findBy(
                                        array(
                                            'supplier' => $arrSupplier[0]->getId(),
                                            'statusCampaign' => array(STATUS_ID_PUBLICADA,STATUS_ID_PUBLICADA_COM_RESSALVA, STATUS_ID_FINALIZADA),
                                        )
                                    );
                $dados['comboRecall'] = $recall->getComboRecall($arrRecall);
            } else {
                redirect('serviceds/lists');
            }
        } else {
            $supplier = new Entities\Supplier();
            $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')->findBy(array(),array('corporateName' => 'ASC'));
            $dados['comboSupplier'] = $supplier->getComboSupplier($arrSupplier,array('' => 'selecione'),FALSE);
            
            $dados['supplier'] = array(
                'supplier_id' => ($supplier_id) ? $supplier_id : '',
                'recall_id' => ($recall_id) ? $recall_id : '',
            );
            $dados['comboRecall'] = array('' =>'selecione');
        }
        
        $validation = FALSE;
        
        if ($this->input->post()) {
            if ($this->form_validation->run('search_serviced')){
                $validation = TRUE;
            }
        }
        
        if ($validation || $recall_id) {
            $id = ($this->input->post('recall_id')) ? $this->input->post('recall_id') : $recall_id;
            $dados['serviced'] = $this->getTotalServicedProductByRecall($id);
            $dados['supplier']['supplier_id'] = ($this->input->post('supplier_id')) ? 
                                                $this->input->post('supplier_id'): $supplier_id;
            $dados['recall']['recall_id'] = $id;
            $arrRecall = $this->doctrine->em->getRepository('Entities\Recall')
                                ->findBy(
                                    array(
                                        'supplier' => $dados['supplier']['supplier_id'],
                                        'statusCampaign' => array( STATUS_ID_PUBLICADA,STATUS_ID_PUBLICADA_COM_RESSALVA, STATUS_ID_FINALIZADA),
                                    ), array(
                                        'title' => 'ASC'
                                    )
                                );
            $dados['comboRecall'] = $recall->getComboRecall($arrRecall);
        }
        
        
        
        
        $this->load->view('template/header');
        $this->load->view('serviceds/index');
        $this->load->view('template/messages');
        $this->load->view('serviceds/lists',$dados);
        $this->load->view('template/footer');
    }
    
    /*
     * Retorna um array com o total de atendimento por produto
     */
    private function getTotalServicedProductByRecall($recall_id){
        $dados = array();
        $this->load->database();
        $product = $this->doctrine->em->getRepository('Entities\Product')
                            ->findBy(array('recall' => $recall_id),array('product' => 'ASC'));
        if ($product) {
            foreach ($product as $key => $value) {
                $dql = "select 
                                        ts.id as serviced_id,
                                        sum(ts.quantity) as quantity_serviced,
                                        af.quantity as quantity_affected,
                                        af.id as affected_id,
                                        pr.id as product_id,
                                        pr.product,
                                        pr.quantity_affected as quantity_product,
                                        st.id as state_id,
                                        st.state,
                                        st.state_name,
                                        re.id as recall_id,
                                        re.title,
                                        (select 
                                                    date_insert 
                                        from 
                                                    total_serviceds
                                        where 
                                                    product_id = pr.id 
                                        and 
                                                    state_id = st.id
                                        order by 
                                                    date_insert desc limit 1
                                        ) as date_insert
                        from 
                                        affecteds af
                        inner join
                                        products pr ON af.product_id = pr.id
                        inner join 
                                        recalls re ON pr.recall_id = re.id
                        inner join 
                                        states st ON af.state_id = st.id
                        left join
                                       total_serviceds ts ON ts.product_id = pr.id and ts.state_id = st.id
                        where 
                                        pr.id = ".$value->getId()."
                        GROUP BY 
                                        st.id, pr.id
                        order by 
                                        pr.id,st.state ASC";
                
                $query = $this->db->query($dql);
                $result = $query->result();

                if ($result) {
                    foreach ($result as $k => $res) {
                        $dados[$key][$k] = array(
                            'serviced_id'       => $res->serviced_id,
                            'quantity_serviced' => $res->quantity_serviced,
                            'quantity_affected' => $res->quantity_affected,
                            'affected_id'       => $res->affected_id,
                            'product_id'        => $res->product_id,
                            'product'           => $res->product,
                            'quantity_product'  => $res->quantity_product,
                            'state_id'          => $res->state_id,
                            'state'             => $res->state,
                            'state_name'        => $res->state_name,
                            'recall_id'         => $res->recall_id,
                            'title'             => $res->title,
                            'date_insert'       => isset($res->date_insert) ? date("d/m/Y", strtotime($res->date_insert)) : $res->date_insert,
                            
                        );
                    }
                }
            }
        }
        return $dados;
    }
    
    /*
     * Retorna um array com o total de atendimento de uma campanha
     * Necessario utilizar mysql para o recurso de select into select
     */
    private function getTotalServicedByRecall($recall_id){
        
        $this->load->database();
        
        $dados = array();
        if ($recall_id) {
            
            $dql = "select 
                                    re.id as recall_id,
                                    sum(ts.quantity) as total_serviced,
                                    (select 
                                            sum(quantity)
                                    from 
                                            affecteds af 
                                    where 
                                            af.product_id in(
                                                select 
                                                        id 
                                                from 
                                                        products
                                                where 
                                                        recall_id = ".$recall_id."
                                            )
                                    ) as total_affected
                    from 
                                    affecteds af
                    inner join
                                    products pr on af.product_id = pr.id
                    inner join 
                                    recalls re on pr.recall_id = re.id
                    inner join 
                                    states st on af.state_id = st.id
                    left join
                                    total_serviceds ts on ts.product_id = pr.id and ts.state_id = st.id
                    where 
                                    re.id = ".$recall_id."
                    GROUP BY 
                                    re.id
                    order by 
                                    pr.id,st.state ASC;
                                    
                                    ";                
        
            $query = $this->db->query($dql);
            $result = $query->result();
            if ($result) {
                $dados = array(
                    'recall_id' => $result[0]->recall_id,
                    'total_affected' => $result[0]->total_affected,
                    'total_serviced' => $result[0]->total_serviced,
                );
            }
        }
        return $dados;
    }
    function edit($supplier_id, $recall_id) {
        
        if ((!$supplier_id) || (!$recall_id)) {
            redirect('serviceds/lists');
        }
        
        if ($this->input->post()) {
            if (array_sum($this->input->post('quantity_serviced_new')) !== 0) {
                $this->saveServiced($this->input->post(),$supplier_id,$recall_id);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_QUANTIDADE_ATENDIMENTO_VAZIO);
                redirect('serviceds/edit/'.$supplier_id.'/'.$recall_id);
            }
        }
        
        $dados = array();
        $user = $this->session->userdata('login');
        $dados['user'] = $user;
        
        $dados['supplier_id'] = $supplier_id;
        $dados['recall_id'] = $recall_id;
        
        if (($user['level'] == LEVEL_FORNECEDOR) && ($supplier_id)){
            $this->verifyUser($user['id'], $supplier_id, $recall_id);
        }
        
        $dados['serviced'] = $this->getTotalServicedProductByRecall($recall_id);
        
        $this->load->view('template/header');
        $this->load->view('serviceds/index');
        $this->load->view('template/messages');
        $this->load->view('serviceds/edit',$dados);
        $this->load->view('template/footer');
    }
    
    /*
     * Verifica se o usuario logado esta editando apenas as campanhas dele.
     */
    private function verifyUser($user_id, $supplier_id, $recall_id = NULL) {
        $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')
                            ->findBy(array('user' => $user_id));
        if ($arrSupplier[0]->getId() != $supplier_id) {
            redirect('serviceds/lists');
        } else {
            if ($recall_id) {
                $arrRecall = $this->doctrine->em->getRepository('Entities\Recall')
                            ->findBy(array('supplier' => $arrSupplier[0]->getId()));
                if ($arrRecall) {
                    $arrRecallId = array();
                    foreach ($arrRecall as $key => $value) {
                        $arrRecallId[$key] = $value->getId();
                    }
                    if (!in_array($recall_id, $arrRecallId)) {
                        redirect('serviceds/lists');
                    }
                } else {
                    redirect('serviceds/lists');
                }
            }
        }
    }
    
    private function saveServiced($post,$supplier_id,$recall_id) {
        if ((empty($post)) || (!$supplier_id) || (!$recall_id)) {
            return false;
        } else {
            $arrQuantityServiced = array_filter($post['quantity_serviced_new']);
            $arrIntersect = array();
            
            
            $post['total_affected'] = (!empty($post['total_affected'])) ? 
                                       explode(' ', $post['total_affected']) : array();
            
            $post['total_serviced'] = (!empty($post['total_serviced'])) ? 
                                       explode(' ', $post['total_serviced']) : array();
            
            foreach ($post as $key => $value) {
                $arrIntersect[$key] = array_values(array_intersect_key($value, $arrQuantityServiced));
            }
            
            $count = count($arrIntersect['product_id']);
            
            for ($i=0; $i < $count; $i++) {
                $serviced = new Entities\TotalServiced;    
                $affected = $this->doctrine->em->getRepository('Entities\Affected')
                                             ->find($arrIntersect['affected_id'][$i]);
                $product = $this->doctrine->em->getRepository('Entities\Product')
                                             ->find($arrIntersect['product_id'][$i]);
                $state = $this->doctrine->em->getRepository('Entities\State')
                                             ->find($arrIntersect['state_id'][$i]);
                
                $state_serviced = (int)$arrIntersect['state_id'][$i];
                $state_affected = $affected->getState()->getId();
                
                $quantity_serviced = $arrIntersect['quantity_serviced_new'][$i];
                $serviced_id = $arrIntersect['serviced_id'][$i];
                if ($serviced_id) {
                    $servicedQuantity = $this->doctrine->em->getRepository('Entities\TotalServiced')
                                             ->find($serviced_id);
                    if ($servicedQuantity->getQuantity() == $quantity_serviced) {
                        continue;
                    }
                }
                
                if ($state_serviced == $state_affected) {
                    $this->doctrine->em->getConnection()->beginTransaction();
                    try {
                        $serviced->setDateInsert(new \DateTime());
                        $serviced->setQuantity($arrIntersect['quantity_serviced_new'][$i]);
                        $serviced->setProduct($product);
                        $serviced->setState($state);
                        $this->doctrine->em->persist($serviced);
                        $this->doctrine->em->flush();
                        $this->doctrine->em->getConnection()->commit();
                    }
                    catch(Exception $err){
                        $this->doctrine->em->getConnection()->rollback();
                        throw $err;
                    }
                } else {
                    $this->doctrine->em->getConnection()->rollback();
                    $this->session->set_flashdata('error', validation_errors());
                }
            }
            
            // verifica se a campanha atingiu 100% de atendimento
            $arrTotalServiced = $this->getTotalServicedByRecall($recall_id);
            $recall_id = $arrTotalServiced['recall_id'];
            
            if ($arrTotalServiced['total_serviced'] == $arrTotalServiced['total_affected']) {
                $this->finishRecall($supplier_id,$recall_id);
            } else {
                $this->session->set_flashdata('success', MSG_SUCCESS);
                redirect('serviceds/lists/'.$supplier_id.'/'.$recall_id);
            }
        }
    }
    
    /*
     * Funcao para finalizar a campanha apos esta concluir todos os indices de atendimento
     */
    private function finishRecall($supplier_id,$recall_id) {
        $supplier = $this->doctrine->em->getRepository('Entities\Supplier')
                                             ->find($supplier_id);
        
        $emailSupplierCopy = ($supplier->getEmailCopy()) ? $supplier->getEmailCopy() : NULL;
        
        $user_id = $supplier->getUser()->getId();
        $user = $this->doctrine->em->getRepository('Entities\User')
                                             ->find($user_id);
        
        $recall = $servicedQuantity = $this->doctrine->em->getRepository('Entities\Recall')
                                             ->find($recall_id);
        if (($user) && ($supplier) && ($recall)) {
            $this->load->helper('send_email');
            $mailto   = $user->getEmail();
            $mailfrom = EMAIL_FROM_SENACON;
            $emailcopy = $emailSupplierCopy;
            
            $subject  = "Campanha de Recall finalizada.";
            $message  = "Olá, ".$user->getUsername().'.<br/>';
            $message .= "Informamos que a campanha <b>".$recall->getTitle()." </b>";
            $message .= "Foi finalizada. <br/> <br/>";
            $message .= "Atenciosamente, <br/> SENACON";
            send_email($message,$mailto,$subject,$mailfrom,$emailcopy);
            $recall->setStatusCampaign(STATUS_ID_FINALIZADA);
            $recall->setEndDate(new \DateTime());
            $this->doctrine->em->persist($recall);
            $this->doctrine->em->flush();
            $this->session->set_flashdata('success', MSG_FINISH_RECALL);
        } else {
            $this->session->set_flashdata('error', MSG_ERROR);
        }
        redirect('serviceds/lists/'.$supplier_id.'/'.$recall_id);
    }

    public function detail($product_id, $state_id, $qtd_total) {
        $dados = array();
        if ($product_id && $state_id) {
            $total_serviced = new Entities\TotalServiced;
            $arrServiced = $this->doctrine->em->getRepository('Entities\TotalServiced')
                            ->findBy(array(
                                        'product' => $product_id,
                                        'state' => $state_id,
                                        ), array('dateInsert' => 'ASC')
                                    );
            if ($arrServiced) {
                $dados['dados'] = $total_serviced->getArrayTotalServiced($arrServiced);
                $dados['product'] = $dados['dados'][0]['product'];
                $dados['state_name'] = $dados['dados'][0]['state_name'];
            }
            else {
                $product = $this->doctrine->em->getRepository('Entities\Product')->find($product_id);
                $dados['product'] = $product->getProduct();
                
                $state = $this->doctrine->em->getRepository('Entities\State')->find($state_id);
                $dados['state_name'] = ($state->getStateName());
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_NAO_ENCONTRADO);
            redirect('serviceds/lists');
        }
        $dados['qtd_total_uf'] = ($qtd_total) ? $qtd_total : 0;
        
        $this->load->view('template/header_clean');
        $this->load->view('serviceds/detail',$dados);
        $this->load->view('template/footer_clean');
    }
}