<?php

class Origins extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        
        /**
         * Vefifica se o usuário está logado
         * Se não estiver, redireciona para a página de login
         */
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function index() {
        $this->lists();
    }

    function lists() {
        $origin     = new Entities\Origin;
        $arrOrigin  = $this->doctrine->em->getRepository('Entities\Origin')->findAll();
        $dados['dados'] = $origin->getArrayAllOrigin($arrOrigin);
        $this->load->view('template/header');
        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
        );
        $this->load->view('origins/breadcrumbs',$breadcrumbs);
        $this->load->view('template/messages');
        $this->load->view('origins/lists',$dados);
        $this->load->view('template/footer');
    }

    function insert() {
        if ($this->input->post()) {
            $this->save();
        }
        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
            'Inserir' => 'origins/insert',
        );
        
        $this->load->view('template/header');
        $this->load->view('origins/breadcrumbs',$breadcrumbs);
        $this->load->view('template/messages');
        $this->load->view('origins/insert');
        $this->load->view('template/footer');
    }

    function save() {
        $this->form_validation->set_rules('origin', 'Meio de veiculação', 'required|max_length[20]');
        if ($this->form_validation->run() == TRUE) {
            $origin = new Entities\Origin;
            $origin->setOrigin($this->input->post('origin'));
            $origin->setStatus(TRUE);
            $this->doctrine->em->persist($origin);
            $this->doctrine->em->flush();        
            if (!is_null($origin->getId())) {
                $this->session->set_flashdata('success', MSG_SUCCESS);
            }else{
                $this->session->set_flashdata('error', MSG_ERROR);
            }
            redirect('origins/lists');
        }
    }

    function edit($id) {
        if ($this->input->post()) {
            $this->update();
        }
        $origin = $this->doctrine->em->getRepository('Entities\Origin')->findById($id);
        $dados['dados'] = array();
        if ($origin) {
            foreach ($origin as $b) {
                $dados['dados'] = array(
                    'id' => $b->getId(),
                    'origin' => ($b->getOrigin()),
                    'status' => $b->getStatus(),
                );
            }
        } else {
            redirect('origins/lists');
        }
        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
            'Editar '.$dados['dados']['origin'] => 'origins/edit/'.$id,
        );
        $this->load->view('template/header');
        $this->load->view('origins/breadcrumbs',$breadcrumbs);
        $this->load->view('template/messages');
        $this->load->view('origins/edit', $dados);
        $this->load->view('template/footer');
    }

    function update() {
        $this->form_validation->set_rules('origin', 'Setor', 'required|max_length[20]');
        if ($this->form_validation->run() == TRUE) {
            $origin = $this->doctrine->em->getRepository('Entities\Origin')->find($this->input->post('id'));
            if ($origin) {
                $origin->setStatus($this->input->post('status'));
                $origin->setOrigin($this->input->post('origin'));
                $this->doctrine->em->persist($origin);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR);
            }
            redirect('origins/index');
        }
    }
    
    function remove($id){
        $media = $this->doctrine->em
                ->getRepository('Entities\Media')
                ->findBy(array('origin' => $id));
        if (!$media) {
            $origin = $this->doctrine->em
                ->getRepository('Entities\Origin')
                ->find($id);
            if ($origin) {
                $this->doctrine->em->remove($origin);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_REMOVE);
            }else{
                $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
        }
        redirect('origins/index');
    }
    function view($id) {
        if (!$id) {
            redirect('origins/index');
        }
        $dados = array();
        $media  = new Entities\Media;
        $origin = $this->doctrine->em->getRepository('Entities\Origin')->find($id);
        if (!$origin) {
            redirect('origins/index');
        }
        $dados['dados']['origin']    = $origin->getOrigin();
        $dados['dados']['origin_id'] = $origin->getId();
        $arrMedia = $this->doctrine->em->getRepository('Entities\Media')->findBy(array('origin' => $id));
        $dados['media']  = $media->getArrayAllMedia($arrMedia);
        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
            $origin->getOrigin() => 'origins/view/'.$id,
        );
        $this->load->view('template/header');
        $this->load->view('origins/breadcrumbs',$breadcrumbs);
        $this->load->view('template/messages');
        $this->load->view('origins/view',$dados);
        $this->load->view('template/footer');
    }
}