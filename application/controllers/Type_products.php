<?php

class Type_products extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
    }

    function index() {
            $this->lists();
    }

    function lists() {
        $arrTypeProduct = $this->doctrine->em->getRepository('Entities\TypeProduct')->findBy(array(),array('typeProduct' => 'ASC'));
        $typeProduct = new Entities\TypeProduct;
        $dados['dados'] = $typeProduct->getArrayAllTypeProduct($arrTypeProduct);
        
        $this->load->view('template/header');
        $this->load->view('branches/index');
        $this->load->view('template/messages');
        $this->load->view('type_products/lists',$dados);
        $this->load->view('template/footer');
    }

    function insert() {
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')
                            ->findBy(array('status' => STATUS_ATIVO),array('branch' => 'ASC'));
        if ($this->input->post()) {
            $this->save();
        }
        $dados = array();
        $branch = new Entities\Branch;
        $dados['comboBranch'] = $branch->getComboBranch($arrBranch);
        
        $this->load->view('template/header');
        $this->load->view('branches/index');
        $this->load->view('template/messages');
        $this->load->view('type_products/insert', $dados);
        $this->load->view('template/footer');
    }

    function save() {
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
        $this->form_validation->set_rules('type_product', 'Tipo de Produto', 'required|max_length[70]');
        $this->form_validation->set_rules('branch_id', 'Setor de Atividade', 'required');
        if ($this->form_validation->run() == TRUE) {
            $obj = new Entities\TypeProduct;
            $branch = $this->doctrine->em->getRepository('Entities\Branch')
                            ->find($this->input->post('branch_id'));
            $obj->setTypeProduct($this->input->post('type_product'));
            $obj->setStatus($this->input->post('status'));
            $obj->setBranch($branch);
            $this->doctrine->em->persist($obj);
            $this->doctrine->em->flush();
            $this->session->set_flashdata('success', MSG_SUCCESS);
            redirect('type_products/lists');
        }
        

    }

    function edit($id) {
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
        if ($this->input->post()) {
            $this->update();
        }
        $arrTypeProduct = $this->doctrine->em->getRepository('Entities\TypeProduct')->findById($id);
        $typeProduct = new Entities\TypeProduct;
        $dados['dados'] = $typeProduct->getArrayTypeProduct($arrTypeProduct);
        
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')
                            ->findBy(array(), array('branch' => 'ASC'));
        $branch = new Entities\Branch;
        $dados['comboBranch'] = $branch->getComboBranch($arrBranch);
        
        $this->load->view('template/header');
        $this->load->view('branches/index');
        $this->load->view('template/messages');
        $this->load->view('type_products/insert',$dados);
        $this->load->view('template/footer');
    }
    
    function update() {
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
        $this->form_validation->set_rules('type_product', 'Tipo de Produto', 'required|max_length[70]');
        $this->form_validation->set_rules('branch_id', 'Setor de Atividade', 'required');
        if ($this->form_validation->run() == TRUE) {
            $product = $this->doctrine->em
                            ->getRepository('Entities\TypeProduct')
                            ->find($this->input->post('type_product_id'));
            if ($product) {
                $branch = $this->doctrine->em
                            ->getRepository('Entities\Branch')
                            ->find($this->input->post('branch_id'));
                $product->setTypeProduct($this->input->post('type_product'));
                $product->setStatus($this->input->post('status'));
                $product->setBranch($branch);
                $this->doctrine->em->persist($product);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
            }
            redirect('type_products/lists');
        }
    }
    
    function loadCombo(){
        if ($this->input->post('branch_id')) {
            $product = $this->doctrine->em->getRepository('Entities\TypeProduct')
                            ->findBy(array('branch' => $this->input->post('branch_id')));
            if ($product) {
                $dados = array();
                foreach($product as $value){
                    $dados[$value->getId()] = ($value->getTypeProduct());
                }
            }
            echo json_encode($dados);
        } else {
            echo json_encode(array('msg' => 'err'));
        }
        die;
    }
    
    function remove($id){
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
        if (!$id) {
            redirect('type_products/lists');
        }
        $product = $this->doctrine->em->getRepository('Entities\Product')
                        ->findBy(array('type_product' => $id));
        if ($product) {
            $this->session->set_flashdata('error', MSG_ERROR_REMOVE_RECALL_EXISTS);
            redirect('type_products/lists');
        } else {
            $typeProduct = $this->doctrine->em->getRepository('Entities\TypeProduct')->find($id);
            if ($typeProduct) {
                $this->doctrine->em->remove($typeProduct);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_REMOVE);
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
            }
        }
        redirect('type_products/lists');
    }
}