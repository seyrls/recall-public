<?php

class Partners extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function index() {
        $this->lists();
    }

    function lists() {
        $dados = array();
        $arrGroupPartner = $this->sqlGroupPartner();
        if ($arrGroupPartner) {
            
            foreach ($arrGroupPartner as $key => $value) {
                $dados['dados'][$key] = array(
                    'partner_id'        => $value->partner_id,
                    'partner'           => $value->partner,
                    'email'             => $value->email,
                    'group_id'          => $value->group_id,
                    'name'              => $value->name,
                    'status'            => $value->status,
                    'dateInsert'        => $value->date_insert,
                );
            }
        }
        
        $this->load->view('template/header');
        $this->load->view('partners/index');
        $this->load->view('template/messages');
        $this->load->view('partners/lists',$dados);			
        $this->load->view('template/footer');
    }
    
    /*
     * Funcao nativa utilizando active record para utilizar a funcao GROUP_CONCAT,
     * pois a mesma nao eh possivel de ser utilizada no doctrine
     */
    function sqlGroupPartner($partner_id = NULL) {
        
        $this->load->database();
        
        if ($partner_id) {
            $where = " where p.id = ".$partner_id;
        } else {
            $where = "";
        }
        
        $sql = "SELECT  DISTINCT    p.id as partner_id,
                                    g.id as group_id,
                                    p.email,
                                    p.partner,
                                    p.status,
                                    gp.date_insert,
                                    group_concat(DISTINCT g.name ORDER BY g.name ASC SEPARATOR ' - ') as name,
                                    group_concat(DISTINCT g.id) as group_partner_id
                FROM 
                                    partners p
                LEFT JOIN
                                    group_partners gp on p.id = gp.partner_id
                LEFT JOIN
                                    groups g on g.id = gp.group_id and g.status = ".STATUS_ATIVO."
                {$where}
                    
                GROUP BY 
                                    p.id
                ORDER BY 
                        p.partner ASC";
        
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function insert($group_id = NULL) {
        
        if ($this->input->post()) {
            $this->save();
        }
        
        $arrGroups = $this->doctrine->em->getRepository('Entities\Group')
                                    ->findBy(array('status' => STATUS_ATIVO),array('name' => 'ASC'));
        $group = new \Entities\Group;
        $data['groups'] = $group->getArrayAllGroup($arrGroups);
        
        if ($group_id) {
            $arrGroup  = $this->doctrine->em->getRepository('Entities\Group')
                                            ->find($group_id);
            $data['group_id'] = $arrGroup->getId();
        }
        
        $this->load->view('template/header');
        $this->load->view('partners/index');
        $this->load->view('template/messages');
        $this->load->view('partners/insert', $data);
        $this->load->view('template/footer');
    }

    function save() {
        $partner = new Entities\Partner;
        $this->form_validation->set_rules('partner', 'Entidade', 'required|max_length[200]|is_unique[partners.partner]');
        $this->form_validation->set_rules('email', 'E-mail', 'required|trim|callback_email_partner_check|max_length[1000]');

        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        }
        $this->doctrine->em->getConnection()->beginTransaction();
        try {
                $partner->setStatus($this->input->post('status'));
                $partner->setPartner($this->input->post('partner'));
                $partner->setEmail($this->input->post('email'));
                $this->doctrine->em->persist($partner);
                $this->doctrine->em->flush();
                $arrGroupId = array_unique($this->input->post('idGroup'));
                if ($arrGroupId) {
                    foreach ($arrGroupId as $group_id) {
                        $groupPartner = new Entities\GroupPartner;
                        $group = $this->doctrine->em->getRepository('Entities\Group')
                                        ->find($group_id);
                        $groupPartner->setGroup($group);
                        $groupPartner->setPartner($partner);
                        $groupPartner->setDateInsert(new \DateTime());
                        $this->doctrine->em->persist($groupPartner);
                        $this->doctrine->em->flush();
                    }
                }
                $this->session->set_flashdata('success', MSG_SUCCESS);
                $this->doctrine->em->getConnection()->commit();
            } catch (Exception $ex) {
                $this->doctrine->em->getConnection()->rollback();
                $this->session->set_flashdata('error', MSG_ERROR);
            }
        redirect('partners/index');
    }

    function edit($id) {
        if (!$id) {
            redirect('partners/index');
        }
        if ($this->input->post()) {
            $this->update($id);
        }
        
        $arrGroups = $this->doctrine->em->getRepository('Entities\Group')
                                    ->findBy(array('status' => STATUS_ATIVO),array('name' => 'ASC'));
        $group = new \Entities\Group;
        $dados['groups'] = $group->getArrayAllGroup($arrGroups);
        
        $arrGroupPartner = $this->sqlGroupPartner($id);
        if ($arrGroupPartner) {
            foreach ($arrGroupPartner as $value) {
                $dados['dados'] = array(
                    'partner_id'        => $value->partner_id,
                    'partner'           => $value->partner,
                    'email'             => $value->email,
                    'group_id'          => $value->group_id,
                    'group_partner_id'  => $value->group_partner_id,
                    'name'              => $value->name,
                    'status'            => $value->status,
                    'dateInsert'        => $value->date_insert,
                );
            }
        }
        
        $this->load->view('template/header');
        $this->load->view('partners/index');
        $this->load->view('template/messages');
        $this->load->view('partners/insert',$dados);
        $this->load->view('template/footer');
    }

    /*
     * Verifica dentro de um array de e-mails se possui algum email invalido.
     * Se possuir, retorna FALSE.
     */
    public function email_partner_check($strEmail) {
        if ($strEmail) {
            $arrEmail = explode(',', $strEmail);
            foreach ($arrEmail as $mail) {
                $verifica = trim($mail);
                if (!(filter_var($verifica, FILTER_VALIDATE_EMAIL))) {
                    $this->form_validation->set_message('email_partner_check', 'E-mail(s) preenchido(s) incorretamente.');
                    return FALSE;
                }
            }
        }
        return TRUE;
    }
    
    private function update($id) {
        $partner = $this->doctrine->em->getRepository('Entities\Partner')
                        ->find($id);
        
        $this->form_validation->set_rules('partner', 'Entidade', 'required|max_length[200]');
        $this->form_validation->set_rules('email', 'E-mail', 'required|trim|callback_email_partner_check|max_length[1000]');

        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        }
        
        if ($partner){
            $this->doctrine->em->getConnection()->beginTransaction();
            try {
                $partner->setPartner($this->input->post('partner'));
                $partner->setEmail($this->input->post('email'));
                $partner->setStatus($this->input->post('status'));
                $this->doctrine->em->persist($partner);
                $this->doctrine->em->flush();
                $this->removeGroup($id);
                if ($this->input->post('idGroup')) {
                    $arrGroupId = array_unique($this->input->post('idGroup'));
                    foreach ($arrGroupId as $group_id) {
                        $groupPartner = new Entities\GroupPartner;
                        $group = $this->doctrine->em->getRepository('Entities\Group')
                                        ->find($group_id);
                        $groupPartner->setGroup($group);
                        $groupPartner->setPartner($partner);
                        $groupPartner->setDateInsert(new \DateTime());
                        $this->doctrine->em->persist($groupPartner);
                        $this->doctrine->em->flush();
                    }
                }
                $this->session->set_flashdata('success', MSG_SUCCESS_ALTERADO);
                $this->doctrine->em->getConnection()->commit();
            } catch (Exception $ex) {
                $this->doctrine->em->getConnection()->rollback();
                $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
        }
        redirect('partners/lists');
    }
    
    function remove($id){
        if ($id) {
            $partner = $this->doctrine->em->getRepository('Entities\Partner')->find($id);
            if ($partner) {
                $this->doctrine->em->getConnection()->beginTransaction();
                try {
                    $this->removeGroup($id);
                    $this->doctrine->em->remove($partner);
                    $this->doctrine->em->flush();
                    $this->session->set_flashdata('success', MSG_SUCCESS_REMOVE);
                    $this->doctrine->em->getConnection()->commit();
                } catch (Exception $ex) {
                    $this->doctrine->em->getConnection()->rollback();
                    $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
                }
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
        }
        redirect('partners/index');
    }
    
    private function removeGroup($partner_id) {
        $gp = $this->doctrine->em
                    ->getRepository('Entities\GroupPartner')
                    ->findBy(array('partner' => $partner_id));
        if ($gp) {
            foreach ($gp as $value) {
                $this->doctrine->em->remove($value);
                $this->doctrine->em->flush();
            }
        }
    }
}