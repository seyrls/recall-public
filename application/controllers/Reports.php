<?php

class Reports extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();

        /**
         * Vefifica se o usuário está logado
         * Se não estiver, redireciona para a página de login
         */
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    function index() {
        $this->load->view('template/header');
        $this->load->view('reports/index');
        $this->load->view('template/messages');
        $this->load->view('reports/sumary');
        $this->load->view('template/footer');
    }
    
    function recall() {
        $this->load->library('form_validation');
        $supplier = new Entities\Supplier;
        $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')->findAll();
        $dados['comboSupplier'] = $supplier->getComboSupplier($arrSupplier, array('' => 'Todos'),FALSE);
        
        $branch = new Entities\Branch;
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')->findAll();
        $dados['comboBranch'] = $branch->getComboBranch($arrBranch, array('' => 'Todos'));
        
        $country = new Entities\Country;
        $arrCountry = $this->doctrine->em->getRepository('Entities\Country')->findBy(array(), array('country' => 'ASC'));
        $dados['comboCountry'] = $country->getComboCountry($arrCountry, array('' => 'Todos'));
        
        $risk = new Entities\TypeRisk;
        $arrRisk = $this->doctrine->em->getRepository('Entities\TypeRisk')->findAll();
        $dados['comboRisk'] = $risk->getComboTypeRisk($arrRisk, array('' => 'Todos'));
        
        $recall = new Entities\Recall;
        $dados['comboStatusCampaign'] = $recall->getComboStatusCampaignPublish();
        
        if ($this->input->post()) {
            if ($this->input->post('start_date')){
                $this->form_validation->set_rules('start_date', 'Período início', 'check_date_valid');
            }
            if ($this->input->post('end_date')){
                $this->form_validation->set_rules('end_date', 'Período final', 'check_date_valid');
            }
            if (($this->input->post('start_date')) && ($this->input->post('end_date'))) {
                $this->form_validation->set_rules('start_date', 'Período final', 'check_date_diff['.$this->input->post('end_date').']');
            }
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', validation_errors());
            }
        }
        $dados['dados'] = $this->sqlReportRecall($this->input->post());
        
        $this->load->view('template/header');
        $this->load->view('reports/index');
        $this->load->view('template/messages');
        $this->load->view('reports/report_recall',$dados);
        $this->load->view('template/footer');
    }
    
    
    
    function chartRecall($recall_id) {
        if (!$recall_id) {
            redirect('reports/recall');
        }
        $dados = $this->sqlGetChartRecall($recall_id);
        $series_data[0] = array('State','Afetados','Atendidos');
        $totalAffected = 0;
        $totalServiced = 0;
        $title = '';
        if ($dados) {
            foreach ($dados as $key => $value) {
                $qtd_affected = ($value['quantity_affected']) ? $value['quantity_affected'] : 0;
                $qtd_serviced = ($value['quantity_serviced']) ? $value['quantity_serviced'] : 0;
                $totalAffected += $qtd_affected;
                $totalServiced += $qtd_serviced;
                $title = $value['title'];
                $state = $value['state_name'];
                $series_data[($key+1)] = array(
                    $state, 
                    (int) $qtd_affected,
                    (int) $qtd_serviced
                );
            }
        }
        
        $this->view_data['series_data'] = json_encode($series_data);
        $this->view_data['totalAffected'] = $totalAffected;
        $this->view_data['totalServiced'] = $totalServiced;
        $this->view_data['title'] = $title;
        
        $this->load->view('template/header_clean');
        $this->load->view('reports/charts/chart_recall',$this->view_data);
        $this->load->view('template/footer_clean');
    }
    
    function serviced() {
        $supplier = new Entities\Supplier;
        $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')->findBy(array(), array('corporateName' => 'ASC'));
        $dados['comboSupplier'] = $supplier->getComboSupplier($arrSupplier, array('' => 'Todos'),FALSE);
        
        $branch = new Entities\Branch;
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')->findAll();
        $dados['comboBranch'] = $branch->getComboBranch($arrBranch, array('' => 'Todos'));
        
        $typeProduct = new Entities\TypeProduct;
        if ($this->input->post('branch_id')) {
            $arrTypeProduct = $this->doctrine->em->getRepository('Entities\TypeProduct')
                          ->findBy(array('branch' => $this->input->post('branch_id')),
                                   array('typeProduct' => 'ASC'));
        } else {
            $arrTypeProduct = array();
        }
        
        $dados['comboTypeProduct']  = $typeProduct->getComboTypeProduct($arrTypeProduct, array('' => 'Todos'));
        
        $dados['dados'] = $this->sqlReportServiced($this->input->post());
        
        $this->load->view('template/header');
        $this->load->view('reports/index');
        $this->load->view('template/messages');
        $this->load->view('reports/report_serviced',$dados);
        $this->load->view('template/footer');
    }
    
    function chartServiced() {
        
        $supplier_id = $this->uri->segment(3);
        
        // dados para apresentar no grafico de pizza
        $series_data[0] = array('Campanhas de Recall','Atendidos','Afetados');
        $dados = $this->sqlChartServiced($supplier_id);
        if ($dados) {
            foreach ($dados as $key => $value) {
                $series_data[($key+1)] = array(
                    $value['title'],
                    $value['total_serviced'],
                    $value['total_affected'],
                );
            }
        }
        
        // retorno da busca realizada
        $this->view_data['series_data'] = json_encode($series_data);
        $this->view_data['corporate_name'] = $dados[0]['corporate_name'];
        
        $this->load->view('template/header_clean');
        $this->load->view('reports/charts/chart_serviced',$this->view_data);
        $this->load->view('template/footer_clean');
    }
    
    
    function affected() {
        
        $dados = array();

        $branch = new Entities\Branch;
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')->findAll();
        $dados['comboBranch'] = $branch->getComboBranch($arrBranch, array('' => 'Todos'));
        
        $typeProduct = new Entities\TypeProduct;
        if ($this->input->post('branch_id')) {
            $arrTypeProduct = $this->doctrine->em->getRepository('Entities\TypeProduct')
                          ->findBy(array('branch' => $this->input->post('branch_id')),
                                   array('typeProduct' => 'ASC'));
        } else {
            $arrTypeProduct = array();
        }
        $dados['comboTypeProduct']  = $typeProduct->getComboTypeProduct($arrTypeProduct, array('' => 'Todos'));
        
        if ($this->input->post()) {
            $dados['dados'] = $this->sqlReportAffected($this->input->post());
        }
        
        $this->load->view('template/header');
        $this->load->view('reports/index');
        $this->load->view('template/messages');
        $this->load->view('reports/report_affected',$dados);
        $this->load->view('template/footer');
    }
    
    function chartAffected() {
        
        $type_product_id = $this->uri->segment(3);
        if (!$type_product_id) {
            return false;
        }
        
        // dados para apresentar no grafico de pizza
        $series_data[0] = array('Produto','Quantidade Afetada');
        $dados = $this->sqlChartAffected($type_product_id);
        if ($dados) {
            foreach ($dados as $key => $value) {
                $series_data[($key+1)] = array(
                    $value['product'],
                    $value['total_affected']+0
                );
            }
        }
        
        // retorno da busca realizada
        $this->view_data['series_data']  = json_encode($series_data);
        $this->view_data['type_product'] = $dados[0]['type_product'];
        $this->view_data['branch']       = $dados[0]['branch'];

        $this->load->view('template/header_clean');
        $this->load->view('reports/charts/chart_affected',$this->view_data);
        $this->load->view('template/footer_clean');
    }
    
    function risk() {
        
        $dados = array();
        
        $supplier = new Entities\Supplier;
        $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')->findAll();
        $dados['comboSupplier'] = $supplier->getComboSupplier($arrSupplier, array('' => 'Todos'),FALSE);
        
        if ($this->input->post()) {
            $dados['dados'] = $this->sqlReportRisk($this->input->post());
        }
        
        $this->load->view('template/header');
        $this->load->view('reports/index');
        $this->load->view('template/messages');
        $this->load->view('reports/report_risk',$dados);
        $this->load->view('template/footer');
    }
    
    function chartRisk() {
        
        $parameters = array(
            'supplier_id' => $this->uri->segment(3),
        );
        // dados para apresentar no grafico de pizza
        $series_data[0] = array('Tipo de risco','Quantidade por campanha');
        $dados = $this->sqlReportRisk($parameters,TRUE);
        if ($dados) {
            foreach ($dados as $key => $value) {
                $series_data[($key+1)] = array(
                    $value['type_risk'],
                    $value['total_risk']+0
                );
            }
        }
        
        // retorno da busca realizada
        $this->view_data['series_data'] = json_encode($series_data);
        $this->view_data['corporate_name'] = $dados[0]['corporate_name'];

        $this->load->view('template/header_clean');
        $this->load->view('reports/charts/chart_risk',$this->view_data);
        $this->load->view('template/footer_clean');
    }
    
    
    private function sqlReportRecall($parametros = array()) {
        $this->load->helper('status');
        $andwhere = "";
        if ($parametros) {
            extract($parametros);
            
            $data_ini = ($start_date) ? implode("-",array_reverse(explode("/",$start_date))) : NULL;
            $data_fim = ($end_date) ? implode("-",array_reverse(explode("/",$end_date))) : NULL;
            
            if (($data_ini) && ($data_fim)) {
                $andwhere .= " and re.start_date between CAST('{$data_ini}' as DATE) and CAST('{$data_fim}' as DATE)";
            } elseif ($data_ini) {
                $andwhere .= " and re.start_date >= CAST('{$data_ini}' as DATE)";
            }elseif ($data_fim) {
                $andwhere .= " and re.start_date <= CAST('{$data_fim}' as DATE)";
            }
            
            if ($title) {
                $andwhere .= " and re.title LIKE '%{$title}%'";
            }
            if ($protocol) {
                $protocolo = str_replace(array('.','/','-'), '', $protocol);
                $andwhere .= " and re.protocol LIKE '%{$protocolo}%'";
            }
            if ($status_campaign) {
                $andwhere .= " and re.status_campaign = {$status_campaign}";
            }
            if ($supplier_id) {
                $andwhere .= " and su.id = {$supplier_id}";
            }
            if ($country_id) {
                $andwhere .= " and co.id = {$country_id}";
            }
            if ($type_risk_id) {
                $andwhere .= " and tr.id = {$type_risk_id}";
            }
            
            if ($supplier_id) {
                $andwhere .= " and su.id = {$supplier_id}";
            }
        }
        
        $dados = array();
                $dql = "select 
                         re.id as recall_id,
                         re.start_date,
                         re.title,
                         re.protocol,
                         re.status_campaign,
                         su.id as supplier_id,
                         su.trade_name,
                         tr.type_risk,
                         co.country,
                         group_concat(DISTINCT pr.product ORDER BY pr.id ASC SEPARATOR ' | ') as product,
                         (select 
                                sum(quantity)
                          from 
                                affecteds
                          where 
                                product_id in ( select 
                                                        id 
                                                from 
                                                        products
                                                where 
                                                        recall_id = re.id )
                         )as total_affected,
                        (select 
                                sum(quantity) 
                          from 
                                total_serviceds
                          where 
                                product_id in (select 
                                                        id 
                                                from 
                                                        products
                                                where 
                                                        recall_id = re.id)
                         )as total_serviced
         from 
                         recalls re
         inner join 
                         suppliers su on re.supplier_id = su.id
         inner join 
                         products pr on pr.recall_id = re.id
         inner join 
                         type_risks tr on tr.id = re.type_risk_id
         inner join 
                         countries co on co.id = pr.country_id
         where
                         re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                ".STATUS_ID_FINALIZADA.")
        {$andwhere}
             
         GROUP BY 
                         re.id
         order by 
                         su.trade_name, re.start_date";

        $query = $this->db->query($dql);
        $result = $query->result();

        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'recall_id'         => $value->recall_id,
                    'start_date'        => $value->start_date,
                    'title'             => $value->title,
                    'protocol'          => $value->protocol,
                    'status_campaign'   => $value->status_campaign,
                    'supplier_id'       => $value->supplier_id,
                    'trade_name'        => $value->trade_name,
                    'type_risk'         => $value->type_risk,
                    'country'           => $value->country,
                    'product'           => $value->product,
                    'total_affected'    => $value->total_affected,
                    'total_serviced'    => $value->total_serviced,
                );
            }
        }

        return $dados;
    }
    
    function sqlGetChartRecall($recall_id){
        if (!$recall_id) {
            return FALSE;
        }
        
        $dql = "select 
                                (select 
                                            sum(af.quantity)
                                from 
                                            affecteds af
                                inner join 
                                            states st2 on af.state_id = st2.id 
                                where 
                                            product_id in (select 
                                                                    id 
                                                           from 
                                                                    products
                                                            where 
                                                                    recall_id = {$recall_id}
                                                           )
                                 and 
                                            st2.id = st.id group by state
                                 ) as quantity_affected,
                                 SUM(ts.quantity) AS quantity_serviced,
                                 st.state,
                                 st.state_name,
                                 re.id as recall_id,
                                 re.title
                 from 
                                 affecteds af
                 inner join
                                 products pr on af.product_id = pr.id
                 inner join 
                                 recalls re on pr.recall_id = re.id
                 inner join 
                                 states st on af.state_id = st.id
                 left join
                                 total_serviceds ts on ts.product_id = pr.id and ts.state_id = st.id
                 where 
                                 re.id = {$recall_id}
                 GROUP BY 
                                 st.state
                 order by 
                                 st.state ASC;";
        
        $query = $this->db->query($dql);
        $result = $query->result();

        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'quantity_affected' => $value->quantity_affected,
                    'quantity_serviced' => $value->quantity_serviced,
                    'state'             => $value->state,
                    'state_name'        => $value->state_name,
                    'recall_id'         => $value->recall_id,
                    'title'             => $value->title,
                );
            }
        }
        return $dados;
    }
    
    /*
     * Sql com os dados do recall (query nativa do MYSQL necessaria para utilizar a funcao 'group_concat'
     */
    private function sqlReportServiced($parametros = array()) {
        
        $andwhere = "";
        if ($parametros) {
            extract($parametros);
            if ($supplier_id) {
                $andwhere .= " and su.id = {$supplier_id}";
            }
        }
        
        $dados = array();
        $dql = "select 
			su.id as supplier_id,
                        su.trade_name,
                        (select 
                               sum(quantity)
                         from 
                               affecteds a 
                         where 
                               product_id in ( 
                                    select 
                                           id 
                                   from 
                                           products
                                   where 
                                           recall_id in (
                                                select 
                                                            id
                                                from 
                                                            recalls 
                                                where 
                                                            supplier_id = su.id
                                                and 
                                                            status_campaign in (".STATUS_ID_PUBLICADA.",
                                                                                ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                                                ".STATUS_ID_FINALIZADA.")
                                            )
                                )
                           )as total_affected,
                           (select 
                               sum(quantity)
                         from 
                               total_serviceds t
                         where 
                               product_id in ( 
                                    select 
                                           id 
                                   from 
                                           products
                                   where 
                                           recall_id in (
                                                select 
                                                            id
                                                from 
                                                            recalls 
                                                where 
                                                            supplier_id = su.id
                                                and 
                                                            status_campaign in (".STATUS_ID_PUBLICADA.",
                                                                                ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                                                ".STATUS_ID_FINALIZADA.") 
                                            )
                                    )
                           )as total_serviced,
                           group_concat(DISTINCT re.title ORDER BY re.title ASC SEPARATOR ' | ') as recall
               from 
                        affecteds af
               inner join
                        products pr on af.product_id = pr.id
               inner join 
                        recalls re on pr.recall_id = re.id
               inner join 
                        states st on af.state_id = st.id
               inner join 
                        suppliers su on re.supplier_id = su.id
               inner join
                        total_serviceds ts on ts.product_id = pr.id and ts.state_id = st.id
               where
                        re.status_campaign in (".STATUS_ID_PUBLICADA.",".STATUS_ID_PUBLICADA_COM_RESSALVA.",".STATUS_ID_FINALIZADA.")
               {$andwhere}
               GROUP BY 
                        su.id
               order by 
                        su.trade_name ASC;";
            
        $query = $this->db->query($dql);
        $result = $query->result();
        
        
        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'supplier_id'       => $value->supplier_id,
                    'trade_name'        => $value->trade_name,
                    'recall'            => $value->recall,
                    'total_affected'    => $value->total_affected,
                    'total_serviced'    => $value->total_serviced,
                );
            }
        }
        return $dados;
    }
    
    
    /*
     * Retorna um array com o total de atendimento por produto
     */
    private function sqlChartServiced($supplier_id) {
        
        $dados = array();
        
        if ($supplier_id) {
            $dql = "select 
                            (select 
                                    sum(quantity)
                              from 
                                    affecteds
                              where 
                                    product_id in   (select 
                                                            id 
                                                    from 
                                                            products
                                                    where 
                                                            recall_id = re.id 
                                                    )
                            )as total_affected,
                            (select 
                                    sum(quantity) 
                              from 
                                    total_serviceds
                              where 
                                    product_id in (select 
                                                            id 
                                                    from 
                                                            products
                                                    where 
                                                            recall_id = re.id
                                                   )
                             )as total_serviced,
                             re.id as recall_id,
                             re.title,
                             su.id as supplier_id,
                             su.trade_name,
                             su.corporate_name
                    from 
                                    affecteds af
                    inner join
                                    products pr on af.product_id = pr.id
                    inner join 
                                    recalls re on pr.recall_id = re.id 
                                    and 
                                    re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                            ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                            ".STATUS_ID_FINALIZADA.")
                    inner join 
                                    suppliers su on su.id = re.supplier_id
                    inner join 
                                    states st on af.state_id = st.id
                    left join
                                    total_serviceds ts on ts.product_id = pr.id and ts.state_id = st.id
                    where 
                                    su.id = {$supplier_id}
                    GROUP BY 
                                    re.id
                    order by 
                                    total_affected ASC";

            $query = $this->db->query($dql);
            $result = $query->result();

            if ($result) {
                foreach ($result as $key => $value) {
                    $dados[$key] = array(
                        'total_affected'    => $value->total_affected,
                        'total_serviced'    => $value->total_serviced,
                        'recall_id'         => $value->recall_id,
                        'title'             => $value->title,
                        'supplier_id'       => $value->supplier_id,
                        'trade_name'        => $value->trade_name,
                        'corporate_name'    => $value->corporate_name,
                    );
                }
            }
        }
        return $dados;
    }
    
    private function sqlReportAffected($parameters = array()) {
        
        $condition = "";
        $dados = array();
        
        if ($parameters) {
            extract($parameters);
            if ($branch_id) {
                $condition .= " and br.id = {$branch_id}";
            }
            if ($type_product_id) {
                $condition .= " and tp.id = {$type_product_id}";
            }
        }
        
        $dql = "select 
                        br.branch,
                        tp.type_product,
                        tp.id as type_product_id,
                        (select 
                               sum(quantity)
                         from 
                               affecteds a 
                         where 
                               product_id in ( 
                                    select 
                                           id 
                                   from 
                                           products
                                   where 
                                           recall_id in (
                                                select 
                                                            id
                                                from 
                                                            recalls 
                                                where 
                                                            status_campaign in (".STATUS_ID_PUBLICADA.",
                                                                                ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                                                ".STATUS_ID_FINALIZADA.")
                                            )
                                 and 
                                 			id = pr.id
                                )
                           )as total_affected,
                           (select 
                               sum(quantity)
                         from 
                               total_serviceds t
                         where 
                               product_id in ( 
                                    select 
                                           id 
                                   from 
                                           products
                                   where 
                                           recall_id in (
                                                select 
                                                            id
                                                from 
                                                            recalls 
                                                where 
                                                            status_campaign in (".STATUS_ID_PUBLICADA.",
                                                                                ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                                                ".STATUS_ID_FINALIZADA.")
                                            )
                                    and 
                                 				id = pr.id
                                    )
                           )as total_serviced,
                           group_concat(DISTINCT pr.product ORDER BY pr.product ASC SEPARATOR ' | ') as product
               from 
                        affecteds af
               inner join
                        products pr on af.product_id = pr.id
               inner join
								type_products tp on pr.type_product_id = tp.id
               inner join
								branches br on tp.branch_id = br.id
               inner join 
                        recalls re on pr.recall_id = re.id
               inner join 
                        states st on af.state_id = st.id
               inner join
                        total_serviceds ts on ts.product_id = pr.id and ts.state_id = st.id
               where
                        re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                            ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                            ".STATUS_ID_FINALIZADA.")
               {$condition}
               GROUP BY 
                        tp.id
               order by 
                        br.branch ASC;";
    
        $query = $this->db->query($dql);
        $result = $query->result();
        
        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'branch'            => $value->branch,
                    'type_product_id'   => $value->type_product_id,
                    'type_product'      => $value->type_product,
                    'total_affected'    => $value->total_affected,
                    'total_serviced'    => $value->total_serviced,
                    'product'           => $value->product,
                );
            }
        }
        return $dados;
    }
    
    
    private function sqlChartAffected($type_product_id) {
        
        $dados = array();
        
        if ($type_product_id) {
        
            $dql = "select 
                            tp.id,
                            tp.type_product,
                            tp.id as type_product_id,
                            br.branch,
                            pr.product,
                            (select 
                                   sum(quantity)
                             from 
                                   affecteds a 
                             where 
                                   product_id in ( 
                                        select 
                                               id 
                                       from 
                                               products
                                       where 
                                               recall_id in (
                                                    select 
                                                                id
                                                    from 
                                                                recalls 
                                                    where 
                                                                status_campaign in (".STATUS_ID_PUBLICADA.",
                                                                                    ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                                                    ".STATUS_ID_FINALIZADA.")
                                                )
                                     and 
                                                            id = pr.id
                                    )
                               )as total_affected,
                               (select 
                                   sum(quantity)
                             from 
                                   total_serviceds t
                             where 
                                   product_id in ( 
                                        select 
                                               id 
                                       from 
                                               products
                                       where 
                                               recall_id in (
                                                    select 
                                                                id
                                                    from 
                                                                recalls 
                                                    where 
                                                                status_campaign in (".STATUS_ID_PUBLICADA.",
                                                                                    ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                                                    ".STATUS_ID_FINALIZADA.") 
                                                )
                                        and 
                                                                    id = pr.id
                                        )
                               )as total_serviced
                   from 
                            affecteds af
                   inner join
                            products pr on af.product_id = pr.id
                   inner join
                                                                    type_products tp on pr.type_product_id = tp.id
                   inner join
                                                                    branches br on tp.branch_id = br.id
                   inner join 
                            recalls re on pr.recall_id = re.id
                   inner join 
                            states st on af.state_id = st.id
                   inner join
                            total_serviceds ts on ts.product_id = pr.id and ts.state_id = st.id
                   where
                            re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                ".STATUS_ID_FINALIZADA.")
                   and 
                            tp.id = {$type_product_id}
                   GROUP BY 
                            tp.id, pr.id
                   order by 
                            br.branch ASC;";

            $query = $this->db->query($dql);
            $result = $query->result();

            if ($result) {
                foreach ($result as $key => $value) {
                    $dados[$key] = array(
                        'branch'            => $value->branch,
                        'type_product'      => $value->type_product,
                        'type_product_id'   => $value->type_product_id,
                        'total_affected'    => $value->total_affected,
                        'total_serviced'    => $value->total_serviced,
                        'product'           => $value->product,
                    );
                }
            }
            return $dados;
        } else {
            return false;
        }
            
    }
    
    private function sqlReportRisk($parameters = array(),$groupRisk = FALSE) {
        
        $condition = "";
        
        if ($parameters) {
            extract($parameters);
            if ($supplier_id) {
                $condition .= " and su.id = {$supplier_id}";
            }
        }
        
        // caso a flag de agrupar esta TRUE, entao agrupa pelo tipo de risco tambem
        $group = ($groupRisk) ? " su.id, tr.id" : " su.id";
        
        $dados = array();
        
        $dql = "select 
                                group_concat(DISTINCT tr.type_risk ORDER BY tr.type_risk ASC SEPARATOR ' | ') as type_risk,
				count(tr.id) as total_risk,
				su.id as supplier_id,
                                su.trade_name,
                                su.corporate_name
                from 
                                type_risks tr
                inner join 
                                recalls re on tr.id = re.type_risk_id
                inner join 
                                suppliers su on re.supplier_id = su.id
                where 
                                re.title is not null
                {$condition}
                group by 
                                {$group}
                order by 
                                su.trade_name ASC;";
    
        $query = $this->db->query($dql);
        $result = $query->result();

        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'type_risk'         => $value->type_risk,
                    'total_risk'        => $value->total_risk,
                    'supplier_id'       => $value->supplier_id,
                    'trade_name'        => $value->trade_name,
                    'corporate_name'    => $value->corporate_name,
                );
            }
        }
        return $dados;
    }
    
}