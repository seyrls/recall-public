<?php

class Administrations extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();

        /**
         * Vefifica se o usuário está logado.
         * Se não estiver, redireciona para a página de login
         */
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }

    /*
     * Tela do dashboard - Administrador do sistema
     */
    function index($supplier_id = NULL) {
        $user = $this->session->userdata('login');
        if ($user['level'] == LEVEL_ADMINISTRADOR) {
            if ($supplier_id) {
                $arrRecallPublish = $this->doctrine->em->getRepository('Entities\Recall')
                                        ->findBy(array(
                                                'statusCampaign' => array(
                                                    STATUS_ID_FINALIZADA,
                                                    STATUS_ID_PUBLICADA,
                                                    STATUS_ID_PUBLICADA_COM_RESSALVA,
                                                    ),
                                                    'supplier' => $supplier_id
                                                )
                                           );
                $arrRecallAndamento = $this->doctrine->em->getRepository('Entities\Recall')
                                            ->findBy(array(
                                                     'statusCampaign' => array(
                                                         STATUS_ID_EM_CADASTRAMENTO,
                                                         STATUS_ID_AGUARDANDO_PUBLICACAO,
                                                         STATUS_ID_PENDENCIA_CADASTRO,
                                                         ),
                                                'supplier' => $supplier_id
                                                     )
                                                );
                $arrSupplierUrl = $this->doctrine->em->getRepository('Entities\Supplier')->find($supplier_id);
                $dados['supplier_id'] = $supplier_id;
                $dados['trade_name'] = $arrSupplierUrl->getTradeName();
            } else {
                $arrRecallPublish = $this->doctrine->em->getRepository('Entities\Recall')
                                        ->findBy(array(
                                                'statusCampaign' => array(
                                                    STATUS_ID_FINALIZADA,
                                                    STATUS_ID_PUBLICADA,
                                                    STATUS_ID_PUBLICADA_COM_RESSALVA,
                                                    )
                                                )
                                           );
                $arrRecallAndamento = $this->doctrine->em->getRepository('Entities\Recall')
                                            ->findBy(array(
                                                     'statusCampaign' => array(
                                                         STATUS_ID_EM_CADASTRAMENTO,
                                                         STATUS_ID_AGUARDANDO_PUBLICACAO,
                                                         STATUS_ID_PENDENCIA_CADASTRO,
                                                         )
                                                     )
                                                );
                $dados['supplier_id'] = NULL;
                $dados['trade_name'] = 'TODOS';
            }
            
            $supplier = new Entities\Supplier;
            $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')->findBy(array(),array('tradeName' => 'ASC'));
            $dados['comboSupplier'] = $supplier->getComboSupplier($arrSupplier, array('' => 'Todos'),FALSE);

            $dados['countRecallPublish'] = count($arrRecallPublish);
            $dados['countRecallAndamento'] = count($arrRecallAndamento);

            // instanciando a model Report contendo os relatorios
            $this->load->model('Report_model');
            
            $totalAffected = $this->Report_model->sqlTotalAffected($supplier_id);
            $totalServiced = $this->Report_model->sqlTotalServiced($supplier_id);
            
            $percent = ($totalAffected > 0) ? (($totalServiced*100)/$totalAffected) : 0;
            $progress = round($percent, 0).'%';
            
            
            // relatorio de produtos atendidos por ano
            $dadosAffected = $this->Report_model->sqlTotalAffectedByYear($supplier_id);
            $series_data_affected[0] = array('Ano','Atendidos','Afetados');

            if ($dadosAffected) {
                foreach ($dadosAffected as $key => $value) {
                    $series_data_affected[($key+1)] = array(
                         $value['ano'],
                        (int) $value['total_serviced'],
                        (int) $value['total_affected'],
                    );
                }   
            } else {
                // caso nao retorn valor para o relatorio
                $series_data_affected[1] = array(
                    date('Y'),0,0
                );
            }
            $dados['series_data_affected'] = json_encode($series_data_affected);
            
            // relatorio de situacao das campanhas de recall
            $series_data_status[0] = array('Situação da Campanha','Quantidade');
            $dadosStatus = $this->Report_model->sqlStatusCampaign($supplier_id);
            if ($dadosStatus) {
                foreach ($dadosStatus as $key => $value) {
                    $series_data_status[($key+1)] = array(
                        $value['status_campaign'],
                        $value['total']
                    );
                }
            }
            
            $dados['series_data_status'] = json_encode($series_data_status);
            
            $dados['totalAffected'] = $totalAffected;
            $dados['totalServiced'] = $totalServiced;
            $dados['indiceServiced'] = $progress;
            $dados['arr_type_risk'] = $this->Report_model->sqlTotalRisk($supplier_id);
            $dados['arr_branch'] = $this->Report_model->sqlTotalBranch($supplier_id);
            $dados['arr_media'] = $this->Report_model->sqlTotalMedia($supplier_id);
            
            $this->load->view('template/header');
            $this->load->view('administrations/dashboard',$dados);
            $this->load->view('template/footer');
        } else {
            redirect('recalls/index');
        }
    }
//    function settings() {        
//        
//        $dados = array(
//            'user'     => 'seyr.souza',
//            'email'     => EMAIL_FROM_SENACON,
//            'password'  => '',
//        );
//        $this->load->view('template/header');
//        $this->load->view('administrations/settings',$dados);
//        $this->load->view('template/footer');
//    }
}