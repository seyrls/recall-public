<?php

class Logins extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('send_email');
        $this->load->library('form_validation');
    }

    function index() {
        if ($this->input->post()) {
            $this->loginAdm();
        }
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('logins/index');
        $this->load->view('portal-padrao/footer');
    }

    /*
     * Busca usuario ADM na base de dados.
     * return array
     */
    private function getUserAdminByEmail($email) {
        
        $result = array();
        if ($email) {
            //acessado banco de dados login
            $db_login = $this->load->database('login', TRUE);
            $sql = "SELECT 
                        u.CodigoUsuario,
			identificador,
			u.NumeroCPF,
			convert(varchar(max), u.Senha, 2) as Senha,
                        case when u.databloqueio is not null 
                        	then 'B' 
        		        when u.datadesativacao is not null 
                                then 'P' 
                                else 'A' 
            		end as suscod2,
                	-- suscod,
                        u.NumeroTentativasLogon,
			u.Nome,
			u.Email
                    FROM 
                        seguranca.usuario as u
                    where 
                        u.Email = '{$email}'";
            $query = $db_login->query($sql);
            $result = $query->result();
        }
        return $result;
    }
    
    /*
     * Login admin
     */
    function loginAdm() {
        
        if ($this->form_validation->run('login_adm')){
            $email = $this->input->post('email');
            $result = $this->getUserAdminByEmail($email);
            
            if ($result) {
                $password = strtoupper(md5($this->input->post('password')));
                // verifica se a senha informada confere com a senha do banco de dados.
                if ($result[0]->Senha == $password) {
                    if ($result[0]->CodigoUsuario == LOGIN_ADMIN_STATUS_BLOQUEADO) {
                        $this->session->set_flashdata('error', ALERTA_ERRO_LOGIN_STATUS_BLOQUEADO);
                        redirect('logins/index');
                    } elseif ($result[0]->CodigoUsuario == LOGIN_ADMIN_STATUS_PENDENTE) {
                        $this->session->set_flashdata('error', ALERTA_ERRO_LOGIN_STATUS_PENDENTE);
                        redirect('logins/index');
                    } else {
                        $arr = array(
                            'id' => $result[0]->CodigoUsuario,
                            'username' => $result[0]->identificador,
                            'level' => LEVEL_ADMINISTRADOR,
                            'email' => $result[0]->Email,
                        );
                        $this->session->set_userdata('login',$arr);
                        redirect(base_url().'administrations');
                    }
                } else {
                    // senha invalida. altera o numero de tentativa
                    $db_login = $this->load->database('login', TRUE);
                    $result[0]->NumeroTentativasLogon = ($result[0]->NumeroTentativasLogon + 1);
                    $sql = "UPDATE 
                                    seguranca.usuario 
                            SET 
                                    NumeroTentativasLogon = {$result[0]->NumeroTentativasLogon}
                            WHERE 
                                    CodigoUsuario = {$result[0]->CodigoUsuario}";
                    $db_login->query($sql);
                    
                    // caso o numero de tentativas exceda o limite, altera o status para bloqueado e altera zera o numero de tentativas.
                    if ($result[0]->NumeroTentativasLogon > LOGIN_ADMIN_LIMITE_DE_TENTATIVAS) {
                        $sql = "UPDATE 
                                    seguranca.usuario 
                            SET 
                                    NumeroTentativasLogon = 0
                                    -- suscod = '".LOGIN_ADMIN_STATUS_BLOQUEADO."' 
                            WHERE 
                                    CodigoUsuario = {$result[0]->CodigoUsuario}";
                        $db_login->query($sql);
                        $this->session->set_flashdata('error', ALERTA_ERRO_LOGIN_STATUS_BLOQUEADO);
                        redirect('logins/index');
                    } else {
                        $this->session->set_flashdata('error', ALERTA_ERRO_LOGIN_SENHA);
                        redirect('logins/index');
                    }
                }
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_USUARIO_NAO_ENCONTRADO);
                redirect('logins/index');   
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('logins/index');
        }
    }

    function forgotPassword(){
        $dados = array();
        
        if ($this->input->post()) {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
            if ($this->form_validation->run() == TRUE){
                $user = $this->doctrine->em->getRepository('Entities\User')
                                    ->findOneBy(array('email' => $this->input->post('email')));
                if ($user) {
                    $this->doctrine->em->getConnection()->beginTransaction();
                    // gerar senha aleatoria
                    $numeros = 0123456789;
                    $letras = "abcdefghijklmnopqrstuvyxwz";
                    $password = str_shuffle($numeros);
                    $password .= str_shuffle($letras);
                    $senha = substr(str_shuffle($password),0,6);
                    
                    $mailCripto = urlencode(base64_encode($user->getEmail()));
                    
                    $message = "Olá, ".$user->getUserName().'.<br/>';
                    $message .= "Sua senha temporária para acessar o sistema de recall é: <b>".$senha."</b><br/><br/>";
                    $message .= "É necessário alterá-la 
                                <a href='".base_url('principal/ativarConta/'.$mailCripto)."'>clicando aqui</a>
                                para acessar o sistema.<br/><br/>";
                    $message .= "Atenciosamente, <br/> SENACON";
                    $mailto  = $user->getEmail();
                    $subject = "Recuperar Senha do Sistema de Recall";
                    
                    try {
                        $user->setPassword($senha);
                        $user->setStatus(STATUS_AGUARDANDO);
                        send_email($message,$mailto,$subject);
                        $this->doctrine->em->persist($user);
                        $this->doctrine->em->flush();
                       $this->doctrine->em->getConnection()->commit();
                       $this->session->set_flashdata('success', MSG_SUCCESS_NEW_PASSWORD);
                       redirect('logins/forgotPassword');
                    } catch (Exception $ex) {
                        $this->doctrine->em->getConnection()->rollback();
                        $this->session->set_flashdata('error', MSG_ERROR);
                    }
                } else {
                    $this->session->set_flashdata('error', 'Ops! Email não encontrado.');
                    redirect('logins/forgotPassword');
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('logins/forgotPassword');
            }
        }
        
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('logins/forgot_password',$dados);
        $this->load->view('portal-padrao/footer');
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
    /*
     * Acessar Conta Fornecedor
     */
    
    function accountSupplier() {
        if ($this->input->post()) {
            $this->loginSupplier();
        }
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('logins/login_supplier');
        $this->load->view('portal-padrao/footer');
    }
    
    /*
     * Login Fornecedor
     */
    function loginSupplier() {
        if ($this->form_validation->run('login')){
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            
            $user = $this->doctrine->em->getRepository('Entities\User')
                        ->findBy(array('email'=> $email, 'password' => $password));
            if ($user) {
                if ($user[0]->getStatus() == STATUS_AGUARDANDO) {
                    $this->session->set_flashdata('error', MSG_ERROR_SENHA_NAO_ATIVADA);
                    redirect('logins/accountSupplier');
                }
                $arr = array(
                    'id' =>$user[0]->getId(),
                    'username' => $user[0]->getUsername(),
                    'level' => $user[0]->getLevel()->getLevel(),
                    'email' => $user[0]->getEmail(),
                );
                $this->session->set_userdata('login',$arr);
                redirect(base_url().'administrations');
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_USUARIO_NAO_ENCONTRADO);
                redirect('logins/accountSupplier');   
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('logins/accountSupplier');
        }
    }
}
