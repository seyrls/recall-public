<?php

class Profiles extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        
        /**
         * Vefifica se o usuário está logado
         * Se não estiver, redireciona para a página de login
         */
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }
    
    function index() {
        $dados['user'] =  $this->session->userdata('login');
        
        $this->load->view('template/header');
        $this->load->view('profiles/index');
        $this->load->view('template/messages');
        $this->load->view('profiles/edit', $dados);
        $this->load->view('template/footer');
    }
    
    function edit() {
        $dados['mensagem'] = NULL;
        
        if ((($this->input->post('password'))) or (($this->input->post('cnpj')))){
            $arr = $this->session->userdata('login');
            
            $user = $this->doctrine->em->getRepository('Entities\User')
                                ->find($arr['id']);
            if (($this->input->post('password'))){
                $user->setPassword($this->input->post('password'));
            }

            if (($this->input->post('cnpj'))){
                $user->setCnpj($this->input->post('cnpj'));
            }
            
            $this->doctrine->em->flush();
            
            $dados = array(
                'id' =>$user->getId(),
                'username' => $user->getUsername(),
                'level' => $user->getLevel()->getLevel(),
                'cnpj' => $user->getCnpj(),
            );
            $this->session->set_userdata('login',$dados);
                
            $dados['mensagem'] = 'Usuário atualizado com sucesso!';
        }else{
            redirect(base_url().'administrations');
        }

        $this->load->view('template/header');
        $this->load->view('profiles/index', $dados);
        $this->load->view('template/messages');
        $this->load->view('template/footer');
    }
}