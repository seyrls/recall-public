<?php

class Sources extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        
        if (!($this->session->userdata('login'))){
            redirect(base_url(). 'logins');
        }
    }
    
    function insert($media_id) {
        if ($this->input->post()) {
            $this->save();
        }
        $dados['dados'] = array();
        $arrMedia = $this->doctrine->em->getRepository('Entities\Media')
                ->findById($media_id);
        $media = new Entities\Media;
        $dados['dados'] = $media->getArrayMedia($arrMedia);
        $dados['dados']['source'] = NULL;
        $dados['comboMedia'] = $media->getComboMedia($arrMedia);
        
        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
        );
        
        $this->load->view('template/header');
        $this->load->view('origins/breadcrumbs',$breadcrumbs);
        $this->load->view('sources/edit', $dados);
        $this->load->view('template/footer');
    }

    function insertPopup($media_id) {
        if ($this->input->post()) {
            $this->save();
        }
        $dados['dados'] = array();
        $arrMedia = $this->doctrine->em->getRepository('Entities\Media')
                ->findById($media_id);
        $media = new Entities\Media;
        $dados['dados'] = $media->getArrayMedia($arrMedia);
        $dados['dados']['source'] = NULL;
        $dados['comboMedia'] = $media->getComboMedia($arrMedia);
        
        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
        );
        
        $this->load->view('template/header_clean');
        $this->load->view('sources/edit', $dados);
        $this->load->view('template/footer_clean');
    }

    private function save() {
        $this->form_validation->set_rules('source', 'Fonte', 'required|max_length[100]');
        if ($this->form_validation->run() == TRUE) {
            $action = $this->uri->segment(2);
            $source = new Entities\Source;
            $media = $this->doctrine->em->getRepository('Entities\Media')
                            ->find($this->input->post('media_id'));
            $source->setSource($this->input->post('source'));
            $source->setMedia($media);
            $source->setStatus($this->input->post('status'));
            $this->doctrine->em->persist($source);
            $this->doctrine->em->flush();
            if (!is_null($source->getId())) {
                $this->session->set_flashdata('success', MSG_SUCCESS);
                if ($action == 'insert'){
                    redirect('medias/view/'.$this->input->post('media_id'));
                } else {
                    // cadastro popup
                    echo "<script> 
                            var teste = '<option value=\'".$source->getId()."\'>".$source->getSource()."</option>';
                            var select = document.getElementById('source');
                            window.opener.location.reload(true);
                            window.close();
                        </script>";
                }
            } else {
                $this->session->set_flashdata('error', MSG_ERROR);
            }
        }
    }

    function edit($id) {
        if (!$id) {
            redirect('sources/view'.$id);
        }
        if ($this->input->post()) {
            $this->update();
        }
        $arrSource = $this->doctrine->em->getRepository('Entities\Source')->findById($id);
        if (!$arrSource) {
            redirect('sources/view'.$id);
        }
        $origin_id  = $arrSource[0]->getMedia()->getOrigin()->getId();
        $origin     = ($arrSource[0]->getMedia()->getOrigin()->getOrigin());
        $media_id   = $arrSource[0]->getMedia()->getId();
        $name_media = ($arrSource[0]->getMedia()->getMedia());
        
        $dados['dados'] = array();
        $source = new Entities\Source;
        $dados['dados'] = $source->getArraySource($arrSource);
                
        $media = new Entities\Media;
        $arrMedia  = $this->doctrine->em->getRepository('Entities\Media')
                                        ->findBy(array('origin' => $origin_id), array('media' => 'ASC'));
        $dados['comboMedia'] = $media->getComboMedia($arrMedia);
        
        $breadcrumbs['caminho'] = array(
            'Meios de veiculação' => 'origins/index',
            $origin => 'origins/view/'.$origin_id,
            $name_media => 'medias/view/'.$media_id,
            'Editar '.$dados['dados']['source'] => 'sources/edit/'.$dados['dados']['source_id'],
        );
        
        $this->load->view('template/header');
        $this->load->view('origins/breadcrumbs',$breadcrumbs);
        $this->load->view('sources/edit', $dados);
        $this->load->view('template/footer');
    }
    
    private function update() {
        $this->form_validation->set_rules('source', 'Fonte', 'required|max_length[100]');
        if ($this->form_validation->run() == TRUE) {
            $source = $this->doctrine->em->getRepository('Entities\Source')
                            ->find($this->input->post('source_id'));
            $media = $this->doctrine->em->getRepository('Entities\Media')
                            ->find($this->input->post('media_id'));
            if (($source) && ($media)) {
                try {
                    $source->setSource($this->input->post('source'));
                    $source->setStatus($this->input->post('status'));
                    $source->setMedia($media);
                    $this->doctrine->em->persist($source);
                    $this->doctrine->em->flush();
                    $this->session->set_flashdata('success', MSG_SUCCESS_ALTERADO);
                } catch (Exception $ex) {
                    $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
                }
            } else {
                $this->session->set_flashdata('error', MSG_ERROR_ALTERADO);
            }
            redirect('medias/view/'.$this->input->post('media_id'));
        }
    }
    
    function remove($id){
        $source = $this->doctrine->em
                ->getRepository('Entities\Source')
                ->find($id);
        
        $media_id = $source->getMedia()->getId();
        
        $mediaPlans = $this->doctrine->em
                ->getRepository('Entities\MediaPlan')
                ->findBy(array('source' => $id));
        if ((!$mediaPlans) && ($source)) {
                $this->doctrine->em->remove($source);
                $this->doctrine->em->flush();
                $this->session->set_flashdata('success', MSG_SUCCESS_REMOVE);
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_REMOVE_RECALL_EXISTS);
        }
        redirect('medias/view/'.$media_id);
    }
}
