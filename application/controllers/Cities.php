<?php

class Cities extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
	
    function combocities($state_id = null) {
        if ($state_id) {
            $query = $this->doctrine->em->getRepository('Entities\City')
                                ->findBy(
                                   array('state'=> $state_id), 
                                   array('city' => 'ASC')
                                 );

            $html = '<select name="city_id" class="form-control">
                        <option value="" selected="selected">-- Escolha o municipio --</option>';
            foreach ($query as $c){
                $html .= '<option value="'.$c->getId().'">'.($c->getCity()).'</option>';
            }

            $html .= "</select>";
        } else {
            $html = '<select name="city_id" class="form-control">
                        <option value="" selected="selected">-- Escolha o municipio --</option>';
        }
        
        echo $html;
    }
    
    function loadCombo(){
        if ($this->input->post('state_id')) {
            $city = $this->doctrine->em->getRepository('Entities\City')
                                ->findBy(
                                   array('state'=> $this->input->post('state_id')), 
                                   array('city' => 'ASC')
                                 );
            if ($city) {
                $dados = array();
                foreach($city as $value){
                    $dados[$value->getId()] = ($value->getCity());
                }
            }
            echo json_encode($dados);
        } else {
            echo json_encode(array('msg' => 'err'));
        }
        die;
    }
}