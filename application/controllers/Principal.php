<?php

class Principal extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }
	
    /*
     * Pagina inicial contendo as campanhas publicadas
     */
    function index($offset=0) {
        $limit = 4; // quantidade de campanhas por pagina
        $arrRecall = $this->listRecall($offset,$limit);
        $data = array();
        
        $data['recall']             = $arrRecall['search'];
        $data['num_results']        = $arrRecall['num_rows'];
        $supplier                   = new Entities\Supplier;
        $branch                     = new Entities\Branch;
        $arrSupplier                = $this->doctrine->em->getRepository('Entities\Supplier')->findAll();
        $arrBranch                  = $this->doctrine->em->getRepository('Entities\Branch')->findBy(array('status' => TRUE),array('branch' => 'ASC'));
        $data['comboSupplier']      = $supplier->getComboSupplier($arrSupplier);
        $data['comboBranch']        = $branch->getComboBranch($arrBranch);
        
        if ($this->input->post('branch_id')) {
            $branch_id = $this->input->post('branch_id');
            $arrTypeProduct = $this->doctrine->em->getRepository('Entities\TypeProduct')
                              ->findBy(array('branch' => $branch_id),array('typeProduct' => 'ASC'));
            $type_product   = new Entities\TypeProduct;
            $data['type_product']  = $type_product->getComboTypeProduct($arrTypeProduct);
        } else {
            $data['type_product'] = array('' => 'Todos');
        }
        
        // paginacao
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url().'principal/index/';
        $config['total_rows'] = $data['num_results'];
        $config['per_page'] = $limit;
        $config['url_segment'] = 3;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('portal-padrao/index', $data);
        $this->load->view('portal-padrao/footer');
    }
    
    /*
     *  query para retornar as campanhas de recall publicadas
     */
    private function listRecall($offset, $limit) {
        
        $andWhere = "";
        
        if ($this->input->post()) {
            $supplier_id        = $this->input->post('supplier_id');
            $type_product_id    = $this->input->post('type_product_id');
            $branch_id          = $this->input->post('branch_id');
            $start_date_ini     = trim($this->input->post('start_date_ini'));
            $start_date_fim     = $this->input->post('start_date_fim');
            $search             = trim($this->input->post('search'));
            
            if ($supplier_id) {
                $andWhere .= " and r.supplier = {$supplier_id}";
            }
            if ($type_product_id) {
                $andWhere .= " and p.type_product = {$type_product_id}";
            }
            if ($branch_id) {
                $andWhere .= " and tp.branch = {$branch_id}";
            }
            if ($start_date_ini) {
                $start_date_ini = implode("-",array_reverse(explode("/",$start_date_ini)));
                $andWhere .= " and r.startDate >= '{$start_date_ini}'";
            }
            if ($start_date_fim) {
                $start_date_fim = implode("-",array_reverse(explode("/",$start_date_fim)));
                $andWhere .= " and r.startDate <= '".$start_date_fim."'";
            }
            if ($search) {
                $andWhere .= " and r.title like '%".$search."%'";
                $andWhere .= " or p.product like '%".$search."%'";
                $andWhere .= " or m.corporateName like '%".$search."%'";
            }   
        }
        
        $dql = "
            SELECT DISTINCT (r.id),
                r.title, 
                r.description, 
                r.dateInsert, 
                i.path,
                r.statusCampaign
            FROM 
                    Entities\Recall r
            INNER JOIN 
                    Entities\Product p WITH r.id = p.recall
            INNER JOIN
                    Entities\TypeProduct tp WITH tp.id = p.type_product
            INNER JOIN
                    Entities\Manufacturer m WITH m.id = p.manufacturer
            LEFT JOIN 
                    Entities\Image i WITH p.id = i.product
            WHERE 
                    r.statusCampaign in (".STATUS_ID_PUBLICADA.","
                    .STATUS_ID_PUBLICADA_COM_RESSALVA.",".STATUS_ID_FINALIZADA.")
            {$andWhere}
            GROUP BY 
                    r.id
            ORDER BY 
                    r.dateInsert DESC 
            ";
            
        $sql = $this->doctrine->em->createQuery($dql);
        
        $num_results = count($sql->getResult());
        $query = $sql->setFirstResult($offset)->setMaxResults($limit);
        $recall = array();
         
        foreach ($query->getResult() as $q){
            $recall[] = array(
                'id' => $q['id'],
                'title' => utf8_encode($q['title']),
                'description' => utf8_encode($q['description']),
                'date_insert' => $q['dateInsert'],
                'path' =>$q['path'],
                'status' => $q['statusCampaign']
            );
        }
        $result['search'] = $recall;
        $result['num_rows'] = $num_results;
        return $result;
    }
    
    /*
     * Detalhe da campanha de recall
     */
    function detailRecall($id) {
        $this->load->helper('status');
        $dados  = array();
        $recall = $this->doctrine->em->getRepository('Entities\Recall')->find($id);
        if (!$recall) {
            redirect('principal/index');
        }
        $arrProduct = $this->doctrine->em->getRepository('Entities\Product')->findBy(array('recall' => $id));
        $product = new Entities\Product;
        
        $dados['dados']['campaign']     = $recall->getArrayCampaign($recall);
        $dados['dados']['info']         = $recall->getArrayInfoRecall($recall);
        $dados['dados']['location']     = $recall->getArrayLocationRecall($recall);
        $dados['dados']['notice_risk']  = $recall->getNoticeRisk();
        $dados['dados']['reasonReservation']  = ($recall->getReasonReservation()) ? $recall->getReasonReservation() : NULL;
        $dados['dados']['product']      = $product->getArrayAllProduct($arrProduct);
        
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('portal-padrao/recall', $dados);
        $this->load->view('portal-padrao/footer');
    }
    
    /*
     * Adicionar newsletter
     */
    function addSubscriber(){
        $supplier                   = new Entities\Supplier;
        $branch                     = new Entities\Branch;
        $arrSupplier                = $this->doctrine->em->getRepository('Entities\Supplier')->findAll();
        $arrBranch                  = $this->doctrine->em->getRepository('Entities\Branch')->findBy(array('status' => TRUE),array('branch' => 'ASC'));
        $data['comboSupplier']      = $supplier->getComboSupplier($arrSupplier);
        $data['comboBranch']        = $branch->getComboBranch($arrBranch);
        $data['type_product']       = array('' => 'Selecione');
        
        if ($this->input->post()) {
            if ($this->form_validation->run('subscriber')){
                $email = $this->input->post('email');
                $this->doctrine->em->getConnection()->beginTransaction();
                try {
                    $this->saveSubscriber();
                    $this->session->set_flashdata('success', MSG_SUCCESS);
                    $this->doctrine->em->getConnection()->commit();
                    $this->session->set_userdata('subscriber_email',$email);
                    redirect('principal/viewSubscriber');
                } catch (Exception $ex) {
                    $this->session->set_flashdata('error', validation_errors());
                    $this->doctrine->em->getConnection()->rollback();
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
            }
        } else {
            $this->session->unset_userdata('subscriber_email');
        }
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('portal-padrao/insert_subscriber',$data);
        $this->load->view('portal-padrao/footer');
    }
    
    /*
     * Salvar newsletter
     */
    private function saveSubscriber(){
        $postBranch = $this->input->post('branch_id');
        $postTypeProduct = $this->input->post('type_product_id');
        
        # tira os valores iguais do array tipo_produtos e depois ordena numericamente a chave do array
        $arrNewBranch = array();
        foreach ($postBranch as $key => $value) {
            if (array_key_exists($key, $postTypeProduct)) {
                $arrNewBranch[$key] = $value;
            }
        }
        $arrBranch = array_values($arrNewBranch);
        $arrTypeProduct = array_values($postTypeProduct);
        $total = count($arrTypeProduct);
        
        if ($total > 0) {
            $subscriber = $this->doctrine->em->getRepository('Entities\Subscriber')
                               ->findOneBy(array('email' => $this->input->post('email')));
            if (!$subscriber) {
                $subscriber = new Entities\Subscriber();
                $subscriber->setName($this->input->post('name'));
                $subscriber->setEmail($this->input->post('email'));
                $subscriber->setStatus(STATUS_ATIVO);
                $this->doctrine->em->persist($subscriber);
                $this->doctrine->em->flush();
            }
            if (in_array(0,$arrBranch)) {
                $option = new Entities\Option();
                $option->setFlagAll(FLAG_ALL_ATIVO);
                $option->setStatus(STATUS_ATIVO);
                $option->setSubscriber($subscriber);
                if ($subscriber->getId()) {
                    $arrCheck = $this->doctrine->em->getRepository('Entities\Option')
                                ->findBy(array(
                                                 'branch' => null, 
                                                 'type_product' => null,
                                                 'subscriber' => $subscriber->getId()
                                              )
                                        );
                    if (!$arrCheck) {
                        $this->doctrine->em->persist($option);
                        $this->doctrine->em->flush();
                    }
                } else {
                    $this->doctrine->em->persist($option);
                    $this->doctrine->em->flush();
                }
            } else {
                for ($i=0; $i < $total; $i++) {
                    $option = new Entities\Option();
                    $branch = $this->doctrine->em->getRepository('Entities\Branch')
                                   ->find($arrBranch[$i]);
                    $typeProduct = $this->doctrine->em->getRepository('Entities\TypeProduct')
                               ->find($arrTypeProduct[$i]);
                    if ($subscriber->getId()) {
                        $arrCheck = $this->doctrine->em->getRepository('Entities\Option')
                                    ->findBy(array(
                                                     'branch' => ($arrBranch[$i] == 0) ? NULL : $arrBranch[$i], 
                                                     'type_product' => ($arrTypeProduct[$i] == 0) ? NULL : $arrTypeProduct[$i],
                                                     'subscriber' => $subscriber->getId()
                                                  )
                                            );
                        if ($arrCheck) {
                            $option->setStatus(STATUS_ATIVO);
                        } else {
                            $option->setSubscriber($subscriber);
                            $option->setBranch($branch);
                            $option->setTypeProduct($typeProduct);
                            $option->setFlagAll(FLAG_ALL_INATIVO);
                            $option->setStatus(STATUS_ATIVO);
                            $this->doctrine->em->persist($option);
                            $this->doctrine->em->flush();
                        }
                    } else {
                        $this->doctrine->em->persist($option);
                        $this->doctrine->em->flush();
                    }
                }
            }
        }
    }
    
    /*
     * Visualizar newsletter cadastradas
     */
    function viewSubscriber(){
        $dados = array();
        $email = $this->session->userdata('subscriber_email');
        
        if ($email) {
            $subscriber = $this->doctrine->em->getRepository('Entities\Subscriber')
                                    ->findOneBy(array('email' => $email));
            if ($subscriber) {
                $option = new \Entities\Option;
                $arrOptions = $this->doctrine->em->getRepository('Entities\Option')
                                    ->findBy(array('subscriber' => $subscriber->getId()),array('id' => 'DESC'));
                if ($arrOptions) {
                    $dados['options'] = $option->getArrayAllOptions($arrOptions);    
                }
            }
        }
        
        if ($this->input->post()) {
            $this->session->unset_userdata('subscriber_email');
            $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|trim');
            if ($this->form_validation->run() == TRUE){
                $subscriber = $this->doctrine->em->getRepository('Entities\Subscriber')
                                    ->findOneBy(array('email' => $this->input->post('email')));
                if ($subscriber) {
                    $option = new \Entities\Option;
                    $arrOptions = $this->doctrine->em->getRepository('Entities\Option')
                                        ->findBy(array('subscriber' => $subscriber->getId()),array('id' => 'DESC'));
                    if ($arrOptions) {
                        $dados['options']   = $option->getArrayAllOptions($arrOptions);    
                    } else {
                        $this->session->set_flashdata('error', 'Ops! Não há solicitações cadastradas para o e-mail: <b>'.$this->input->post('email').'</b>');
                        redirect('principal/viewSubscriber');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Ops! Não há solicitações cadastradas para o e-mail: <b>'.$this->input->post('email').'</b>');
                    redirect('principal/viewSubscriber');
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('principal/viewSubscriber');
            }
        }
        
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('portal-padrao/view_subscriber',$dados);
        $this->load->view('portal-padrao/footer');
    }
    
    /*
     * Remover solicitacoes de newsletter
     */
    function removeItensNewsletter() {
        if ($this->input->post('removeNewsletter')) {
            try {
                $this->doctrine->em->getConnection()->beginTransaction();
                    foreach ($this->input->post('removeNewsletter') as $value) {
                        $option = $this->doctrine->em->getRepository('Entities\Option')
                                       ->find($value);
                        $this->doctrine->em->remove($option);
                        $this->doctrine->em->flush();
                    }
                
                $subscriber_id = $option->getSubscriber()->getId();
                $subscriber = $this->doctrine->em->getRepository('Entities\Subscriber')
                                   ->find($subscriber_id);
                
                $this->doctrine->em->remove($subscriber);
                $this->doctrine->em->flush();
                
                $this->session->set_flashdata('success', MSG_SUCCESS);
                $this->doctrine->em->getConnection()->commit();
            } catch (Exception $ex) {
                $this->session->set_flashdata('error', MSG_ERROR_REMOVE);
                $this->doctrine->em->getConnection()->rollback();
            }
        }
        redirect('principal/viewSubscriber');
    }
    
    /*
     * Ativar conta de login
     */
    function ativarConta() {
        $emailCripto = $this->uri->segment(3);
        if ($emailCripto) {
            $decoded = base64_decode(urldecode($emailCripto));
            $user = $this->doctrine->em->getRepository('Entities\User')
                                   ->findBy(array('email' => $decoded));
            if (!$user) {
                $this->session->set_flashdata('error', MSG_ERROR_USUARIO_NAO_ENCONTRADO);
                redirect('principal/index');
            }
        } else {
            $this->session->set_flashdata('error', MSG_ERROR_USUARIO_NAO_ENCONTRADO);
            redirect('principal/index');
        }
        
        if ($this->input->post()) {
            if ($this->form_validation->run('alter_password')){
                $senhaAtual = $user[0]->getPassword();
                $senhaForm = md5($this->input->post('password'));
                if ($senhaAtual == $senhaForm) {
                    $user[0]->setPassword($this->input->post('password_new'));
                    $user[0]->setStatus(STATUS_ATIVO);
                    $this->doctrine->em->persist($user[0]);
                    $this->doctrine->em->flush();
                    $this->session->set_flashdata('success', MSG_SUCCESS_SENHA_ALTERADA);
                    
                    $arr = array(
                        'id' =>$user[0]->getId(),
                        'username' => $user[0]->getUsername(),
                        'level' => $user[0]->getLevel()->getLevel(),
                        'email' => $user[0]->getEmail(),
                    );
                    $this->session->set_userdata('login',$arr);
                    redirect(base_url().'administrations');
                } else {
                    $this->session->set_flashdata('error', MSG_ERROR_USUARIO_SENHA_NAO_CONFERE);
                    redirect('principal/ativarConta/'.$emailCripto);
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('principal/ativarConta/'.$emailCripto);
            }
        }
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('portal-padrao/ativar_conta');
        $this->load->view('portal-padrao/footer');
    }
    
    /*
     * Visualizar dashboard
     */
    function report($supplier_id = NULL) {
        
        // instanciar banco de dados
        $this->load->database();
        
        if ($supplier_id) {
            $arrRecallPublish = $this->doctrine->em->getRepository('Entities\Recall')
                                    ->findBy(array(
                                            'statusCampaign' => array(
                                                STATUS_ID_FINALIZADA,
                                                STATUS_ID_PUBLICADA,
                                                STATUS_ID_PUBLICADA_COM_RESSALVA,
                                                ),
                                                'supplier' => $supplier_id
                                            )
                                       );
            $arrRecallAndamento = $this->doctrine->em->getRepository('Entities\Recall')
                                        ->findBy(array(
                                                 'statusCampaign' => array(
                                                     STATUS_ID_EM_CADASTRAMENTO,
                                                     STATUS_ID_AGUARDANDO_PUBLICACAO,
                                                     STATUS_ID_PENDENCIA_CADASTRO,
                                                     ),
                                            'supplier' => $supplier_id
                                                 )
                                            );
            $arrSupplierUrl = $this->doctrine->em->getRepository('Entities\Supplier')->find($supplier_id);
            $dados['supplier_id'] = $supplier_id;
            $dados['trade_name'] = $arrSupplierUrl->getTradeName();
        } else {
            $arrRecallPublish = $this->doctrine->em->getRepository('Entities\Recall')
                                    ->findBy(array(
                                            'statusCampaign' => array(
                                                STATUS_ID_FINALIZADA,
                                                STATUS_ID_PUBLICADA,
                                                STATUS_ID_PUBLICADA_COM_RESSALVA,
                                                )
                                            )
                                       );
            $arrRecallAndamento = $this->doctrine->em->getRepository('Entities\Recall')
                                        ->findBy(array(
                                                 'statusCampaign' => array(
                                                     STATUS_ID_EM_CADASTRAMENTO,
                                                     STATUS_ID_AGUARDANDO_PUBLICACAO,
                                                     STATUS_ID_PENDENCIA_CADASTRO,
                                                     )
                                                 )
                                            );
            $dados['supplier_id'] = NULL;
            $dados['trade_name'] = 'TODOS';
        }

        $supplier = new Entities\Supplier;
        $arrSupplier = $this->doctrine->em->getRepository('Entities\Supplier')->findAll();
        $dados['comboSupplier'] = $supplier->getComboSupplier($arrSupplier, array('' => 'Todos'),FALSE);

        $dados['countRecallPublish'] = count($arrRecallPublish);
        $dados['countRecallAndamento'] = count($arrRecallAndamento);

        // instanciando a model Report
        $this->load->model('Report_model');

        $totalAffected = $this->Report_model->sqlTotalAffected();
        $totalServiced = $this->Report_model->sqlTotalServiced($supplier_id);

        $percent = ($totalAffected > 0) ? (($totalServiced*100)/$totalAffected) : 0;
        $progress = round($percent, 0).'%';


        $dados['totalAffected'] = $totalAffected;
        $dados['totalServiced'] = $totalServiced;
        $dados['indiceServiced'] = $progress;
        $dados['arr_type_risk'] = $this->Report_model->sqlTotalRisk($supplier_id);
        $dados['arr_branch'] = $this->Report_model->sqlTotalBranch($supplier_id);
        $dados['arr_media'] = $this->Report_model->sqlTotalMedia($supplier_id);
        
        
        
        // grafico produtos atendidos por ano
        $dadosAffected = $this->Report_model->sqlTotalAffectedByYear();
        $series_data_affected[0] = array('Ano','Atendidos','Afetados');
        
        if ($dadosAffected) {
            foreach ($dadosAffected as $key => $value) {
                $series_data_affected[($key+1)] = array(
                     $value['ano'],
                    (int) $value['total_serviced'],
                    (int) $value['total_affected'],
                );
            }   
        }
        $dados['series_data_affected'] = json_encode($series_data_affected);
        
        // grafico risco
        $series_data_risk[0] = array('Tipo de risco','Quantidade por campanha');
        $dadosRisk = $this->Report_model->sqlTotalRisk();
        if ($dadosRisk) {
            foreach ($dadosRisk as $key => $value) {
                $series_data_risk[($key+1)] = array(
                    $value['type_risk'],
                    $value['total_risk']+0
                );
            }
        }
        $dados['series_data_risk'] = json_encode($series_data_risk);
        
        // grafico setor atividade
        $series_data_branch[0] = array('Setor de Atividade','Quantidade por campanha');
        $dadosBranch = $this->Report_model->sqlTotalBranch();
        
        if ($dadosBranch) {
            foreach ($dadosBranch as $key => $value) {
                $series_data_branch[($key+1)] = array(
                    $value['branch'],
                    $value['total_branch']+0
                );
            }
        }
        
        $dados['series_data_branch'] = json_encode($series_data_branch);
        
        
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('portal-padrao/report',$dados);
        $this->load->view('portal-padrao/footer');
    }
    
    /*
     * Solicitacao de cadastro de fornecedor
     */
    function requestSupplier() {
        
        if ($this->input->post()) {
            $this->load->database(); // necessario para utilizar a funcao is_unique na validacao
            if ($this->form_validation->run('supplier_request')){
                $this->doctrine->em->getConnection()->beginTransaction();
                try {
                    $this->saveSupplier();
                    $this->session->set_flashdata('success', MSG_SUCCESS);
                    $this->doctrine->em->getConnection()->commit();
                    redirect('principal/requestSupplier');
                } catch (Exception $ex) {
                    $this->session->set_flashdata('error', validation_errors());
                    $this->doctrine->em->getConnection()->rollback();
                }
            }
        }
        
        $branch = new Entities\Branch;
        $arrBranch = $this->doctrine->em->getRepository('Entities\Branch')
                                        ->findBy(array('status' => TRUE));
        $dados['comboBranch'] = $branch->getComboBranch($arrBranch);
        
        $state = new Entities\State;
        $arrState = $this->doctrine->em->getRepository('Entities\State')
                                    ->findBy(array(),array('state' =>'ASC'));
        $dados['comboState'] = $state->getComboState($arrState);
        
        $statePost = ($this->input->post('state_id')) ? $this->input->post('state_id') : NULL;
        if ($statePost) {
            $city = new Entities\City;
            $arrCity = $this->doctrine->em->getRepository('Entities\City')
                                    ->findBy(array('state' => $statePost),array('city' =>'ASC'));
            $dados['comboCity'] = $city->getComboCity($arrCity);
        } else {
            $dados['comboCity'] = array('' => 'selecione');
        }

        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('portal-padrao/request_supplier', $dados);
        $this->load->view('portal-padrao/footer');
    }
    
    /*
     * Salvar solicitacao de cadastro do fornecedor
     */
    private function saveSupplier() {
        $this->load->helper('send_email');
        
        // gerar senha aleatoria
        $numeros = 0123456789;
        $letras = "abcdefghijklmnopqrstuvyxwz";
        $password = str_shuffle($numeros);
        $password .= str_shuffle($letras);
        $senha = substr(str_shuffle($password),0,6);
        
        $user = new Entities\User;
        
        // salva os dados do usuario
        $user->setPassword($senha);
        $user->setStatus(STATUS_SOLICITADO);
        $user->setUsername($this->input->post('username'));
        $user->setEmail($this->input->post('email'));
        $level = $this->doctrine->em->getRepository('Entities\Level')
                ->find(LEVEL_FORNECEDOR);
        $user->setLevel($level);
        $this->doctrine->em->persist($user);
        $this->doctrine->em->flush();

        if ($user->getId()) {
            // salva os dados do fornecedor
            $supplier = new Entities\Supplier;
            $branch = $this->doctrine->em->getRepository('Entities\Branch')->find($this->input->post('branch_id'));
            $city = $this->doctrine->em->getRepository('Entities\City')->find($this->input->post('city_id'));
            $user = $this->doctrine->em->getRepository('Entities\User')->find($user->getId());

            $cnpj = str_replace(array('.','/','-'), '', $this->input->post('cnpj'));
            $commercial_phone = str_replace(array('(',')','-',' '), '', $this->input->post('commercial_phone'));
            $fax_phone = str_replace(array('(',')','-',' '), '', $this->input->post('fax_phone'));
            $zip = str_replace('-', '', $this->input->post('zip'));

            $supplier->setCnpj($cnpj);
            $supplier->setCorporateName($this->input->post('corporate_name'));
            $supplier->setTradeName($this->input->post('trade_name'));
            $supplier->setStateRegistration($this->input->post('state_registration'));
            $supplier->setEmailCopy($this->input->post('email_copy'));
            $supplier->setAddress($this->input->post('address'));
            $supplier->setComplement($this->input->post('complement'));
            $supplier->setDistrict($this->input->post('district'));
            $supplier->setZip($zip);
            $supplier->setCommercialPhone($commercial_phone);
            $supplier->setFaxPhone($fax_phone);
            $supplier->setCity($city);
            $supplier->setBranch($branch);
            $supplier->setUser($user);
            $this->doctrine->em->persist($supplier);
            $this->doctrine->em->flush();
            
            $message = "Olá, ".$user->getUserName().'.<br/>';
            $message .= "Recebemos sua solicitação de cadastro de fornecedor para o SISTEMA NACIONAL DE ALERTAS DE RECALL. <br/>";
            $message .= "Após analisarmos seus dados, entraremos em contato com você. <br/><br/>";
            $message .= "Atenciosamente, <br/> SENACON";
            $mailto  = $user->getEmail();
            $subject = "Solicitação de cadastro de fornecedor Sistema de Recall";

            // envio de email com os parametros acima
            send_email($message,$mailto,$subject);
        
        } else {
            return false;
        }
    }
    /*
     * Dados abertos com as campanhas de recall
     */
    public function openData() {
        
        $this->load->model('Report_model');
        $yearCampaign = $this->Report_model->sqlTotalRecallByYear();
        $dados = array('year' => $yearCampaign);
        
        $this->load->view('portal-padrao/header');
        $this->load->view('portal-padrao/menu');
        $this->load->view('portal-padrao/open_data',$dados);
        $this->load->view('portal-padrao/footer');
    }
}