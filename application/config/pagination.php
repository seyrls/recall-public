<?php 

$config['per_page'] = 3;
$config['uri_segment'] = 3;
$config['num_links'] = 2;
$config['page_query_string'] = FALSE;
//$config['display_pages'] = FALSE; 
//$config['query_string_segment'] = 'page';
$config['full_tag_open'] = '<div class="pagination pagination-small pagination-centered"><ul>';
$config['full_tag_close'] = '</ul></div>';

$config['first_link'] = '&laquo; Primeira';
$config['first_tag_open'] = '<li class="prev page">';
$config['first_tag_close'] = '</li>';

$config['last_link'] = 'Última &raquo;';
$config['last_tag_open'] = '<li class="next page">';
$config['last_tag_close'] = '</li>';

$config['next_link'] = 'Próxima &rarr;';
$config['next_tag_open'] = '<li class="next page">';
$config['next_tag_close'] = '</li>';

$config['prev_link'] = '&larr; Anterior';
$config['prev_tag_open'] = '<li class="prev page">';
$config['prev_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="active"><a href="">';
$config['cur_tag_close'] = '</a></li>';

$config['num_tag_open'] = '<li class="page">';
$config['num_tag_close'] = '</li>';

$config['anchor_class'] = 'follow_link';