<?php
/*
 * Validacoes de todos os formularios 
 */
$config = array(
            'campaign' => array(
                array(
                    'field'   => 'supplier_id', 
                    'label'   => 'Fornecedor', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'title', 
                    'label'   => 'Título', 
                    'rules'   => 'required|trim|max_length[300]',
                ),
                array(
                    'field'   => 'protocol', 
                    'label'   => 'Nº do protocolo', 
                    'rules'   => 'min_length[10]|trim|max_length[45]'
                ),
                array(
                    'field'   => 'description', 
                    'label'   => 'Descrição', 
                    'rules'   => 'required|max_length[400]|trim'
                ),
                array(
                    'field'   => 'corrective_measure', 
                    'label'   => 'Medida de correção', 
                    'rules'   => 'required|max_length[300]'
                ),
                array(
                    'field'   => 'reason_accident', 
                    'label'   => 'Descrição do acidente', 
                    'rules'   => 'max_length[500]'
                )
          ),
            'info' => array(
                array(
                    'field'   => 'faulty_part',
                    'label'   => 'Componente', 
                    'rules'   => 'required|trim|max_length[200]',
                ),
                array(
                    'field'   => 'date_faulty',
                    'label'   => 'Data da constatação', 
                    'rules'   => 'required|trim|check_date_valid',
                ),
                array(
                    'field'   => 'faulty',
                    'label'   => 'Defeito',
                    'rules'   => 'required|trim|max_length[1000]',
                ),
                array(
                    'field'   => 'risk_faulty', 
                    'label'   => 'Descrição do risco', 
                    'rules'   => 'required|trim|max_length[1000]',
                ),
                array(
                    'field'   => 'type_risk_id', 
                    'label'   => 'Tipo de risco', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'faulty_describe', 
                    'label'   => 'Implicações do risco', 
                    'rules'   => 'required|max_length[1000]',
                ),
          ),
            'location' => array(
                array(
                    'field'   => 'customer_service', 
                    'label'   => 'Local de atendimento', 
                    'rules'   => 'required|trim|max_length[200]',
                ),
                array(
                    'field'   => 'address_service', 
                    'label'   => 'Endereço', 
                    'rules'   => 'max_length[200]'
                ),
                array(
                    'field'   => 'site', 
                    'label'   => 'Sitio eletrônico', 
                    'rules'   => 'max_length[150]|check_site_valid'
                ),
                array(
                    'field'   => 'phone_service', 
                    'label'   => 'Telefone', 
                    'rules'   => 'required|max_length[20]',
                ),
                array(
                    'field'   => 'notice_service', 
                    'label'   => 'Observação', 
                    'rules'   => 'max_length[500]'
                ),
                array(
                    'field'   => 'email', 
                    'label'   => 'Correio eletrônico', 
                    'rules'   => 'valid_email|trim|max_length[150]'
                ),
          ),
            'media' => array(
                array(
                    'field'   => 'origin[]', 
                    'label'   => 'Meio de veiculação',  
                    'rules'   => 'required|trim',
                ),
                array(
                    'field'   => 'media[]', 
                    'label'   => 'Veículo de mídia', 
                    'rules'   => 'required|trim',
                ),
                array(
                    'field'   => 'source[]', 
                    'label'   => 'Fonte', 
                    'rules'   => 'required|trim',
                ),
                array(
                    'field'   => 'date_insertion[]', 
                    'label'   => 'Data de veiculação', 
                    'rules'   => 'required|trim|check_date_valid',
                ),
                array(
                    'field'   => 'total_insertion[]',
                    'label'   => 'Total de inserção',
                    'rules'   => 'required|numeric|max_length[11]',
                ),
                array(
                    'field'   => 'date_insertion_end[]', 
                    'label'   => 'Data final de veiculação', 
                    'rules'   => 'required|trim|check_date_valid',
                ),
                array(
                    'field'   => 'media_cost[]', 
                    'label'   => 'Custo da inserção', 
                    'rules'   => 'required|max_length[13]',
                ),
            ),
            'risk' => array(
                array(
                    'field'   => 'notice_risk', 
                    'label'   => 'Mensagem de risco', 
                    'rules'   => 'required|max_length[1000]',
                ),
            ),
            'process' => array(
                array(
                    'field'   => 'possui_processo',
                    'Label'   => 'Possui processo judicial',
                    'rules'   => 'required',
                ),
            ),
            'subscriber' => array(
                array(
                    'field'   => 'name',
                    'Label'   => 'Nome',
                    'rules'   => 'required|max_length[100]',
                ),
                array(
                    'field'   => 'email',
                    'Label'   => 'E-mail',
                    'rules'   => 'required|valid_email|trim|max_length[80]',
                ),
            ),
            'partners' => array(
                array(
                    'field'   => 'partner',
                    'Label'   => 'Empresa Parceira',
                    'rules'   => 'required|max_length[200]',
                ),
                array(
                    'field'   => 'email',
                    'Label'   => 'E-mail',
                    'rules'   => 'required|trim|callback_email_partner_check|max_length[1000]',
                ),
            ),
            'protocol_number' => array(
                array(
                    'field'   => 'protocol', 
                    'label'   => 'Nº do protocolo', 
                    'rules'   => 'required|min_length[10]|trim|max_length[40]'
                ),
            ),
            'search_serviced' => array(
                array(
                    'field'   => 'supplier_id', 
                    'label'   => 'Fornecedor', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'recall_id', 
                    'label'   => 'Campanha de Recall', 
                    'rules'   => 'required',
                ),
            ),
            'group' => array(
                array(
                    'field'   => 'name', 
                    'label'   => 'Grupo', 
                    'rules'   => 'required|max_length[100]',
                ),
            ),
            'reason_reservation' => array(
                array(
                    'field'   => 'reasonReservation', 
                    'label'   => 'Motivo da Ressalva', 
                    'rules'   => 'required|max_length[1000]',
                ),
            ),
            'new_user' => array(
                array(
                    'field'   => 'username', 
                    'label'   => 'Nome do usuário', 
                    'rules'   => 'required|max_length[45]',
                ),
                array(
                    'field'   => 'email', 
                    'label'   => 'E-mail', 
                    'rules'   => 'valid_email|trim|required|is_unique[users.email]|max_length[80]', // verifica se ja possui email no banco
                ),
                array(
                    'field'   => 'level_id', 
                    'label'   => 'Perfil', 
                    'rules'   => 'required',
                ),
            ),
            'user' => array(
                array(
                    'field'   => 'username', 
                    'label'   => 'Nome do usuário', 
                    'rules'   => 'required|max_length[45]',
                ),
                array(
                    'field'   => 'email', 
                    'label'   => 'E-mail', 
                    'rules'   => 'valid_email|trim|required|max_length[80]',
                ),
                array(
                    'field'   => 'level_id', 
                    'label'   => 'Perfil', 
                    'rules'   => 'required',
                ),
            ),
            'new_supplier_user' => array(
                array(
                    'field'   => 'username', 
                    'label'   => 'Nome do usuário', 
                    'rules'   => 'required|max_length[45]',
                ),
                array(
                    'field'   => 'email', 
                    'label'   => 'E-mail', 
                    'rules'   => 'valid_email|trim|required|is_unique[users.email]|max_length[80]', // verifica se ja possui email no banco
                ),
                array(
                    'field'   => 'email_copy', 
                    'label'   => 'E-mail(s) com Cópia', 
                    'rules'   => 'callback_email_supplier_check|trim',
                ),
                array(
                    'field'   => 'level_id', 
                    'label'   => 'Perfil', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'corporate_name', 
                    'label'   => 'Razão Social', 
                    'rules'   => 'required|max_length[150]',
                ),
                array(
                    'field'   => 'trade_name', 
                    'label'   => 'Nome Fantasia', 
                    'rules'   => 'required|max_length[200]',
                ),
                array(
                    'field'   => 'cnpj', 
                    'label'   => 'CNPJ', 
                    'rules'   => 'required|max_length[20]|check_cnpj_valid',
                ),
                array(
                    'field'   => 'state_registration', 
                    'label'   => 'Inscrição Estadual', 
                    'rules'   => 'max_length[20]|numeric',
                ),
                array(
                    'field'   => 'branch_id', 
                    'label'   => 'Ramo de atividades', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'address', 
                    'label'   => 'Endereço', 
                    'rules'   => 'required|max_length[150]',
                ),
                array(
                    'field'   => 'complement', 
                    'label'   => 'Complemento', 
                    'rules'   => 'max_length[100]',
                ),
                array(
                    'field'   => 'district', 
                    'label'   => 'Bairro', 
                    'rules'   => 'required|max_length[70]',
                ),
                array(
                    'field'   => 'state_id', 
                    'label'   => 'UF', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'city_id', 
                    'label'   => 'Município', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'zip', 
                    'label'   => 'CEP', 
                    'rules'   => 'required|max_length[10]|check_cep_valid',
                ),
                array(
                    'field'   => 'commercial_phone', 
                    'label'   => 'Telefone Comercial', 
                    'rules'   => 'required|max_length[20]|check_ddd_valid',
                ),
            ),
            'supplier_user' => array(
                array(
                    'field'   => 'username', 
                    'label'   => 'Nome do usuário', 
                    'rules'   => 'required|max_length[45]',
                ),
                array(
                    'field'   => 'email', 
                    'label'   => 'E-mail', 
                    'rules'   => 'valid_email|trim|required|max_length[80]',
                ),
                array(
                    'field'   => 'email_copy', 
                    'label'   => 'E-mail(s) com Cópia', 
                    'rules'   => 'callback_email_supplier_check|trim',
                ),
                array(
                    'field'   => 'level_id', 
                    'label'   => 'Perfil', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'corporate_name', 
                    'label'   => 'Razão Social', 
                    'rules'   => 'required|max_length[150]',
                ),
                array(
                    'field'   => 'trade_name', 
                    'label'   => 'Nome Fantasia', 
                    'rules'   => 'required|max_length[200]',
                ),
                array(
                    'field'   => 'cnpj', 
                    'label'   => 'CNPJ', 
                    'rules'   => 'required|max_length[20]|check_cnpj_valid',
                ),
                array(
                    'field'   => 'state_registration', 
                    'label'   => 'Inscrição Estadual', 
                    'rules'   => 'max_length[20]|numeric',
                ),
                array(
                    'field'   => 'branch_id', 
                    'label'   => 'Ramo de atividades', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'address', 
                    'label'   => 'Endereço', 
                    'rules'   => 'required|max_length[150]',
                ),
                array(
                    'field'   => 'complement', 
                    'label'   => 'Complemento', 
                    'rules'   => 'max_length[100]',
                ),
                array(
                    'field'   => 'district', 
                    'label'   => 'Bairro', 
                    'rules'   => 'required|max_length[70]',
                ),
                array(
                    'field'   => 'state_id', 
                    'label'   => 'UF', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'city_id', 
                    'label'   => 'Município', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'zip', 
                    'label'   => 'CEP', 
                    'rules'   => 'required|max_length[10]|check_cep_valid',
                ),
                array(
                    'field'   => 'commercial_phone', 
                    'label'   => 'Telefone Comercial', 
                    'rules'   => 'required|max_length[20]|check_ddd_valid',
                ),
            ),
            'supplier_request' => array(
                array(
                    'field'   => 'username', 
                    'label'   => 'Nome do usuário', 
                    'rules'   => 'required|max_length[45]',
                ),
                array(
                    'field'   => 'email', 
                    'label'   => 'E-mail', 
                    'rules'   => 'valid_email|trim|required|is_unique[users.email]|max_length[80]',
                ),
                array(
                    'field'   => 'corporate_name', 
                    'label'   => 'Razão Social', 
                    'rules'   => 'required|max_length[150]',
                ),
                array(
                    'field'   => 'trade_name', 
                    'label'   => 'Nome Fantasia', 
                    'rules'   => 'required|max_length[200]',
                ),
                array(
                    'field'   => 'cnpj', 
                    'label'   => 'CNPJ', 
                    'rules'   => 'required|max_length[20]|check_cnpj_valid',
                ),
                array(
                    'field'   => 'state_registration', 
                    'label'   => 'Inscrição Estadual', 
                    'rules'   => 'max_length[20]|numeric',
                ),
                array(
                    'field'   => 'branch_id', 
                    'label'   => 'Ramo de atividades', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'address', 
                    'label'   => 'Endereço', 
                    'rules'   => 'required|max_length[150]',
                ),
                array(
                    'field'   => 'complement', 
                    'label'   => 'Complemento', 
                    'rules'   => 'max_length[100]',
                ),
                array(
                    'field'   => 'district', 
                    'label'   => 'Bairro', 
                    'rules'   => 'required|max_length[70]',
                ),
                array(
                    'field'   => 'state_id', 
                    'label'   => 'UF', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'city_id', 
                    'label'   => 'Município', 
                    'rules'   => 'required',
                ),
                array(
                    'field'   => 'zip', 
                    'label'   => 'CEP', 
                    'rules'   => 'required|max_length[10]|check_cep_valid',
                ),
                array(
                    'field'   => 'commercial_phone', 
                    'label'   => 'Telefone Comercial', 
                    'rules'   => 'required|max_length[20]|check_ddd_valid',
                ),
            ),
            'manufacturers' => array(
                array(
                    'field'   => 'corporate_name', 
                    'label'   => 'Razão Social', 
                    'rules'   => 'required|max_length[150]',
                ),
                array(
                    'field'   => 'trade_name', 
                    'label'   => 'Nome Fantasia',
                    'rules'   => 'required|max_length[200]',
                ),
            ),
            'login' => array(
                array(
                    'field'   => 'email', 
                    'label'   => 'E-mail', 
                    'rules'   => 'required|valid_email|trim',
                ),
                array(
                    'field'   => 'password', 
                    'label'   => 'Senha', 
                    'rules'   => 'required|trim|max_length[10]|min_length[6]',
                ),
            ),
            'login_adm' => array(
                array(
                    'field'   => 'email', 
                    'label'   => 'E-mail', 
                    'rules'   => 'required|valid_email|trim',
                ),
                array(
                    'field'   => 'password', 
                    'label'   => 'Senha', 
                    'rules'   => 'required|trim|max_length[10]',
                ),
            ),
            'alter_password' => array(
                array(
                    'field'   => 'password', 
                    'label'   => 'Senha Atual', 
                    'rules'   => 'required|trim|max_length[10]|min_length[6]',
                ),
                array(
                    'field'   => 'password_new', 
                    'label'   => 'Nova Senha', 
                    'rules'   => 'required|matches[confirm_password_new]|trim|max_length[10]|min_length[6]',
                ),
                array(
                    'field'   => 'confirm_password_new', 
                    'label'   => 'Confirmar Nova Senha', 
                    'rules'   => 'required|trim|max_length[10]|min_length[6]',
                ),
            ),
         );