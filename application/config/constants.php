<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

define('MAX_UPLOAD_FILE_SIZE', 10000000);
define('MSG_UPLOAD_ACIMA_PERMITIDO', 'Não é permitido fazer upload de arquivo com mais de 10MB.');

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*
 * MENSAGENS
 */

define('MSG_SUCCESS_NEW_PASSWORD','Recuperação de senha enviada com sucesso.');

define('MSG_SUCCESS', 'Item salvo com sucesso.');
define('MSG_SUCCESS_ALTERADO', 'Item alterado com sucesso.');
define('MSG_SUCCESS_REMOVE', 'Item removido com sucesso.');
define('MSG_SEND_RECALL', 'Campanha enviada com sucesso.');
define('MSG_APPROVE_RECALL', 'Campanha aprovada com sucesso.');
define('MSG_DISAPPROVE_RECALL', 'Campanha devolvida com sucesso.');
define('MSG_FINISH_RECALL', 'Campanha finalizada com sucesso.');
define('MSG_ARQUIVADA', 'Campanha inativa com sucesso.');
define('MSG_ERROR', 'Ocorreu um erro no cadastro. Verifique se os campos foram preenchidos corretamente.');
define('MSG_ERROR_ALTERADO', 'Não é possivel alterar este ítem.');
define('MSG_ERROR_REMOVE', 'Não é possivel remover este ítem. Verifique se o mesmo está sendo utilizado em outro lugar.');
define('MSG_ERROR_REMOVE_RECALL_EXISTS', 'Não é possivel remover este ítem. Existe uma campanha cadastrada com o mesmo.');
define('MSG_ERROR_REMOVE_GROUP_EXISTS', 'Não é possivel remover este ítem. Existe empresas vinculadas à ele.');
define('MSG_ERROR_QUANTIDADE_ATENDIMENTO_VAZIO', 'É necessário lançar pelo menos uma quantidade.');
define('MSG_ERROR_CAMPO_MENSAGEM_OBRIGATORIO', 'É necessário preencher o campo da mensagem.');

define('MSG_ERROR_USUARIO_NAO_ENCONTRADO', 'Usuário/Senha não encontrado.');
define('MSG_ERROR_USUARIO_SENHA_NAO_CONFERE', 'Senha atual não confere.');
define('MSG_SUCCESS_SENHA_ALTERADA', 'Senha alterada com sucesso.');
define('MSG_ERROR_SENHA_NAO_ATIVADA', 'Seu cadastro não foi ativado.');

define('MSG_ERROR_NAO_ENCONTRADO', 'Item nao encontrado.');

/**
 * MENSAGENS DE ERRO USUARIO LOGIN
 */

define('ALERTA_ERRO_LOGIN_CPF','O CPF informado não está cadastrado.');
define('ALERTA_ERRO_LOGIN_USUARIO', 'O usuário informado não está cadastrado.');
define('ALERTA_ERRO_LOGIN_STATUS_PENDENTE', 'Sua conta está pendente para aprovação, aguarde a avaliação dos administradores.');
define('ALERTA_ERRO_LOGIN_STATUS_BLOQUEADO', 'Sua conta está bloqueada.');
//define('ALERTA_ERRO_LOGIN_STATUS_BLOQUEADO', 'Usuário bloqueado por exceder a quantidade de tentativas de login com senha inválida.');
define('ALERTA_ERRO_LOGIN_SENHA', 'A senha informada não é válida.');
define('ALERTA_ERRO_LOGIN_TENTATIVA', 'Você excedeu a quantidade de tentativas.');
define('ALERTA_ERRO_LOGIN_PERMISSAO', 'Você não possui permissão de acesso em nenhum dos módulos.');


/************** SEGURANÇA **************/
define( 'LOGIN_ADMIN_LIMITE_DE_TENTATIVAS', 4 );
// lista de status de usuário
define( 'LOGIN_ADMIN_STATUS_ATIVO', 'A' );
define( 'LOGIN_ADMIN_STATUS_PENDENTE', 'P' );
define( 'LOGIN_ADMIN_STATUS_BLOQUEADO', 'B' );


/*
 * LEVEL USERS
 */
define('LEVEL_ADMINISTRADOR',1);
define('LEVEL_OPERADOR',2);
define('LEVEL_FORNECEDOR',3);

/*
 * TELAS LIBERADAS PARA CADASTRO - TIMELINE
 */

define('TIME_LINE_CAMPAIGN',1);
define('TIME_LINE_INFO',2);
define('TIME_LINE_PRODUCT',3);
define('TIME_LINE_LOCATION',4);
define('TIME_LINE_MEDIA',5);
define('TIME_LINE_RISK',6);
define('TIME_LINE_PROCESS',7);
define('TIME_LINE_SEND_CAMPAIGN',8);
define('TIME_LINE_ENVIADA',9);


/*
 * WORKFLOW RECALL
 */

define('STATUS_ID_EM_CADASTRAMENTO'         ,1);
define('STATUS_ID_AGUARDANDO_PUBLICACAO'    ,2);
define('STATUS_ID_PUBLICADA'                ,3);
define('STATUS_ID_PUBLICADA_COM_RESSALVA'   ,4);
define('STATUS_ID_PENDENCIA_CADASTRO'       ,5);
define('STATUS_ID_ARQUIVADO'                ,6);
define('STATUS_ID_FINALIZADA'               ,7);

define('STATUS_EM_CADASTRAMENTO'        ,'Em cadastramento');
define('STATUS_AGUARDANDO_PUBLICACAO'   ,'Aguardando publicação');
define('STATUS_PUBLICADO'               ,'Publicada');
define('STATUS_PUBLICADO_COM_RESSALVA'  ,'Publicada com ressalva');
define('STATUS_PENDENCIA_CADASTRO'      ,'Pendencia de cadastro');
define('STATUS_ARQUIVADO'               ,'Inativa');
define('STATUS_FINALIZADA'              ,'Finalizada');

/*
 * TYPE UPLOAD
 */
define('UPLOAD_ALL',1);
define('UPLOAD_FILE',2);
define('UPLOAD_IMAGE',3);

/*
 * ACTION TO REGISTER LOGS
 */
define('ACTION_SEND_RECALL','Enviar Campanha - recall_id: ');
define('ACTION_APPROVE_RECALL','Aprovar Campanha - recall_id: ');
define('ACTION_DISAPPROVE_RECALL','Devolver Campanha - recall_id: ');
define('ACTION_REMOVE_PRODUCT','Remover Produto - product_id: ');

/*
 * FILES BELONGS TO
 */
define('FILE_BELONGS_TO_MEDIA_PLAN',1);
define('FILE_BELONGS_TO_RISK_ALERT',2);
define('FILE_BELONGS_TO_MESSAGE_DISAPPROVE_RECALL',3);

/*
 * STATUS ATIVO / INATIVO
 */
define('STATUS_ATIVO',1);
define('STATUS_INATIVO',0);
define('STATUS_AGUARDANDO',2);
define('STATUS_SOLICITADO',3);

/*
 * FLAG
 */
define('FLAG_ALL_ATIVO',1);
define('FLAG_ALL_INATIVO',0);

/*
 * Envio de e-mail
 */

//define('EMAIL_FROM_SENACON','seyr.souza@mj.gov.br');
define('EMAIL_FROM_SENACON','rafael_lima106@hotmail.com');
define('EMAIL_FROM_TESTE','rafaellima106@hotmail.com');