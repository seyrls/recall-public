<?php

class Report_model extends CI_Model {
    
     function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    
    /*
    * retorna o total dos produtos afetados das campanhas de recall
    * que possuem o status = Publicada, Publicada com Ressalva ou Finalizada
    * return int
    */
    function sqlTotalAffected($supplier_id = NULL) {
        $andWhere = ($supplier_id) ? " and re.supplier_id = {$supplier_id}" : "";
        $total = 0;
        $dql = "select 
                                sum(af.quantity) as total_affected  
                from 
                                affecteds af
                inner join 
                                products pr on pr.id = af.product_id
                inner join 
                                recalls re on re.id = pr.recall_id
                where 
                                re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                    ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                    ".STATUS_ID_FINALIZADA.")
        {$andWhere};";
        
        $query = $this->db->query($dql);
        $result = $query->result();
        
        if ($result) {
            $total = (int) $result[0]->total_affected;
        }
        
        return $total;
    }
    
    /*
     * retorna o total dos produtos atendidos das campanhas de recall
     * que possuem o status = Publicada, Publicada com Ressalva ou Finalizada
     * return int
     */
    public function sqlTotalServiced($supplier_id = NULL) {
        $andWhere = ($supplier_id) ? " and re.supplier_id = {$supplier_id}" : "";
        $total = 0;
        
        $dql = "select 
                                sum(ts.quantity) as total_serviced
                from 
                                total_serviceds ts
                inner join 
                                products pr on pr.id = ts.product_id
                inner join 
                                recalls re on re.id = pr.recall_id
                where 
                                re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                    ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                    ".STATUS_ID_FINALIZADA.")
        {$andWhere};";
        
        $query = $this->db->query($dql);
        $result = $query->result();
        
        if ($result) {
            $total = (int) $result[0]->total_serviced;
        }
        
        return $total;
    }
    
    /*
     * retorna o total dos tipos de riscos das campanhas de recall
     * que possuem o status = Publicada, Publicada com Ressalva ou Finalizada
     * return array
     */
    public function sqlTotalRisk($supplier_id = NULL) {
        
        $andWhere = ($supplier_id) ? " and supplier_id = {$supplier_id}" : "";
        
        $dados = array();
        
        $dql = "select 
                                type_risk,
                                count(type_risk) as total_risk
                from 
                                type_risks tp 
                inner join 
                                recalls re on re.type_risk_id = tp.id
                where 
                                re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                    ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                    ".STATUS_ID_FINALIZADA.")
                {$andWhere}
                group by 
                                type_risk
                order by 
                                total_risk desc";
        
        $query = $this->db->query($dql);
        $result = $query->result();
        
        
        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'total_risk' => $value->total_risk,
                    'type_risk' => $value->type_risk,
                );
            }
        }
        return $dados;
    }
    
    /*
     * retorna o total dos setores de atividades das campanhas de recall
     * que possuem o status = Publicada, Publicada com Ressalva ou Finalizada
     * return array
     */
    public function sqlTotalBranch($supplier_id = NULL) {
        
        $andWhere = ($supplier_id) ? " and supplier_id = {$supplier_id}" : "";
        $dados = array();
        
        $dql = "select 
                                branch,
                                count(branch) as total_branch
                from 
                                branches br
                inner join
                                type_products tp on br.id = tp.branch_id
                inner join
                                products pr on pr.type_product_id = tp.id and pr.recall_id in (
                                        select 
                                                    id
                                        from 
                                                    recalls
                                        where 
                                                    status_campaign in (".STATUS_ID_PUBLICADA.",
                                                            ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                            ".STATUS_ID_FINALIZADA.")
                                        {$andWhere}
                                        )
                group by 
                                branch
                order by 
                                total_branch desc";
        
        $query = $this->db->query($dql);
        $result = $query->result();
        
        
        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'total_branch' => $value->total_branch,
                    'branch' => $value->branch,
                );
            }
        }
        return $dados;
    }
    
    /*
     * retorna o total dos planos de media das campanhas de recall
     * que possuem o status = Publicada, Publicada com Ressalva ou Finalizada
     * return array
     */
    public function sqlTotalMedia($supplier_id = NULL) {
        
        $andWhere = ($supplier_id) ? " and re.supplier_id = {$supplier_id}" : "";
        $dados = array();
        
        $dql = "select 
                                ori.origin,
                                sum(mp.media_cost) as media_cost
                from 
                                media_plans mp
                inner join
                                sources so on mp.source_id = so.id
                inner join
                                medias me on me.id = so.media_id
                inner join
                                origins ori on me.origin_id = ori.id
                inner join
                                recalls re on re.id = mp.recall_id and re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                            ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                            ".STATUS_ID_FINALIZADA.")
                                                            {$andWhere}
                group by 
                                ori.origin;";
        
        $query = $this->db->query($dql);
        $result = $query->result();
        
        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'media_cost_sum' => $value->media_cost,
                    'media_cost' => number_format($value->media_cost, 2, ',', '.'),
                    'origin' => $value->origin,
                );
            }
        }
        return $dados;
    }
    
    /*
     * retorna o total dos produtos afetados agrupados por ano
     * que possuem o status = Publicada, Publicada com Ressalva ou Finalizada
     * return array
     */
    public function sqlTotalAffectedByYear($supplier_id = NULL) {
        
        $andWhere = ($supplier_id) ? " and re.supplier_id = {$supplier_id}" : "";
        $dados = array();
        
        $dql = "select 
                            ano,
                            t.total_affected,
                            t.total_serviced
                from (
                            -- AFFECTED
                            select 
                                            YEAR(start_date) as ano,
                                            sum(af.quantity) as total_affected,
                                            (select 
                                                          sum(ts2.quantity) as total_serviced
                                            from 
                                                         total_serviceds ts2
                                            inner join 
                                                         products pr2 on pr2.id = ts2.product_id
                                            inner join 
                                                         recalls re2 on re2.id = pr2.recall_id
                                            where 
                                                         re2.id = re.id 
                                            ) as total_serviced

                            from 
                                            affecteds af
                            inner join 
                                            products pr on pr.id = af.product_id
                            inner join 
                                            recalls re on re.id = pr.recall_id
                            where 
                                            re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                            ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                            ".STATUS_ID_FINALIZADA.")
                            {$andWhere}
                            GROUP BY 
                                            YEAR(start_date)
                            UNION ALL

                            select 
                                            YEAR(start_date) as ano,
                                            null as total_affected,
                                            sum(ts.quantity) as total_serviced
                            from 
                                            total_serviceds ts
                            inner join 
                                            products pr on pr.id = ts.product_id
                            inner join 
                                            recalls re on re.id = pr.recall_id
                            where 
                                            re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                            ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                            ".STATUS_ID_FINALIZADA.")
                            and 
                                            quantity IS NOT NULL 
                            {$andWhere}
                                
                            GROUP BY 
                                                              YEAR(start_date)
                ) t
                group by
                                            ano
                ORDER BY  
                                            ano;";
        
        $query = $this->db->query($dql);
        $result = $query->result();
        
        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'ano' => $value->ano,
                    'total_affected' => $value->total_affected,
                    'total_serviced' => $value->total_serviced,
                );
            }
        }
        return $dados;
    }
    
    /*
     * retorna os status da campanha de recall
     * return array
     */
    public function sqlStatusCampaign($supplier_id = NULL) {
        
        $this->load->helper('status');
        $arrDados = array();
        
        $where = ($supplier_id) ? " where supplier_id = {$supplier_id}" : "";
        
        $dql = "select
			 status_campaign
                from 
			recalls 
                {$where}
                order by 
			status_campaign asc;";
        
        $query = $this->db->query($dql);
        $result = $query->result();
        
        if ($result) {
            $arrNovo = array();
            
            foreach ($result as $value) {
                $arrNovo[] = $value->status_campaign;
            }
            
            $arrCount = array_count_values($arrNovo);
            
            foreach ($arrCount as $key => $value) {
                $arrDados[] = array(
                    'status_campaign' => get_status_campaign($key, FALSE),
                    'total' => (int) $value,
                );
            }
        }
        
        return $arrDados;
    }
    
    /*
     * Funcao para mostrar dados abertos de todas as campanhas de recall
     */
    public function sqlOpenDataRecall($year) {
        $this->load->helper('status');
        $andwhere = "";
        if ($year) {
            $date_ini = $year.'-01-01';
            $date_fim = $year.'-12-31';
            $andwhere .= " and re.start_date between CAST('{$date_ini}' as DATE) and CAST('{$date_fim}' as DATE)";
        }
        
        $dados = array();
                $dql = "select 
                         re.start_date,
                         re.end_date,
                         re.title,
                         su.trade_name,
                         su.corporate_name,
                         tr.type_risk,
                         co.country,
                         group_concat(DISTINCT pr.product ORDER BY pr.id ASC SEPARATOR ' | ') as product,
                         (select 
                                sum(quantity)
                          from 
                                affecteds
                          where 
                                product_id in ( select 
                                                        id 
                                                from 
                                                        products
                                                where 
                                                        recall_id = re.id )
                         )as total_affected,
                        (select 
                                sum(quantity) 
                          from 
                                total_serviceds
                          where 
                                product_id in (select 
                                                        id 
                                                from 
                                                        products
                                                where 
                                                        recall_id = re.id)
                         )as total_serviced
         from 
                         recalls re
         inner join 
                         suppliers su on re.supplier_id = su.id
         inner join 
                         products pr on pr.recall_id = re.id
         inner join 
                         type_risks tr on tr.id = re.type_risk_id
         inner join 
                         countries co on co.id = pr.country_id
         where
                         re.status_campaign in (".STATUS_ID_PUBLICADA.",
                                                ".STATUS_ID_PUBLICADA_COM_RESSALVA.",
                                                ".STATUS_ID_FINALIZADA.")
        {$andwhere}

         GROUP BY 
                         re.id
         order by 
                         su.trade_name, re.start_date";
    
    
        $query = $this->db->query($dql);
        $result = $query->result();

        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'data_inicio'       => $value->start_date,
                    'end_date'          => $value->end_date,
                    'titulo'            => $value->title,
                    'fornecedor'        => $value->corporate_name,
                    'tipo_risco'        => $value->type_risk,
                    'pais_origem'       => $value->country,
                    'produto'           => $value->product,
                    'total_afetado'     => $value->total_affected,
                    'total_atendido'    => $value->total_serviced,
                );
            }
        }

        return $dados;
    }
    
    /*
     * Funcao para mostrar dados abertos de todas as campanhas de recall
     */
    public function sqlTotalRecallByYear() {

        $dados = array();
        
        $dql = "select
              YEAR(start_date) as year
        from 
                      recalls
        where 
                      status_campaign in (3,4,7)
        GROUP BY 
                      YEAR(start_date)
        ORDER BY
                      start_date DESC";
        
        $query = $this->db->query($dql);
        $result = $query->result();
        
        if ($result) {
            foreach ($result as $key => $value) {
                $dados[$key] = array(
                    'year' => $value->year
                );
            }
        }
        return $dados;
    }
}

    