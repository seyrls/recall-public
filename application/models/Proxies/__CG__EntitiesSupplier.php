<?php

namespace Proxies\__CG__\Entities;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class Supplier extends \Entities\Supplier implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }

    /** @private */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int) $this->_identifier["id"];
        }
        $this->__load();
        return parent::getId();
    }

    public function setCnpj($cnpj)
    {
        $this->__load();
        return parent::setCnpj($cnpj);
    }

    public function getCnpj()
    {
        $this->__load();
        return parent::getCnpj();
    }

    public function setCorporateName($corporateName)
    {
        $this->__load();
        return parent::setCorporateName($corporateName);
    }

    public function getCorporateName()
    {
        $this->__load();
        return parent::getCorporateName();
    }

    public function setTradeName($tradeName)
    {
        $this->__load();
        return parent::setTradeName($tradeName);
    }

    public function getTradeName()
    {
        $this->__load();
        return parent::getTradeName();
    }

    public function setStateRegistration($stateRegistration)
    {
        $this->__load();
        return parent::setStateRegistration($stateRegistration);
    }

    public function getStateRegistration()
    {
        $this->__load();
        return parent::getStateRegistration();
    }

    public function setEmailCopy($emailCopy)
    {
        $this->__load();
        return parent::setEmailCopy($emailCopy);
    }

    public function getEmailCopy()
    {
        $this->__load();
        return parent::getEmailCopy();
    }

    public function setAddress($address)
    {
        $this->__load();
        return parent::setAddress($address);
    }

    public function getAddress()
    {
        $this->__load();
        return parent::getAddress();
    }

    public function setComplement($complement)
    {
        $this->__load();
        return parent::setComplement($complement);
    }

    public function getComplement()
    {
        $this->__load();
        return parent::getComplement();
    }

    public function setDistrict($district)
    {
        $this->__load();
        return parent::setDistrict($district);
    }

    public function getDistrict()
    {
        $this->__load();
        return parent::getDistrict();
    }

    public function setZip($zip)
    {
        $this->__load();
        return parent::setZip($zip);
    }

    public function getZip()
    {
        $this->__load();
        return parent::getZip();
    }

    public function setCommercialPhone($commercialPhone)
    {
        $this->__load();
        return parent::setCommercialPhone($commercialPhone);
    }

    public function getCommercialPhone()
    {
        $this->__load();
        return parent::getCommercialPhone();
    }

    public function setFaxPhone($faxPhone)
    {
        $this->__load();
        return parent::setFaxPhone($faxPhone);
    }

    public function getFaxPhone()
    {
        $this->__load();
        return parent::getFaxPhone();
    }

    public function setCity(\Entities\City $city = NULL)
    {
        $this->__load();
        return parent::setCity($city);
    }

    public function getCity()
    {
        $this->__load();
        return parent::getCity();
    }

    public function setBranch(\Entities\Branch $branch = NULL)
    {
        $this->__load();
        return parent::setBranch($branch);
    }

    public function getBranch()
    {
        $this->__load();
        return parent::getBranch();
    }

    public function setUser(\Entities\User $user = NULL)
    {
        $this->__load();
        return parent::setUser($user);
    }

    public function getUser()
    {
        $this->__load();
        return parent::getUser();
    }

    public function getComboSupplier($supplier, $defult = array (
  '' => 'Selecione',
), $ativo = true)
    {
        $this->__load();
        return parent::getComboSupplier($supplier, $defult, $ativo);
    }

    public function getArraySupplier($supplier)
    {
        $this->__load();
        return parent::getArraySupplier($supplier);
    }

    public function getArrayAllSupplier($supplier)
    {
        $this->__load();
        return parent::getArrayAllSupplier($supplier);
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id', 'cnpj', 'corporateName', 'tradeName', 'stateRegistration', 'emailCopy', 'address', 'complement', 'district', 'zip', 'commercialPhone', 'faxPhone', 'city', 'branch', 'user');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields as $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}