<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Process
 */
class Process
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $processNumber;

    /**
     * @var \DateTime
     */
    private $dateProcess;

    /**
     * @var string
     */
    private $staff;

    /**
     * @var string
     */
    private $distrito;

    /**
     * @var string
     */
    private $situation;

    /**
     * @var string
     */
    private $describeProcess;
    
    /**
     * @var \Entities\Recall
     */
    private $recall;

    /**
     * @var string
     */
    private $identification;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set processNumber
     *
     * @param string $processNumber
     * @return Process
     */
    public function setProcessNumber($processNumber)
    {
        $this->processNumber = $processNumber;
    
        return $this;
    }

    /**
     * Get processNumber
     *
     * @return string 
     */
    public function getProcessNumber()
    {
        return $this->processNumber;
    }

    /**
     * Set dateProcess
     *
     * @param \DateTime $dateProcess
     * @return Process
     */
    public function setDateProcess($dateProcess)
    {
        if (is_null($dateProcess)){
            $this->dateProcess = NULL;
        }else{
           $this->dateProcess = new \DateTime($dateProcess);
        }
        return $this;
    }

    /**
     * Get dateProcess
     *
     * @return \DateTime 
     */
    public function getDateProcess()
    {
        return $this->dateProcess;
    }

    /**
     * Set staff
     *
     * @param string $staff
     * @return Process
     */
    public function setStaff($staff)
    {
        $this->staff = $staff;
    
        return $this;
    }

    /**
     * Get staff
     *
     * @return string 
     */
    public function getStaff()
    {
        return $this->staff;
    }

    /**
     * Set distrito
     *
     * @param string $distrito
     * @return Process
     */
    public function setDistrito($distrito)
    {
        $this->distrito = $distrito;
    
        return $this;
    }

    /**
     * Get distrito
     *
     * @return string 
     */
    public function getDistrito()
    {
        return $this->distrito;
    }

    /**
     * Set situation
     *
     * @param string $situation
     * @return Process
     */
    public function setSituation($situation)
    {
        $this->situation = $situation;
    
        return $this;
    }

    /**
     * Get situation
     *
     * @return string 
     */
    public function getSituation()
    {
        return $this->situation;
    }

    /**
     * Set describeProcess
     *
     * @param string $describeProcess
     * @return Process
     */
    public function setDescribeProcess($describeProcess)
    {
        $this->describeProcess = $describeProcess;
    
        return $this;
    }

    /**
     * Get describeProcess
     *
     * @return string 
     */
    public function getDescribeProcess()
    {
        return $this->describeProcess;
    }
    
    /**
     * Set recall
     *
     * @param \Entities\Recall $recall
     * @return Process
     */
    public function setRecall(\Entities\Recall $recall = null)
    {
        $this->recall = $recall;
    
        return $this;
    }

    /**
     * Get recall
     *
     * @return \Entities\Recall 
     */
    public function getRecall()
    {
        return $this->recall;
    }
    
    /**
     * Set identification
     *
     * @param string $identification
     * @return Process
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;
    
        return $this;
    }

    /**
     * Get identification
     *
     * @return string 
     */
    public function getIdentification()
    {
        return $this->identification;
    }
}