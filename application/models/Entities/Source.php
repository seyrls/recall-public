<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Source
 */
class Source
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $source;

    /**
     * @var \Entities\Media
     */
    private $media;
    
    /**
     * @var tinyint
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return Source
     */
    public function setSource($source)
    {
        $this->source = utf8_decode($source);
    
        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return utf8_encode($this->source);
    }

    /**
     * Set media
     *
     * @param \Entities\Media $media
     * @return Source
     */
    public function setMedia(\Entities\Media $media = null)
    {
        $this->media = $media;
    
        return $this;
    }

    /**
     * Get media
     *
     * @return \Entities\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }
    
    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    
    /*
     * Metodo para retornar todos as fontes de midia
     * return Array
     */
    public function getArrayAllSource(Array $source = array()) {
        $dados = array();
        if ($source) {
            foreach ($source as $key => $value) {
                $dados[$key] = array(
                    'source_id' => $value->getid(),
                    'source'    => ($value->getSource()),
                    'status'    => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
    
    /*
     * Metodo para retornar todos as fontes de midia
     * return Array
     */
    public function getArraySource(Array $source = array()) {
        $dados = array();
        if ($source) {
            foreach ($source as $value) {
                $dados = array(
                    'source_id' => $value->getid(),
                    'source'    => ($value->getSource()),
                    'status'    => $value->getStatus(),
                    'media_id'  => $value->getMedia()->getId(),
                    'media'     => $value->getMedia()->getMedia(),
                );
            }
        }
        return $dados;
    }
}