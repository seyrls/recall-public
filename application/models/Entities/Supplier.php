<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplier
 */
class Supplier
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $cnpj;

    /**
     * @var string
     */
    private $corporateName;

    /**
     * @var string
     */
    private $tradeName;

    /**
     * @var integer
     */
    private $stateRegistration;
    
    /**
     * @var string
     */
    private $emailCopy;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $complement;

    /**
     * @var string
     */
    private $district;

    /**
     * @var integer
     */
    private $zip;

    /**
     * @var string
     */
    private $commercialPhone;

    /**
     * @var string
     */
    private $faxPhone;

    /**
     * @var \Entities\City
     */
    private $city;

    /**
     * @var \Entities\Branch
     */
    private $branch;
    
    /**
     * @var \Entities\User
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cnpj
     *
     * @param string $cnpj
     * @return Supplier
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
    
        return $this;
    }

    /**
     * Get cnpj
     *
     * @return string 
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Set corporateName
     *
     * @param string $corporateName
     * @return Supplier
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = utf8_decode($corporateName);
    
        return $this;
    }

    /**
     * Get corporateName
     *
     * @return string 
     */
    public function getCorporateName()
    {
        return utf8_encode($this->corporateName);
    }

    /**
     * Set tradeName
     *
     * @param string $tradeName
     * @return Supplier
     */
    public function setTradeName($tradeName)
    {
        $this->tradeName = utf8_decode($tradeName);
    
        return $this;
    }

    /**
     * Get tradeName
     *
     * @return string 
     */
    public function getTradeName()
    {
        return utf8_encode($this->tradeName);
    }

    /**
     * Set stateRegistration
     *
     * @param integer $stateRegistration
     * @return Supplier
     */
    public function setStateRegistration($stateRegistration)
    {
        $this->stateRegistration = $stateRegistration;
    
        return $this;
    }

    /**
     * Get stateRegistration
     *
     * @return integer 
     */
    public function getStateRegistration()
    {
        return $this->stateRegistration;
    }
    
    /**
     * Set emailCopy
     *
     * @param string emailCopy
     * @return Supplier
     */
    public function setEmailCopy($emailCopy)
    {
        $this->emailCopy = $emailCopy;
    
        return $this;
    }

    /**
     * Get emailCopy
     *
     * @return string 
     */
    public function getEmailCopy()
    {
        return $this->emailCopy;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Supplier
     */
    public function setAddress($address)
    {
        $this->address = utf8_decode($address);
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return utf8_encode($this->address);
    }

    /**
     * Set complement
     *
     * @param string $complement
     * @return Supplier
     */
    public function setComplement($complement)
    {
        $this->complement = utf8_decode($complement);
    
        return $this;
    }

    /**
     * Get complement
     *
     * @return string 
     */
    public function getComplement()
    {
        return utf8_encode($this->complement);
    }

    /**
     * Set district
     *
     * @param string $district
     * @return Supplier
     */
    public function setDistrict($district)
    {
        $this->district = utf8_decode($district);
    
        return $this;
    }

    /**
     * Get district
     *
     * @return string 
     */
    public function getDistrict()
    {
        return utf8_encode($this->district);
    }

    /**
     * Set zip
     *
     * @param integer $zip
     * @return Supplier
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    
        return $this;
    }

    /**
     * Get zip
     *
     * @return integer 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set commercialPhone
     *
     * @param string $commercialPhone
     * @return Supplier
     */
    public function setCommercialPhone($commercialPhone)
    {
        $this->commercialPhone = $commercialPhone;
    
        return $this;
    }

    /**
     * Get commercialPhone
     *
     * @return string 
     */
    public function getCommercialPhone()
    {
        return $this->commercialPhone;
    }

    /**
     * Set faxPhone
     *
     * @param string $faxPhone
     * @return Supplier
     */
    public function setFaxPhone($faxPhone)
    {
        $this->faxPhone = $faxPhone;
    
        return $this;
    }

    /**
     * Get faxPhone
     *
     * @return string 
     */
    public function getFaxPhone()
    {
        return $this->faxPhone;
    }

    /**
     * Set city
     *
     * @param \Entities\City $city
     * @return Supplier
     */
    public function setCity(\Entities\City $city = null)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return \Entities\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set branch
     *
     * @param \Entities\Branch $branch
     * @return Supplier
     */
    public function setBranch(\Entities\Branch $branch = null)
    {
        $this->branch = $branch;
    
        return $this;
    }

    /**
     * Get branch
     *
     * @return \Entities\Branch 
     */
    public function getBranch()
    {
        return $this->branch;
    }
    
    /**
     * Set user
     *
     * @param \Entities\User $user
     * @return Supplier
     */
    public function setUser(\Entities\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Entities\User
     */
    public function getUser()
    {
        return $this->user;
    }
   
    /*
     * return array select com os fornecedores
     */
    public function getComboSupplier($supplier, $defult = array('' => 'Selecione'), $ativo = TRUE){
        $dados = $defult;
        if ($supplier) {
            foreach ($supplier as $value) {
                if ($ativo) {
                    if ($value->getUser()->getStatus() == STATUS_ATIVO) {
                        $dados[$value->getId()] = $value->getTradeName();
                    }
                } else {
                    $dados[$value->getId()] = $value->getTradeName();
                }
                
            }
        }
        return $dados;
    }
    
    function getArraySupplier($supplier) {
        $dados = array();
        if ($supplier) {
            foreach ($supplier as $value) {
                $dados = array(
                    'supplier_id' => $value->getId(),
                    'cnpj' => $value->getCnpj(),
                    'corporate_name' => $value->getCorporateName(),
                    'trade_name' => $value->getTradeName(),
                    'state_registration' => $value->getStateRegistration(),
                    'email_copy' => $value->getEmailCopy(),
                    'state' => $value->getCity()->getState()->getState(),
                    'state_id' => $value->getCity()->getState()->getId(),
                    'city' => ($value->getCity()->getCity()),
                    'city_id' => ($value->getCity()->getId()),
                    'address' => ($value->getAddress()),
                    'complement' => $value->getComplement(),
                    'district' => $value->getDistrict(),
                    'zip' => $value->getZip(),
                    'commercial_phone' => $value->getCommercialPhone(),
                    'fax_phone' => $value->getFaxPhone(),
                    'branch_id' => $value->getBranch()->getId(),
                    'branch' => $value->getBranch()->getBranch(),
                    'email' => $value->getUser()->getEmail(),
                    'username' => $value->getUser()->getUsername(),
                );
            }
        }
        return $dados;
    }
    
    function getArrayAllSupplier($supplier) {
        $dados = array();
        if ($supplier) {
            foreach ($supplier as $key => $value) {
                $dados[$key] = array(
                    'supplier_id' => $value->getId(),
                    'cnpj' => $value->getCnpj(),
                    'corporate_name' => $value->getCorporateName(),
                    'trade_name' => $value->getTradeName(),
                    'state_registration' => $value->getStateRegistration(),
                    'email_copy' => $value->getEmailCopy(),
                    'state' => $value->getCity()->getState()->getState(),
                    'state_id' => $value->getCity()->getState()->getId(),
                    'city' => ($value->getCity()->getCity()),
                    'city_id' => ($value->getCity()->getId()),
                    'address' => ($value->getAddress()),
                    'complement' => $value->getComplement(),
                    'district' => $value->getDistrict(),
                    'zip' => $value->getZip(),
                    'commercial_phone' => $value->getCommercialPhone(),
                    'fax_phone' => $value->getFaxPhone(),
                    'branch_id' => $value->getBranch()->getId(),
                    'branch' => $value->getBranch()->getBranch(),
                    'email' => $value->getUser()->getEmail(),
                    'username' => $value->getUser()->getUsername(),
                    'user_id' => $value->getUser()->getId(),
                    'status' => $value->getUser()->getStatus(),
                );
            }
        }
        return $dados;
    }
    
}