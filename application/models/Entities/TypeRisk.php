<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeRisk
 */
class TypeRisk
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $typeRisk;
    
    /**
     * @var tinyint
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeRisk
     *
     * @param string $typeRisk
     * @return TypeRisk
     */
    public function setTypeRisk($typeRisk)
    {
        $this->typeRisk = utf8_decode($typeRisk);
    
        return $this;
    }

    /**
     * Get typeRisk
     *
     * @return string 
     */
    public function getTypeRisk()
    {
        return utf8_encode($this->typeRisk);
    }
/**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
   
    /*
     * return array select com os tipos de risco
     */
    public function getComboTypeRisk($typeRisk, $defult = array('' => 'Selecione')){
        $dados = $defult;
        if ($typeRisk) {
            foreach ($typeRisk as $value) {
                $dados[$value->getId()] = ($value->getTypeRisk());
            }
        }
        return $dados;
    }
}