<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Affected
 */
class Affected
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var \Entities\State
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity="\Entities\Product", inversedBy="affected")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Affected
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set state
     *
     * @param \Entities\State $state
     * @return Affected
     */
    public function setState(\Entities\State $state = null)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return \Entities\State 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set product
     *
     * @param \Entities\Product $product
     * @return Affected
     */
    public function setProduct(\Entities\Product $product = null)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return \Entities\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    public function __toString()
    {
        return $this->quantity;
    }
    
    /*
     * Metodo para retornar todos as quantidades afetadas pelo produto
     * return Array
     */
    public function getArrayAllAffected(Array $affected = array()) {
        
        $dados = array();
        
        if ($affected) {
            foreach ($affected as $key => $value) {
                $dados[$key] = array(
                    'affected_id'               => $value->getid(),
                    'quantity'                  => $value->getQuantity(),
                    'iduf'                      => $value->getState()->getId(),
                    'state_name'                => $value->getState()->getStateName(),
                );
            }
        }
        return $dados;
    }
}