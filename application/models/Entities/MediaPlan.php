<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaPlan
 */
class MediaPlan
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateInsertion;

    /**
     * @var \DateTime
     */
    private $dateInsertionEnd;

    /**
     * @var integer
     */
    private $totalInsertion;

    /**
     * @var \Entities\Recall
     */
    private $recall;
    
    /**
     * @var \Entities\Source
     */
    private $source;

    /**
     * @var decimal
     */
    private $mediaCost;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateInsertion
     *
     * @param \DateTime $dateInsertion
     * @return MediaPlan
     */
    public function setDateInsertion($dateInsertion)
    {
        $this->dateInsertion = new \DateTime($dateInsertion);
    
        return $this;
    }

    /**
     * Get dateInsertion
     *
     * @return \DateTime 
     */
    public function getDateInsertion()
    {
        return $this->dateInsertion;
    }

    /**
     * Set dateInsertionEnd
     *
     * @param \DateTime $dateInsertionEnd
     * @return MediaPlan
     */
    public function setDateInsertionEnd($dateInsertionEnd)
    {
        $this->dateInsertionEnd = new \DateTime($dateInsertionEnd);
    
        return $this;
    }

    /**
     * Get dateInsertionEnd
     *
     * @return \DateTime
     */
    public function getDateInsertionEnd()
    {
        return $this->dateInsertionEnd;
    }

    /**
     * Set totalInsertion
     *
     * @param integer $totalInsertion
     * @return MediaPlan
     */
    public function setTotalInsertion($totalInsertion)
    {
        $this->totalInsertion = $totalInsertion;
    
        return $this;
    }

    /**
     * Get totalInsertion
     *
     * @return integer 
     */
    public function getTotalInsertion()
    {
        return $this->totalInsertion;
    }

    /**
     * Set recall
     *
     * @param \Entities\Recall $recall
     * @return MediaPlan
     */
    public function setRecall(\Entities\Recall $recall = null)
    {
        $this->recall = $recall;
    
        return $this;
    }

    /**
     * Get recall
     *
     * @return \Entities\Recall 
     */
    public function getRecall()
    {
        return $this->recall;
    }
    
     /**
     * Set source
     *
     * @param \Entities\Source $source
     * @return MediaPlan
     */
    public function setSource(\Entities\Source $source = null)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return \Entities\Source
     */
    public function getSource()
    {
        return $this->source;
    }
    
    /**
     * Set mediaCost
     *
     * @param decimal $mediaCost
     * @return MediaPlan
     */
    public function setMediaCost($mediaCost)
    {
        $this->mediaCost = $mediaCost;
    
        return $this;
    }

    /**
     * Get mediaCost
     *
     * @return decimal
     */
    public function getMediaCost()
    {
        return $this->mediaCost;
    }
    
    /*
     * retorna um array com todas as midias por recall
     */
    function getArrayMedia($media) {
        
        $dados = array();
        
        if ($media) {
            foreach($media as $key => $value) {
                $dados[$key]['id_media_plan']       = $value->getId();
                $dados[$key]['b_source']            = $value->getSource()->getId();
                $dados[$key]['source']              = ($value->getSource()->getSource());
                $dados[$key]['media']               = ($value->getSource()->getMedia()->getMedia());
                $dados[$key]['origin']              = ($value->getSource()->getMedia()->getOrigin()->getOrigin());
                $dados[$key]['date_insertion']      = (array) $value->getDateInsertion();
                $dados[$key]['date_insertion_end']  = (array) $value->getDateInsertionEnd();
                $dados[$key]['total_insertion']     = $value->getTotalInsertion();
                $dados[$key]['media_cost']          = number_format($value->getMediaCost(), 2, ',', '.');
                $dados[$key]['media_cost_sum']      = $value->getMediaCost();
            }   
        }
        return $dados;
    }
}