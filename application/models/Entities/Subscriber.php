<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subscriber
 */
class Subscriber
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;
    
    /**
     * @var tinyint
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Subscriber
     */
    public function setName($name)
    {
        $this->name = utf8_decode($name);
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return utf8_encode($this->name);
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Subscriber
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    public function getArraySubscriber(Array $branch = array()) {
        $dados = array();
        if ($branch) {
            foreach ($branch as $key => $value) {
                $dados[$key] = array(
                    'branch_id' => $value->getid(),
                    'branch'    => utf8_encode($value->getBranch()),
                    'status'    => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
}