<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupPartner
 */
class GroupPartner
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\Group
     */
    private $group;

    /**
     * @var \Entities\Partner
     */
    private $partner;
    
    /**
     * @var \DateTime
     */
    private $dateInsert;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group
     *
     * @param \Entities\Group $group
     * @return GroupPartner
     */
    public function setGroup(\Entities\Group $group = null)
    {
        $this->group = $group;
    
        return $this;
    }

    /**
     * Get group
     *
     * @return \Entities\Group 
     */
    public function getGroup()
    {
        return $this->group;
    }
    
    /**
     * Set partner
     *
     * @param \Entities\Partner $partner
     * @return GroupPartner
     */
    public function setPartner(\Entities\Partner $partner = null)
    {
        $this->partner = $partner;
    
        return $this;
    }

    /**
     * Get partner
     *
     * @return \Entities\Partner 
     */
    public function getPartner()
    {
        return $this->partner;
    }
    
    /**
     * Set dateInsert
     *
     * @param \DateTime $dateInsert
     * @return GroupPartner
     */
    public function setDateInsert($dateInsert)
    {
        $this->dateInsert = $dateInsert;
    
        return $this;
    }

    /**
     * Get dateInsert
     *
     * @return \DateTime 
     */
    public function getDateInsert()
    {
        return $this->dateInsert;
    }
    
    public function getArrayAllGroupPartner($arrGroupPartner) {
        $dados = array();
        if ($arrGroupPartner) {
            foreach ($arrGroupPartner as $key => $value) {
                $dados[$key] = array(
                    'partner_id'        => $value->getPartner()->getId(),
                    'partner'           => $value->getPartner()->getPartner(),
                    'email'             => $value->getPartner()->getEmail(),
                    'group_id'          => $value->getGroup()->getId(),
                    'name'              => utf8_encode($value->getGroup()->getName()),
                    'status'            => $value->getPartner()->getStatus(),
                    'dateInsert'        => $value->getDateInsert(),
                );
            }
        }
        return $dados;
    }
}