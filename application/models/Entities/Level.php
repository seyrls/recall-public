<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Level
 */
class Level
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $level;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Level
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }
    
    /*
     * return array select com os perfis
     */
    public function getComboLevel ($level){
        $dados = array(
            '' => 'Selecione'
        );
        if ($level) {
            foreach ($level as $value) {
                $dados[$value->getLevel()] = $value->getName();
            }
        }
        return $dados;
    }
}