<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Manufacturer
 */
class Manufacturer
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $corporateName;

    /**
     * @var string
     */
    private $tradeName;
    
    /**
     * @var tinyint
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set corporateName
     *
     * @param string $corporateName
     * @return Manufacturer
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = utf8_decode($corporateName);
    
        return $this;
    }

    /**
     * Get corporateName
     *
     * @return string 
     */
    public function getCorporateName()
    {
        return utf8_encode($this->corporateName);
    }

    /**
     * Set tradeName
     *
     * @param string $tradeName
     * @return Manufacturer
     */
    public function setTradeName($tradeName)
    {
        $this->tradeName = utf8_decode($tradeName);
    
        return $this;
    }

    /**
     * Get tradeName
     *
     * @return string 
     */
    public function getTradeName()
    {
        return utf8_encode($this->tradeName);
    }
    
    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    public  function getArrayManufacturer($arrManufacturer){
        $dados = array();
        if ($arrManufacturer) {
            foreach ($arrManufacturer as $value){
                $dados = array(
                    'manufacturer_id'   => $value->getId(),
                    'corporate_name'    => ($value->getCorporateName()),
                    'trade_name'        => ($value->getTradeName()),
                    'status'            => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
    
    public  function getArrayAllManufacturer($arrManufacturer){
        $dados = array();
        if ($arrManufacturer) {
            foreach ($arrManufacturer as $key => $value){
                $dados[$key] = array(
                    'manufacturer_id'   => $value->getId(),
                    'corporate_name'    => ($value->getCorporateName()),
                    'trade_name'        => ($value->getTradeName()),
                    'status'            => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
    
    public function getComboManufacturer($arrManufacturer) {
        $dados = array(
            '' => 'Selecione'
        );
        if ($arrManufacturer) {
            foreach ($arrManufacturer as $value) {
                $dados[$value->getId()] = ($value->getCorporateName());
            }
        }
        return $dados;
    }
}