<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Product
 */
class Product
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $product;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $yearManufacture;

    /**
     * @var integer
     */
    private $quantityAffected;

    /**
     * @ORM\OneToMany(targetEntity="\Entities\Image", mappedBy="product")
     */
    private $image;
    
    /**
     * @ORM\OneToMany(targetEntity="\Entities\Batch", mappedBy="product")
     */
    private $batch;
    
    /**
     * @ORM\OneToMany(targetEntity="\Entities\Affected", mappedBy="product")
     */
    private $affected;

    /**
     * @var \Entities\TypeProduct
     */
    private $type_product;

    /**
     * @var \Entities\Manufacturer
     */
    private $manufacturer;

    /**
     * @var \Entities\Recall
     */
    private $recall;
    
    /**
     * @var \Entities\Country
     */
    private $country;
    
    /**
     * @var \Entities\Country
     */
    private $countryExport;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->image    = new ArrayCollection();
        $this->batch    = new ArrayCollection();
        $this->affected = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param string $product
     * @return Product
     */
    public function setProduct($product)
    {
        $this->product = utf8_decode($product);
    
        return $this;
    }

    /**
     * Get product
     *
     * @return string 
     */
    public function getProduct()
    {
        return utf8_encode($this->product);
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Product
     */
    public function setModel($model)
    {
        $this->model = utf8_decode($model);
    
        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return utf8_encode($this->model);
    }

    /**
     * Set yearManufacture
     *
     * @param string $yearManufacture
     * @return Product
     */
    public function setYearManufacture($yearManufacture)
    {
        $this->yearManufacture = $yearManufacture;
    
        return $this;
    }

    /**
     * Get yearManufacture
     *
     * @return string 
     */
    public function getYearManufacture()
    {
        return $this->yearManufacture;
    }

    /**
     * Set quantityAffected
     *
     * @param integer $quantityAffected
     * @return Product
     */
    public function setQuantityAffected($quantityAffected)
    {
        $this->quantityAffected = $quantityAffected;
    
        return $this;
    }

    /**
     * Get quantityAffected
     *
     * @return integer 
     */
    public function getQuantityAffected()
    {
        return $this->quantityAffected;
    }

    /**
     * Add image
     *
     * @param \Entities\Image $image
     * @return Product
     */
    public function addImage(\Entities\Image $image)
    {
        $this->image[] = $image;
    
        return $this;
    }

    /**
     * Remove image
     *
     * @param \Entities\Image $image
     */
    public function removeImage(\Entities\Image $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImage()
    {
        return $this->image;
    }
    /**
     * Add batch
     *
     * @param \Entities\Batch $batch
     * @return Product
     */
    public function addBatch(\Entities\Batch $batch)
    {
        $this->batch[] = $batch;
    
        return $this;
    }

    /**
     * Remove batch
     *
     * @param \Entities\Batch $batch
     */
    public function removeBatch(\Entities\Batch $batch)
    {
        $this->batch->removeElement($batch);
    }

    /**
     * Get batch
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBatch()
    {
        return $this->batch;
    }
    
    /**
     * Add affected
     *
     * @param \Entities\Affected $affected
     * @return Product
     */
    public function addAffected(\Entities\Affected $affected)
    {
        $this->affected[] = $affected;
    
        return $this;
    }

    /**
     * Remove affected
     *
     * @param \Entities\Affected $affected
     */
    public function removeAffected(\Entities\Affected $affected)
    {
        $this->affected->removeElement($affected);
    }

    /**
     * Get affected
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAffected()
    {
        return $this->affected;
    }

    /**
     * Set type_product
     *
     * @param \Entities\TypeProduct $typeProduct
     * @return Product
     */
    public function setTypeProduct(\Entities\TypeProduct $typeProduct = null)
    {
        $this->type_product = $typeProduct;
    
        return $this;
    }

    /**
     * Get type_product
     *
     * @return \Entities\TypeProduct 
     */
    public function getTypeProduct()
    {
        return $this->type_product;
    }

    /**
     * Set manufacturer
     *
     * @param \Entities\Manufacturer $manufacturer
     * @return Product
     */
    public function setManufacturer(\Entities\Manufacturer $manufacturer = null)
    {
        $this->manufacturer = $manufacturer;
    
        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return \Entities\Manufacturer 
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set recall
     *
     * @param \Entities\Recall $recall
     * @return Product
     */
    public function setRecall(\Entities\Recall $recall = null)
    {
        $this->recall = $recall;
    
        return $this;
    }

    /**
     * Get recall
     *
     * @return \Entities\Recall 
     */
    public function getRecall()
    {
        return $this->recall;
    }
    
    
    
    


    /**
     * Set country
     *
     * @param \Entities\Country $country
     * @return Product
     */
    public function setCountry(\Entities\Country $country = null)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return \Entities\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Set countryExport
     *
     * @param \Entities\Country $countryExport
     * @return Product
     */
    public function setCountryExport(\Entities\Country $countryExport = null)
    {
        $this->countryExport = $countryExport;
    
        return $this;
    }

    /**
     * Get countryExport
     *
     * @return \Entities\Country 
     */
    public function getCountryExport()
    {
        return $this->countryExport;
    }
    
    
    public function __toString()
    {
        return $this->product;
    }
    
    
    /*
     * Metodo para retornar um array com todos os produtos de uma recall
     * return Array
     */    
    function getArrayAllProduct($arrProduct) {
        
        if ($arrProduct) {
            foreach ($arrProduct as $key => $value) {
                $dados[$key] = array(
                    'product_id'        => $value->getId(),
                    'product'           => $value->getProduct(),
                    'model'             => $value->getModel(),
                    'year_manufacture'  => $value->getYearManufacture(),
                    'quantity_affected' => $value->getQuantityAffected(),
                    'type_product'      => ($value->getTypeProduct()->getTypeProduct()),
                    'branch'            => ($value->getTypeProduct()->getBranch()->getBranch()),
                    'corporate_name'    => $value->getManufacturer()->getCorporateName(),
                    'manufacturer_id'   => $value->getManufacturer()->getId(),
                    'country'           => ($value->getCountry()->getCountry()),
                    'country_export'    => ($value->getCountryExport()),
                );
                
                $image = $value->getImage();
                $arrImg = array();
                if ($image) {
                    foreach($image as $ki => $vi) {
                        $arrImg[$ki] = $vi->getPath();
                    }
                }
                $dados[$key]['image'] = $arrImg;
                
                $batch = $value->getBatch();
                $arrBatch = array();
                if ($batch) {
                    foreach($batch as $kb => $vb) {
                        $arrBatch[$key][] = array(
                            'batch_id'                  => $vb->getid(),
                            'initial_batch'             => $vb->getInitialBatch(),
                            'final_batch'               => $vb->getFinalBatch(),
                            'manufacturer_initial_date' => ($vb->getManufactureInitialDate()) ? 
                                                           (array)$vb->getManufactureInitialDate() : NULL,
                            'manufacturer_final_date'   => ($vb->getManufactureFinalDate()) ? 
                                                           (array)$vb->getManufactureFinalDate() : null,
                            'expiry_date'               => ($vb->getExpiryDate()) ? 
                                                           (array)$vb->getExpiryDate() : null,
                        );
                    }
                }
                $dados[$key]['batch'] = $arrBatch;
                
                $affected = $value->getAffected();
                $arrAffected = array();
                if ($affected) {
                    foreach($affected as $ka => $va) {
                        $arrAffected[$key][] = array(
                            'affected_id'   => $va->getid(),
                            'quantity'      => $va->getQuantity(),
                            'state_name'    => ($va->getState()->getStateName()),
                        );
                    }
                }
                $dados[$key]['affected'] = $arrAffected;
            }
        }
        return $dados;
    }
    
    /*
     * return array select com as campanhas de Recall
     */
    public function getComboProduct($product){
        $dados = array(
            '' => 'Selecione'
        );
        if ($product) {
            foreach ($product as $value) {
                $dados[$value->getId()] = $value->getProduct();
            }
        }
        return $dados;
    }
    
    public function getBranchTypeProduct($product) {
        $dados = array();
        if ($product) {
            foreach ($product as $key => $value) {
                $dados['branch_id'][$key] = $value->getTypeProduct()->getBranch()->getId();
                $dados['type_product_id'][$key] = $value->getTypeProduct()->getId();
            }
        }
        return $dados;
    }
}