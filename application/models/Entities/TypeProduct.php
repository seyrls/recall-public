<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeProduct
 */
class TypeProduct
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $typeProduct;

    /**
     * @var \Entities\Branch
     */
    private $branch;

    /**
     * @var tinyint
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeProduct
     *
     * @param string $typeProduct
     * @return TypeProduct
     */
    public function setTypeProduct($typeProduct)
    {
        $this->typeProduct = utf8_decode($typeProduct);
    
        return $this;
    }

    /**
     * Get typeProduct
     *
     * @return string 
     */
    public function getTypeProduct()
    {
        return utf8_encode($this->typeProduct);
    }

    /**
     * Set branch
     *
     * @param \Entities\Branch $branch
     * @return TypeProduct
     */
    public function setBranch(\Entities\Branch $branch = null)
    {
        $this->branch = $branch;
    
        return $this;
    }

    /**
     * Get branch
     *
     * @return \Entities\Branch 
     */
    public function getBranch()
    {
        return $this->branch;
    }
    
    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    public function getComboTypeProduct($arrTypeProduct, $defult = array('' => 'Selecione')){
        $dados = $defult;
        if ($arrTypeProduct) {
            foreach ($arrTypeProduct as $value) {
                $dados[$value->getId()] = ($value->getTypeProduct());
            }
        }
        return $dados;
    }
    
    public  function getArrayTypeProduct($arrTypeProduct){
        $dados = array();
        if ($arrTypeProduct) {
            foreach ($arrTypeProduct as $value){
                $dados = array(
                    'type_product_id'   => $value->getId(),
                    'type_product'      => ($value->getTypeProduct()),
                    'branch_id'         => $value->getBranch()->getId(),
                    'branch'            => ($value->getBranch()->getBranch()),
                    'status'            => $value->getStatus()
                );
            }
        }
        return $dados;
    }
    
    public  function getArrayAllTypeProduct($arrTypeProduct){
        $dados = array();
        if ($arrTypeProduct) {
            foreach ($arrTypeProduct as $key => $value){
                $dados[$key] = array(
                    'type_product_id'   => $value->getId(),
                    'type_product'      => ($value->getTypeProduct()),
                    'branch_id'         => $value->getBranch()->getId(),
                    'branch'            => ($value->getBranch()->getBranch()),
                    'status'            => $value->getStatus()
                );
            }
        }
        return $dados;
    }
}