<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partner
 */
class Partner
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $partner;

    /**
     * @var string
     */
    private $email;
    
    /**
     * @var tinyint
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set partner
     *
     * @param string $partner
     * @return Partner
     */
    public function setPartner($partner)
    {
        $this->partner = utf8_decode($partner);
    
        return $this;
    }

    /**
     * Get partner
     *
     * @return string 
     */
    public function getPartner()
    {
        return utf8_encode($this->partner);
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Partner
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    public function getArrayPartner(Array $partner = array()) {
        $dados = array();
        if ($partner) {
            foreach ($partner as $value) {
                $dados = array(
                    'partner_id' => $value->getid(),
                    'partner'    => ($value->getPartner()),
                    'email'      => $value->getEmail(),
                    'status'     => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
}