<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Link
 */
class Link
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var \Entities\Recall
     */
    private $recall;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Link
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
  
    /**
     * Set recall
     *
     * @param \Entities\Recall $recall
     * @return Link
     */
    public function setRecall(\Entities\Recall $recall = null)
    {
        $this->recall = $recall;
    
        return $this;
    }

    /**
     * Get recall
     *
     * @return \Entities\Recall 
     */
    public function getRecall()
    {
        return $this->recall;
    }
}