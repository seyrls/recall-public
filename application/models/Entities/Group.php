<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Group
 */
class Group
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var tinyint
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = utf8_decode($name);
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return utf8_encode($this->name);
    }

    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    public function getArrayGroup(Array $group = array()) {
        $dados = array();
        if ($group) {
            foreach ($group as $value) {
                $dados = array(
                    'group_id' => $value->getid(),
                    'name'     => ($value->getName()),
                    'status'   => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
    public function getArrayAllGroup(Array $group = array()) {
        $dados = array();
        if ($group) {
            foreach ($group as $key => $value) {
                $dados[$key] = array(
                    'group_id' => $value->getid(),
                    'name'     => ($value->getName()),
                    'status'   => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
    
    /*
     * Retorna um array com todos os grupos
     */
    public function getComboGroup($group) {
        $dados = array(
            '' => 'Selecione'
        );
        if ($group) {
            foreach ($group as $value) {
                $dados[$value->getId()] = ($value->getName());
            }
        }
        return $dados;
    }
}