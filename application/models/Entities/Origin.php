<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Origin
 */
class Origin
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $origin;

    /**
     * @var tinyint
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set origin
     *
     * @param string $origin
     * @return Origin
     */
    public function setOrigin($origin)
    {
        $this->origin = utf8_decode($origin);
    
        return $this;
    }

    /**
     * Get origin
     *
     * @return string 
     */
    public function getOrigin()
    {
        return utf8_encode($this->origin);
    }
    
    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    public function getComboOrigin($origin) {
        $dados = array(
            '' => 'Selecione'
        );
        if ($origin) {
            foreach ($origin as $value) {
                $dados[$value->getId()] = ($value->getOrigin());
            }
        }
        return $dados;
    }
    
    /*
     * Metodo para retornar todos as origens
     * return Array
     */
    public function getArrayAllOrigin(Array $origin = array()) {
        $dados = array();
        if ($origin) {
            foreach ($origin as $key => $value) {
                $dados[$key] = array(
                    'origin_id' => $value->getid(),
                    'origin'    => ($value->getOrigin()),
                    'status'    => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
    
    public function getArrayOrigin(Array $origin = array()) {
        $dados = array();
        if ($origin) {
            foreach ($origin as $value) {
                $dados = array(
                    'origin_id' => $value->getid(),
                    'origin'    => ($value->getOrigin()),
                    'status'    => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
}