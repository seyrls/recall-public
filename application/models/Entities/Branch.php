<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
* Branch
* @OrderBy({"branch" = "ASC"})
* @ORM\Table(name="branches", options={"collate"="utf8_general_ci", "charset"="utf8", "engine"="InnoDB"})  
* @ORM\Entity
*/ 
class Branch
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $branch;
    
    /**
     * @var tinyint
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set branch
     *
     * @param string $branch
     * @return Branch
     */
    public function setBranch($branch)
    {
        $this->branch = utf8_decode($branch);
    
        return $this;
    }

    /**
     * Get branch
     *
     * @return string 
     */
    public function getBranch()
    {
        return utf8_encode($this->branch);
    }
    
    
    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /*
     * Retorna um array com todos os ramos
     */
    public function getComboBranch($branch, $defult = array('' => 'Selecione')){
        $dados = $defult;
        if ($branch) {
            foreach ($branch as $value) {
                $dados[$value->getId()] = $value->getBranch();
            }
        }
        return $dados;
    }
    
    /*
     * Metodo para retornar todos os lotes de um produto
     * return Array
     */
    public function getArrayAllBranch(Array $branch = array()) {
        $dados = array();
        if ($branch) {
            foreach ($branch as $key => $value) {
                $dados[$key] = array(
                    'branch_id' => $value->getid(),
                    'branch'    => $value->getBranch(),
                    'status'    => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
    
    public function getArrayBranch(Array $branch = array()) {
        $dados = array();
        if ($branch) {
            foreach ($branch as $value) {
                $dados = array(
                    'branch_id' => $value->getid(),
                    'branch'    => $value->getBranch(),
                    'status'    => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
}