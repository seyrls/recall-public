<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 */
class Country
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $country;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Country
     */
    public function setCountry($country)
    {
        $this->country = utf8_decode($country);
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return utf8_encode($this->country);
    }
    
    public function getComboCountry($arrCountry, $defult = array('' => 'Selecione')){
        $dados = $defult;
        if ($arrCountry) {
            foreach ($arrCountry as $value) {
                $dados[$value->getId()] = ($value->getCountry());
            }
        }
        return $dados;
    }
}
