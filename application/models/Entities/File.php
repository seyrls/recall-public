<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * File
 */
class File
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @var \Entities\Recall
     */
    private $recall;
    
    /**
     * @var integer
     */
    private $belongsTo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     * @return File
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    
        return $this;
    }

    /**
     * Get filePath
     *
     * @return string 
     */
    public function getFilePath()
    {
        return $this->filePath;
    }
    
    /**
     * Set belongsTo
     *
     * @param string $belongsTo
     * @return File
     */
    public function setBelongsTo($belongsTo)
    {
        $this->belongsTo = $belongsTo;
    
        return $this;
    }

    /**
     * Get belongsTo
     *
     * @return string 
     */
    public function getBelongsTo()
    {
        return $this->belongsTo;
    }

    /**
     * Set recall
     *
     * @param \Entities\Recall $recall
     * @return File
     */
    public function setRecall(\Entities\Recall $recall = null)
    {
        $this->recall = $recall;
    
        return $this;
    }

    /**
     * Get recall
     *
     * @return \Entities\Recall 
     */
    public function getRecall()
    {
        return $this->recall;
    }
}