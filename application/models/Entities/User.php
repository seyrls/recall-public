<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @var \Entities\Level
     */
    private $level;
    
    /**
     * @var int
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = utf8_decode($username);
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return utf8_encode($this->username);
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = MD5($password);
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set level
     *
     * @param \Entities\Level $level
     * @return User
     */
    public function setLevel(\Entities\Level $level = null)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return \Entities\Level 
     */
    public function getLevel()
    {
        return $this->level;
    }
    
    /**
     * Set status
     *
     * @param int $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    function getArrayAllUser($arrUser) {
        $dados = array();
        if ($arrUser) {
            foreach ($arrUser as $key => $value) {
                $dados[$key] = array(
                    'user_id' => $value->getId(),
                    'username' => $value->getUsername(),
                    'email' => $value->getEmail(),
                    'level' => $value->getLevel()->getLevel(),
                    'name_level' => $value->getLevel()->getName(),
                    'status'    => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
    
    function getArrayUser($arrUser) {
        $dados = array();
        if ($arrUser) {
            foreach ($arrUser as $value) {
                $dados = array(
                    'user_id'   => $value->getId(),
                    'username'  => $value->getUsername(),
                    'email'     => $value->getEmail(),
                    'level'     => $value->getLevel()->getLevel(),
                    'name_level' => $value->getLevel()->getName(),
                    'status'    => $value->getStatus(),
                );
            }
        }
        return $dados;
    }
}