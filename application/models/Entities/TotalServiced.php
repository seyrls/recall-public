<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * TotalServiced
 */
class TotalServiced
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $dateInsert;
    /**
     * @ORM\ManyToOne(targetEntity="\Entities\Product", inversedBy="totalServiced")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @var \Entities\State
     */
    private $state;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return TotalServiced
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set dateInsert
     *
     * @param \DateTime $dateInsert
     * @return TotalServiced
     */
    public function setDateInsert($dateInsert)
    {
        $this->dateInsert = $dateInsert;
    
        return $this;
    }

    /**
     * Get dateInsert
     *
     * @return \DateTime 
     */
    public function getDateInsert()
    {
        return $this->dateInsert->format('d-m-Y H:i:s');
    }

    /**
     * Set product
     *
     * @param \Entities\Product $product
     * @return TotalServiced
     */
    public function setProduct(\Entities\Product $product = null)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return \Entities\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set state
     *
     * @param \Entities\State $state
     * @return TotalServiced
     */
    public function setState(\Entities\State $state = null)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return \Entities\State 
     */
    public function getState()
    {
        return $this->state;
    }
    
    function getArrayTotalServiced($arrServiced) {
        $dados = array();
        if ($arrServiced) {
            foreach ($arrServiced as $key => $value) {
                $dados[$key] = array(
                    'total_serviced_id'         => $value->getId(),
                    'quantity'                  => (int)$value->getQuantity(),
                    'date_insert'               => (array) $value->getDateInsert(),
                    'product_id'                => $value->getProduct()->getId(),
                    'product'                   => $value->getProduct()->getProduct(),
                    'quantity_affected'         => $value->getProduct()->getQuantityAffected(),
                    'state_id'                  => $value->getState()->getId(),
                    'state'                     => $value->getState()->getState(),
                    'state_name'                => utf8_encode($value->getState()->getStateName()),
                    'recall_id'                 => $value->getProduct()->getRecall()->getId(),
                    'recall'                    => $value->getProduct()->getRecall()->getTitle(),
                );
            }
        }
        return $dados;
    }
}