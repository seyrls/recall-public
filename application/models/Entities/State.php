<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * State
 */
class State
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $stateName;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return State
     */
    public function setState($state)
    {
        $this->state = utf8_decode($state);
    
        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return utf8_encode($this->state);
    }

    /**
     * Set stateName
     *
     * @param string $stateName
     * @return State
     */
    public function setStateName($stateName)
    {
        $this->stateName = utf8_decode($stateName);
    
        return $this;
    }

    /**
     * Get stateName
     *
     * @return string 
     */
    public function getStateName()
    {
        return utf8_encode($this->stateName);
    }
    
    /*
     * retorna um array com todos os estados
     */
    public function getComboState($estados) {
        $dados = array(
                '' => 'Selecione'
        );
        if ($estados) {
            foreach ($estados as $value) {
                $dados[$value->getId()] = ($value->getStateName());
            }
        }
        return $dados;
    }
}