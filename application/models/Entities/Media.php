<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Media
 */
class Media
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $media;

    /**
     * @var \Entities\Origin
     */
    private $origin;

    /**
     * @var tinyint
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set media
     *
     * @param string $media
     * @return Media
     */
    public function setMedia($media)
    {
        $this->media = utf8_decode($media);
    
        return $this;
    }

    /**
     * Get media
     *
     * @return string 
     */
    public function getMedia()
    {
        return utf8_encode($this->media);
    }

    /**
     * Set origin
     *
     * @param \Entities\Origin $origin
     * @return Media
     */
    public function setOrigin(\Entities\Origin $origin = null)
    {
        $this->origin = $origin;
    
        return $this;
    }

    /**
     * Get origin
     *
     * @return \Entities\Origin 
     */
    public function getOrigin()
    {
        return $this->origin;
    }
    
    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /*
     * Metodo para retornar todos as midias
     * return Array
     */
    public function getArrayMedia(Array $media = array()) {
        $dados = array();
        if ($media) {
            foreach ($media as $value) {
                $dados = array(
                    'media_id'  => $value->getid(),
                    'media'     => ($value->getMedia()),
                    'status'    => $value->getStatus(),
                    'origin_id' => $value->getOrigin()->getId(),
                    'origin'    => $value->getOrigin()->getOrigin(),
                );
            }
        }
        return $dados;
    }
    
    /*
     * Metodo para retornar todos as midias
     * return Array
     */
    public function getArrayAllMedia(Array $media = array()) {
        $dados = array();
        if ($media) {
            foreach ($media as $key => $value) {
                $dados[$key] = array(
                    'media_id'  => $value->getid(),
                    'media'     => ($value->getMedia()),
                    'status'    => $value->getStatus(),
                    'origin_id' => $value->getOrigin()->getId(),
                    'origin'    => $value->getOrigin()->getOrigin(),
                );
            }
        }
        return $dados;
    }
    
    public function getComboMedia($media) {
        $dados = array(
            '' => 'Selecione'
        );
        if ($media) {
            foreach ($media as $value) {
                $dados[$value->getId()] = ($value->getMedia());
            }
        }
        return $dados;
    }
}