<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 */
class City
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $city;

    /**
     * @var \Entities\State
     */
    private $state;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return City
     */
    public function setCity($city)
    {
        $this->city = utf8_decode($city);
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return utf8_encode($this->city);
    }

    /**
     * Set state
     *
     * @param \Entities\State $state
     * @return City
     */
    public function setState(\Entities\State $state = null)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return \Entities\State 
     */
    public function getState()
    {
        return $this->state;
    }
    
    /*
     * retorna um array com todos as cidades
     */
    public function getComboCity($cidades) {
        
        $dados = array(
                '' => 'Selecione'
        );
        if ($cidades) {
            foreach ($cidades as $value) {
                $dados[$value->getId()] = $value->getCity();
            }
        }
        return $dados;
    }
}