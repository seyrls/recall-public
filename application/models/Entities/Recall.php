<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Recall
 */
class Recall
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $protocol;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $correctiveMeasure;

    /**
     * @var string
     */
    private $customerService;

    /**
     * @var string
     */
    private $site;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $addressService;

    /**
     * @var string
     */
    private $phoneService;

    /**
     * @var string
     */
    private $noticeService;

    /**
     * @var integer
     */
    private $reportAccident;

    /**
     * @var \DateTime
     */
    private $dateInsert;

    /**
     * @var integer
     */
    private $statusCampaign;

    /**
     * @var \DateTime
     */
    private $approvalDate;

    /**
     * @var string
     */
    private $faultyPart;

    /**
     * @var \DateTime
     */
    private $dateFaulty;

    /**
     * @var string
     */
    private $faultyDescribe;

    /**
     * @var string
     */
    private $faulty;

    /**
     * @var string
     */
    private $riskFaulty;

    /**
     * @var string
     */
    private $noticeRisk;

    /**
     * @var \Entities\Supplier
     */
    private $supplier;
    
    /**
     * @var integer
     */
    private $timeLine;
    
    /**
     * @var string
     */
    private $reasonDisapprove;
    
    /**
     * @var string
     */
    private $reasonReservation;
    
    /**
     * @var string
     */
    private $reasonAccident;

    /**
     * @var \Entities\TypeRisk
     */
    private $typeRisk;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Recall
     */
    public function setTitle($title)
    {
        $this->title = utf8_decode($title);
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return utf8_encode($this->title);
    }

    /**
     * Set protocol
     *
     * @param string $protocol
     * @return Recall
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    
        return $this;
    }

    /**
     * Get protocol
     *
     * @return string 
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Recall
     */
    public function setStartDate($startDate)
    {
        $this->startDate = ($startDate);
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Recall
     */
    public function setEndDate($endDate)
    {
        $this->endDate = ($endDate);
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Recall
     */
    public function setDescription($description)
    {
        $this->description = utf8_decode($description);
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return utf8_encode($this->description);
    }

    /**
     * Set correctiveMeasure
     *
     * @param string $correctiveMeasure
     * @return Recall
     */
    public function setCorrectiveMeasure($correctiveMeasure)
    {
        $this->correctiveMeasure = utf8_decode($correctiveMeasure);
    
        return $this;
    }

    /**
     * Get correctiveMeasure
     *
     * @return string 
     */
    public function getCorrectiveMeasure()
    {
        return utf8_encode($this->correctiveMeasure);
    }

    /**
     * Set customerService
     *
     * @param string $customerService
     * @return Recall
     */
    public function setCustomerService($customerService)
    {
        $this->customerService = utf8_decode($customerService);
    
        return $this;
    }

    /**
     * Get customerService
     *
     * @return string 
     */
    public function getCustomerService()
    {
        return utf8_encode($this->customerService);
    }

    /**
     * Set site
     *
     * @param string $site
     * @return Recall
     */
    public function setSite($site)
    {
        $this->site = utf8_decode($site);
    
        return $this;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return utf8_encode($this->site);
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Recall
     */
    public function setEmail($email)
    {
        $this->email = utf8_decode($email);
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return utf8_encode($this->email);
    }

    /**
     * Set addressService
     *
     * @param string $addressService
     * @return Recall
     */
    public function setAddressService($addressService)
    {
        $this->addressService = utf8_decode($addressService);
    
        return $this;
    }

    /**
     * Get addressService
     *
     * @return string 
     */
    public function getAddressService()
    {
        return utf8_encode($this->addressService);
    }

    /**
     * Set phoneService
     *
     * @param string $phoneService
     * @return Recall
     */
    public function setPhoneService($phoneService)
    {
        $this->phoneService = $phoneService;
    
        return $this;
    }

    /**
     * Get phoneService
     *
     * @return string 
     */
    public function getPhoneService()
    {
        return $this->phoneService;
    }

    /**
     * Set noticeService
     *
     * @param string $noticeService
     * @return Recall
     */
    public function setNoticeService($noticeService)
    {
        $this->noticeService = utf8_decode($noticeService);
    
        return $this;
    }

    /**
     * Get noticeService
     *
     * @return string 
     */
    public function getNoticeService()
    {
        return utf8_encode($this->noticeService);
    }

    /**
     * Set reportAccident
     *
     * @param integer $reportAccident
     * @return Recall
     */
    public function setReportAccident($reportAccident)
    {
        $this->reportAccident = $reportAccident;
    
        return $this;
    }

    /**
     * Get reportAccident
     *
     * @return integer 
     */
    public function getReportAccident()
    {
        return $this->reportAccident;
    }

    /**
     * Set dateInsert
     *
     * @param \DateTime $dateInsert
     * @return Recall
     */
    public function setDateInsert($dateInsert)
    {
        $this->dateInsert = $dateInsert;
    
        return $this;
    }

    /**
     * Get dateInsert
     *
     * @return \DateTime 
     */
    public function getDateInsert()
    {
        return $this->dateInsert;
    }

    /**
     * Set statusCampaign
     *
     * @param integer $statusCampaign
     * @return Recall
     */
    public function setStatusCampaign($statusCampaign)
    {
        $this->statusCampaign = $statusCampaign;
    
        return $this;
    }

    /**
     * Get statusCampaign
     *
     * @return integer 
     */
    public function getStatusCampaign()
    {
        return $this->statusCampaign;
    }

    /**
     * Set approvalDate
     *
     * @param \DateTime $approvalDate
     * @return Recall
     */
    public function setApprovalDate($approvalDate)
    {
        $this->approvalDate = $approvalDate;
    
        return $this;
    }

    /**
     * Get approvalDate
     *
     * @return \DateTime 
     */
    public function getApprovalDate()
    {
        return $this->approvalDate;
    }

    /**
     * Set faultyPart
     *
     * @param string $faultyPart
     * @return Recall
     */
    public function setFaultyPart($faultyPart)
    {
        $this->faultyPart = utf8_decode($faultyPart);
    
        return $this;
    }

    /**
     * Get faultyPart
     *
     * @return string 
     */
    public function getFaultyPart()
    {
        return utf8_encode($this->faultyPart);
    }

    /**
     * Set dateFaulty
     *
     * @param \DateTime $dateFaulty
     * @return Recall
     */
    public function setDateFaulty($dateFaulty)
    {
        $this->dateFaulty = new \DateTime ($dateFaulty);
    
        return $this;
    }

    /**
     * Get dateFaulty
     *
     * @return \DateTime 
     */
    public function getDateFaulty()
    {
        return $this->dateFaulty;
    }

    /**
     * Set faultyDescribe
     *
     * @param string $faultyDescribe
     * @return Recall
     */
    public function setFaultyDescribe($faultyDescribe)
    {
        $this->faultyDescribe = utf8_decode($faultyDescribe);
    
        return $this;
    }

    /**
     * Get faultyDescribe
     *
     * @return string 
     */
    public function getFaultyDescribe()
    {
        return utf8_encode($this->faultyDescribe);
    }

    /**
     * Set faulty
     *
     * @param string $faulty
     * @return Recall
     */
    public function setFaulty($faulty)
    {
        $this->faulty = utf8_decode($faulty);
    
        return $this;
    }

    /**
     * Get faulty
     *
     * @return string 
     */
    public function getFaulty()
    {
        return utf8_encode($this->faulty);
    }

    /**
     * Set riskFaulty
     *
     * @param string $riskFaulty
     * @return Recall
     */
    public function setRiskFaulty($riskFaulty)
    {
        $this->riskFaulty = utf8_decode($riskFaulty);
    
        return $this;
    }

    /**
     * Get riskFaulty
     *
     * @return string 
     */
    public function getRiskFaulty()
    {
        return utf8_encode($this->riskFaulty);
    }

    /**
     * Set noticeRisk
     *
     * @param string $noticeRisk
     * @return Recall
     */
    public function setNoticeRisk($noticeRisk)
    {
        $this->noticeRisk = utf8_decode($noticeRisk);
    
        return $this;
    }

    /**
     * Get noticeRisk
     *
     * @return string 
     */
    public function getNoticeRisk()
    {
        return utf8_encode($this->noticeRisk);
    }

    /**
     * Set supplier
     *
     * @param \Entities\Supplier $supplier
     * @return Recall
     */
    public function setSupplier(\Entities\Supplier $supplier = null)
    {
        $this->supplier = $supplier;
    
        return $this;
    }

    /**
     * Get supplier
     *
     * @return \Entities\Supplier 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }
    
    
    /**
     * Set timeLine
     *
     * @param integer $timeLine
     * @return Recall
     */
    public function setTimeLine($timeLine)
    {
        $this->timeLine = $timeLine;
    
        return $this;
    }

    /**
     * Get timeLine
     *
     * @return integer 
     */
    public function getTimeLine()
    {
        return $this->timeLine;
    }
    
    /**
     * Set reasonDisapprove
     *
     * @param string $reasonDisapprove
     * @return Recall
     */
    public function setReasonDisapprove($reasonDisapprove)
    {
        $this->reasonDisapprove = utf8_decode($reasonDisapprove);
    
        return $this;
    }

    /**
     * Get reasonDisapprove
     *
     * @return string 
     */
    public function getReasonDisapprove()
    {
        return utf8_encode($this->reasonDisapprove);
    }
    
    /**
     * Set reasonReservation
     *
     * @param string $reasonReservation
     * @return Recall
     */
    public function setReasonReservation($reasonReservation)
    {
        $this->reasonReservation = utf8_decode($reasonReservation);
    
        return $this;
    }

    /**
     * Get reasonReservation
     *
     * @return string 
     */
    public function getReasonReservation()
    {
        return utf8_encode($this->reasonReservation);
    }
    
     /**
     * Set reasonAccident
     *
     * @param string $reasonAccident
     * @return Recall
     */
    public function setReasonAccident($reasonAccident)
    {
        $this->reasonAccident = utf8_decode($reasonAccident);
    
        return $this;
    }

    /**
     * Get reasonAccident
     *
     * @return string 
     */
    public function getReasonAccident()
    {
        return utf8_encode($this->reasonAccident);
    }
    
    /**
     * Set typeRisk
     *
     * @param \Entities\TypeRisk $typeRisk
     * @return Recall
     */
    public function setTypeRisk(\Entities\TypeRisk $typeRisk = null)
    {
        $this->typeRisk = $typeRisk;
    
        return $this;
    }

    /**
     * Get typeRisk
     *
     * @return \Entities\TypeRisk 
     */
    public function getTypeRisk()
    {
        return $this->typeRisk;
    }
    
    /*
     * Retorna todas as campanhas
     */
    public function getArrayAllCampaign($recall){
        $dados = array();
        if ($recall) {
            foreach ($recall as $key => $value){
                $dados[$key] = array(
                    'recall_id' => $value->getId(),
                    'title' => $value->getTitle(),
                    'protocol' => $value->getProtocol(),
                    'start_date' => (array) $value->getStartDate(),
                    'end_date' => (array) $value->getEndDate(),
                    'description' => $value->getDescription(),
                    'corrective_measure' => $value->getCorrectiveMeasure(),
                    'customer_service'  => $value->getCustomerService(),
                    'site'  => $value->getSite(),
                    'email'  => $value->getEmail(),
                    'address_service'  => $value->getAddressService(),
                    'phone_service'  => $value->getPhoneService(),
                    'notice_service'  => $value->getNoticeService(),
                    'report_accident'  => $value->getReportAccident(),
                    'date_insert'  => (array) $value->getDateInsert(),
                    'status_campaign'  => $value->getStatusCampaign(),
                    'approval_date'  => $value->getApprovalDate(),
                    'faulty_part'  => $value->getFaultyPart(),
                    'date_faulty'  => (array) $value->getDateFaulty(),
                    'faulty_describe'  => $value->getFaultyDescribe(),
                    'faulty'  => $value->getFaulty(),
                    'risk_faulty'  => $value->getRiskFaulty(),
                    'notice_risk'  => $value->getNoticeRisk(),
                    'supplier' => $value->getSupplier()->getTradeName(),
                );
            }
        }
        return $dados;
    }
    
    
    /*
     * Metodo para retornar os dados da campanha de um recall
     */
    public function getArrayCampaign($recall = array()){
        $dados = array();
        if ($recall) {
            $dados = array(
                'id'                    =>$recall->getId(),
                'title'                 => $recall->getTitle(),
                'protocol'              => $recall->getProtocol(),
                'start_date'            => (array) $recall->getStartDate(),
                'end_date'              => ($recall->getEndDate()) ? (array)$recall->getEndDate() : NULL,
                'description'           => $recall->getDescription(),
                'corrective_measure'    => $recall->getCorrectiveMeasure(),
                'customer_service'      => $recall->getCustomerService(),
                'report_accident'       => $recall->getReportAccident(),
                'supplier_id'           => $recall->getSupplier()->getId(),
                'status_campaign'       => $recall->getStatusCampaign(),
                'reason_disapprove'     => $recall->getReasonDisapprove(),
                'reason_reservation'    => $recall->getReasonReservation(),
                'reason_accident'       => $recall->getReasonAccident(),
            );
        }
        return $dados;
    }
    
    /*
     * Metodo para retornar um array com os dados das informacoes tecnicas de um recall
     */
    public function getArrayInfoRecall($recall) {
        
        $dados = array();
        
        if ($recall) {
            $dados = array(
                'faulty_part'       => $recall->getFaultyPart(),
                'date_faulty'       => (array) $recall->getDateFaulty(),
                'faulty_describe'   => $recall->getFaultyDescribe(),
                'faulty'            => $recall->getFaulty(),
                'risk_faulty'       => $recall->getRiskFaulty(),
                'notice_risk'       => $recall->getNoticeRisk(),
                'type_risk_id'      => ($recall->getTypeRisk()) ? $recall->getTypeRisk()->getId() : NULL,
                'type_risk'         => ($recall->getTypeRisk()) ? ($recall->getTypeRisk()->getTypeRisk()) : NULL
            );
        }
        return $dados;
    }
    
    /*
     * Metodo para retornar um array com os dados da localizacao de um recall
     */
    public function getArrayLocationRecall($recall) {
        
        $dados = array();

        if ($recall) {
            $dados = array(
                'customer_service'  => $recall->getCustomerService(),
                'address_service'   => $recall->getAddressService(),
                'site'              => $recall->getSite(),
                'email'             => $recall->getEmail(),
                'phone_service'     => $recall->getPhoneService(),
                'notice_service'    => $recall->getNoticeService(),
            );
        }
        return $dados;
    }
    
    /*
     * return array select com as campanhas de Recall
     */
    public function getComboRecall($recall){
        $dados = array(
            '' => 'Selecione'
        );
        if ($recall) {
            foreach ($recall as $value) {
                $dados[$value->getId()] = ($value->getTitle());
            }
        }
        return $dados;
    }
    
    /*
     * return array select com as campanhas de Recall
     */
    public function getComboStatusCampaign(){
        $dados = array(
            '' => 'Todos',
            STATUS_ID_EM_CADASTRAMENTO => STATUS_EM_CADASTRAMENTO,
            STATUS_ID_AGUARDANDO_PUBLICACAO => STATUS_AGUARDANDO_PUBLICACAO,
            STATUS_ID_PENDENCIA_CADASTRO => STATUS_PENDENCIA_CADASTRO,
            STATUS_ID_PUBLICADA_COM_RESSALVA => STATUS_PUBLICADO_COM_RESSALVA,
            STATUS_ID_PUBLICADA => STATUS_PUBLICADO,
            STATUS_ID_FINALIZADA => STATUS_FINALIZADA,
            STATUS_ID_ARQUIVADO => STATUS_ARQUIVADO,
        );
        return $dados;
    }
    
    /*
     * return array select com as campanhas de Recall Publicadas
     */
    public function getComboStatusCampaignPublish(){
        $dados = array(
            '' => 'Todos',
            STATUS_ID_PUBLICADA_COM_RESSALVA => STATUS_PUBLICADO_COM_RESSALVA,
            STATUS_ID_PUBLICADA => STATUS_PUBLICADO,
            STATUS_ID_FINALIZADA => STATUS_FINALIZADA,
        );
        return $dados;
    }
}