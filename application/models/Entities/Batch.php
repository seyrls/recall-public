<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Batch
 */
class Batch
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $initialBatch;

    /**
     * @var string
     */
    private $finalBatch;

    /**
     * @var \DateTime
     */
    private $manufactureInitialDate;

    /**
     * @var \DateTime
     */
    private $manufactureFinalDate;

    /**
     * @var \DateTime
     */
    private $expiryDate;

    /**
     * @ORM\ManyToOne(targetEntity="\Entities\Product", inversedBy="batch")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set initialBatch
     *
     * @param string $initialBatch
     * @return Batch
     */
    public function setInitialBatch($initialBatch)
    {
        $this->initialBatch = $initialBatch;
    
        return $this;
    }

    /**
     * Get initialBatch
     *
     * @return string 
     */
    public function getInitialBatch()
    {
        return $this->initialBatch;
    }

    /**
     * Set finalBatch
     *
     * @param string $finalBatch
     * @return Batch
     */
    public function setFinalBatch($finalBatch)
    {
        $this->finalBatch = $finalBatch;
    
        return $this;
    }

    /**
     * Get finalBatch
     *
     * @return string 
     */
    public function getFinalBatch()
    {
        return $this->finalBatch;
    }

    /**
     * Set manufactureInitialDate
     *
     * @param \DateTime $manufactureInitialDate
     * @return Batch
     */
    public function setManufactureInitialDate($manufactureInitialDate)
    {
        $this->manufactureInitialDate = new \DateTime($manufactureInitialDate);
    
        return $this;
    }

    /**
     * Get manufactureInitialDate
     *
     * @return \DateTime 
     */
    public function getManufactureInitialDate()
    {
        return $this->manufactureInitialDate;
    }

    /**
     * Set manufactureFinalDate
     *
     * @param \DateTime $manufactureFinalDate
     * @return Batch
     */
    public function setManufactureFinalDate($manufactureFinalDate)
    {
        $this->manufactureFinalDate = new \DateTime($manufactureFinalDate);
    
        return $this;
    }

    /**
     * Get manufactureFinalDate
     *
     * @return \DateTime 
     */
    public function getManufactureFinalDate()
    {
        return $this->manufactureFinalDate;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     * @return Batch
     */
    public function setExpiryDate($expiryDate)
    {
        if (is_null($expiryDate)){
            $this->expiryDate = NULL;
        }else{
           $this->expiryDate = new \DateTime($expiryDate);
        }
        
        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime 
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set product
     *
     * @param \Entities\Product $product
     * @return Batch
     */
    public function setProduct(\Entities\Product $product = null)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return \Entities\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    /*
     * Metodo para retornar todos os lotes de um produto
     * return Array
     */
    public function getArrayAllBatch(Array $batch = array()) {
        $dados = array();
        if ($batch) {
            foreach ($batch as $key => $value) {
                $dados[$key] = array(
                    'batch_id'                  => $value->getid(),
                    'initial_batch'             => $value->getInitialBatch(),
                    'final_batch'               => $value->getFinalBatch(),
                    'manufacturer_initial_date' => ($value->getManufactureInitialDate()) ? 
                                                   (array)$value->getManufactureInitialDate() : NULL,
                    'manufacturer_final_date'   => ($value->getManufactureFinalDate()) ? 
                                                   (array)$value->getManufactureFinalDate() : null,
                    'expiry_date'               => ($value->getExpiryDate()) ? 
                                                   (array)$value->getExpiryDate() : null,
                );
            }
        }
        return $dados;
    }
    public function __toString()
    {
        return $this->nitialBatch;
    }
}