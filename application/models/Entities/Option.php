<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Option
 */
class Option
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var tinyint
     */
    private $flag_all;

    /**
     * @var tinyint
     */
    private $status;
    
    /**
     * @var \Entities\Subscriber
     */
    private $subscriber;

    /**
     * @var \Entities\TypeProduct
     */
    private $type_product;
    
    /**
     * @var \Entities\Branch
     */
    private $branch;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set flag_all
     *
     * @param tinyint $flagAll
     * @return Option
     */
    public function setFlagAll($flagAll)
    {
        $this->flag_all = $flagAll;
    
        return $this;
    }

    /**
     * Get flag_all
     *
     * @return tinyint 
     */
    public function getFlagAll()
    {
        return $this->flag_all;
    }

    /**
     * Set subscriber
     *
     * @param \Entities\Subscriber $subscriber
     * @return Option
     */
    public function setSubscriber(\Entities\Subscriber $subscriber = null)
    {
        $this->subscriber = $subscriber;
    
        return $this;
    }

    /**
     * Get subscriber
     *
     * @return \Entities\Subscriber 
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }
    
    /**
     * Set type_product
     *
     * @param \Entities\TypeProduct $typeProduct
     * @return Option
     */
    public function setTypeProduct(\Entities\TypeProduct $typeProduct = null)
    {
        $this->type_product = $typeProduct;
    
        return $this;
    }

    /**
     * Get type_product
     *
     * @return \Entities\TypeProduct 
     */
    public function getTypeProduct()
    {
        return $this->type_product;
    }
    
    
    /**
     * Set branch
     *
     * @param \Entities\Branch $branch
     * @return Option
     */
    public function setBranch(\Entities\Branch $branch = null)
    {
        $this->branch = $branch;
    
        return $this;
    }

    /**
     * Get branch
     *
     * @return \Entities\Branch
     */
    public function getBranch()
    {
        return $this->branch;
    }
    
    /**
     * Set status
     *
     * @param tinyint $status
     * @return Status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return tinyint
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    function getArrayAllOptions($arrOptions){
        $dados = array();
        if ($arrOptions) {
            foreach ($arrOptions as $key => $value) {
                $dados[$key] = array(
                    'option_id'         => $value->getId(),
                    'flag_all'          => $value->getFlagAll(),
                    'status'            => $value->getStatus(),
                    'subscriber_id'     => $value->getSubscriber()->getId(),
                    'name'              => $value->getSubscriber()->getName(),
                    'email'             => $value->getSubscriber()->getEmail(),
                    'type_product_id'   => ($value->getTypeProduct()) ? $value->getTypeProduct()->getId() : NULL,
                    'type_product'      => ($value->getTypeProduct()) ? ($value->getTypeProduct()->getTypeProduct()) : NULL,
                    'branch_id'         => ($value->getBranch()) ? $value->getBranch()->getId() : NULL,
                    'branch'            => ($value->getBranch()) ? ($value->getBranch()->getBranch()) : NULL,
                );
            }
        }
        return $dados;
    }
    
    function getArrayOptions($arrOptions){
        $dados = array();
        if ($arrOptions) {
            foreach ($arrOptions as $value) {
                $dados = array(
                    'option_id'         => $value->getId(),
                    'flag_all'          => $value->getFlagAll(),
                    'status'            => $value->getStatus(),
                    'subscriber_id'     => $value->getSubscriber()->getId(),
                    'name'              => $value->getSubscriber()->getName(),
                    'email'             => $value->getSubscriber()->getEmail(),
                    'type_product_id'   => ($value->getTypeProduct()) ? $value->getTypeProduct()->getId() : NULL,
                    'type_product'      => ($value->getTypeProduct()) ? ($value->getTypeProduct()->getTypeProduct()) : NULL,
                    'branch_id'         => ($value->getBranch()) ? $value->getBranch()->getId() : NULL,
                    'branch'            => ($value->getBranch()) ? ($value->getBranch()->getBranch()) : NULL,
                );
            }
        }
        return $dados;
    }
    
}