<?php

/*
 * Classe para criacao de functions de consulta no banco de dados que
 * nao utiliza o Doctrine
 */
class Function_model extends CI_Model {
    
     function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    /*
     * Retorna a quantidade de solicitacoes de fornecedores
     */
    function getTotalSupplierRequest(){
        $dql = "
                SELECT  
                        u.username as username
                FROM 
                        Entities\Supplier s
                INNER JOIN
                        Entities\User u WITH u.id = s.user
                WHERE 
                        u.status in (".STATUS_SOLICITADO.")
                ";
        $sql = $this->doctrine->em->createQuery($dql);
        $total = count($sql->getResult());
        return $total;
    }
}